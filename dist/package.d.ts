declare const _exports: {
    "name": string;
    "version": string;
    "description": string;
    "main": string;
    "engines": {
        "node": string;
        "npm": string;
    };
    "scripts": {
        "start": string;
        "clean": string;
        "transpile": string;
        "build": string;
        "send:mail": string;
        "load:fake": string;
        "test": string;
        "seed": string;
        "migrate": string;
        "rollback": string;
        "make:seeder": string;
        "make:migration": string;
        "lint": string;
        "lint:fix": string;
        "prettify": string;
    };
    "lint-staged": {
        "*.{ts, js}": string[];
    };
    "private": boolean;
    "license": string;
    "keywords": string[];
    "dependencies": {
        "bcrypt": string;
        "body-parser": string;
        "bookshelf": string;
        "bookshelf-case-converter-plugin": string;
        "chalk": string;
        "cors": string;
        "dotenv": string;
        "express": string;
        "helmet": string;
        "http-status-codes": string;
        "joi": string;
        "jsonwebtoken": string;
        "knex": string;
        "knex-migrate": string;
        "lodash": string;
        "morgan": string;
        "mysql": string;
        "nodemailer": string;
        "nodemailer-markdown": string;
        "pg": string;
        "winston": string;
        "winston-daily-rotate-file": string;
    };
    "devDependencies": {
        "@babel/core": string;
        "@babel/preset-env": string;
        "@babel/preset-typescript": string;
        "@types/bcrypt": string;
        "@types/bookshelf": string;
        "@types/cors": string;
        "@types/dotenv": string;
        "@types/express": string;
        "@types/faker": string;
        "@types/helmet": string;
        "@types/jest": string;
        "@types/joi": string;
        "@types/jsonwebtoken": string;
        "@types/lodash": string;
        "@types/morgan": string;
        "@types/nodemailer": string;
        "@types/supertest": string;
        "@types/winston": string;
        "babel-jest": string;
        "backpack-core": string;
        "faker": string;
        "fork-ts-checker-webpack-plugin": string;
        "husky": string;
        "jest": string;
        "json-loader": string;
        "lint-staged": string;
        "npm-run-all": string;
        "prettier": string;
        "rimraf": string;
        "supertest": string;
        "ts-loader": string;
        "ts-node": string;
        "tslint": string;
        "tslint-config-prettier": string;
        "tslint-consistent-codestyle": string;
        "typescript": string;
    };
};
export = _exports;
