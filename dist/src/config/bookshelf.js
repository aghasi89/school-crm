"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("bookshelf"));
var knex_1 = tslib_1.__importDefault(require("./knex"));
var bookshelf = bookshelf_1.default(knex_1.default);
bookshelf.plugin('bookshelf-case-converter-plugin');
exports.default = bookshelf;
//# sourceMappingURL=bookshelf.js.map