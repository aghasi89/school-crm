declare const _default: {
    errors: {
        "portInUse": string;
        "invalidToken": string;
        "uniqueEmail": string;
        "invalidCredentials": string;
        "accessTokenExpired": string;
        "noToken": string;
        "sessionNotMaintained": string;
        "refreshTokenExpired": string;
        "portRequirePrivilege": string;
        "unAuthorized": string;
        "notFound": string;
    };
    messages: {
        "auth": {
            "loginSuccess": string;
            "logoutSuccess": string;
            "invalidCredentials": string;
            "accessTokenRefreshed": string;
        };
        "users": {
            "insert": string;
            "fetch": string;
            "delete": string;
            "fetchAll": string;
            "me": string;
            "update": string;
        };
        "bank": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "bankBranche": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
        };
        "billingMethod": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "classification": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "materialValue": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "materialValueGroup": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "warehouse": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "serviceType": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
        };
        "serviceName": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
        };
        "billingAccount": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "subdivision": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "measurementUnit": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "coWorkers": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
        };
        "position": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
        };
        "profession": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
        };
        "expenseAccount": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "typeOfIncome": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "typeOfVacation": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
        };
        "addition": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
        };
        "tabel": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "employee": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "contract": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "group": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "headPosition": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "accountantPosition": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "partners": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "currency": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "calculationsType": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "accountOfEmplCalculations": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "analiticGroup1": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "analiticGroup2": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "subsection": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "typesOfActions": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "warehouseEntryOrder": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "warehouseExitOrder": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "services": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "cashRegister": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
            "count": string;
        };
        "priceOfServices": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
        };
        "formulas": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
        };
        "structuralSubdivision": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "edit": string;
        };
        "billingAccountInBanks": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "receivedServices": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "cashRegisterEntry": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "operations": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "accountProduct": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "cashRegisterExit": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
        "hmxProfitTax": {
            "fetchAll": string;
            "insert": string;
            "fetch": string;
            "delete": string;
            "count": string;
            "edit": string;
        };
    };
    name: string;
    version: string;
    host: string;
    environment: string;
    appUrl: string;
    port: string;
    pagination: {
        page: number;
        maxRows: number;
    };
    auth: {
        saltRounds: string | number;
        accessTokenDuration: string;
        accessTokenDurationReset: string;
        accessTokenDurationResetLong: string;
        refreshTokenDuration: string;
        emailVerificationDuration: string | number;
        accessTokenSecretKey: string;
        refreshTokenSecretKey: string;
    };
    logging: {
        dir: string;
        level: string;
        maxSize: string;
        maxFiles: string;
        datePattern: string;
    };
};
export default _default;
