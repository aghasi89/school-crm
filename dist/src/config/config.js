"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var dotenv = tslib_1.__importStar(require("dotenv"));
var package_json_1 = tslib_1.__importDefault(require("../../package.json"));
var errors_json_1 = tslib_1.__importDefault(require("../resources/lang/errors.json"));
var messages_json_1 = tslib_1.__importDefault(require("../resources/lang/messages.json"));
dotenv.config();
var isTestEnvironment = process.env.NODE_ENV === 'test';
exports.default = {
    errors: errors_json_1.default,
    messages: messages_json_1.default,
    name: package_json_1.default.name,
    version: package_json_1.default.version,
    host: process.env.APP_HOST || '0.0.0.0',
    environment: process.env.NODE_ENV || 'development',
    appUrl: process.env.APP_URL || 'http://localhost:8888',
    port: (isTestEnvironment ? process.env.TEST_APP_PORT : process.env.APP_PORT) || '8000',
    pagination: {
        page: 1,
        maxRows: 20
    },
    auth: {
        saltRounds: process.env.SALT_ROUNDS || 11,
        accessTokenDuration: process.env.ACCESS_TOKEN_DURATION || '10m',
        accessTokenDurationReset: process.env.ACCESS_TOKEN_DURATION_RESET || '1m',
        accessTokenDurationResetLong: process.env.ACCESS_TOKEN_DURATION_RESET_LONG || '24h',
        refreshTokenDuration: process.env.REFRESH_TOKEN_DURATION || '24h',
        emailVerificationDuration: process.env.EMAIL_VERIFICATION_DURATION || 24,
        accessTokenSecretKey: process.env.ACCESS_TOKEN_SECRET_KEY || '<ACCESS_TOKEN_SECRET_KEY>',
        refreshTokenSecretKey: process.env.REFRESH_TOKEN_SECRET_KEY || '<REFRESH_TOKEN_SECRET_KEY>'
    },
    logging: {
        dir: process.env.LOGGING_DIR || 'logs',
        level: process.env.LOGGING_LEVEL || 'debug',
        maxSize: process.env.LOGGING_MAX_SIZE || '20m',
        maxFiles: process.env.LOGGING_MAX_FILES || '7d',
        datePattern: process.env.LOGGING_DATE_PATTERN || 'YYYY-MM-DD'
    }
};
//# sourceMappingURL=config.js.map