"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var knex_1 = tslib_1.__importDefault(require("knex"));
var dotenv = tslib_1.__importStar(require("dotenv"));
dotenv.config();
var dbConfig = {
    client: process.env.DB_CLIENT,
    connection: {
        charset: 'utf8',
        timezone: 'UTC',
        host: process.env.DB_HOST,
        port: +(process.env.DB_PORT || '5432'),
        database: process.env.NODE_ENV === 'test' ? process.env.TEST_DB_NAME : process.env.DB_NAME,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD
    }
};
exports.default = knex_1.default(dbConfig);
//# sourceMappingURL=knex.js.map