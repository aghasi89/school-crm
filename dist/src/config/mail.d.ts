declare const _default: {
    smtp: {
        port: string | number;
        host: string;
        auth: {
            user: string;
            pass: string;
        };
    };
    from: {
        address: string;
        name: string;
    };
};
export default _default;
