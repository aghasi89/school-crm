"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var dotenv = tslib_1.__importStar(require("dotenv"));
dotenv.config();
exports.default = {
    smtp: {
        port: process.env.MAIL_PORT || 2525,
        host: process.env.MAIL_HOST || 'smtp.mailtrap.io',
        auth: {
            user: process.env.MAIL_SMTP_USERNAME || 'MAILTRAP_SMTP_USERNAME',
            pass: process.env.MAIL_SMTP_PASSWORD || 'MAILTRAP_SMTP_PASSWORD'
        }
    },
    from: {
        address: 'vano.varderesyan94@gmail.com',
        name: 'Typescript API Starter'
    }
};
//# sourceMappingURL=mail.js.map