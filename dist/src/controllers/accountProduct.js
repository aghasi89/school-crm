"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var accountProductService = tslib_1.__importStar(require("../services/accountProductService"));
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var messages = config_1.default.messages;
function index(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var params, response, error_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    params = Object(req.params);
                    return [4, accountProductService.fetchAll(params.limit, params.offset)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.accountProduct.fetchAll
                    });
                    return [3, 3];
                case 2:
                    error_1 = _a.sent();
                    next(error_1);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.index = index;
function count(_, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var response, error_2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4, accountProductService.count()];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.accountProduct.count
                    });
                    return [3, 3];
                case 2:
                    error_2 = _a.sent();
                    next(error_2);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.count = count;
function store(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var accountProductPayload, accountProduct, error_3;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    accountProductPayload = req.body;
                    return [4, accountProductService.insert(accountProductPayload)];
                case 1:
                    accountProduct = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: accountProduct,
                        message: messages.accountProduct.insert
                    });
                    return [3, 3];
                case 2:
                    error_3 = _a.sent();
                    next(error_3);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.store = store;
function getOne(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, response, error_4;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    id = Number(req.params.id);
                    return [4, accountProductService.getById(id)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.accountProduct.fetch
                    });
                    return [3, 3];
                case 2:
                    error_4 = _a.sent();
                    next(error_4);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.getOne = getOne;
function destroy(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, response, error_5;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    id = Number(req.params.id);
                    return [4, accountProductService.destroy(id)];
                case 1:
                    response = (_a.sent());
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.accountProduct.delete
                    });
                    return [3, 3];
                case 2:
                    error_5 = _a.sent();
                    next(error_5);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.destroy = destroy;
function update(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, accountProductPayload, response, error_6;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    id = Number(req.params.id);
                    accountProductPayload = req.body;
                    return [4, accountProductService.update(id, accountProductPayload)];
                case 1:
                    response = (_a.sent());
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.accountProduct.edit
                    });
                    return [3, 3];
                case 2:
                    error_6 = _a.sent();
                    next(error_6);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=accountProduct.js.map