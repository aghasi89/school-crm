import { Request, Response, NextFunction } from 'express';
export declare function login(req: Request, res: Response, next: NextFunction): Promise<void>;
export declare function refresh(_: Request, res: Response, next: NextFunction): Promise<void>;
export declare function logout(_: Request, res: Response, next: NextFunction): Promise<void>;
