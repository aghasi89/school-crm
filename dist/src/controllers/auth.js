"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HTTPStatus = tslib_1.__importStar(require("http-status-codes"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var authService = tslib_1.__importStar(require("../services/authService"));
var messages = config_1.default.messages;
function login(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var data, error_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4, authService.login(req.body)];
                case 1:
                    data = _a.sent();
                    res.status(HTTPStatus.OK).json({
                        data: data,
                        code: HTTPStatus.OK,
                        message: messages.auth.loginSuccess
                    });
                    return [3, 3];
                case 2:
                    error_1 = _a.sent();
                    next(error_1);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.login = login;
function refresh(_, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var token, jwtPayload, data, error_2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    token = String(res.locals.refreshToken);
                    jwtPayload = res.locals.jwtPayload;
                    return [4, authService.refresh(token, jwtPayload)];
                case 1:
                    data = _a.sent();
                    res.status(HTTPStatus.OK).json({
                        data: data,
                        code: HTTPStatus.OK,
                        message: messages.auth.accessTokenRefreshed
                    });
                    return [3, 3];
                case 2:
                    error_2 = _a.sent();
                    next(error_2);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.refresh = refresh;
function logout(_, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var token, error_3;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    token = String(res.locals.refreshToken);
                    return [4, authService.logout(token)];
                case 1:
                    _a.sent();
                    res.status(HTTPStatus.OK).json({
                        code: HTTPStatus.OK,
                        message: messages.auth.logoutSuccess
                    });
                    return [3, 3];
                case 2:
                    error_3 = _a.sent();
                    next(error_3);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.logout = logout;
//# sourceMappingURL=auth.js.map