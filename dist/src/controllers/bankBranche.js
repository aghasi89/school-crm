"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var bankBrancheService = tslib_1.__importStar(require("../services/bankBrancheService"));
var messages = config_1.default.messages;
function index(_, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var response, err_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4, bankBrancheService.fetchAll()];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.bankBranche.fetchAll
                    });
                    return [3, 3];
                case 2:
                    err_1 = _a.sent();
                    next(err_1);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.index = index;
function store(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var bankBranchePayload, response, err_2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    bankBranchePayload = req.body;
                    return [4, bankBrancheService.insert(bankBranchePayload)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.bankBranche.insert
                    });
                    return [3, 3];
                case 2:
                    err_2 = _a.sent();
                    next(err_2);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.store = store;
//# sourceMappingURL=bankBranche.js.map