"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var cashRegisterService = tslib_1.__importStar(require("../services/cashRegisterService"));
var messages = config_1.default.messages;
function index(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var params, response, err_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    params = Object(req.params);
                    return [4, cashRegisterService.fetchAll(params.limit, params.offset)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.cashRegister.fetchAll
                    });
                    return [3, 3];
                case 2:
                    err_1 = _a.sent();
                    next(err_1);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.index = index;
function count(_, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var response, error_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4, cashRegisterService.count()];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.cashRegister.count
                    });
                    return [3, 3];
                case 2:
                    error_1 = _a.sent();
                    next(error_1);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.count = count;
function store(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var cashRegisterPayload, response, err_2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    cashRegisterPayload = req.body;
                    return [4, cashRegisterService.insert(cashRegisterPayload)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.cashRegister.insert
                    });
                    return [3, 3];
                case 2:
                    err_2 = _a.sent();
                    next(err_2);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.store = store;
function getOne(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, response, error_2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    id = Number(req.params.id);
                    return [4, cashRegisterService.getById(id)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.cashRegister.fetch
                    });
                    return [3, 3];
                case 2:
                    error_2 = _a.sent();
                    next(error_2);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.getOne = getOne;
function destroy(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, response, error_3;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    id = Number(req.params.id);
                    return [4, cashRegisterService.destroy(id)];
                case 1:
                    response = (_a.sent());
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.cashRegister.delete
                    });
                    return [3, 3];
                case 2:
                    error_3 = _a.sent();
                    next(error_3);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.destroy = destroy;
function update(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, partnersPayload, response, error_4;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    id = Number(req.params.id);
                    partnersPayload = req.body;
                    return [4, cashRegisterService.update(id, partnersPayload)];
                case 1:
                    response = (_a.sent());
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.cashRegister.edit
                    });
                    return [3, 3];
                case 2:
                    error_4 = _a.sent();
                    next(error_4);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=cashRegister.js.map