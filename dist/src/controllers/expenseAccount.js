"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var expenseAccountService = tslib_1.__importStar(require("../services/expenseAccountService"));
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var messages = config_1.default.messages;
function index(_, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var params, response, error_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    params = Object(_.params);
                    return [4, expenseAccountService.fetchAll(params.limit, params.offset)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.expenseAccount.fetchAll
                    });
                    return [3, 3];
                case 2:
                    error_1 = _a.sent();
                    next(error_1);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.index = index;
function count(_, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var response, error_2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4, expenseAccountService.count()];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.expenseAccount.count
                    });
                    return [3, 3];
                case 2:
                    error_2 = _a.sent();
                    next(error_2);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.count = count;
function store(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var expenseAccountPayload, expenseAccount, error_3;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    expenseAccountPayload = req.body;
                    return [4, expenseAccountService.insert(expenseAccountPayload)];
                case 1:
                    expenseAccount = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: expenseAccount,
                        message: messages.expenseAccount.insert
                    });
                    return [3, 3];
                case 2:
                    error_3 = _a.sent();
                    next(error_3);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.store = store;
function getOne(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, response, error_4;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    id = Number(req.params.id);
                    return [4, expenseAccountService.getById(id)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.expenseAccount.fetch
                    });
                    return [3, 3];
                case 2:
                    error_4 = _a.sent();
                    next(error_4);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.getOne = getOne;
function destroy(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, response, error_5;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    id = Number(req.params.id);
                    return [4, expenseAccountService.destroy(id)];
                case 1:
                    response = (_a.sent());
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.expenseAccount.delete
                    });
                    return [3, 3];
                case 2:
                    error_5 = _a.sent();
                    next(error_5);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.destroy = destroy;
function update(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, expenseAccountPayload, response, error_6;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    id = Number(req.params.id);
                    expenseAccountPayload = req.body;
                    return [4, expenseAccountService.update(id, expenseAccountPayload)];
                case 1:
                    response = (_a.sent());
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.subdivision.edit
                    });
                    return [3, 3];
                case 2:
                    error_6 = _a.sent();
                    next(error_6);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=expenseAccount.js.map