"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
function index(req, res) {
    res.status(HttpStatus.OK).json({
        name: req.app.locals.name,
        version: req.app.locals.version
    });
}
exports.index = index;
//# sourceMappingURL=home.js.map