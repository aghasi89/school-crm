"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var localDbService = tslib_1.__importStar(require("../services/localDbService"));
function getOne(req, res, next) {
    try {
        var key = req.params.key;
        console.log(key);
        var response = localDbService.getByKey(key);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: 'info'
        });
    }
    catch (error) {
        next(error);
    }
}
exports.getOne = getOne;
//# sourceMappingURL=localDb.js.map