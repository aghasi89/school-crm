import { Request, Response, NextFunction } from 'express';
export declare function index(_: Request, res: Response, next: NextFunction): Promise<void>;
export declare function store(req: Request, res: Response, next: NextFunction): Promise<void>;
