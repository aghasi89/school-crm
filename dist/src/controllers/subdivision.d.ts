import { Request, Response, NextFunction } from 'express';
export declare function index(req: Request, res: Response, next: NextFunction): Promise<void>;
export declare function count(_: Request, res: Response, next: NextFunction): Promise<void>;
export declare function store(req: Request, res: Response, next: NextFunction): Promise<void>;
export declare function getOne(req: Request, res: Response, next: NextFunction): Promise<void>;
export declare function destroy(req: Request, res: Response, next: NextFunction): Promise<void>;
export declare function update(req: Request, res: Response, next: NextFunction): Promise<void>;
