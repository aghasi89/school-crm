"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var userService = tslib_1.__importStar(require("../services/userService"));
var messages = config_1.default.messages;
function index(_, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var response, err_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4, userService.fetchAll()];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.users.fetchAll
                    });
                    return [3, 3];
                case 2:
                    err_1 = _a.sent();
                    next(err_1);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.index = index;
function me(_, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var response, err_2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    console.log(res.locals.loggedInPayload, '-------------');
                    return [4, userService.me(res.locals)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.users.me
                    });
                    return [3, 3];
                case 2:
                    err_2 = _a.sent();
                    next(err_2);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.me = me;
function update(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var userPayload, response, err_3;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    console.log(res.locals.loggedInPayload, '-------------');
                    userPayload = req.body;
                    return [4, userService.update(res.locals, userPayload)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.users.update
                    });
                    return [3, 3];
                case 2:
                    err_3 = _a.sent();
                    next(err_3);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.update = update;
function store(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var userPayload, response, err_4;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    userPayload = req.body;
                    return [4, userService.insert(userPayload)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.users.insert
                    });
                    return [3, 3];
                case 2:
                    err_4 = _a.sent();
                    next(err_4);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.store = store;
function storeAdmin(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var userPayload, response, err_5;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    userPayload = req.body;
                    return [4, userService.insertAdmin(userPayload)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.users.insert
                    });
                    return [3, 3];
                case 2:
                    err_5 = _a.sent();
                    console.log(err_5);
                    next(err_5);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.storeAdmin = storeAdmin;
function resetPassword(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var resetPayload, response, err_6;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    resetPayload = req.body;
                    return [4, userService.resetPassword(resetPayload)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.users.insert
                    });
                    return [3, 3];
                case 2:
                    err_6 = _a.sent();
                    next(err_6);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.resetPassword = resetPassword;
function checkToken(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var checkTokenPayload, response, err_7;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    checkTokenPayload = req.body;
                    return [4, userService.checkToken(checkTokenPayload['token'])];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.users.insert
                    });
                    return [3, 3];
                case 2:
                    err_7 = _a.sent();
                    next(err_7);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.checkToken = checkToken;
function checkTokenLong(req, _, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var checkTokenPayload, err_8;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    checkTokenPayload = req.body;
                    return [4, userService.checkTokenLong(checkTokenPayload['token'])];
                case 1:
                    _a.sent();
                    next();
                    return [3, 3];
                case 2:
                    err_8 = _a.sent();
                    next(err_8);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.checkTokenLong = checkTokenLong;
function newPassword(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var resetPasswordPayload, response, err_9;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    resetPasswordPayload = req.body;
                    return [4, userService.newPassword(resetPasswordPayload)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.users.insert
                    });
                    return [3, 3];
                case 2:
                    err_9 = _a.sent();
                    next(err_9);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.newPassword = newPassword;
function getUniqueValue(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var uniqueValueParamsPayload, response, err_10;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    uniqueValueParamsPayload = req.params;
                    return [4, userService.getUniqueValue(uniqueValueParamsPayload)];
                case 1:
                    response = _a.sent();
                    res.status(HttpStatus.OK).json({
                        code: HttpStatus.OK,
                        data: response,
                        message: messages.users.insert
                    });
                    return [3, 3];
                case 2:
                    err_10 = _a.sent();
                    next(err_10);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.getUniqueValue = getUniqueValue;
//# sourceMappingURL=user.js.map