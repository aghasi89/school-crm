"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var faker_1 = tslib_1.__importDefault(require("faker"));
var userService = tslib_1.__importStar(require("../../services/userService"));
function run() {
    return userService.insert({
        password: 'secret',
        name: faker_1.default.name.findName(),
        email: faker_1.default.internet.email()
    });
}
exports.run = run;
//# sourceMappingURL=userFactory.js.map