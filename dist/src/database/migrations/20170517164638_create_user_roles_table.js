"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    var _this = this;
    return knex.schema
        .createTable(Table_1.default.USER_ROLES, function (table) {
        table.increments('id').primary();
        table
            .string('name', 50)
            .unique()
            .notNullable();
        table.string('description', 100).nullable();
        table.timestamps(true, true);
    })
        .then(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            return [2, knex(Table_1.default.USER_ROLES)
                    .truncate()
                    .then(function () {
                    return Promise.all([
                        knex(Table_1.default.USER_ROLES).insert([
                            {
                                id: 1,
                                name: 'Admin',
                                description: 'This is super admin.'
                            },
                            {
                                id: 2,
                                name: 'Normal User',
                                description: 'This is normal user.'
                            }
                        ])
                    ]);
                })];
        });
    }); });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.USER_ROLES);
}
exports.down = down;
//# sourceMappingURL=20170517164638_create_user_roles_table.js.map