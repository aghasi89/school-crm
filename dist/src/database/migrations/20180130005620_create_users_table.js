"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema.createTable(Table_1.default.USERS, function (table) {
        table.increments('id').primary();
        table.string('name').notNullable();
        table
            .string('email')
            .notNullable()
            .unique();
        table.string('password').notNullable();
        table
            .integer('role_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.USER_ROLES);
        table.timestamps(true, true);
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.USERS);
}
exports.down = down;
//# sourceMappingURL=20180130005620_create_users_table.js.map