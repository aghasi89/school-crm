"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema.createTable(Table_1.default.USER_SESSIONS, function (table) {
        table.increments('id').primary();
        table.text('token').notNullable();
        table
            .integer('user_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.USERS);
        table
            .boolean('is_active')
            .notNullable()
            .defaultTo(true);
        table.timestamps(true, true);
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.USER_SESSIONS);
}
exports.down = down;
//# sourceMappingURL=20180517164647_create_user_sessions_table.js.map