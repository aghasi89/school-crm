"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.BANK, function (table) {
        table.increments('id');
        table.string('code').notNullable();
        table.string('name', 1000).notNullable();
        table.string('swift').notNullable();
    })
        .createTable(Table_1.default.BANK_BRANCHE, function (table) {
        table.increments('id');
        table.string('branche_code');
        table.string('branche_address').notNullable();
        table.integer('bank_id').references('bank.id').unsigned().index().onDelete('CASCADE');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.BANK).dropTable(Table_1.default.BANK_BRANCHE);
}
exports.down = down;
//# sourceMappingURL=20200113101805_bank.js.map