"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.SUBDIVISION, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
        table.string('code', 1000).unique('code').notNullable();
        table.timestamps(true, true);
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.SUBDIVISION);
}
exports.down = down;
//# sourceMappingURL=20200113104653_subdivision.js.map