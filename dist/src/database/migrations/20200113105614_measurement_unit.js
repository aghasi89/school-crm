"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.MEASUREMENT_UNIT, function (table) {
        table.increments('id');
        table.string('code', 255).notNullable();
        table.string('unit', 255).notNullable();
        table.string('abbreviation', 255).notNullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.MEASUREMENT_UNIT);
}
exports.down = down;
//# sourceMappingURL=20200113105614_measurement_unit.js.map