"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.CO_WORKERS, function (table) {
        table.increments('id');
        table.string('code', 1000).notNullable();
        table.string('hvhh', 1000).notNullable();
        table.string('name', 1000).notNullable();
        table.string('creditor', 1000).notNullable();
        table.string('debetor', 1000).notNullable();
        table.string('legal_address', 1000).notNullable();
        table.string('work_address', 1000).notNullable();
        table.string('transfer_purpose', 1000).notNullable();
        table.string('inflow_account', 1000).notNullable();
        table.string('leakage_account', 1000).notNullable();
        table.string('director', 1000).notNullable();
        table.string('accountent', 1000).notNullable();
        table.string('bank_account', 1000).notNullable();
        table.integer('bank_id').references('bank.id').unsigned().index().onDelete('CASCADE');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.CO_WORKERS);
}
exports.down = down;
//# sourceMappingURL=20200113111449_co_workers.js.map