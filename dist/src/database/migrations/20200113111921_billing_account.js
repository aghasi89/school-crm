"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.BILLING_ACCOUNT, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
        table.string('bank_account', 1000).notNullable();
        table.string('account', 1000).notNullable();
        table.boolean('isMain').nullable();
        table
            .integer('partners_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.PARTNERS)
            .onDelete('CASCADE');
        table.string('serial_number', 1000).notNullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.BILLING_ACCOUNT);
}
exports.down = down;
//# sourceMappingURL=20200113111921_billing_account.js.map