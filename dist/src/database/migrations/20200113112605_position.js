"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.POSITION, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
        table.integer('subdivision_id').references('subdivision.id').unsigned().index().onDelete('CASCADE');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.POSITION);
}
exports.down = down;
//# sourceMappingURL=20200113112605_position.js.map