"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.PROFESSION, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.PROFESSION);
}
exports.down = down;
//# sourceMappingURL=20200120095222_profession.js.map