"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.TABEL, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
        table.string('hours', 1000).notNullable();
        table.string('year', 1000).notNullable();
        table.text('months').notNullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.TABEL);
}
exports.down = down;
//# sourceMappingURL=20200120100159_tabel.js.map