"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.EXPENSE_ACCOUNT, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
    })
        .createTable(Table_1.default.TYPE_OF_INCOME, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
    })
        .createTable(Table_1.default.TYPE_OF_VACATION, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
    })
        .createTable(Table_1.default.ADDITION, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
        table
            .integer('tabel_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.TABEL)
            .onDelete('CASCADE');
        table
            .integer('type_of_income_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.TYPE_OF_INCOME)
            .onDelete('CASCADE');
        table
            .integer('type_of_vacation_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.TYPE_OF_VACATION)
            .onDelete('CASCADE');
        table
            .integer('expense_account_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.EXPENSE_ACCOUNT)
            .onDelete('CASCADE');
        table.string('coefficient').nullable();
        table.boolean('recalculation').nullable();
        table.boolean('is_income').nullable();
        table.boolean('declining_income').nullable();
        table.boolean('is_trade_union').nullable();
        table.boolean('is_for_tax_purposes_only').nullable();
        table.boolean('is_mandatory_pension').nullable();
        table.boolean('by_the_employer_mandatory_pension').nullable();
        table.boolean('participates_on_account_of_actual_hours').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.TABLE_CODE)
        .dropTable(Table_1.default.TYPE_OF_INCOME)
        .dropTable(Table_1.default.TYPE_OF_VACATION)
        .dropTable(Table_1.default.EXPENSE_ACCOUNT)
        .dropTable(Table_1.default.ADDITION);
}
exports.down = down;
//# sourceMappingURL=20200120100222_addition.js.map