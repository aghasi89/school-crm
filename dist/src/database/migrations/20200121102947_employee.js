"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.EMPLOYEE, function (table) {
        table.increments('id');
        table
            .integer('tabel_counter')
            .unique('true')
            .unsigned();
        table.string('full_name', 1000).notNullable();
        table.string('first_name', 1000).notNullable();
        table.string('last_name', 1000).notNullable();
        table.integer('subdivision_id').references('subdivision.id').unsigned().index().onDelete('CASCADE');
    })
        .createTable(Table_1.default.CONTRACT, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
    })
        .createTable(Table_1.default.DOCUMENT_TYPE, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
    })
        .createTable(Table_1.default.EMPLOYEE_GENERAL, function (table) {
        table.increments('id');
        table
            .integer('employee_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.EMPLOYEE)
            .onDelete('CASCADE');
        table
            .integer('profession_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.PROFESSION)
            .onDelete('CASCADE');
        table.string('gender').nullable();
        table.date('birthdate').nullable();
        table
            .integer('contract_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.CONTRACT)
            .onDelete('CASCADE');
        table
            .integer('account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE');
        table.date('date_of_accept').nullable();
        table.string('accepted_command').nullable();
        table.date('release_date').nullable();
        table.string('release_command').nullable();
    })
        .createTable(Table_1.default.ADDRESSES, function (table) {
        table.increments('id');
        table
            .integer('employee_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.EMPLOYEE)
            .onDelete('CASCADE');
        table.boolean('place_of_registration').notNullable();
        table.string('state').nullable();
        table.string('community').nullable();
        table.string('city').nullable();
        table.string('street').nullable();
        table.boolean('same_residence').nullable();
        table.boolean('HH_residence').nullable();
        table.string('HH_state').nullable();
        table.string('HH_community').nullable();
        table.string('HH_city').nullable();
        table.string('HH_street').nullable();
        table.string('country').nullable();
        table.string('addressee1').nullable();
        table.string('addressee2').nullable();
        table.string('addressee3').nullable();
        table.string('post').nullable();
    })
        .createTable(Table_1.default.OTHER_INFORMATION, function (table) {
        table.increments('id');
        table
            .integer('employee_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.EMPLOYEE)
            .onDelete('CASCADE');
        table.string('bank_number').notNullable();
        table.string('social_card_number').nullable();
        table
            .integer('document_type_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.DOCUMENT_TYPE)
            .onDelete('CASCADE');
        table.string('passport_number').nullable();
        table.date('due_date').nullable();
        table.string('by_whom').nullable();
        table.string('nationality').nullable();
        table.string('another_document_number').nullable();
        table.string('phone').nullable();
        table.string('phone2').nullable();
        table.string('email').nullable();
        table.string('language').nullable();
        table.string('marital_status').nullable();
        table.string('education').nullable();
        table.string('service').nullable();
        table.string('members_of_family').nullable();
    })
        .createTable(Table_1.default.EMPLOYEE_ADDITION, function (table) {
        table.increments('id');
        table
            .integer('employee_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.EMPLOYEE)
            .onDelete('CASCADE');
        table
            .integer('addition_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ADDITION)
            .onDelete('CASCADE');
        table.string('is_main');
        table.string('money');
    })
        .createTable(Table_1.default.EMPLOYEE_POSITION, function (table) {
        table.increments('id');
        table
            .integer('position_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.POSITION)
            .onDelete('CASCADE');
        table
            .integer('employee_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.EMPLOYEE)
            .onDelete('CASCADE');
        table.date('start_of_position').nullable();
        table.date('end_of_position').nullable();
    })
        .createTable(Table_1.default.EMPLOYEE_ACCOUNTS, function (table) {
        table.increments('id');
        table
            .integer('account_of_employee_calculation_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE');
        table
            .integer('employee_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.EMPLOYEE)
            .onDelete('CASCADE');
        table.string('percent').nullable();
        table.string('type').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.EMPLOYEE)
        .dropTable(Table_1.default.CONTRACT)
        .dropTable(Table_1.default.DOCUMENT_TYPE)
        .dropTable(Table_1.default.EMPLOYEE_GENERAL)
        .dropTable(Table_1.default.EXPENSE_ACCOUNT)
        .dropTable(Table_1.default.EMPLOYEE_ADDITION)
        .dropTable(Table_1.default.OTHER_INFORMATION)
        .dropTable(Table_1.default.ADDRESSES)
        .dropTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .dropTable(Table_1.default.EMPLOYEE_POSITION)
        .dropTable(Table_1.default.EMPLOYEE_ACCOUNTS);
}
exports.down = down;
//# sourceMappingURL=20200121102947_employee.js.map