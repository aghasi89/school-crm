"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.BILLING_SHEET, function (table) {
        table.increments('id');
        table
            .integer('employee_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.EMPLOYEE)
            .onDelete('CASCADE');
        table.integer('subdivision_id').references('subdivision.id').unsigned().index().onDelete('CASCADE');
        table.string('date', 1000).notNullable();
        table.boolean('show_hours').notNullable();
        table.boolean('expanded').notNullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.BILLING_SHEET);
}
exports.down = down;
//# sourceMappingURL=20200121154140_billing_sheet.js.map