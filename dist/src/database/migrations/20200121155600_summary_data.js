"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.SUMMARY_DATA, function (table) {
        table.increments('id');
        table
            .integer('employee_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.EMPLOYEE)
            .onDelete('CASCADE');
        table.integer('subdivision_id').references('subdivision.id').unsigned().index().onDelete('CASCADE');
        table.date('start_date').notNullable();
        table.date('end_date').notNullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.SUMMARY_DATA);
}
exports.down = down;
//# sourceMappingURL=20200121155600_summary_data.js.map