"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.GROUP, function (table) {
        table.increments('id');
        table.string('name').nullable();
        table.string('accumulator', 1000).notNullable();
    })
        .createTable(Table_1.default.HEAD_POSITION, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
    })
        .createTable(Table_1.default.ACCOUNTANT_POSITION, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
    })
        .createTable(Table_1.default.PARTNERS, function (table) {
        table.increments('id');
        table.string('hvhh').nullable();
        table.string('name', 1000).notNullable();
        table.string('full_name', 1000).notNullable();
        table
            .integer('group_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.GROUP)
            .onDelete('CASCADE');
        table
            .integer('head_position_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.HEAD_POSITION)
            .onDelete('CASCADE');
        table
            .integer('accountant_position_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ACCOUNTANT_POSITION)
            .onDelete('CASCADE');
        table.boolean('aa_hpayer').notNullable();
        table.string('legal_address').notNullable();
        table.string('practical_address').notNullable();
        table.string('head_aah').notNullable();
        table.string('accountant_aah').notNullable();
        table.string('certificate_number').notNullable();
        table.string('passport_number').notNullable();
        table.string('main_purpose_of_payment').notNullable();
        table.string('phone').notNullable();
        table.string('email').notNullable();
        table.string('contract').notNullable();
        table.string('date_contract').notNullable();
        table.string('percentage_of_sales_discount').notNullable();
        table.string('another_additional_information').nullable();
        table.string('another_delivery_time').nullable();
        table.string('another_fullname').nullable();
        table.string('another_credentials_number').nullable();
        table.string('another_credentials_date').nullable();
    }).createTable(Table_1.default.ADDITIONAL_ADDRESSE_PARTNERS, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
        table
            .integer('partners_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.PARTNERS)
            .onDelete('CASCADE');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.PARTNERS);
}
exports.down = down;
//# sourceMappingURL=20200130114609_partners.js.map