"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.CURRENCY, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
        table.string('currency', 1000).notNullable();
    })
        .createTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS_TYPE, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
    })
        .createTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
        table.boolean('is_accumulated_account').nullable();
        table
            .integer('acumulated_account_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE');
        table.boolean('off_balance_sheet').nullable();
        table.boolean('accounting_by_partners').nullable();
        table.boolean('analytical_group_1').nullable();
        table.boolean('analytical_group_2').nullable();
        table
            .integer('calculations_type_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS_TYPE)
            .onDelete('CASCADE');
    })
        .createTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS_CURRENCY, function (table) {
        table.increments('id');
        table
            .integer('employee_calculations_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE');
        table
            .integer('currency_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.CURRENCY)
            .onDelete('CASCADE');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS_TYPE)
        .dropTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .dropTable(Table_1.default.CURRENCY);
}
exports.down = down;
//# sourceMappingURL=20200203123057_account_of_employee_calculations.js.map