"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.ANALITIC_GROUP_1, function (table) {
        table.increments('id');
        table.integer('code').unique();
        table.string('name', 1000).notNullable();
        table.boolean('is_accumulate');
        table
            .integer('analitic_group_1_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_1)
            .onDelete('CASCADE');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.ANALITIC_GROUP_1);
}
exports.down = down;
//# sourceMappingURL=20200212113304_analitic_group_1.js.map