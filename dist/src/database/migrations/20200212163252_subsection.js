"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.SUBSECTION, function (table) {
        table.increments('id');
        table.string('name');
        table.integer('code').unique();
        table
            .integer('types_of_actions_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.TYPES_OF_ACTIONS)
            .onDelete('CASCADE');
        table
            .integer('customer_account_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE');
        table
            .integer('prepaid_account_received_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE');
        table
            .integer('aah_account_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.SUBSECTION);
}
exports.down = down;
//# sourceMappingURL=20200212163252_subsection.js.map