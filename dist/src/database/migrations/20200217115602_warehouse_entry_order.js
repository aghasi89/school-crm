"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.WAREHOUSE_ENTRY_ORDER, function (table) {
        table.increments('id');
        table.date('date');
        table
            .integer('warehouse_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.WAREHOUSE)
            .onDelete('CASCADE').nullable();
        table.integer('document_number').unsigned().unique();
        table
            .integer('partners_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.PARTNERS)
            .onDelete('CASCADE').nullable();
        table.string('name');
        table
            .integer('partners_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('prepaid_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('analitic_group_2_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_2)
            .onDelete('CASCADE');
        table
            .integer('analitic_group_1_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_1)
            .onDelete('CASCADE');
        table.text('document_n');
        table.date('document_date');
        table.text('comment');
        table.text('power_of_attorney');
        table.text('mediator');
        table.text('container');
        table.text('accountant');
        table.text('allow');
        table.text('accept');
        table.text('document_of_transport');
        table.date('document_of_transport_date');
        table.text('type_of_acquisition_of_na');
        table.text('calculation_style_of_aah');
        table.boolean('include_aah_in_cost');
    })
        .createTable(Table_1.default.WAREHOUSE_ENTRY_ORDER_PRODUCT, function (table) {
        table.increments('id');
        table
            .integer('warehouse_entry_order_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.WAREHOUSE_ENTRY_ORDER)
            .onDelete('CASCADE').nullable();
        table
            .integer('material_value_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.MATERIAL_VALUE)
            .onDelete('CASCADE').nullable();
        table.integer('point');
        table.integer('count');
        table.decimal('price');
        table.decimal('money');
        table.boolean('is_aah').nullable();
        table
            .integer('accounts_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('classification_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.CLASSIFICATION)
            .onDelete('CASCADE').nullable();
    })
        .createTable(Table_1.default.WAREHOUSE_ENTRY_ORDER_FUNCTIONS, function (table) {
        table.increments('id');
        table
            .integer('warehouse_entry_order_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.WAREHOUSE_ENTRY_ORDER)
            .onDelete('CASCADE').nullable();
        table.integer('operations_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.OPERATIONS)
            .onDelete('CASCADE').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.WAREHOUSE_ENTRY_ORDER);
}
exports.down = down;
//# sourceMappingURL=20200217115602_warehouse_entry_order.js.map