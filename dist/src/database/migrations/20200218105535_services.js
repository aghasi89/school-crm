"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.SERVICES, function (table) {
        table.increments('id');
        table.string('code').unique();
        table.string('name');
        table.string('full_name');
        table.integer('measurement_unit_id').references('measurement_unit.id').unsigned().index().onDelete('CASCADE');
        table.integer('classification_id').references('classification.id').unsigned().index().onDelete('CASCADE');
        table.string('account_id').references('accounts.account').index().onDelete('CASCADE');
        table.decimal('wholesale_price').nullable();
        table.decimal('retailer_price').nullable();
        table.string('bar_code').nullable();
        table.boolean('is_aah').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.SERVICES);
}
exports.down = down;
//# sourceMappingURL=20200218105535_services.js.map