"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.CASH_REGISTER, function (table) {
        table.increments('id');
        table.string('code').unique();
        table.string('name');
        table.string('account_id').references('accounts.account').index().onDelete('CASCADE');
        table.boolean('is_main');
        table.string('dmo_n');
        table.string('deo_n');
        table.string('hdm_regis_n');
        table.string('ip');
        table.string('port');
        table.string('password');
        table.string('hdm_non_taxable');
        table.string('hdm_taxable');
        table.string('hdm_print_type');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.CASH_REGISTER);
}
exports.down = down;
//# sourceMappingURL=20200218124726_cash_register.js.map