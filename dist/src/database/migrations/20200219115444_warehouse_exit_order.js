"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.WAREHOUSE_EXIT_ORDER, function (table) {
        table.increments('id');
        table.date('date');
        table
            .integer('warehouse_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.WAREHOUSE)
            .onDelete('CASCADE').nullable();
        table.integer('document_number').unsigned().unique();
        table
            .string('expense_account_id')
            .references('account')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('analitic_group_2_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_2)
            .onDelete('CASCADE');
        table
            .integer('analitic_group_1_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_1)
            .onDelete('CASCADE');
        table.text('comment');
        table.text('power_of_attorney');
        table.text('mediator');
        table.text('container');
        table.text('accountant');
        table.text('allow');
        table.text('has_requested');
    })
        .createTable(Table_1.default.WAREHOUSE_EXIT_ORDER_PRODUCT, function (table) {
        table.increments('id');
        table
            .integer('warehouse_exit_order_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.WAREHOUSE_EXIT_ORDER)
            .onDelete('CASCADE').nullable();
        table
            .integer('material_value_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.MATERIAL_VALUE)
            .onDelete('CASCADE').nullable();
        table.integer('point');
        table.integer('count');
        table.decimal('money');
        table.integer('batch').nullable();
    })
        .createTable(Table_1.default.WAREHOUSE_EXIT_ORDER_FUNCTIONS, function (table) {
        table.increments('id');
        table
            .integer('warehouse_exit_order_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.WAREHOUSE_ENTRY_ORDER)
            .onDelete('CASCADE').nullable();
        table.integer('operations_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.OPERATIONS)
            .onDelete('CASCADE').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.WAREHOUSE_EXIT_ORDER)
        .dropTable(Table_1.default.WAREHOUSE_EXIT_ORDER_PRODUCT)
        .dropTable(Table_1.default.WAREHOUSE_EXIT_ORDER_FUNCTIONS);
}
exports.down = down;
//# sourceMappingURL=20200219115444_warehouse_exit_order.js.map