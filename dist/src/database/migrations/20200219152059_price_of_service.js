"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.PRICE_OF_SERVICES, function (table) {
        table.increments('id');
        table.date('start_date').notNullable();
        table.date('end_date').notNullable();
        table
            .integer('services_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.SERVICES)
            .onDelete('CASCADE');
        table.text('type').notNullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.PRICE_OF_SERVICES);
}
exports.down = down;
//# sourceMappingURL=20200219152059_price_of_service.js.map