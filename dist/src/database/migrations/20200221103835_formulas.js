"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.FORMULAS, function (table) {
        table.increments('id');
        table.string('code').unique();
        table.string('caption');
        table.string('expression');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.FORMULAS);
}
exports.down = down;
//# sourceMappingURL=20200221103835_formulas.js.map