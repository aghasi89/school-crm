"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.STRUCTURAL_SUBDIVISION, function (table) {
        table.increments('id');
        table.string('code').unique();
        table.string('name');
        table
            .integer('partner_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.PARTNERS)
            .onDelete('CASCADE');
        table
            .string('expense_account_id')
            .references('account')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.STRUCTURAL_SUBDIVISION);
}
exports.down = down;
//# sourceMappingURL=20200221152918_structural_subdivision.js.map