"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.BILLING_ACCOUNT_IN_BANKS, function (table) {
        table.increments('id');
        table.string('billing_account');
        table.string('name');
        table
            .integer('currency_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.CURRENCY)
            .onDelete('CASCADE');
        table
            .string('account_id')
            .references('account')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE');
        table.boolean('is_main');
        table.string('number_of_document'),
            table.string('e_services');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.BILLING_ACCOUNT_IN_BANKS);
}
exports.down = down;
//# sourceMappingURL=20200226093232_billing_account_in_banks.js.map