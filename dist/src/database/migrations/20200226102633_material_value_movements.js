"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS, function (table) {
        table.increments('id');
        table.date('date');
        table
            .integer('warehouse_exit_order_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.WAREHOUSE_EXIT_ORDER)
            .onDelete('CASCADE').nullable();
        table
            .integer('warehouse_entry_order_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.WAREHOUSE_ENTRY_ORDER)
            .onDelete('CASCADE').nullable();
        table.integer('document_number').unsigned().unique();
        table.string('print_at_sale_prices').nullable();
        table
            .integer('analitic_group_2_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_2)
            .onDelete('CASCADE');
        table
            .integer('analitic_group_1_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_1)
            .onDelete('CASCADE');
        table.text('comment');
        table.text('accountant');
        table.text('mediator');
        table.text('allow');
        table.text('shipper_registration_book_info');
        table.text('output_method');
        table.text('seria');
        table.text('date_of_discharge').nullable();
        table.text('has_requested');
        table.text('additional_comment');
    })
        .createTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS_PRODUCT, function (table) {
        table.increments('id');
        table
            .integer('material_movements_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS)
            .onDelete('CASCADE').nullable();
        table
            .integer('material_value_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.MATERIAL_VALUE)
            .onDelete('CASCADE').nullable();
        table.integer('point');
        table.integer('count');
        table.decimal('money');
        table.integer('batch').nullable();
    })
        .createTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS_FUNCTIONS, function (table) {
        table.increments('id');
        table
            .integer('material_movements_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS)
            .onDelete('CASCADE').nullable();
        table.integer('operations_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.OPERATIONS)
            .onDelete('CASCADE').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS)
        .dropTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS_PRODUCT)
        .dropTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS_FUNCTIONS);
}
exports.down = down;
//# sourceMappingURL=20200226102633_material_value_movements.js.map