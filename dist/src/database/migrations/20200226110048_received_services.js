"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.RECEIVED_SERVICES, function (table) {
        table.increments('id');
        table
            .integer('currency_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.CURRENCY)
            .onDelete('CASCADE');
        table.text('currency_exchange_rate_1');
        table.text('currency_exchange_rate_2');
        table.boolean('previous_day_exchange_rate');
        table
            .integer('partners_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.PARTNERS)
            .onDelete('CASCADE');
        table
            .string('provider_account_id')
            .references('account')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .string('advance_payment_account_id')
            .references('account')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('analitic_group_1_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_1)
            .onDelete('CASCADE');
        table
            .integer('analitic_group_2_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_2)
            .onDelete('CASCADE');
        table.text('output_method');
        table.integer('purchase_document_number');
        table.date('purchase_document_date');
        table.text('comment');
        table.text('purchase_type_of_service');
        table.text('calculation_type_aah');
        table.boolean('include_aah_in_expense');
        table.text('form_of_reflection_aah');
        table.text('typical_operation');
    })
        .createTable(Table_1.default.RECEIVED_SERVICES_DIRECTORY, function (table) {
        table.increments('id');
        table
            .integer('received_service_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.RECEIVED_SERVICES)
            .onDelete('CASCADE').nullable();
        table
            .integer('service_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.SERVICES)
            .onDelete('CASCADE').nullable();
        table.integer('point');
        table.integer('count');
        table.decimal('price');
        table.decimal('money');
        table.boolean('aah');
        table.text('account').nullable();
    })
        .createTable(Table_1.default.RECEIVED_SERVICES_OPERATIONS, function (table) {
        table.increments('id');
        table
            .integer('received_service_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.RECEIVED_SERVICES)
            .onDelete('CASCADE').nullable();
        table.integer('operations_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.OPERATIONS)
            .onDelete('CASCADE').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.RECEIVED_SERVICES)
        .dropTable(Table_1.default.RECEIVED_SERVICES_DIRECTORY)
        .dropTable(Table_1.default.RECEIVED_SERVICES_OPERATIONS);
}
exports.down = down;
//# sourceMappingURL=20200226110048_received_services.js.map