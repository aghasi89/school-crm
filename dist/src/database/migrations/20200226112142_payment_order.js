"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.PAYMENT_ORDER, function (table) {
        table.increments('id');
        table.date('date');
        table.date('date_of_formulation').nullable();
        table.text('payer_name').nullable();
        table.text('payer_hvhh').nullable();
        table.text('provide_name').nullable();
        table.text('provide_hvhh').nullable();
        table
            .integer('provide_partners_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.PARTNERS)
            .onDelete('CASCADE').nullable();
        table
            .integer('d_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.BILLING_ACCOUNT_IN_BANKS)
            .onDelete('CASCADE').nullable();
        table.text('d_bank').nullable();
        table
            .integer('d_currency_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.CURRENCY)
            .onDelete('CASCADE').nullable();
        table
            .integer('c_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.BILLING_ACCOUNT)
            .onDelete('CASCADE').nullable();
        table.text('c_bank').nullable();
        table
            .integer('c_correspondent_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('c_currency_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table.text('amount_currency1').nullable();
        table.text('amount_currency2').nullable();
        table
            .integer('analitic_group_2_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_2)
            .onDelete('CASCADE');
        table
            .integer('analitic_group_1_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_1)
            .onDelete('CASCADE');
        table.text('typical_operation').nullable();
    })
        .createTable(Table_1.default.PAYMENT_ORDER_FUNCTIONS, function (table) {
        table.increments('id');
        table
            .integer('payment_order_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.PAYMENT_ORDER)
            .onDelete('CASCADE').nullable();
        table.integer('operations_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.OPERATIONS)
            .onDelete('CASCADE').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS)
        .dropTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS_PRODUCT)
        .dropTable(Table_1.default.MATERIAL_VALUE_MOVEMENTS_FUNCTIONS);
}
exports.down = down;
//# sourceMappingURL=20200226112142_payment_order.js.map