"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.CASH_REGISTER_ENTRY, function (table) {
        table.increments('id');
        table
            .integer('cash_register_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.CASH_REGISTER)
            .onDelete('CASCADE');
        table.date('date');
        table.string('hdm_n').nullable();
        table.string('document_number');
        table
            .integer('correspondent_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('entry_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('analitic_group_2_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_2)
            .onDelete('CASCADE');
        table
            .integer('analitic_group_1_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_1)
            .onDelete('CASCADE');
        table
            .integer('currency_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.CURRENCY)
            .onDelete('CASCADE').nullable();
        table.text('amount_currency1').nullable();
        table.text('amount_currency2').nullable();
        table
            .integer('partners_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.PARTNERS)
            .onDelete('CASCADE').nullable();
        table.text('received').nullable();
        table.text('basis').nullable();
        table.text('attached').nullable();
        table.text('optiona').nullable();
        table.text('typical_operation').nullable();
    })
        .createTable(Table_1.default.CASH_REGISTER_ENTRY_FUNCTIONS, function (table) {
        table.increments('id');
        table
            .integer('cash_register_entry_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.CASH_REGISTER_ENTRY)
            .onDelete('CASCADE').nullable();
        table.integer('operations_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.OPERATIONS)
            .onDelete('CASCADE').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.CASH_REGISTER_ENTRY)
        .dropTable(Table_1.default.CASH_REGISTER_ENTRY_FUNCTIONS);
}
exports.down = down;
//# sourceMappingURL=20200226120520_cash_register_entry.js.map