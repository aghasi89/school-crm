"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.PAYMENTS_BILLING_ACCOUN, function (table) {
        table.increments('id');
        table.date('date');
        table.integer('document_number').unsigned().unique();
        table.date('calculation_date');
        table.string('payment_type').nullable();
        table
            .integer('subdivision_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.SUBDIVISION)
            .onDelete('CASCADE').nullable();
        table
            .integer('analitic_group_2_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_2)
            .onDelete('CASCADE');
        table
            .integer('analitic_group_1_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.ANALITIC_GROUP_1)
            .onDelete('CASCADE');
        table
            .integer('settlement_account_id')
            .unsigned()
            .nullable()
            .references('id')
            .inTable(Table_1.default.BILLING_ACCOUNT_IN_BANKS)
            .onDelete('CASCADE').nullable();
        table.text('comment');
        table.date('date_of_formulation').nullable();
        table
            .integer('correspondent_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('exit_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
    })
        .createTable(Table_1.default.PAYMENTS_BILLING_ACCOUN_LIST, function (table) {
        table.increments('id');
        table
            .integer('payments_billing_accoun_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.PAYMENTS_BILLING_ACCOUN)
            .onDelete('CASCADE').nullable();
        table
            .integer('subdivision_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.SUBDIVISION)
            .onDelete('CASCADE').nullable();
        table
            .integer('employee_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.EMPLOYEE)
            .onDelete('CASCADE').nullable();
        table.decimal('pay_money');
        table.decimal('is_cash');
    })
        .createTable(Table_1.default.PAYMENTS_BILLING_ACCOUN_OPERATIONS, function (table) {
        table.increments('id');
        table
            .integer('payments_bi_ac_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.PAYMENTS_BILLING_ACCOUN)
            .onDelete('CASCADE').nullable();
        table.integer('operations_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.OPERATIONS)
            .onDelete('CASCADE').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.PAYMENTS_BILLING_ACCOUN)
        .dropTable(Table_1.default.PAYMENTS_BILLING_ACCOUN_LIST)
        .dropTable(Table_1.default.PAYMENTS_BILLING_ACCOUN_OPERATIONS);
}
exports.down = down;
//# sourceMappingURL=20200227111346_payments_billing_account.js.map