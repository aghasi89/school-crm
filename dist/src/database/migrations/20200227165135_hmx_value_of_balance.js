"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.HMX_VALUE_OF_BALANCE, function (table) {
        table.increments('id');
        table.string('code').unique();
        table.string('date');
        table.string('value_beginning_of_year');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.FORMULAS);
}
exports.down = down;
//# sourceMappingURL=20200227165135_hmx_value_of_balance.js.map