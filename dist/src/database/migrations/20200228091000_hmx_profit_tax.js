"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.HMX_PROFIT_TAX, function (table) {
        table.increments('id');
        table.string('code').unique();
        table.string('name');
        table.string('year_price');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.FORMULAS);
}
exports.down = down;
//# sourceMappingURL=20200228091000_hmx_profit_tax.js.map