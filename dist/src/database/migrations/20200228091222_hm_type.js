"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.HM_TYPE, function (table) {
        table.increments('id');
        table.string('code').unique();
        table.string('name');
        table
            .integer('initial_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table
            .integer('stale_account_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
            .onDelete('CASCADE').nullable();
        table.string('usefull_service_duration');
        table
            .integer('hmx_profit_tax_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.HMX_PROFIT_TAX)
            .onDelete('CASCADE').nullable();
        table
            .integer('parent_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.HM_TYPE)
            .onDelete('CASCADE').nullable();
    });
}
exports.up = up;
function down(knex) {
    return knex.schema.dropTable(Table_1.default.FORMULAS);
}
exports.down = down;
//# sourceMappingURL=20200228091222_hm_type.js.map