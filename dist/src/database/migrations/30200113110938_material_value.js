"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function up(knex) {
    return knex.schema
        .createTable(Table_1.default.BILLING_METHOD, function (table) {
        table.increments('id');
        table.string('name', 1000).notNullable();
        table.string('abbreviation').notNullable();
    })
        .createTable(Table_1.default.CLASSIFICATION, function (table) {
        table.increments('id');
        table.integer('code').unique();
        table.string('name', 1000).notNullable();
    })
        .createTable(Table_1.default.MATERIAL_VALUE_GROUP, function (table) {
        table.increments('id');
        table.integer('code').unique();
        table.string('name', 1000).notNullable();
        table
            .integer('material_value_group_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable(Table_1.default.MATERIAL_VALUE_GROUP)
            .onDelete('CASCADE');
    })
        .createTable(Table_1.default.MATERIAL_VALUE, function (table) {
        table.increments('id');
        table.string('name');
        table.integer('measurement_unit_id').references('measurement_unit.id').unsigned().index().onDelete('CASCADE');
        table.integer('classification_id').references('classification.id').unsigned().index().onDelete('CASCADE');
        table.integer('account_id').references('account_of_employee_calculations.id').unsigned().index().onDelete('CASCADE');
        table.decimal('wholesale_price').nullable();
        table.decimal('retailer_price').nullable();
        table
            .integer('currency_id')
            .unsigned()
            .references('id')
            .inTable(Table_1.default.CURRENCY)
            .onDelete('CASCADE').nullable();
        table.decimal('wholesale_price_currency').nullable();
        table.text('characteristic').nullable();
        table.string('bar_code').nullable();
        table.string('external_code').nullable();
        table.string('hcb_coefficient').nullable();
        table.integer('billing_method_id').references('billing_method.id').unsigned().index().onDelete('CASCADE');
        table.boolean('is_aah').nullable();
        table.integer('sales_revenue_account_id').references('account_of_employee_calculations.id').unsigned().index().onDelete('CASCADE');
        table.integer('retail_revenue_account_id').references('account_of_employee_calculations.id').unsigned().index().onDelete('CASCADE');
        table.integer('sales_expense_account_id').references('account_of_employee_calculations.id').unsigned().index().onDelete('CASCADE');
        table.integer('material_value_group_id').references('material_value_group.id').unsigned().index().onDelete('CASCADE');
    });
}
exports.up = up;
function down(knex) {
    return knex.schema
        .dropTable(Table_1.default.BILLING_METHOD)
        .dropTable(Table_1.default.CLASSIFICATION)
        .dropTable(Table_1.default.MATERIAL_VALUE);
}
exports.down = down;
//# sourceMappingURL=30200113110938_material_value.js.map