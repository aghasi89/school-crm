"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bcrypt = tslib_1.__importStar(require("../../utils/bcrypt"));
var Role_1 = tslib_1.__importDefault(require("../../resources/enums/Role"));
var Table_1 = tslib_1.__importDefault(require("../../resources/enums/Table"));
function seed(knex) {
    var _this = this;
    return knex(Table_1.default.USERS).then(function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
        var _a, _b, _c, _d, _e;
        return tslib_1.__generator(this, function (_f) {
            switch (_f.label) {
                case 0:
                    _b = (_a = Promise).all;
                    _d = (_c = knex(Table_1.default.USERS)).insert;
                    _e = {
                        role_id: Role_1.default.ADMIN,
                        name: 'Sagar Chamling',
                        email: 'sgr.raee@gmail.com'
                    };
                    return [4, bcrypt.hash('secret')];
                case 1: return [2, _b.apply(_a, [[
                            _d.apply(_c, [[
                                    (_e.password = _f.sent(),
                                        _e)
                                ]])
                        ]])];
            }
        });
    }); });
}
exports.seed = seed;
//# sourceMappingURL=user_table_seeder.js.map