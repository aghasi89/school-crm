interface AccountantPositionDetail {
    id?: number;
    name: string;
}
export default AccountantPositionDetail;
