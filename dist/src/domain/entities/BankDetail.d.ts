interface BankDetail {
    id?: number;
    code: string;
    name: string;
    swift: string;
}
export default BankDetail;
