interface BillingMethodDetail {
    id?: number;
    name: string;
    abbreviation: string;
}
export default BillingMethodDetail;
