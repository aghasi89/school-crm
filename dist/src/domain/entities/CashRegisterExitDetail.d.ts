import CashRegister from '../../models/CashRegister';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import Currency from '../../models/Currency';
import Partners from '../../models/Partners';
interface CashRegisterExitDetail {
    id: number;
    cashRegisterId: number;
    cashRegister?: CashRegister;
    date: Date;
    documentNumber: string;
    correspondentAccountId: number;
    correspondedAccount?: AccountOfEmployeeCalculations;
    exitAccountId: number;
    exitAccount?: AccountOfEmployeeCalculations;
    analiticGroup_1Id: number;
    analiticGroup1?: AnaliticGroup1;
    analiticGroup_2Id: number;
    analiticGroup2?: AnaliticGroup2;
    currencyId: number;
    currencies?: Currency;
    amountCurrency1: string;
    amountCurrency2: string;
    partnersId: number;
    partners?: Partners;
    received: string;
    npNshPassword: string;
    basis: string;
    appendix: string;
    otherInformation: string;
    optiona: string;
    typicalOperation: string;
}
interface CashRegisterExitFunctionsDetail {
    cashRegisterExitId?: number;
    operationsId: number;
}
export { CashRegisterExitDetail, CashRegisterExitFunctionsDetail };
