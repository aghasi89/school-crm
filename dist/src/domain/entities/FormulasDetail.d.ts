export interface FormulasDetail {
    id?: number;
    code: string;
    caption: string;
    expression: string;
}
