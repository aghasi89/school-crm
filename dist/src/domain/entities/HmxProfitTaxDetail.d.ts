interface HmxProfitTaxDetail {
    id?: number;
    code: string;
    name: string;
    yearPrice: string;
}
export default HmxProfitTaxDetail;
