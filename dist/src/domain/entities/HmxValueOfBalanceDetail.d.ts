interface HmxValueOfBalanceDetail {
    id?: number;
    code: string;
    name: Date;
    valueBeginningOfYear: string;
}
export default HmxValueOfBalanceDetail;
