import Partners from '../../models/Partners';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
interface OperationsDetail {
    id?: number;
    debitId: number;
    dPartnersId: number;
    dAnaliticGroup_2Id: number;
    dAnaliticGroup_1Id: number;
    creditId: number;
    cPartnersId: number;
    cAnaliticGroup_2Id: number;
    cAnaliticGroup_1Id: number;
    money: number;
    comment: string;
    partners: Partners;
    analiticGroups1: AnaliticGroup1;
    analiticGroups2: AnaliticGroup2;
}
export default OperationsDetail;
