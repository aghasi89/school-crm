import Subdivision from '../../models/Subdivision';
export interface PositionDetail {
    id?: number;
    name: string;
    subdivisionId: number;
    subdivision?: Subdivision;
}
