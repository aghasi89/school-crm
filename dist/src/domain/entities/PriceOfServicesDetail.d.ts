import Services from '../../models/Services';
export interface PriceOfServicesDetail {
    id?: number;
    startDate: string;
    endDate: string;
    servicesId: number;
    services?: Services;
    type: string;
}
