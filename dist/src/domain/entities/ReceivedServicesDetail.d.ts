import Operations from '../../models/Operations';
import Currency from '../../models/Currency';
import Partners from '../../models/Partners';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
interface ReceivedServicesDetail {
    id?: number;
    currencyId: number;
    currencies?: Currency;
    currencyExchangeRate1: string;
    currencyExchangeRate2: string;
    previousDayExchangeRate: boolean;
    partnersId: number;
    partners?: Partners;
    providerAccountId: string;
    advancePaymentAccountId: string;
    analiticGroup_1Id: number;
    analiticGroup_2Id: number;
    analiticGroup1?: AnaliticGroup1;
    analiticGroup2?: AnaliticGroup2;
    outputMethod: string;
    purchaseDocumentNumber: number;
    purchaseDocumentDate: Date;
    comment: string;
    purchaseTypeOfService: string;
    calculationTypeAah: string;
    includeAahInExpense: boolean;
    formOfReflectionAah: string;
    typicalOperation: string;
}
interface ReceivedServicesDirectoryDetail {
    receivedServiceId?: number;
    serviceId: number;
    point: number;
    count: number;
    price: number;
    money: number;
    aah: boolean;
    account: string;
}
interface ReceivedServicesOperationsDetail {
    receivedServiceId?: number;
    operationsId: number;
    operations: Operations;
}
export { ReceivedServicesDetail, ReceivedServicesDirectoryDetail, ReceivedServicesOperationsDetail };
