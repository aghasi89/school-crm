interface TypeOfIncomeDetail {
    id?: number;
    name: string;
}
export default TypeOfIncomeDetail;
