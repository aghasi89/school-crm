interface TypeOfVacationDetail {
    id?: number;
    name: string;
}
export default TypeOfVacationDetail;
