import Warehouse from '../../models/Warehouse';
import Partners from '../../models/Partners';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
interface WarehouseEntryOrderDetail {
    id?: number;
    date: Date;
    warehouseId: number;
    warehouse?: Warehouse;
    partnersId: number;
    partners?: Partners;
    partnersAccountId: number;
    prepaidAccountId: number;
    analiticGroup_2Id: number;
    analiticGroup2?: AnaliticGroup2;
    analiticGroup_1Id: number;
    analiticGroup1?: AnaliticGroup1;
    documentN: string;
    documentDate: Date;
    comment: string;
    powerOfAttorney: string;
    mediator: string;
    container: string;
    accountant: string;
    allow: string;
    accept: string;
    documentOfTransport: string;
    documentOfTransportDate: Date;
    typeOfAcquisitionOfNa: string;
    calculationStyleOfAah: string;
    includeAahInCost: boolean;
}
interface WarehouseEntryOrderProductDetail {
    id?: number;
    warehouseEntryOrderId: number;
    materialValueId: number;
    point: number;
    count: number;
    price: number;
    money: number;
    isAah: boolean;
    accountsId: number;
    classificationId: number;
}
interface WarehouseEntryOrderFunctionsDetail {
    id?: number;
    warehouseEntryOrderId: number;
    debitId: number;
    dPartnersId: number;
    dAnaliticGroup_2Id: number;
    dAnaliticGroup_1Id: number;
    creditId: number;
    cPartnersIdisAah: number;
    cAnaliticGroup_2Id: number;
    cAnaliticGroup_1Id: number;
    money: number;
    comment: string;
}
export { WarehouseEntryOrderDetail, WarehouseEntryOrderProductDetail, WarehouseEntryOrderFunctionsDetail };
