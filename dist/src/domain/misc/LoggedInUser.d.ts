import JWTPayload from './JWTPayload';
interface LoggedInUser extends JWTPayload {
    sessionId: number;
}
export default LoggedInUser;
