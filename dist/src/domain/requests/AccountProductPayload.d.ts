interface AahAccountTypePayload {
    name: string;
}
interface AccountProductPayload {
    date: Date;
    documentNumber: string;
    partnersId: number;
    linesCode: string;
    contract: string;
    contractDate: Date;
    subsectionId: number;
    analiticGroup_2Id: number;
    analiticGroup_1Id: number;
    seria: string;
    number: string;
    dateWriteOff: string;
    comment: string;
    aahAccountTypeId: number;
    warehouseId: number;
    address: string;
    aah: string;
}
interface AccountProductsOfProductsPayload {
    type: string;
    warehouseId: number;
    code: number;
    name: number;
    point: number;
    count: number;
    price: number;
    money: number;
    expenseAccountId: string;
    incomeAccountId: string;
    batch: number;
    isAah: number;
}
interface AccountProductsFunctionsPayload {
    accountProductsId: number;
    operationsId: number;
}
export { AahAccountTypePayload, AccountProductPayload, AccountProductsOfProductsPayload, AccountProductsFunctionsPayload };
