interface BankBranchePayload {
    brancheCode: string;
    brancheAddress: string;
    bankId: number;
}
export default BankBranchePayload;
