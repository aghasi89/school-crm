interface BankPayload {
    code: string;
    name: string;
    swift: string;
}
export default BankPayload;
