interface BillingAccountInBanksPayload {
    billingAccount: string;
    name: string;
    currencyId: number;
    accountId: number;
    isMain: boolean;
    numberOfDocument: string;
    eServices: string;
}
export default BillingAccountInBanksPayload;
