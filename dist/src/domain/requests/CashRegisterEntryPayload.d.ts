interface CashRegisterEntryPayload {
    cashRegisterId: number;
    date: Date;
    hdmN: string;
    documentNumber: string;
    correspondentAccountId: number;
    entryAccountId: number;
    analiticGroup_1Id: number;
    analiticGroup_2Id: number;
    currencyId: number;
    amountCurrency1: string;
    amountCurrency2: string;
    partnersId: number;
    received: string;
    basis: string;
    attached: string;
    optiona: string;
    typicalOperation: string;
}
interface CashRegisterEntryFunctionsPayload {
    cashRegisterEntryId?: number;
    operationsId: number;
}
export { CashRegisterEntryPayload, CashRegisterEntryFunctionsPayload };
