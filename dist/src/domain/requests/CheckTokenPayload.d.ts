interface CheckTokenPayload {
    token: string;
}
export default CheckTokenPayload;
