interface HmxProfitTaxPayload {
    code: string;
    name: string;
    yearPrice: string;
}
export default HmxProfitTaxPayload;
