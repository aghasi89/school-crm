interface PaginetPayload {
    limit: number;
    offset: number;
}
export default PaginetPayload;
