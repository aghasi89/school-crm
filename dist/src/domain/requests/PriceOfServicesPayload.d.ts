interface PriceOfServicesPayload {
    startDate: string;
    endDate: string;
    serviceId: number;
    type: string;
}
export default PriceOfServicesPayload;
