interface ResetPayload {
    email: string;
}
export default ResetPayload;
