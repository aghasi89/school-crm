interface TypeOfActionsPayload {
    name: string;
    code: number;
}
export default TypeOfActionsPayload;
