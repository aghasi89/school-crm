interface WarehouseEntryOrderPayload {
    date: Date;
    warehouse_id: number;
    partnersId: number;
    partnersAccountId: number;
    prepaidAccountId: number;
    analiticGroup_2Id: number;
    analiticGroup_1Id: number;
    documentN: string;
    documentDate: Date;
    comment: string;
    powerOfAttorney: string;
    mediator: string;
    container: string;
    accountant: string;
    allow: string;
    accept: string;
    documentOfTransport: string;
    documentOfTransportDate: Date;
    typeOfAcquisitionOfNa: string;
    calculationStyleOfAah: string;
    includeAahInCost: boolean;
    warehouseEntryOrderProduct?: any;
    warehouseEntryOrderFunctions?: any;
}
interface WarehouseEntryOrderProductPayload {
    warehouseEntryOrderId: number;
    materialValueId: number;
    point: number;
    count: number;
    price: number;
    money: number;
    isAah: boolean;
    accountsId: number;
    classificationId: number;
}
interface WarehouseEntryOrderFunctionsPayload {
    warehouseEntryOrderId?: number;
    debitId: number;
    dPartnersId: number;
    dAnaliticGroup_2Id: number;
    dAnaliticGroup_1Id: number;
    creditId: number;
    cPartnersIdisAah: number;
    cAnaliticGroup_2Id: number;
    cAnaliticGroup_1Id: number;
    money: number;
    comment: string;
}
export { WarehouseEntryOrderPayload, WarehouseEntryOrderProductPayload, WarehouseEntryOrderFunctionsPayload };
