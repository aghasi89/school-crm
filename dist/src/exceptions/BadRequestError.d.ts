import Error from './Error';
declare class BadRequestError extends Error {
    message: string;
    constructor(message: string);
}
export default BadRequestError;
