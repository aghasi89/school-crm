"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var Error_1 = tslib_1.__importDefault(require("./Error"));
var BadRequestError = (function (_super) {
    tslib_1.__extends(BadRequestError, _super);
    function BadRequestError(message) {
        var _this = _super.call(this, message, HttpStatus.BAD_REQUEST) || this;
        _this.message = message;
        return _this;
    }
    return BadRequestError;
}(Error_1.default));
exports.default = BadRequestError;
//# sourceMappingURL=BadRequestError.js.map