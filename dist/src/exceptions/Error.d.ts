declare class Error {
    message: string;
    isCustom: boolean;
    statusCode: number;
    constructor(message: string, statusCode: number);
}
export default Error;
