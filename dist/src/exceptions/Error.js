"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Error = (function () {
    function Error(message, statusCode) {
        this.isCustom = true;
        this.message = message;
        this.statusCode = statusCode;
    }
    return Error;
}());
exports.default = Error;
//# sourceMappingURL=Error.js.map