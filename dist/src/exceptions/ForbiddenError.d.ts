import Error from './Error';
declare class ForbiddenError extends Error {
    message: string;
    constructor(message: string);
}
export default ForbiddenError;
