"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var Error_1 = tslib_1.__importDefault(require("./Error"));
var ForbiddenError = (function (_super) {
    tslib_1.__extends(ForbiddenError, _super);
    function ForbiddenError(message) {
        var _this = _super.call(this, message, HttpStatus.FORBIDDEN) || this;
        _this.message = message;
        return _this;
    }
    return ForbiddenError;
}(Error_1.default));
exports.default = ForbiddenError;
//# sourceMappingURL=ForbiddenError.js.map