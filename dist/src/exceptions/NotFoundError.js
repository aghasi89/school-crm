"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var Error_1 = tslib_1.__importDefault(require("./Error"));
var NotFoundError = (function (_super) {
    tslib_1.__extends(NotFoundError, _super);
    function NotFoundError(message) {
        var _this = _super.call(this, message, HttpStatus.NOT_FOUND) || this;
        _this.message = message;
        return _this;
    }
    return NotFoundError;
}(Error_1.default));
exports.default = NotFoundError;
//# sourceMappingURL=NotFoundError.js.map