import Error from './Error';
declare class UnauthorizedError extends Error {
    message: string;
    constructor(message: string);
}
export default UnauthorizedError;
