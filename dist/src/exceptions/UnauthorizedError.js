"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var Error_1 = tslib_1.__importDefault(require("./Error"));
var UnauthorizedError = (function (_super) {
    tslib_1.__extends(UnauthorizedError, _super);
    function UnauthorizedError(message) {
        var _this = _super.call(this, message, HttpStatus.UNAUTHORIZED) || this;
        _this.message = message;
        return _this;
    }
    return UnauthorizedError;
}(Error_1.default));
exports.default = UnauthorizedError;
//# sourceMappingURL=UnauthorizedError.js.map