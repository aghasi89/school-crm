import Error from './Error';
declare class UniqueUserError extends Error {
    message: string;
    constructor(message: string);
}
export default UniqueUserError;
