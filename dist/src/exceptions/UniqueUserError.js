"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var Error_1 = tslib_1.__importDefault(require("./Error"));
var UniqueUserError = (function (_super) {
    tslib_1.__extends(UniqueUserError, _super);
    function UniqueUserError(message) {
        var _this = _super.call(this, message, HttpStatus.BAD_REQUEST) || this;
        _this.message = message;
        return _this;
    }
    return UniqueUserError;
}(Error_1.default));
exports.default = UniqueUserError;
//# sourceMappingURL=UniqueUserError.js.map