"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var app_1 = tslib_1.__importDefault(require("./app"));
var logger_1 = tslib_1.__importDefault(require("./utils/logger"));
var config_1 = tslib_1.__importDefault(require("./config/config"));
var nodeErrorHandler_1 = tslib_1.__importDefault(require("./middlewares/nodeErrorHandler"));
var port = config_1.default.port, host = config_1.default.host;
app_1.default
    .listen(+port, host, function () {
    logger_1.default.log('info', "Server started at http://" + host + ":" + port);
})
    .on('error', nodeErrorHandler_1.default);
//# sourceMappingURL=index.js.map