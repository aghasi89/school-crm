import { Request, Response, NextFunction } from 'express';
declare function authenticate(req: Request, res: Response, next: NextFunction): Promise<void>;
declare function authenticateAdmin(req: Request, res: Response, next: NextFunction): Promise<void>;
export { authenticate, authenticateAdmin };
