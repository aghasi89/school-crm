import { Request, Response, NextFunction } from 'express';
export default function genericErrorHandler(err: any, _: Request, res: Response, __: NextFunction): void;
