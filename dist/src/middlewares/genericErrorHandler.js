"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
function genericErrorHandler(err, _, res, __) {
    var error = buildError(err);
    logger_1.default.log('error', 'Error: %s', err.stack || err.message);
    res.status(error.code).json(error);
}
exports.default = genericErrorHandler;
function buildError(err) {
    if (err.isJoi) {
        return {
            code: HttpStatus.BAD_REQUEST,
            message: HttpStatus.getStatusText(HttpStatus.BAD_REQUEST),
            data: err.details &&
                err.details.map(function (error) { return ({
                    param: error.path.join('.'),
                    message: error.message
                }); })
        };
    }
    if (err.isBoom) {
        return {
            code: err.output.statusCode,
            message: err.output.payload.message || err.output.payload.error
        };
    }
    if (err.isCustom) {
        return {
            code: err.statusCode,
            message: err.message
        };
    }
    if (err.sqlMessage) {
        return {
            code: HttpStatus.BAD_REQUEST,
            message: err.sqlMessage
        };
    }
    return {
        code: HttpStatus.INTERNAL_SERVER_ERROR,
        message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR)
    };
}
//# sourceMappingURL=genericErrorHandler.js.map