/// <reference types="node" />
export default function nodeErrorHandler(err: NodeJS.ErrnoException): void;
