"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var config_1 = tslib_1.__importDefault(require("../config/config"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var errors = config_1.default.errors;
function nodeErrorHandler(err) {
    switch (err.code) {
        case 'EACCES':
            logger_1.default.log('error', errors.portRequirePrivilege);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            logger_1.default.log('error', errors.portInUse);
            process.exit(1);
            break;
        default:
            throw err;
    }
}
exports.default = nodeErrorHandler;
//# sourceMappingURL=nodeErrorHandler.js.map