import { Request, Response, NextFunction } from 'express';
export default function notFoundError(_: Request, res: Response, __: NextFunction): void;
