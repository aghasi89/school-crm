"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var HttpStatus = tslib_1.__importStar(require("http-status-codes"));
function notFoundError(_, res, __) {
    res.status(HttpStatus.NOT_FOUND).json({
        error: {
            code: HttpStatus.NOT_FOUND,
            message: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
        }
    });
}
exports.default = notFoundError;
//# sourceMappingURL=notFoundHandler.js.map