import Joi from 'joi';
import { Request, Response, NextFunction } from 'express';
export declare function schema(params: Joi.Schema): (req: Request<import("express-serve-static-core").ParamsDictionary>, _: Response, next: NextFunction) => Promise<void>;
export declare function params(params: Joi.Schema): (req: Request<import("express-serve-static-core").ParamsDictionary>, _: Response, next: NextFunction) => Promise<void>;
