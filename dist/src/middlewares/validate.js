"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var validate_1 = tslib_1.__importDefault(require("../utils/validate"));
function schema(params) {
    var _this = this;
    return function (req, _, next) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
        var err_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    logger_1.default.log('info', 'Validating schema');
                    return [4, validate_1.default(req.body, params)];
                case 1:
                    _a.sent();
                    next();
                    return [3, 3];
                case 2:
                    err_1 = _a.sent();
                    next(err_1);
                    return [3, 3];
                case 3: return [2];
            }
        });
    }); };
}
exports.schema = schema;
function params(params) {
    var _this = this;
    return function (req, _, next) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
        var err_2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    logger_1.default.log('info', 'Validating schema');
                    return [4, validate_1.default(req.params, params)];
                case 1:
                    _a.sent();
                    next();
                    return [3, 3];
                case 2:
                    err_2 = _a.sent();
                    next(err_2);
                    return [3, 3];
                case 3: return [2];
            }
        });
    }); };
}
exports.params = params;
//# sourceMappingURL=validate.js.map