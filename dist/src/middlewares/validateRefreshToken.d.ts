import { Request, Response, NextFunction } from 'express';
declare function validateRefreshToken(req: Request, res: Response, next: NextFunction): Promise<void>;
export default validateRefreshToken;
