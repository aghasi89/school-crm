"use strict";
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var jwt = tslib_1.__importStar(require("../utils/jwt"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var ErrorType_1 = tslib_1.__importDefault(require("./../resources/enums/ErrorType"));
var BadRequestError_1 = tslib_1.__importDefault(require("../exceptions/BadRequestError"));
var UnauthorizedError_1 = tslib_1.__importDefault(require("../exceptions/UnauthorizedError"));
var errors = config_1.default.errors;
var tokenErrorMessageMap = (_a = {},
    _a[ErrorType_1.default.INVALID] = errors.invalidToken,
    _a[ErrorType_1.default.EXPIRED] = errors.refreshTokenExpired,
    _a);
function validateRefreshToken(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var response, tokenErrorMessage;
        return tslib_1.__generator(this, function (_a) {
            try {
                res.locals.refreshToken = String(req.headers.authorization).replace('Bearer ', '');
                if (!req.headers.authorization || !res.locals.refreshToken) {
                    throw new BadRequestError_1.default(errors.noToken);
                }
                logger_1.default.log('info', 'JWT: Verifying token - %s', res.locals.refreshToken);
                response = jwt.verifyRefreshToken(res.locals.refreshToken);
                res.locals.jwtPayload = response.data;
                logger_1.default.log('debug', 'JWT: Authentication verified -', res.locals.jwtPayload);
                next();
            }
            catch (err) {
                tokenErrorMessage = tokenErrorMessageMap[err.name];
                logger_1.default.log('error', 'JWT: Authentication failed - %s', err.message);
                if (tokenErrorMessage) {
                    logger_1.default.log('error', 'JWT: Token error - %s', tokenErrorMessage);
                    next(new UnauthorizedError_1.default(tokenErrorMessage));
                }
                else {
                    next(err);
                }
            }
            return [2];
        });
    });
}
exports.default = validateRefreshToken;
//# sourceMappingURL=validateRefreshToken.js.map