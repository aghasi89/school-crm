import bookshelf from '../config/bookshelf';
declare class AccountOfEmployeeCalculations extends bookshelf.Model<AccountOfEmployeeCalculations> {
    get requireFetch(): boolean;
    get tableName(): string;
    currencies(): any;
    calculationsType(): any;
    acumulatedAccount(): any;
}
export default AccountOfEmployeeCalculations;
