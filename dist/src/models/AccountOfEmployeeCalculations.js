"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var CalculationsType_1 = tslib_1.__importDefault(require("./CalculationsType"));
var Currency_1 = tslib_1.__importDefault(require("./Currency"));
var AccountOfEmployeeCalculations = (function (_super) {
    tslib_1.__extends(AccountOfEmployeeCalculations, _super);
    function AccountOfEmployeeCalculations() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AccountOfEmployeeCalculations.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccountOfEmployeeCalculations.prototype, "tableName", {
        get: function () {
            return Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS;
        },
        enumerable: true,
        configurable: true
    });
    AccountOfEmployeeCalculations.prototype.currencies = function () {
        return this.belongsToMany(Currency_1.default, 'calculations_currency', 'employee_calculations_id');
    };
    AccountOfEmployeeCalculations.prototype.calculationsType = function () {
        return this.hasOne(CalculationsType_1.default, 'id', 'calculations_type_id');
    };
    AccountOfEmployeeCalculations.prototype.acumulatedAccount = function () {
        return this.hasOne(AccountOfEmployeeCalculations, 'id', 'acumulated_account_id');
    };
    return AccountOfEmployeeCalculations;
}(bookshelf_1.default.Model));
exports.default = AccountOfEmployeeCalculations;
//# sourceMappingURL=AccountOfEmployeeCalculations.js.map