import bookshelf from '../config/bookshelf';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Subsection from './Subsection';
import Warehouse from './Warehouse';
import Operations from './Operations';
declare class AccountProductsFunctions extends bookshelf.Model<AccountProductsFunctions> {
    get requireFetch(): boolean;
    get tableName(): string;
    accountProducts(): AccountProduct;
    operations(): Operations;
}
declare class AccountProductsOfProducts extends bookshelf.Model<AccountProductsOfProducts> {
    get requireFetch(): boolean;
    get tableName(): string;
    expenseAccount(): AccountOfEmployeeCalculations;
    incomeAccount(): AccountOfEmployeeCalculations;
}
declare class AccountProduct extends bookshelf.Model<AccountProduct> {
    get requireFetch(): boolean;
    get tableName(): string;
    partners(): Partners;
    subsections(): Subsection;
    analiticGroup1(): AnaliticGroup1;
    analiticGroup2(): AnaliticGroup2;
    aahAccountTypes(): AahAccountType;
    warehouse(): Warehouse;
}
declare class AahAccountType extends bookshelf.Model<AahAccountType> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export { AahAccountType, AccountProduct, AccountProductsOfProducts, AccountProductsFunctions };
