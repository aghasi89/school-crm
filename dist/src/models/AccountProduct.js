"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var Partners_1 = tslib_1.__importDefault(require("./Partners"));
var AnaliticGroup1_1 = tslib_1.__importDefault(require("./AnaliticGroup1"));
var AnaliticGroup2_1 = tslib_1.__importDefault(require("./AnaliticGroup2"));
var Subsection_1 = tslib_1.__importDefault(require("./Subsection"));
var Warehouse_1 = tslib_1.__importDefault(require("./Warehouse"));
var Operations_1 = tslib_1.__importDefault(require("./Operations"));
var AccountProductsFunctions = (function (_super) {
    tslib_1.__extends(AccountProductsFunctions, _super);
    function AccountProductsFunctions() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AccountProductsFunctions.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccountProductsFunctions.prototype, "tableName", {
        get: function () {
            return Table_1.default.ACCOUNT_PRODUCTS_OF_PRODUCTS;
        },
        enumerable: true,
        configurable: true
    });
    AccountProductsFunctions.prototype.accountProducts = function () {
        return this.belongsTo(AccountProduct, 'account_products_id');
    };
    AccountProductsFunctions.prototype.operations = function () {
        return this.belongsTo(Operations_1.default, 'operations_id');
    };
    return AccountProductsFunctions;
}(bookshelf_1.default.Model));
exports.AccountProductsFunctions = AccountProductsFunctions;
var AccountProductsOfProducts = (function (_super) {
    tslib_1.__extends(AccountProductsOfProducts, _super);
    function AccountProductsOfProducts() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AccountProductsOfProducts.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccountProductsOfProducts.prototype, "tableName", {
        get: function () {
            return Table_1.default.ACCOUNT_PRODUCTS_OF_PRODUCTS;
        },
        enumerable: true,
        configurable: true
    });
    AccountProductsOfProducts.prototype.expenseAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'expense_account');
    };
    AccountProductsOfProducts.prototype.incomeAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'income_account');
    };
    return AccountProductsOfProducts;
}(bookshelf_1.default.Model));
exports.AccountProductsOfProducts = AccountProductsOfProducts;
var AccountProduct = (function (_super) {
    tslib_1.__extends(AccountProduct, _super);
    function AccountProduct() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AccountProduct.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccountProduct.prototype, "tableName", {
        get: function () {
            return Table_1.default.ACCOUNT_PRODUCTS;
        },
        enumerable: true,
        configurable: true
    });
    AccountProduct.prototype.partners = function () {
        return this.belongsTo(Partners_1.default, 'partners_id');
    };
    AccountProduct.prototype.subsections = function () {
        return this.belongsTo(Subsection_1.default, 'subsection_id');
    };
    AccountProduct.prototype.analiticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'analitic_group_1_id');
    };
    AccountProduct.prototype.analiticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'analitic_group_2_id');
    };
    AccountProduct.prototype.aahAccountTypes = function () {
        return this.belongsTo(AahAccountType, 'aah_account_type_id');
    };
    AccountProduct.prototype.warehouse = function () {
        return this.belongsTo(Warehouse_1.default, 'warehouse_id');
    };
    return AccountProduct;
}(bookshelf_1.default.Model));
exports.AccountProduct = AccountProduct;
var AahAccountType = (function (_super) {
    tslib_1.__extends(AahAccountType, _super);
    function AahAccountType() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AahAccountType.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AahAccountType.prototype, "tableName", {
        get: function () {
            return Table_1.default.AAH_ACCOUNT_TYPE;
        },
        enumerable: true,
        configurable: true
    });
    return AahAccountType;
}(bookshelf_1.default.Model));
exports.AahAccountType = AahAccountType;
//# sourceMappingURL=AccountProduct.js.map