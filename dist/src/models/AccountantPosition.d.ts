import bookshelf from '../config/bookshelf';
declare class AccountantPosition extends bookshelf.Model<AccountantPosition> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default AccountantPosition;
