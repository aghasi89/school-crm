"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AccountantPosition = (function (_super) {
    tslib_1.__extends(AccountantPosition, _super);
    function AccountantPosition() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AccountantPosition.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccountantPosition.prototype, "tableName", {
        get: function () {
            return Table_1.default.ACCOUNTANT_POSITION;
        },
        enumerable: true,
        configurable: true
    });
    return AccountantPosition;
}(bookshelf_1.default.Model));
exports.default = AccountantPosition;
//# sourceMappingURL=AccountantPosition.js.map