import bookshelf from '../config/bookshelf';
import ExpenseAccount from './ExpenseAccount';
import Tabel from './Tabel';
import TypeOfIncome from './TypeOfIncome';
import TypeOfVacation from './TypeOfVacation';
declare class AdditionMethod extends bookshelf.Model<AdditionMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
    expenseAccount(): ExpenseAccount;
    tabel(): Tabel;
    typeOfIncome(): TypeOfIncome;
    typeOfVacation(): TypeOfVacation;
}
export default AdditionMethod;
