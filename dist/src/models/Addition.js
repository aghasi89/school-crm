"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var ExpenseAccount_1 = tslib_1.__importDefault(require("./ExpenseAccount"));
var Tabel_1 = tslib_1.__importDefault(require("./Tabel"));
var TypeOfIncome_1 = tslib_1.__importDefault(require("./TypeOfIncome"));
var TypeOfVacation_1 = tslib_1.__importDefault(require("./TypeOfVacation"));
var AdditionMethod = (function (_super) {
    tslib_1.__extends(AdditionMethod, _super);
    function AdditionMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AdditionMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AdditionMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.ADDITION;
        },
        enumerable: true,
        configurable: true
    });
    AdditionMethod.prototype.expenseAccount = function () {
        return this.belongsTo(ExpenseAccount_1.default, 'expense_account_id');
    };
    AdditionMethod.prototype.tabel = function () {
        return this.belongsTo(Tabel_1.default, 'tabel_id');
    };
    AdditionMethod.prototype.typeOfIncome = function () {
        return this.belongsTo(TypeOfIncome_1.default, 'type_of_income_id');
    };
    AdditionMethod.prototype.typeOfVacation = function () {
        return this.belongsTo(TypeOfVacation_1.default, 'type_of_vacation_id');
    };
    return AdditionMethod;
}(bookshelf_1.default.Model));
exports.default = AdditionMethod;
//# sourceMappingURL=Addition.js.map