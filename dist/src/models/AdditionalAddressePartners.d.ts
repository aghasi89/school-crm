import bookshelf from "../config/bookshelf";
declare class AdditionalAddressePartners extends bookshelf.Model<AdditionalAddressePartners> {
    get requiredFetch(): boolean;
    get tableName(): string;
}
export default AdditionalAddressePartners;
