"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AdditionalAddressePartners = (function (_super) {
    tslib_1.__extends(AdditionalAddressePartners, _super);
    function AdditionalAddressePartners() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AdditionalAddressePartners.prototype, "requiredFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AdditionalAddressePartners.prototype, "tableName", {
        get: function () {
            return Table_1.default.ADDITIONAL_ADDRESSE_PARTNERS;
        },
        enumerable: true,
        configurable: true
    });
    return AdditionalAddressePartners;
}(bookshelf_1.default.Model));
exports.default = AdditionalAddressePartners;
//# sourceMappingURL=AdditionalAddressePartners.js.map