import bookshelf from '../config/bookshelf';
declare class Addresses extends bookshelf.Model<Addresses> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default Addresses;
