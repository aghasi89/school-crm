"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Addresses = (function (_super) {
    tslib_1.__extends(Addresses, _super);
    function Addresses() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Addresses.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Addresses.prototype, "tableName", {
        get: function () {
            return Table_1.default.ADDRESSES;
        },
        enumerable: true,
        configurable: true
    });
    return Addresses;
}(bookshelf_1.default.Model));
exports.default = Addresses;
//# sourceMappingURL=Address.js.map