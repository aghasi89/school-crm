import bookshelf from '../config/bookshelf';
declare class AnaliticGroup1 extends bookshelf.Model<AnaliticGroup1> {
    get requireFetch(): boolean;
    get tableName(): string;
    parent(): any;
}
export default AnaliticGroup1;
