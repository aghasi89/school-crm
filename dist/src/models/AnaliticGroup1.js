"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AnaliticGroup1 = (function (_super) {
    tslib_1.__extends(AnaliticGroup1, _super);
    function AnaliticGroup1() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AnaliticGroup1.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnaliticGroup1.prototype, "tableName", {
        get: function () {
            return Table_1.default.ANALITIC_GROUP_1;
        },
        enumerable: true,
        configurable: true
    });
    AnaliticGroup1.prototype.parent = function () {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id');
    };
    return AnaliticGroup1;
}(bookshelf_1.default.Model));
exports.default = AnaliticGroup1;
//# sourceMappingURL=AnaliticGroup1.js.map