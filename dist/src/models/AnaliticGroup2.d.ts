import bookshelf from '../config/bookshelf';
declare class AnaliticGroup2 extends bookshelf.Model<AnaliticGroup2> {
    get requireFetch(): boolean;
    get tableName(): string;
    parent(): any;
}
export default AnaliticGroup2;
