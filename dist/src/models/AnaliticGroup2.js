"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AnaliticGroup2 = (function (_super) {
    tslib_1.__extends(AnaliticGroup2, _super);
    function AnaliticGroup2() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AnaliticGroup2.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AnaliticGroup2.prototype, "tableName", {
        get: function () {
            return Table_1.default.ANALITIC_GROUP_2;
        },
        enumerable: true,
        configurable: true
    });
    AnaliticGroup2.prototype.parent = function () {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id');
    };
    return AnaliticGroup2;
}(bookshelf_1.default.Model));
exports.default = AnaliticGroup2;
//# sourceMappingURL=AnaliticGroup2.js.map