import bookshelf from '../config/bookshelf';
declare class Bank extends bookshelf.Model<Bank> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default Bank;
