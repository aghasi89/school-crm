"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Bank = (function (_super) {
    tslib_1.__extends(Bank, _super);
    function Bank() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Bank.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Bank.prototype, "tableName", {
        get: function () {
            return Table_1.default.BANK;
        },
        enumerable: true,
        configurable: true
    });
    return Bank;
}(bookshelf_1.default.Model));
exports.default = Bank;
//# sourceMappingURL=Bank.js.map