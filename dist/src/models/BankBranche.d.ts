import bookshelf from '../config/bookshelf';
declare class BankBranche extends bookshelf.Model<BankBranche> {
    get requireFetch(): boolean;
    get tableName(): string;
    get hasTimestamps(): boolean;
}
export default BankBranche;
