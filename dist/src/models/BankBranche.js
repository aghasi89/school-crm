"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var BankBranche = (function (_super) {
    tslib_1.__extends(BankBranche, _super);
    function BankBranche() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(BankBranche.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BankBranche.prototype, "tableName", {
        get: function () {
            return Table_1.default.BANK_BRANCHE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BankBranche.prototype, "hasTimestamps", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return BankBranche;
}(bookshelf_1.default.Model));
exports.default = BankBranche;
//# sourceMappingURL=BankBranche.js.map