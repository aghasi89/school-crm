import bookshelf from "../config/bookshelf";
import Partners from './Partners';
declare class BillingAccount extends bookshelf.Model<BillingAccount> {
    get requiredFetch(): boolean;
    get tableName(): string;
    billingAccount(): Partners;
}
export default BillingAccount;
