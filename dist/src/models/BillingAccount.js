"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Partners_1 = tslib_1.__importDefault(require("./Partners"));
var BillingAccount = (function (_super) {
    tslib_1.__extends(BillingAccount, _super);
    function BillingAccount() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(BillingAccount.prototype, "requiredFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BillingAccount.prototype, "tableName", {
        get: function () {
            return Table_1.default.BILLING_ACCOUNT;
        },
        enumerable: true,
        configurable: true
    });
    BillingAccount.prototype.billingAccount = function () {
        return this.belongsTo(Partners_1.default, 'partner_id');
    };
    return BillingAccount;
}(bookshelf_1.default.Model));
exports.default = BillingAccount;
//# sourceMappingURL=BillingAccount.js.map