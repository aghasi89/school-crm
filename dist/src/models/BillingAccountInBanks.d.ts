import bookshelf from '../config/bookshelf';
import Currency from './Currency';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';
declare class BillingAccountInBanksMethod extends bookshelf.Model<BillingAccountInBanksMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
    currencies(): Currency;
    accounts(): AccountOfEmployeeCalculations;
}
export default BillingAccountInBanksMethod;
