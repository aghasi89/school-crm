"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Currency_1 = tslib_1.__importDefault(require("./Currency"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var BillingAccountInBanksMethod = (function (_super) {
    tslib_1.__extends(BillingAccountInBanksMethod, _super);
    function BillingAccountInBanksMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(BillingAccountInBanksMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BillingAccountInBanksMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.BILLING_ACCOUNT_IN_BANKS;
        },
        enumerable: true,
        configurable: true
    });
    BillingAccountInBanksMethod.prototype.currencies = function () {
        return this.belongsTo(Currency_1.default, 'currency_id');
    };
    BillingAccountInBanksMethod.prototype.accounts = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'account_id', 'account');
    };
    return BillingAccountInBanksMethod;
}(bookshelf_1.default.Model));
exports.default = BillingAccountInBanksMethod;
//# sourceMappingURL=BillingAccountInBanks.js.map