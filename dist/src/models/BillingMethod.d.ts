import bookshelf from '../config/bookshelf';
declare class BillingMethod extends bookshelf.Model<BillingMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default BillingMethod;
