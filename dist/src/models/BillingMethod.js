"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var BillingMethod = (function (_super) {
    tslib_1.__extends(BillingMethod, _super);
    function BillingMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(BillingMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BillingMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.BILLING_METHOD;
        },
        enumerable: true,
        configurable: true
    });
    return BillingMethod;
}(bookshelf_1.default.Model));
exports.default = BillingMethod;
//# sourceMappingURL=BillingMethod.js.map