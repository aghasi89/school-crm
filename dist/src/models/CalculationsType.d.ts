import bookshelf from '../config/bookshelf';
declare class CalculationsType extends bookshelf.Model<CalculationsType> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default CalculationsType;
