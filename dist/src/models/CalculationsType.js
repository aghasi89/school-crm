"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var CalculationsType = (function (_super) {
    tslib_1.__extends(CalculationsType, _super);
    function CalculationsType() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CalculationsType.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CalculationsType.prototype, "tableName", {
        get: function () {
            return Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS_TYPE;
        },
        enumerable: true,
        configurable: true
    });
    return CalculationsType;
}(bookshelf_1.default.Model));
exports.default = CalculationsType;
//# sourceMappingURL=CalculationsType.js.map