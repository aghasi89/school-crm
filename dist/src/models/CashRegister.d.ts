import bookshelf from '../config/bookshelf';
declare class CashRegister extends bookshelf.Model<CashRegister> {
    get requireFetch(): boolean;
    get tableName(): string;
    account(): any;
}
export default CashRegister;
