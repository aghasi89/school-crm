"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var CashRegister = (function (_super) {
    tslib_1.__extends(CashRegister, _super);
    function CashRegister() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CashRegister.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CashRegister.prototype, "tableName", {
        get: function () {
            return Table_1.default.CASH_REGISTER;
        },
        enumerable: true,
        configurable: true
    });
    CashRegister.prototype.account = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'account_id');
    };
    return CashRegister;
}(bookshelf_1.default.Model));
exports.default = CashRegister;
//# sourceMappingURL=CashRegister.js.map