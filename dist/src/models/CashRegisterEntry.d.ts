import bookshelf from '../config/bookshelf';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Currency from './Currency';
import CashRegister from './CashRegister';
import Operations from './Operations';
declare class CashRegisterEntryFunctions extends bookshelf.Model<CashRegisterEntryFunctions> {
    get requireFetch(): boolean;
    get tableName(): string;
    operations(): Operations;
}
declare class CashRegisterEntry extends bookshelf.Model<CashRegisterEntry> {
    get requireFetch(): boolean;
    get tableName(): string;
    cashRegister(): CashRegister;
    correspondentAccount(): AccountOfEmployeeCalculations;
    entryAccount(): AccountOfEmployeeCalculations;
    analiticGroup2(): AnaliticGroup2;
    analiticGroup1(): AnaliticGroup1;
    currencies(): Currency;
    partners(): Partners;
}
export { CashRegisterEntry, CashRegisterEntryFunctions };
