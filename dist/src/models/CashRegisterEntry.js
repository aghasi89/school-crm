"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var Partners_1 = tslib_1.__importDefault(require("./Partners"));
var AnaliticGroup1_1 = tslib_1.__importDefault(require("./AnaliticGroup1"));
var AnaliticGroup2_1 = tslib_1.__importDefault(require("./AnaliticGroup2"));
var Currency_1 = tslib_1.__importDefault(require("./Currency"));
var CashRegister_1 = tslib_1.__importDefault(require("./CashRegister"));
var Operations_1 = tslib_1.__importDefault(require("./Operations"));
var CashRegisterEntryFunctions = (function (_super) {
    tslib_1.__extends(CashRegisterEntryFunctions, _super);
    function CashRegisterEntryFunctions() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CashRegisterEntryFunctions.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CashRegisterEntryFunctions.prototype, "tableName", {
        get: function () {
            return Table_1.default.CASH_REGISTER_ENTRY_FUNCTIONS;
        },
        enumerable: true,
        configurable: true
    });
    CashRegisterEntryFunctions.prototype.operations = function () {
        return this.belongsTo(Operations_1.default, 'operations_id');
    };
    return CashRegisterEntryFunctions;
}(bookshelf_1.default.Model));
exports.CashRegisterEntryFunctions = CashRegisterEntryFunctions;
var CashRegisterEntry = (function (_super) {
    tslib_1.__extends(CashRegisterEntry, _super);
    function CashRegisterEntry() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CashRegisterEntry.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CashRegisterEntry.prototype, "tableName", {
        get: function () {
            return Table_1.default.CASH_REGISTER_ENTRY;
        },
        enumerable: true,
        configurable: true
    });
    CashRegisterEntry.prototype.cashRegister = function () {
        return this.belongsTo(CashRegister_1.default, 'cash_register_id');
    };
    CashRegisterEntry.prototype.correspondentAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'correspondent_account_id');
    };
    CashRegisterEntry.prototype.entryAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'entry_account_id');
    };
    CashRegisterEntry.prototype.analiticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'analitic_group_2_id');
    };
    CashRegisterEntry.prototype.analiticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'analitic_group_1_id');
    };
    CashRegisterEntry.prototype.currencies = function () {
        return this.belongsTo(Currency_1.default, 'currency_id');
    };
    CashRegisterEntry.prototype.partners = function () {
        return this.belongsTo(Partners_1.default, 'partners_id');
    };
    return CashRegisterEntry;
}(bookshelf_1.default.Model));
exports.CashRegisterEntry = CashRegisterEntry;
//# sourceMappingURL=CashRegisterEntry.js.map