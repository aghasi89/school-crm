import bookshelf from '../config/bookshelf';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Currency from './Currency';
import CashRegister from './CashRegister';
import Operations from './Operations';
declare class CashRegisterExitFunctions extends bookshelf.Model<CashRegisterExitFunctions> {
    get requireFetch(): boolean;
    get tableName(): string;
    operations(): Operations;
}
declare class CashRegisterExit extends bookshelf.Model<CashRegisterExit> {
    get requireFetch(): boolean;
    get tableName(): string;
    cashRegister(): CashRegister;
    correspondentAccount(): AccountOfEmployeeCalculations;
    exitAccount(): AccountOfEmployeeCalculations;
    analiticGroup2(): AnaliticGroup2;
    analiticGroup1(): AnaliticGroup1;
    currencies(): Currency;
    partners(): Partners;
}
export { CashRegisterExit, CashRegisterExitFunctions };
