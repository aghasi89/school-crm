"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var Partners_1 = tslib_1.__importDefault(require("./Partners"));
var AnaliticGroup1_1 = tslib_1.__importDefault(require("./AnaliticGroup1"));
var AnaliticGroup2_1 = tslib_1.__importDefault(require("./AnaliticGroup2"));
var Currency_1 = tslib_1.__importDefault(require("./Currency"));
var CashRegister_1 = tslib_1.__importDefault(require("./CashRegister"));
var Operations_1 = tslib_1.__importDefault(require("./Operations"));
var CashRegisterExitFunctions = (function (_super) {
    tslib_1.__extends(CashRegisterExitFunctions, _super);
    function CashRegisterExitFunctions() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CashRegisterExitFunctions.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CashRegisterExitFunctions.prototype, "tableName", {
        get: function () {
            return Table_1.default.CASH_REGISTER_EXIT_FUNCTIONS;
        },
        enumerable: true,
        configurable: true
    });
    CashRegisterExitFunctions.prototype.operations = function () {
        return this.belongsTo(Operations_1.default, 'operations_id');
    };
    return CashRegisterExitFunctions;
}(bookshelf_1.default.Model));
exports.CashRegisterExitFunctions = CashRegisterExitFunctions;
var CashRegisterExit = (function (_super) {
    tslib_1.__extends(CashRegisterExit, _super);
    function CashRegisterExit() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CashRegisterExit.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CashRegisterExit.prototype, "tableName", {
        get: function () {
            return Table_1.default.CASH_REGISTER_EXIT;
        },
        enumerable: true,
        configurable: true
    });
    CashRegisterExit.prototype.cashRegister = function () {
        return this.belongsTo(CashRegister_1.default, 'cash_register_id');
    };
    CashRegisterExit.prototype.correspondentAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'correspondent_account_id');
    };
    CashRegisterExit.prototype.exitAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'exit_account_id');
    };
    CashRegisterExit.prototype.analiticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'analitic_group_2_id');
    };
    CashRegisterExit.prototype.analiticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'analitic_group_1_id');
    };
    CashRegisterExit.prototype.currencies = function () {
        return this.belongsTo(Currency_1.default, 'currency_id');
    };
    CashRegisterExit.prototype.partners = function () {
        return this.belongsTo(Partners_1.default, 'partners_id');
    };
    return CashRegisterExit;
}(bookshelf_1.default.Model));
exports.CashRegisterExit = CashRegisterExit;
//# sourceMappingURL=CashRegisterExit.js.map