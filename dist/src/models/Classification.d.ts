import bookshelf from '../config/bookshelf';
declare class ClassificationMethod extends bookshelf.Model<ClassificationMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default ClassificationMethod;
