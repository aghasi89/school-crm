"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var ClassificationMethod = (function (_super) {
    tslib_1.__extends(ClassificationMethod, _super);
    function ClassificationMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(ClassificationMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ClassificationMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.CLASSIFICATION;
        },
        enumerable: true,
        configurable: true
    });
    return ClassificationMethod;
}(bookshelf_1.default.Model));
exports.default = ClassificationMethod;
//# sourceMappingURL=Classification.js.map