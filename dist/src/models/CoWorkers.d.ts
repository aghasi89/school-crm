import bookshelf from "../config/bookshelf";
declare class CoWorkers extends bookshelf.Model<CoWorkers> {
    get requireFetch(): boolean;
    get tableName(): string;
    get hasTimestamps(): boolean;
}
export default CoWorkers;
