"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var CoWorkers = (function (_super) {
    tslib_1.__extends(CoWorkers, _super);
    function CoWorkers() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CoWorkers.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CoWorkers.prototype, "tableName", {
        get: function () {
            return Table_1.default.CO_WORKERS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CoWorkers.prototype, "hasTimestamps", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return CoWorkers;
}(bookshelf_1.default.Model));
exports.default = CoWorkers;
//# sourceMappingURL=CoWorkers.js.map