import bookshelf from '../config/bookshelf';
declare class Contract extends bookshelf.Model<Contract> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default Contract;
