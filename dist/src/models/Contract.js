"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Contract = (function (_super) {
    tslib_1.__extends(Contract, _super);
    function Contract() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Contract.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Contract.prototype, "tableName", {
        get: function () {
            return Table_1.default.CONTRACT;
        },
        enumerable: true,
        configurable: true
    });
    return Contract;
}(bookshelf_1.default.Model));
exports.default = Contract;
//# sourceMappingURL=Contract.js.map