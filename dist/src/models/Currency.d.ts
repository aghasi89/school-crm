import bookshelf from '../config/bookshelf';
declare class Currency extends bookshelf.Model<Currency> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default Currency;
