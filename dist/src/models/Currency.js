"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Currency = (function (_super) {
    tslib_1.__extends(Currency, _super);
    function Currency() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Currency.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Currency.prototype, "tableName", {
        get: function () {
            return Table_1.default.CURRENCY;
        },
        enumerable: true,
        configurable: true
    });
    return Currency;
}(bookshelf_1.default.Model));
exports.default = Currency;
//# sourceMappingURL=Currency.js.map