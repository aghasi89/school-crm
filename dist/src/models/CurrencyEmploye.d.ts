import bookshelf from '../config/bookshelf';
declare class CurrencyEmploye extends bookshelf.Model<CurrencyEmploye> {
    get requireFetch(): boolean;
    get tableName(): string;
    currencies(): any;
}
export default CurrencyEmploye;
