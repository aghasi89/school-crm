"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Currency_1 = tslib_1.__importDefault(require("./Currency"));
var CurrencyEmploye = (function (_super) {
    tslib_1.__extends(CurrencyEmploye, _super);
    function CurrencyEmploye() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CurrencyEmploye.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CurrencyEmploye.prototype, "tableName", {
        get: function () {
            return Table_1.default.ACCOUNT_OF_EMPLOYEE_CALCULATIONS_CURRENCY;
        },
        enumerable: true,
        configurable: true
    });
    CurrencyEmploye.prototype.currencies = function () {
        return this.hasMany(Currency_1.default, 'currency_id');
    };
    return CurrencyEmploye;
}(bookshelf_1.default.Model));
exports.default = CurrencyEmploye;
//# sourceMappingURL=CurrencyEmploye.js.map