import bookshelf from '../config/bookshelf';
import Tabel from './Tabel';
import Subdivision from './Subdivision';
import Position from './Position';
declare class EMPLOYEE extends bookshelf.Model<EMPLOYEE> {
    get requireFetch(): boolean;
    get tableName(): string;
    subdivision(): Subdivision;
    tabel(): Tabel;
    position(): Position;
    general(): any;
    addressies(): any;
    otherInformation(): any;
    employeePosition(): any;
    employeeAccounts(): any;
    employeeAddition(): any;
}
export default EMPLOYEE;
