"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Tabel_1 = tslib_1.__importDefault(require("./Tabel"));
var Subdivision_1 = tslib_1.__importDefault(require("./Subdivision"));
var Position_1 = tslib_1.__importDefault(require("./Position"));
var EmployeeGeneral_1 = tslib_1.__importDefault(require("./EmployeeGeneral"));
var Address_1 = tslib_1.__importDefault(require("./Address"));
var OtherInformation_1 = tslib_1.__importDefault(require("./OtherInformation"));
var EmployeePosition_1 = tslib_1.__importDefault(require("./EmployeePosition"));
var EmployeeAccounts_1 = tslib_1.__importDefault(require("./EmployeeAccounts"));
var EmployeeAddition_1 = tslib_1.__importDefault(require("./EmployeeAddition"));
var EMPLOYEE = (function (_super) {
    tslib_1.__extends(EMPLOYEE, _super);
    function EMPLOYEE() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(EMPLOYEE.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EMPLOYEE.prototype, "tableName", {
        get: function () {
            return Table_1.default.EMPLOYEE;
        },
        enumerable: true,
        configurable: true
    });
    EMPLOYEE.prototype.subdivision = function () {
        return this.belongsTo(Subdivision_1.default, 'subdivision_id');
    };
    EMPLOYEE.prototype.tabel = function () {
        return this.belongsTo(Tabel_1.default, 'tabel_id');
    };
    EMPLOYEE.prototype.position = function () {
        return this.belongsTo(Position_1.default, 'position_id');
    };
    EMPLOYEE.prototype.general = function () {
        return this.hasOne(EmployeeGeneral_1.default, 'employee_id');
    };
    EMPLOYEE.prototype.addressies = function () {
        return this.hasOne(Address_1.default, 'employee_id');
    };
    EMPLOYEE.prototype.otherInformation = function () {
        return this.hasOne(OtherInformation_1.default, 'employee_id');
    };
    EMPLOYEE.prototype.employeePosition = function () {
        return this.hasMany(EmployeePosition_1.default);
    };
    EMPLOYEE.prototype.employeeAccounts = function () {
        return this.hasMany(EmployeeAccounts_1.default);
    };
    EMPLOYEE.prototype.employeeAddition = function () {
        return this.hasMany(EmployeeAddition_1.default);
    };
    return EMPLOYEE;
}(bookshelf_1.default.Model));
exports.default = EMPLOYEE;
//# sourceMappingURL=Employee.js.map