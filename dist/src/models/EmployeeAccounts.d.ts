import bookshelf from '../config/bookshelf';
declare class EmployeeAccounts extends bookshelf.Model<EmployeeAccounts> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default EmployeeAccounts;
