"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var EmployeeAccounts = (function (_super) {
    tslib_1.__extends(EmployeeAccounts, _super);
    function EmployeeAccounts() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(EmployeeAccounts.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EmployeeAccounts.prototype, "tableName", {
        get: function () {
            return Table_1.default.EMPLOYEE_ACCOUNTS;
        },
        enumerable: true,
        configurable: true
    });
    return EmployeeAccounts;
}(bookshelf_1.default.Model));
exports.default = EmployeeAccounts;
//# sourceMappingURL=EmployeeAccounts.js.map