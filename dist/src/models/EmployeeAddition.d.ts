import bookshelf from '../config/bookshelf';
declare class EmployeeAddition extends bookshelf.Model<EmployeeAddition> {
    get requireFetch(): boolean;
    get tableName(): string;
    addition(): any;
}
export default EmployeeAddition;
