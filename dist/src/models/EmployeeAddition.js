"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Addition_1 = tslib_1.__importDefault(require("./Addition"));
var EmployeeAddition = (function (_super) {
    tslib_1.__extends(EmployeeAddition, _super);
    function EmployeeAddition() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(EmployeeAddition.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EmployeeAddition.prototype, "tableName", {
        get: function () {
            return Table_1.default.EMPLOYEE_ADDITION;
        },
        enumerable: true,
        configurable: true
    });
    EmployeeAddition.prototype.addition = function () {
        return this.belongsTo(Addition_1.default, 'addition_id');
    };
    return EmployeeAddition;
}(bookshelf_1.default.Model));
exports.default = EmployeeAddition;
//# sourceMappingURL=EmployeeAddition.js.map