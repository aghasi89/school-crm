import bookshelf from '../config/bookshelf';
import Profession from './Profession';
import Contract from './Contract';
import Addition from './Addition';
declare class EmployeeGeneral extends bookshelf.Model<EmployeeGeneral> {
    get requireFetch(): boolean;
    get tableName(): string;
    profession(): Profession;
    contract(): Contract;
    addition(): Addition;
}
export default EmployeeGeneral;
