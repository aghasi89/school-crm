"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Profession_1 = tslib_1.__importDefault(require("./Profession"));
var Contract_1 = tslib_1.__importDefault(require("./Contract"));
var Addition_1 = tslib_1.__importDefault(require("./Addition"));
var EmployeeGeneral = (function (_super) {
    tslib_1.__extends(EmployeeGeneral, _super);
    function EmployeeGeneral() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(EmployeeGeneral.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EmployeeGeneral.prototype, "tableName", {
        get: function () {
            return Table_1.default.EMPLOYEE_GENERAL;
        },
        enumerable: true,
        configurable: true
    });
    EmployeeGeneral.prototype.profession = function () {
        return this.belongsTo(Profession_1.default, 'profession_id');
    };
    EmployeeGeneral.prototype.contract = function () {
        return this.belongsTo(Contract_1.default, 'contract_id');
    };
    EmployeeGeneral.prototype.addition = function () {
        return this.belongsTo(Addition_1.default, 'addition_id');
    };
    return EmployeeGeneral;
}(bookshelf_1.default.Model));
exports.default = EmployeeGeneral;
//# sourceMappingURL=EmployeeGeneral.js.map