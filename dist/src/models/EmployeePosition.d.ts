import bookshelf from '../config/bookshelf';
declare class EmployeePosition extends bookshelf.Model<EmployeePosition> {
    get requireFetch(): boolean;
    get tableName(): string;
    positon(): any;
}
export default EmployeePosition;
