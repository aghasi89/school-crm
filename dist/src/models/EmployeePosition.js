"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Position_1 = tslib_1.__importDefault(require("./Position"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var EmployeePosition = (function (_super) {
    tslib_1.__extends(EmployeePosition, _super);
    function EmployeePosition() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(EmployeePosition.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EmployeePosition.prototype, "tableName", {
        get: function () {
            return Table_1.default.EMPLOYEE_POSITION;
        },
        enumerable: true,
        configurable: true
    });
    EmployeePosition.prototype.positon = function () {
        return this.belongsTo(Position_1.default, 'position_id');
    };
    return EmployeePosition;
}(bookshelf_1.default.Model));
exports.default = EmployeePosition;
//# sourceMappingURL=EmployeePosition.js.map