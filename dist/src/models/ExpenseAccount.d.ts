import bookshelf from '../config/bookshelf';
declare class ExpenseAccountMethod extends bookshelf.Model<ExpenseAccountMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default ExpenseAccountMethod;
