"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var ExpenseAccountMethod = (function (_super) {
    tslib_1.__extends(ExpenseAccountMethod, _super);
    function ExpenseAccountMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(ExpenseAccountMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseAccountMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.EXPENSE_ACCOUNT;
        },
        enumerable: true,
        configurable: true
    });
    return ExpenseAccountMethod;
}(bookshelf_1.default.Model));
exports.default = ExpenseAccountMethod;
//# sourceMappingURL=ExpenseAccount.js.map