import bookshelf from '../config/bookshelf';
declare class FormulasMethod extends bookshelf.Model<FormulasMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default FormulasMethod;
