"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var FormulasMethod = (function (_super) {
    tslib_1.__extends(FormulasMethod, _super);
    function FormulasMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(FormulasMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormulasMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.FORMULAS;
        },
        enumerable: true,
        configurable: true
    });
    return FormulasMethod;
}(bookshelf_1.default.Model));
exports.default = FormulasMethod;
//# sourceMappingURL=Formulas.js.map