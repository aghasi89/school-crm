import bookshelf from '../config/bookshelf';
declare class Group extends bookshelf.Model<Group> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default Group;
