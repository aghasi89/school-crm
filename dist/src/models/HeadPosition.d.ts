import bookshelf from '../config/bookshelf';
declare class HeadPosition extends bookshelf.Model<HeadPosition> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default HeadPosition;
