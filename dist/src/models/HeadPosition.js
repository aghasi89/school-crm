"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var HeadPosition = (function (_super) {
    tslib_1.__extends(HeadPosition, _super);
    function HeadPosition() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(HeadPosition.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HeadPosition.prototype, "tableName", {
        get: function () {
            return Table_1.default.HEAD_POSITION;
        },
        enumerable: true,
        configurable: true
    });
    return HeadPosition;
}(bookshelf_1.default.Model));
exports.default = HeadPosition;
//# sourceMappingURL=HeadPosition.js.map