import bookshelf from '../config/bookshelf';
declare class HmxProfitTax extends bookshelf.Model<HmxProfitTax> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default HmxProfitTax;
