"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var HmxProfitTax = (function (_super) {
    tslib_1.__extends(HmxProfitTax, _super);
    function HmxProfitTax() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(HmxProfitTax.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HmxProfitTax.prototype, "tableName", {
        get: function () {
            return Table_1.default.HMX_PROFIT_TAX;
        },
        enumerable: true,
        configurable: true
    });
    return HmxProfitTax;
}(bookshelf_1.default.Model));
exports.default = HmxProfitTax;
//# sourceMappingURL=HmxProfitTax.js.map