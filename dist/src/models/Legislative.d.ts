import bookshelf from "../config/bookshelf";
declare class Legislative extends bookshelf.Model<Legislative> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default Legislative;
