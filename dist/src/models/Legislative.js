"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Legislative = (function (_super) {
    tslib_1.__extends(Legislative, _super);
    function Legislative() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Legislative.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Legislative.prototype, "tableName", {
        get: function () {
            return Table_1.default.LEGISLATIVE;
        },
        enumerable: true,
        configurable: true
    });
    return Legislative;
}(bookshelf_1.default.Model));
exports.default = Legislative;
//# sourceMappingURL=Legislative.js.map