import bookshelf from '../config/bookshelf';
declare class MaterialValueMethod extends bookshelf.Model<MaterialValueMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
    measurementUnit(): any;
    classification(): any;
    account(): any;
    billingMethod(): any;
    salesRevenue(): any;
    retailRevenue(): any;
    salesExpense(): any;
    materialValueGroup(): any;
}
export default MaterialValueMethod;
