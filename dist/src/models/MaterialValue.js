"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var MeasurementUnit_1 = tslib_1.__importDefault(require("./MeasurementUnit"));
var Classification_1 = tslib_1.__importDefault(require("./Classification"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var BillingMethod_1 = tslib_1.__importDefault(require("./BillingMethod"));
var MaterialValueGroup_1 = tslib_1.__importDefault(require("./MaterialValueGroup"));
var MaterialValueMethod = (function (_super) {
    tslib_1.__extends(MaterialValueMethod, _super);
    function MaterialValueMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(MaterialValueMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaterialValueMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.MATERIAL_VALUE;
        },
        enumerable: true,
        configurable: true
    });
    MaterialValueMethod.prototype.measurementUnit = function () {
        return this.belongsTo(MeasurementUnit_1.default, 'measurement_unit_id');
    };
    MaterialValueMethod.prototype.classification = function () {
        return this.belongsTo(Classification_1.default, 'classification_id');
    };
    MaterialValueMethod.prototype.account = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'account_id');
    };
    MaterialValueMethod.prototype.billingMethod = function () {
        return this.belongsTo(BillingMethod_1.default, 'billing_method_id');
    };
    MaterialValueMethod.prototype.salesRevenue = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'sales_revenue_account_id');
    };
    MaterialValueMethod.prototype.retailRevenue = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'retail_revenue_account_id');
    };
    MaterialValueMethod.prototype.salesExpense = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'sales_expense_account_id');
    };
    MaterialValueMethod.prototype.materialValueGroup = function () {
        return this.belongsTo(MaterialValueGroup_1.default, 'material_value_group_id');
    };
    return MaterialValueMethod;
}(bookshelf_1.default.Model));
exports.default = MaterialValueMethod;
//# sourceMappingURL=MaterialValue.js.map