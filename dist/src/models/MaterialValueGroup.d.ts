import bookshelf from '../config/bookshelf';
declare class MaterialValueGroupMethod extends bookshelf.Model<MaterialValueGroupMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
    parent(): any;
}
export default MaterialValueGroupMethod;
