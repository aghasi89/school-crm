"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var MaterialValueGroupMethod = (function (_super) {
    tslib_1.__extends(MaterialValueGroupMethod, _super);
    function MaterialValueGroupMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(MaterialValueGroupMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaterialValueGroupMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.MATERIAL_VALUE_GROUP;
        },
        enumerable: true,
        configurable: true
    });
    MaterialValueGroupMethod.prototype.parent = function () {
        return this.belongsTo(MaterialValueGroupMethod, 'material_value_group_id');
    };
    return MaterialValueGroupMethod;
}(bookshelf_1.default.Model));
exports.default = MaterialValueGroupMethod;
//# sourceMappingURL=MaterialValueGroup.js.map