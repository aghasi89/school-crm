import bookshelf from "../config/bookshelf";
declare class MeasurementUnit extends bookshelf.Model<MeasurementUnit> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default MeasurementUnit;
