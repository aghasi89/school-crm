"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var MeasurementUnit = (function (_super) {
    tslib_1.__extends(MeasurementUnit, _super);
    function MeasurementUnit() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(MeasurementUnit.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MeasurementUnit.prototype, "tableName", {
        get: function () {
            return Table_1.default.MEASUREMENT_UNIT;
        },
        enumerable: true,
        configurable: true
    });
    return MeasurementUnit;
}(bookshelf_1.default.Model));
exports.default = MeasurementUnit;
//# sourceMappingURL=MeasurementUnit.js.map