import bookshelf from '../config/bookshelf';
declare class OperationsMethod extends bookshelf.Model<OperationsMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
    debit(): any;
    dPartners(): any;
    dAnaliticGroup2(): any;
    dAnaliticGroup1(): any;
    credit(): any;
    cPartners(): any;
    cAnaliticGroup2(): any;
    cAnaliticGroup1(): any;
}
export default OperationsMethod;
