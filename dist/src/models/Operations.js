"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var Partners_1 = tslib_1.__importDefault(require("./Partners"));
var AnaliticGroup1_1 = tslib_1.__importDefault(require("./AnaliticGroup1"));
var AnaliticGroup2_1 = tslib_1.__importDefault(require("./AnaliticGroup2"));
var OperationsMethod = (function (_super) {
    tslib_1.__extends(OperationsMethod, _super);
    function OperationsMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(OperationsMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OperationsMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.OPERATIONS;
        },
        enumerable: true,
        configurable: true
    });
    OperationsMethod.prototype.debit = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'debit_id');
    };
    OperationsMethod.prototype.dPartners = function () {
        return this.belongsTo(Partners_1.default, 'd_partners_id');
    };
    OperationsMethod.prototype.dAnaliticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'd_analitic_group_2_id');
    };
    OperationsMethod.prototype.dAnaliticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'd_analitic_group_1_id');
    };
    OperationsMethod.prototype.credit = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'credit_id');
    };
    OperationsMethod.prototype.cPartners = function () {
        return this.belongsTo(Partners_1.default, 'c_partners_id');
    };
    OperationsMethod.prototype.cAnaliticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'c_analitic_group_2_id');
    };
    OperationsMethod.prototype.cAnaliticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'c_analitic_group_1_id');
    };
    return OperationsMethod;
}(bookshelf_1.default.Model));
exports.default = OperationsMethod;
//# sourceMappingURL=Operations.js.map