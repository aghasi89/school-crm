import bookshelf from '../config/bookshelf';
declare class OtherInformation extends bookshelf.Model<OtherInformation> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default OtherInformation;
