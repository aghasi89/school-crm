"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var OtherInformation = (function (_super) {
    tslib_1.__extends(OtherInformation, _super);
    function OtherInformation() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(OtherInformation.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OtherInformation.prototype, "tableName", {
        get: function () {
            return Table_1.default.OTHER_INFORMATION;
        },
        enumerable: true,
        configurable: true
    });
    return OtherInformation;
}(bookshelf_1.default.Model));
exports.default = OtherInformation;
//# sourceMappingURL=OtherInformation.js.map