import bookshelf from "../config/bookshelf";
declare class Partners extends bookshelf.Model<Partners> {
    get requireFetch(): boolean;
    get tableName(): string;
    billingAccounts(): any;
    additionalAddressePartners(): any;
}
export default Partners;
