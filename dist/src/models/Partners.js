"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var BillingAccount_1 = tslib_1.__importDefault(require("./BillingAccount"));
var AdditionalAddressePartners_1 = tslib_1.__importDefault(require("./AdditionalAddressePartners"));
var Partners = (function (_super) {
    tslib_1.__extends(Partners, _super);
    function Partners() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Partners.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Partners.prototype, "tableName", {
        get: function () {
            return Table_1.default.PARTNERS;
        },
        enumerable: true,
        configurable: true
    });
    Partners.prototype.billingAccounts = function () {
        return this.hasMany(BillingAccount_1.default, 'partners_id');
    };
    Partners.prototype.additionalAddressePartners = function () {
        return this.hasMany(AdditionalAddressePartners_1.default, 'partners_id');
    };
    return Partners;
}(bookshelf_1.default.Model));
exports.default = Partners;
//# sourceMappingURL=Partners.js.map