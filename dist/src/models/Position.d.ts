import bookshelf from "../config/bookshelf";
import Subdivision from './Subdivision';
declare class Position extends bookshelf.Model<Position> {
    get requireFetch(): boolean;
    get tableName(): string;
    subdivision(): Subdivision;
    employee_position(): any;
}
export default Position;
