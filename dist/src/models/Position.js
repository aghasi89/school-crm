"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Subdivision_1 = tslib_1.__importDefault(require("./Subdivision"));
var EmployeePosition_1 = tslib_1.__importDefault(require("./EmployeePosition"));
var Position = (function (_super) {
    tslib_1.__extends(Position, _super);
    function Position() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Position.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Position.prototype, "tableName", {
        get: function () {
            return Table_1.default.POSITION;
        },
        enumerable: true,
        configurable: true
    });
    Position.prototype.subdivision = function () {
        return this.belongsTo(Subdivision_1.default, 'subdivision_id');
    };
    Position.prototype.employee_position = function () {
        return this.belongsTo(EmployeePosition_1.default, 'position_id');
    };
    return Position;
}(bookshelf_1.default.Model));
exports.default = Position;
//# sourceMappingURL=Position.js.map