import bookshelf from '../config/bookshelf';
import Services from './Services';
declare class PriceOfServicesMethod extends bookshelf.Model<PriceOfServicesMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
    services(): Services;
}
export default PriceOfServicesMethod;
