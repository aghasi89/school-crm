"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Services_1 = tslib_1.__importDefault(require("./Services"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var PriceOfServicesMethod = (function (_super) {
    tslib_1.__extends(PriceOfServicesMethod, _super);
    function PriceOfServicesMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(PriceOfServicesMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PriceOfServicesMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.PRICE_OF_SERVICES;
        },
        enumerable: true,
        configurable: true
    });
    PriceOfServicesMethod.prototype.services = function () {
        return this.belongsTo(Services_1.default, 'services_id');
    };
    return PriceOfServicesMethod;
}(bookshelf_1.default.Model));
exports.default = PriceOfServicesMethod;
//# sourceMappingURL=PriceOfServices.js.map