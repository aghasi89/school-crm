import bookshelf from '../config/bookshelf';
declare class ProfessionMethod extends bookshelf.Model<ProfessionMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default ProfessionMethod;
