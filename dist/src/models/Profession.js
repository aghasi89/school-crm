"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var ProfessionMethod = (function (_super) {
    tslib_1.__extends(ProfessionMethod, _super);
    function ProfessionMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(ProfessionMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProfessionMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.PROFESSION;
        },
        enumerable: true,
        configurable: true
    });
    return ProfessionMethod;
}(bookshelf_1.default.Model));
exports.default = ProfessionMethod;
//# sourceMappingURL=Profession.js.map