import bookshelf from '../config/bookshelf';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';
import Operations from './Operations';
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Services from './Services';
declare class ReceivedServicesOperations extends bookshelf.Model<ReceivedServicesOperations> {
    get requireFetch(): boolean;
    get tableName(): string;
    operations(): Operations;
}
declare class ReceivedServicesDirectory extends bookshelf.Model<ReceivedServicesDirectory> {
    get requireFetch(): boolean;
    get tableName(): string;
    services(): Services;
}
declare class ReceivedServices extends bookshelf.Model<ReceivedServices> {
    get requireFetch(): boolean;
    get tableName(): string;
    providers(): Partners;
    providerAccount(): AccountOfEmployeeCalculations;
    advancePaymentAccount(): AccountOfEmployeeCalculations;
    analiticGroup2(): AnaliticGroup2;
    analiticGroup1(): AnaliticGroup1;
}
export { ReceivedServices, ReceivedServicesOperations, ReceivedServicesDirectory };
