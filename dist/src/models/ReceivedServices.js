"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var Operations_1 = tslib_1.__importDefault(require("./Operations"));
var Partners_1 = tslib_1.__importDefault(require("./Partners"));
var AnaliticGroup1_1 = tslib_1.__importDefault(require("./AnaliticGroup1"));
var AnaliticGroup2_1 = tslib_1.__importDefault(require("./AnaliticGroup2"));
var Services_1 = tslib_1.__importDefault(require("./Services"));
var ReceivedServicesOperations = (function (_super) {
    tslib_1.__extends(ReceivedServicesOperations, _super);
    function ReceivedServicesOperations() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(ReceivedServicesOperations.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ReceivedServicesOperations.prototype, "tableName", {
        get: function () {
            return Table_1.default.RECEIVED_SERVICES_OPERATIONS;
        },
        enumerable: true,
        configurable: true
    });
    ReceivedServicesOperations.prototype.operations = function () {
        return this.belongsTo(Operations_1.default, 'operations_id');
    };
    return ReceivedServicesOperations;
}(bookshelf_1.default.Model));
exports.ReceivedServicesOperations = ReceivedServicesOperations;
var ReceivedServicesDirectory = (function (_super) {
    tslib_1.__extends(ReceivedServicesDirectory, _super);
    function ReceivedServicesDirectory() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(ReceivedServicesDirectory.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ReceivedServicesDirectory.prototype, "tableName", {
        get: function () {
            return Table_1.default.RECEIVED_SERVICES_DIRECTORY;
        },
        enumerable: true,
        configurable: true
    });
    ReceivedServicesDirectory.prototype.services = function () {
        return this.belongsTo(Services_1.default, 'service_id');
    };
    return ReceivedServicesDirectory;
}(bookshelf_1.default.Model));
exports.ReceivedServicesDirectory = ReceivedServicesDirectory;
var ReceivedServices = (function (_super) {
    tslib_1.__extends(ReceivedServices, _super);
    function ReceivedServices() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(ReceivedServices.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ReceivedServices.prototype, "tableName", {
        get: function () {
            return Table_1.default.RECEIVED_SERVICES;
        },
        enumerable: true,
        configurable: true
    });
    ReceivedServices.prototype.providers = function () {
        return this.belongsTo(Partners_1.default, 'partners_id');
    };
    ReceivedServices.prototype.providerAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'provider_account_id');
    };
    ReceivedServices.prototype.advancePaymentAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'advance_payment_account_id');
    };
    ReceivedServices.prototype.analiticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'analitic_group_2_id');
    };
    ReceivedServices.prototype.analiticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'analitic_group_1_id');
    };
    return ReceivedServices;
}(bookshelf_1.default.Model));
exports.ReceivedServices = ReceivedServices;
//# sourceMappingURL=ReceivedServices.js.map