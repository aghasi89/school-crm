import bookshelf from '../config/bookshelf';
declare class ServiceNameMethod extends bookshelf.Model<ServiceNameMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
    get hasTimestamps(): boolean;
}
export default ServiceNameMethod;
