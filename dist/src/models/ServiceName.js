"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var ServiceNameMethod = (function (_super) {
    tslib_1.__extends(ServiceNameMethod, _super);
    function ServiceNameMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(ServiceNameMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ServiceNameMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.SERVICE_NAME;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ServiceNameMethod.prototype, "hasTimestamps", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return ServiceNameMethod;
}(bookshelf_1.default.Model));
exports.default = ServiceNameMethod;
//# sourceMappingURL=ServiceName.js.map