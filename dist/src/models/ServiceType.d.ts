import bookshelf from '../config/bookshelf';
declare class ServiceTypeMethod extends bookshelf.Model<ServiceTypeMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
    get hasTimestamps(): boolean;
}
export default ServiceTypeMethod;
