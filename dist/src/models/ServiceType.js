"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var ServiceTypeMethod = (function (_super) {
    tslib_1.__extends(ServiceTypeMethod, _super);
    function ServiceTypeMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(ServiceTypeMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ServiceTypeMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.SERVICE_TYPE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ServiceTypeMethod.prototype, "hasTimestamps", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return ServiceTypeMethod;
}(bookshelf_1.default.Model));
exports.default = ServiceTypeMethod;
//# sourceMappingURL=ServiceType.js.map