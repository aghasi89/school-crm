import bookshelf from '../config/bookshelf';
declare class Services extends bookshelf.Model<Services> {
    get requireFetch(): boolean;
    get tableName(): string;
    measurementUnit(): any;
    classification(): any;
    account(): any;
}
export default Services;
