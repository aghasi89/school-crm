"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var MeasurementUnit_1 = tslib_1.__importDefault(require("./MeasurementUnit"));
var Classification_1 = tslib_1.__importDefault(require("./Classification"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var Services = (function (_super) {
    tslib_1.__extends(Services, _super);
    function Services() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Services.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Services.prototype, "tableName", {
        get: function () {
            return Table_1.default.SERVICES;
        },
        enumerable: true,
        configurable: true
    });
    Services.prototype.measurementUnit = function () {
        return this.belongsTo(MeasurementUnit_1.default, 'measurement_unit_id');
    };
    Services.prototype.classification = function () {
        return this.belongsTo(Classification_1.default, 'classification_id');
    };
    Services.prototype.account = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'account_id');
    };
    return Services;
}(bookshelf_1.default.Model));
exports.default = Services;
//# sourceMappingURL=Services.js.map