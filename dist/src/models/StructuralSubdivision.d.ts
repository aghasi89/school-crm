import bookshelf from '../config/bookshelf';
import Partners from './Partners';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';
declare class StructuralSubdivisionMethod extends bookshelf.Model<StructuralSubdivisionMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
    partners(): Partners;
    expenseAccount(): AccountOfEmployeeCalculations;
}
export default StructuralSubdivisionMethod;
