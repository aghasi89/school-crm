"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Partners_1 = tslib_1.__importDefault(require("./Partners"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var StructuralSubdivisionMethod = (function (_super) {
    tslib_1.__extends(StructuralSubdivisionMethod, _super);
    function StructuralSubdivisionMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(StructuralSubdivisionMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StructuralSubdivisionMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.STRUCTURAL_SUBDIVISION;
        },
        enumerable: true,
        configurable: true
    });
    StructuralSubdivisionMethod.prototype.partners = function () {
        return this.belongsTo(Partners_1.default, 'partner_id');
    };
    StructuralSubdivisionMethod.prototype.expenseAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'expense_account_id', 'account');
    };
    return StructuralSubdivisionMethod;
}(bookshelf_1.default.Model));
exports.default = StructuralSubdivisionMethod;
//# sourceMappingURL=StructuralSubdivision.js.map