import bookshelf from "../config/bookshelf";
declare class Subdivision extends bookshelf.Model<Subdivision> {
    get requireFetch(): boolean;
    get tableName(): string;
    get hasTimestamps(): boolean;
    get _count(): any;
}
export default Subdivision;
