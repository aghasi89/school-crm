"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Subdivision = (function (_super) {
    tslib_1.__extends(Subdivision, _super);
    function Subdivision() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Subdivision.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Subdivision.prototype, "tableName", {
        get: function () {
            return Table_1.default.SUBDIVISION;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Subdivision.prototype, "hasTimestamps", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Subdivision.prototype, "_count", {
        get: function () {
            return this.count();
        },
        enumerable: true,
        configurable: true
    });
    return Subdivision;
}(bookshelf_1.default.Model));
exports.default = Subdivision;
//# sourceMappingURL=Subdivision.js.map