import bookshelf from "../config/bookshelf";
declare class Subsection extends bookshelf.Model<Subsection> {
    get requireFetch(): boolean;
    get tableName(): string;
    get _count(): any;
    customerAccount(): any;
    aahAccount(): any;
    prepaidAccountReceived(): any;
    typesOfAction(): any;
}
export default Subsection;
