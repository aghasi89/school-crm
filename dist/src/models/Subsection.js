"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var TypesOfActions_1 = tslib_1.__importDefault(require("./TypesOfActions"));
var Subsection = (function (_super) {
    tslib_1.__extends(Subsection, _super);
    function Subsection() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Subsection.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Subsection.prototype, "tableName", {
        get: function () {
            return Table_1.default.SUBSECTION;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Subsection.prototype, "_count", {
        get: function () {
            return this.count();
        },
        enumerable: true,
        configurable: true
    });
    Subsection.prototype.customerAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'customer_account_id');
    };
    Subsection.prototype.aahAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'aah_account_id');
    };
    Subsection.prototype.prepaidAccountReceived = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'prepaid_account_received_id');
    };
    Subsection.prototype.typesOfAction = function () {
        return this.belongsTo(TypesOfActions_1.default, 'TypesOfActionsId');
    };
    return Subsection;
}(bookshelf_1.default.Model));
exports.default = Subsection;
//# sourceMappingURL=Subsection.js.map