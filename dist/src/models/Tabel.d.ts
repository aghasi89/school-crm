import bookshelf from '../config/bookshelf';
declare class TabelMethod extends bookshelf.Model<TabelMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default TabelMethod;
