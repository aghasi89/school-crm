"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var TabelMethod = (function (_super) {
    tslib_1.__extends(TabelMethod, _super);
    function TabelMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(TabelMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TabelMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.TABEL;
        },
        enumerable: true,
        configurable: true
    });
    return TabelMethod;
}(bookshelf_1.default.Model));
exports.default = TabelMethod;
//# sourceMappingURL=Tabel.js.map