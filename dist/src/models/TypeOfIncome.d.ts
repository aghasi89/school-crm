import bookshelf from '../config/bookshelf';
declare class TypeOfIncomeMethod extends bookshelf.Model<TypeOfIncomeMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default TypeOfIncomeMethod;
