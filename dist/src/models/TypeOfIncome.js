"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var TypeOfIncomeMethod = (function (_super) {
    tslib_1.__extends(TypeOfIncomeMethod, _super);
    function TypeOfIncomeMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(TypeOfIncomeMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TypeOfIncomeMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.TYPE_OF_INCOME;
        },
        enumerable: true,
        configurable: true
    });
    return TypeOfIncomeMethod;
}(bookshelf_1.default.Model));
exports.default = TypeOfIncomeMethod;
//# sourceMappingURL=TypeOfIncome.js.map