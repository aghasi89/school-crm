import bookshelf from '../config/bookshelf';
declare class TypeOfVacationMethod extends bookshelf.Model<TypeOfVacationMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default TypeOfVacationMethod;
