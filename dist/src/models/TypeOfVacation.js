"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var TypeOfVacationMethod = (function (_super) {
    tslib_1.__extends(TypeOfVacationMethod, _super);
    function TypeOfVacationMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(TypeOfVacationMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TypeOfVacationMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.TYPE_OF_VACATION;
        },
        enumerable: true,
        configurable: true
    });
    return TypeOfVacationMethod;
}(bookshelf_1.default.Model));
exports.default = TypeOfVacationMethod;
//# sourceMappingURL=TypeOfVacation.js.map