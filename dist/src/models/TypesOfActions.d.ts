import bookshelf from '../config/bookshelf';
declare class TypeOfActionsMethod extends bookshelf.Model<TypeOfActionsMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default TypeOfActionsMethod;
