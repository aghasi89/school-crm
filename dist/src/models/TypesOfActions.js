"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var TypeOfActionsMethod = (function (_super) {
    tslib_1.__extends(TypeOfActionsMethod, _super);
    function TypeOfActionsMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(TypeOfActionsMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TypeOfActionsMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.TYPES_OF_ACTIONS;
        },
        enumerable: true,
        configurable: true
    });
    return TypeOfActionsMethod;
}(bookshelf_1.default.Model));
exports.default = TypeOfActionsMethod;
//# sourceMappingURL=TypesOfActions.js.map