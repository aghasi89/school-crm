import bookshelf from '../config/bookshelf';
import UserRole from './UserRole';
declare class User extends bookshelf.Model<User> {
    get requireFetch(): boolean;
    get tableName(): string;
    get hasTimestamps(): boolean;
    role(): UserRole;
}
export default User;
