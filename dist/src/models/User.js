"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var UserRole_1 = tslib_1.__importDefault(require("./UserRole"));
var User = (function (_super) {
    tslib_1.__extends(User, _super);
    function User() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(User.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "tableName", {
        get: function () {
            return Table_1.default.USERS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "hasTimestamps", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    User.prototype.role = function () {
        return this.belongsTo(UserRole_1.default, 'role_id');
    };
    return User;
}(bookshelf_1.default.Model));
exports.default = User;
//# sourceMappingURL=User.js.map