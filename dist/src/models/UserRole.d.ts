import bookshelf from '../config/bookshelf';
declare class UserRole extends bookshelf.Model<UserRole> {
    get requireFetch(): boolean;
    get tableName(): string;
    get hasTimestamps(): boolean;
}
export default UserRole;
