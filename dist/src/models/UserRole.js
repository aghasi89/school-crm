"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var UserRole = (function (_super) {
    tslib_1.__extends(UserRole, _super);
    function UserRole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(UserRole.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserRole.prototype, "tableName", {
        get: function () {
            return Table_1.default.USER_ROLES;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserRole.prototype, "hasTimestamps", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return UserRole;
}(bookshelf_1.default.Model));
exports.default = UserRole;
//# sourceMappingURL=UserRole.js.map