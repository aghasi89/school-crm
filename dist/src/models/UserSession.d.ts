import bookshelf from '../config/bookshelf';
declare class UserSession extends bookshelf.Model<UserSession> {
    get requireFetch(): boolean;
    get tableName(): string;
    get hasTimestamps(): boolean;
}
export default UserSession;
