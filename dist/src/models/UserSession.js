"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var UserSession = (function (_super) {
    tslib_1.__extends(UserSession, _super);
    function UserSession() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(UserSession.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserSession.prototype, "tableName", {
        get: function () {
            return Table_1.default.USER_SESSIONS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserSession.prototype, "hasTimestamps", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    return UserSession;
}(bookshelf_1.default.Model));
exports.default = UserSession;
//# sourceMappingURL=UserSession.js.map