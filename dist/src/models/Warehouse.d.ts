import bookshelf from '../config/bookshelf';
declare class WarehouseMethod extends bookshelf.Model<WarehouseMethod> {
    get requireFetch(): boolean;
    get tableName(): string;
}
export default WarehouseMethod;
