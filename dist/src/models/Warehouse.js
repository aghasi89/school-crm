"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var WarehouseMethod = (function (_super) {
    tslib_1.__extends(WarehouseMethod, _super);
    function WarehouseMethod() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(WarehouseMethod.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WarehouseMethod.prototype, "tableName", {
        get: function () {
            return Table_1.default.WAREHOUSE;
        },
        enumerable: true,
        configurable: true
    });
    return WarehouseMethod;
}(bookshelf_1.default.Model));
exports.default = WarehouseMethod;
//# sourceMappingURL=Warehouse.js.map