import bookshelf from '../config/bookshelf';
declare class WarehouseEntryOrder extends bookshelf.Model<WarehouseEntryOrder> {
    get requireFetch(): boolean;
    get tableName(): string;
    warehouse(): any;
    partners(): any;
    partnersAccount(): any;
    prepaidAccount(): any;
    analiticGroup2(): any;
    analiticGroup1(): any;
    warehouseEntryOrderFunctions(): any;
    warehouseEntryOrderProduct(): any;
}
declare class WarehouseEntryOrderFunctions extends bookshelf.Model<WarehouseEntryOrderFunctions> {
    get requireFetch(): boolean;
    get tableName(): string;
    materialValue(): any;
    accounts(): any;
    classification(): any;
}
declare class WarehouseEntryOrderProduct extends bookshelf.Model<WarehouseEntryOrderProduct> {
    get requireFetch(): boolean;
    get tableName(): string;
    materialValue(): any;
    accounts(): any;
    classification(): any;
}
export { WarehouseEntryOrder, WarehouseEntryOrderFunctions, WarehouseEntryOrderProduct };
