"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Classification_1 = tslib_1.__importDefault(require("./Classification"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var MaterialValue_1 = tslib_1.__importDefault(require("./MaterialValue"));
var Warehouse_1 = tslib_1.__importDefault(require("./Warehouse"));
var Partners_1 = tslib_1.__importDefault(require("./Partners"));
var AnaliticGroup1_1 = tslib_1.__importDefault(require("./AnaliticGroup1"));
var AnaliticGroup2_1 = tslib_1.__importDefault(require("./AnaliticGroup2"));
var WarehouseEntryOrder = (function (_super) {
    tslib_1.__extends(WarehouseEntryOrder, _super);
    function WarehouseEntryOrder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(WarehouseEntryOrder.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WarehouseEntryOrder.prototype, "tableName", {
        get: function () {
            return Table_1.default.WAREHOUSE_ENTRY_ORDER;
        },
        enumerable: true,
        configurable: true
    });
    WarehouseEntryOrder.prototype.warehouse = function () {
        return this.belongsTo(Warehouse_1.default, 'warehouse_id');
    };
    WarehouseEntryOrder.prototype.partners = function () {
        return this.belongsTo(Partners_1.default, 'partners_id');
    };
    WarehouseEntryOrder.prototype.partnersAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'partners_account_id');
    };
    WarehouseEntryOrder.prototype.prepaidAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'prepaid_account_id');
    };
    WarehouseEntryOrder.prototype.analiticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'analitic_group_2_id');
    };
    WarehouseEntryOrder.prototype.analiticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'analitic_group_1_id');
    };
    WarehouseEntryOrder.prototype.warehouseEntryOrderFunctions = function () {
        return this.hasMany(WarehouseEntryOrderFunctions, 'warehouse_entry_order_id');
    };
    WarehouseEntryOrder.prototype.warehouseEntryOrderProduct = function () {
        return this.hasMany(WarehouseEntryOrderProduct, 'warehouse_entry_order_id');
    };
    return WarehouseEntryOrder;
}(bookshelf_1.default.Model));
exports.WarehouseEntryOrder = WarehouseEntryOrder;
var WarehouseEntryOrderFunctions = (function (_super) {
    tslib_1.__extends(WarehouseEntryOrderFunctions, _super);
    function WarehouseEntryOrderFunctions() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(WarehouseEntryOrderFunctions.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WarehouseEntryOrderFunctions.prototype, "tableName", {
        get: function () {
            return Table_1.default.WAREHOUSE_ENTRY_ORDER_PRODUCT;
        },
        enumerable: true,
        configurable: true
    });
    WarehouseEntryOrderFunctions.prototype.materialValue = function () {
        return this.belongsTo(MaterialValue_1.default, 'material_value_id');
    };
    WarehouseEntryOrderFunctions.prototype.accounts = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'accounts_id');
    };
    WarehouseEntryOrderFunctions.prototype.classification = function () {
        return this.belongsTo(Classification_1.default, 'classification_id');
    };
    return WarehouseEntryOrderFunctions;
}(bookshelf_1.default.Model));
exports.WarehouseEntryOrderFunctions = WarehouseEntryOrderFunctions;
var WarehouseEntryOrderProduct = (function (_super) {
    tslib_1.__extends(WarehouseEntryOrderProduct, _super);
    function WarehouseEntryOrderProduct() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(WarehouseEntryOrderProduct.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WarehouseEntryOrderProduct.prototype, "tableName", {
        get: function () {
            return Table_1.default.WAREHOUSE_ENTRY_ORDER_PRODUCT;
        },
        enumerable: true,
        configurable: true
    });
    WarehouseEntryOrderProduct.prototype.materialValue = function () {
        return this.belongsTo(MaterialValue_1.default, 'material_value_id');
    };
    WarehouseEntryOrderProduct.prototype.accounts = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'accounts_id');
    };
    WarehouseEntryOrderProduct.prototype.classification = function () {
        return this.belongsTo(Classification_1.default, 'classification_id');
    };
    return WarehouseEntryOrderProduct;
}(bookshelf_1.default.Model));
exports.WarehouseEntryOrderProduct = WarehouseEntryOrderProduct;
//# sourceMappingURL=WarehouseEntryOrder.js.map