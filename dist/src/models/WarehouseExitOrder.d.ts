import bookshelf from '../config/bookshelf';
declare class WarehouseExitOrderFunctions extends bookshelf.Model<WarehouseExitOrderFunctions> {
    get requireFetch(): boolean;
    get tableName(): string;
    debit(): any;
    dPartners(): any;
    dAnaliticGroup2(): any;
    dAnaliticGroup1(): any;
    credit(): any;
    cPartners(): any;
    cAnaliticGroup2(): any;
    cAnaliticGroup1(): any;
}
declare class WarehouseExitOrderProduct extends bookshelf.Model<WarehouseExitOrderProduct> {
    get requireFetch(): boolean;
    get tableName(): string;
    materialValue(): any;
    accounts(): any;
    classification(): any;
}
declare class WarehouseExitOrder extends bookshelf.Model<WarehouseExitOrder> {
    get requireFetch(): boolean;
    get tableName(): string;
    warehouse(): any;
    partners(): any;
    partnersAccount(): any;
    prepaidAccount(): any;
    analiticGroup2(): any;
    analiticGroup1(): any;
    warehouseExitOrderFunctions(): any;
    warehouseExitOrderProduct(): any;
}
export { WarehouseExitOrder, WarehouseExitOrderFunctions, WarehouseExitOrderProduct };
