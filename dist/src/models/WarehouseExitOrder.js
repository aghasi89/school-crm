"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bookshelf_1 = tslib_1.__importDefault(require("../config/bookshelf"));
var Table_1 = tslib_1.__importDefault(require("../resources/enums/Table"));
var Classification_1 = tslib_1.__importDefault(require("./Classification"));
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("./AccountOfEmployeeCalculations"));
var MaterialValue_1 = tslib_1.__importDefault(require("./MaterialValue"));
var Warehouse_1 = tslib_1.__importDefault(require("./Warehouse"));
var Partners_1 = tslib_1.__importDefault(require("./Partners"));
var AnaliticGroup1_1 = tslib_1.__importDefault(require("./AnaliticGroup1"));
var AnaliticGroup2_1 = tslib_1.__importDefault(require("./AnaliticGroup2"));
var WarehouseExitOrderFunctions = (function (_super) {
    tslib_1.__extends(WarehouseExitOrderFunctions, _super);
    function WarehouseExitOrderFunctions() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(WarehouseExitOrderFunctions.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WarehouseExitOrderFunctions.prototype, "tableName", {
        get: function () {
            return Table_1.default.WAREHOUSE_EXIT_ORDER_FUNCTIONS;
        },
        enumerable: true,
        configurable: true
    });
    WarehouseExitOrderFunctions.prototype.debit = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'debit_id');
    };
    WarehouseExitOrderFunctions.prototype.dPartners = function () {
        return this.belongsTo(Partners_1.default, 'd_partners_id');
    };
    WarehouseExitOrderFunctions.prototype.dAnaliticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'd_analitic_group_2_id');
    };
    WarehouseExitOrderFunctions.prototype.dAnaliticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'd_analitic_group_1_id');
    };
    WarehouseExitOrderFunctions.prototype.credit = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'credit_id');
    };
    WarehouseExitOrderFunctions.prototype.cPartners = function () {
        return this.belongsTo(Partners_1.default, 'c_partners_id');
    };
    WarehouseExitOrderFunctions.prototype.cAnaliticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'c_analitic_group_2_id');
    };
    WarehouseExitOrderFunctions.prototype.cAnaliticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'c_analitic_group_1_id');
    };
    return WarehouseExitOrderFunctions;
}(bookshelf_1.default.Model));
exports.WarehouseExitOrderFunctions = WarehouseExitOrderFunctions;
var WarehouseExitOrderProduct = (function (_super) {
    tslib_1.__extends(WarehouseExitOrderProduct, _super);
    function WarehouseExitOrderProduct() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(WarehouseExitOrderProduct.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WarehouseExitOrderProduct.prototype, "tableName", {
        get: function () {
            return Table_1.default.WAREHOUSE_EXIT_ORDER_PRODUCT;
        },
        enumerable: true,
        configurable: true
    });
    WarehouseExitOrderProduct.prototype.materialValue = function () {
        return this.belongsTo(MaterialValue_1.default, 'material_value_id');
    };
    WarehouseExitOrderProduct.prototype.accounts = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'accounts_id');
    };
    WarehouseExitOrderProduct.prototype.classification = function () {
        return this.belongsTo(Classification_1.default, 'classification_id');
    };
    return WarehouseExitOrderProduct;
}(bookshelf_1.default.Model));
exports.WarehouseExitOrderProduct = WarehouseExitOrderProduct;
var WarehouseExitOrder = (function (_super) {
    tslib_1.__extends(WarehouseExitOrder, _super);
    function WarehouseExitOrder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(WarehouseExitOrder.prototype, "requireFetch", {
        get: function () {
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WarehouseExitOrder.prototype, "tableName", {
        get: function () {
            return Table_1.default.WAREHOUSE_EXIT_ORDER;
        },
        enumerable: true,
        configurable: true
    });
    WarehouseExitOrder.prototype.warehouse = function () {
        return this.belongsTo(Warehouse_1.default, 'warehouse_id');
    };
    WarehouseExitOrder.prototype.partners = function () {
        return this.belongsTo(Partners_1.default, 'partners_id');
    };
    WarehouseExitOrder.prototype.partnersAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'partners_account_id');
    };
    WarehouseExitOrder.prototype.prepaidAccount = function () {
        return this.belongsTo(AccountOfEmployeeCalculations_1.default, 'prepaid_account_id');
    };
    WarehouseExitOrder.prototype.analiticGroup2 = function () {
        return this.belongsTo(AnaliticGroup2_1.default, 'analitic_group_2_id');
    };
    WarehouseExitOrder.prototype.analiticGroup1 = function () {
        return this.belongsTo(AnaliticGroup1_1.default, 'analitic_group_1_id');
    };
    WarehouseExitOrder.prototype.warehouseExitOrderFunctions = function () {
        return this.hasMany(WarehouseExitOrderFunctions, 'warehouse_exit_order_id');
    };
    WarehouseExitOrder.prototype.warehouseExitOrderProduct = function () {
        return this.hasMany(WarehouseExitOrderProduct, 'warehouse_exit_order_id');
    };
    return WarehouseExitOrder;
}(bookshelf_1.default.Model));
exports.WarehouseExitOrder = WarehouseExitOrder;
//# sourceMappingURL=WarehouseExitOrder.js.map