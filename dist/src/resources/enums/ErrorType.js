"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorType;
(function (ErrorType) {
    ErrorType["INVALID"] = "JsonWebTokenError";
    ErrorType["EXPIRED"] = "TokenExpiredError";
    ErrorType["NO_ROWS_UPDATED_ERROR"] = "No Rows Updated";
})(ErrorType || (ErrorType = {}));
exports.default = ErrorType;
//# sourceMappingURL=ErrorType.js.map