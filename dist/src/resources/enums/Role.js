"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Role;
(function (Role) {
    Role[Role["ADMIN"] = 1] = "ADMIN";
    Role[Role["NORMAL_USER"] = 2] = "NORMAL_USER";
})(Role || (Role = {}));
exports.default = Role;
//# sourceMappingURL=Role.js.map