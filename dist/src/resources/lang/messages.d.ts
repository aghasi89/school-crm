export declare namespace auth {
    export const loginSuccess: string;
    export const logoutSuccess: string;
    export const invalidCredentials: string;
    export const accessTokenRefreshed: string;
}
export declare const users: {
    "insert": string;
    "fetch": string;
    "delete": string;
    "fetchAll": string;
    "me": string;
    "update": string;
};
export declare const bank: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const bankBranche: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
};
export declare const billingMethod: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const classification: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const materialValue: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const materialValueGroup: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const warehouse: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const serviceType: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
};
export declare const serviceName: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
};
export declare const billingAccount: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const subdivision: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const measurementUnit: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const coWorkers: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
};
export declare const position: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
};
export declare const profession: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
};
export declare const expenseAccount: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const typeOfIncome: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const typeOfVacation: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
};
export declare const addition: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
};
export declare const tabel: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const employee: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const contract: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const group: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const headPosition: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const accountantPosition: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const partners: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const currency: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const calculationsType: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const accountOfEmplCalculations: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const analiticGroup1: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const analiticGroup2: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const subsection: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const typesOfActions: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const warehouseEntryOrder: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const warehouseExitOrder: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const services: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const cashRegister: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
    "count": string;
};
export declare const priceOfServices: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
};
export declare const formulas: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
};
export declare const structuralSubdivision: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "edit": string;
};
export declare const billingAccountInBanks: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const receivedServices: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const cashRegisterEntry: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const operations: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const accountProduct: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const cashRegisterExit: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
export declare const hmxProfitTax: {
    "fetchAll": string;
    "insert": string;
    "fetch": string;
    "delete": string;
    "count": string;
    "edit": string;
};
