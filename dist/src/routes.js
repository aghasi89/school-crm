"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var express_1 = require("express");
var validate = tslib_1.__importStar(require("./middlewares/validate"));
var homeController = tslib_1.__importStar(require("./controllers/home"));
var userController = tslib_1.__importStar(require("./controllers/user"));
var authController = tslib_1.__importStar(require("./controllers/auth"));
var authenticate_1 = require("./middlewares/authenticate");
var loginRequest_1 = require("./validators/loginRequest");
var userRequest_1 = require("./validators/userRequest");
var userDataRequest_1 = require("./validators/userDataRequest");
var resetRequest_1 = require("./validators/resetRequest");
var checkTokenRequest_1 = require("./validators/checkTokenRequest");
var resetPasswordRequest_1 = require("./validators/resetPasswordRequest");
var paramRequest_1 = require("./validators/paramRequest");
var paginatRequest_1 = require("./validators/paginatRequest");
var informationGetRequest_1 = require("./validators/informationGetRequest");
var validateRefreshToken_1 = tslib_1.__importDefault(require("./middlewares/validateRefreshToken"));
var bankController = tslib_1.__importStar(require("./controllers/bank"));
var bankRequest_1 = require("./validators/bankRequest");
var bankBrancheController = tslib_1.__importStar(require("./controllers/bankBranche"));
var bankBrancheRequest_1 = require("./validators/bankBrancheRequest");
var billingMethodController = tslib_1.__importStar(require("./controllers/billingMethod"));
var billingMethodRequest_1 = require("./validators/billingMethodRequest");
var classificationController = tslib_1.__importStar(require("./controllers/classification"));
var classificationRequest_1 = require("./validators/classificationRequest");
var materialValueGroupController = tslib_1.__importStar(require("./controllers/materialValueGroup"));
var materialValueGroupRequest_1 = require("./validators/materialValueGroupRequest");
var materialValueController = tslib_1.__importStar(require("./controllers/materialValue"));
var materialValueRequest_1 = require("./validators/materialValueRequest");
var warehosueController = tslib_1.__importStar(require("./controllers/warehosue"));
var warehouseRequest_1 = require("./validators/warehouseRequest");
var serviceTypeController = tslib_1.__importStar(require("./controllers/serviceType"));
var serviceTypeRequest_1 = require("./validators/serviceTypeRequest");
var serviceNameController = tslib_1.__importStar(require("./controllers/serviceName"));
var serviceNameRequest_1 = require("./validators/serviceNameRequest");
var billingAccountController = tslib_1.__importStar(require("./controllers/billingAccount"));
var billingAccountRequest_1 = require("./validators/billingAccountRequest");
var subdivisionController = tslib_1.__importStar(require("./controllers/subdivision"));
var subdivisionRequest_1 = require("./validators/subdivisionRequest");
var measurementUnitController = tslib_1.__importStar(require("./controllers/measurementUnit"));
var measurementUnitRequest_1 = require("./validators/measurementUnitRequest");
var coWorkersController = tslib_1.__importStar(require("./controllers/coWorkers"));
var coWorkersRequest_1 = require("./validators/coWorkersRequest");
var positionController = tslib_1.__importStar(require("./controllers/position"));
var positionRequest_1 = require("./validators/positionRequest");
var professionController = tslib_1.__importStar(require("./controllers/profession"));
var professionRequest_1 = require("./validators/professionRequest");
var expenseAccountController = tslib_1.__importStar(require("./controllers/expenseAccount"));
var expenseAccountRequest_1 = require("./validators/expenseAccountRequest");
var typeOfIncomeController = tslib_1.__importStar(require("./controllers/typeOfIncome"));
var typeOfIncomeRequest_1 = require("./validators/typeOfIncomeRequest");
var typeOfVacationController = tslib_1.__importStar(require("./controllers/typeOfVacation"));
var typeOfVacationRequest_1 = require("./validators/typeOfVacationRequest");
var additionController = tslib_1.__importStar(require("./controllers/addition"));
var additionRequest_1 = require("./validators/additionRequest");
var tabelController = tslib_1.__importStar(require("./controllers/tabel"));
var tabelRequest_1 = require("./validators/tabelRequest");
var employeeController = tslib_1.__importStar(require("./controllers/employee"));
var employeeRequest_1 = require("./validators/employeeRequest");
var contractController = tslib_1.__importStar(require("./controllers/contract"));
var contractRequest_1 = require("./validators/contractRequest");
var groupController = tslib_1.__importStar(require("./controllers/group"));
var groupRequest_1 = require("./validators/groupRequest");
var headPositionController = tslib_1.__importStar(require("./controllers/headPosition"));
var headPositionRequest_1 = require("./validators/headPositionRequest");
var accountantPositionController = tslib_1.__importStar(require("./controllers/accountantPosition"));
var accountantPositionRequest_1 = require("./validators/accountantPositionRequest");
var partnersController = tslib_1.__importStar(require("./controllers/partners"));
var partnersRequest_1 = require("./validators/partnersRequest");
var currencyController = tslib_1.__importStar(require("./controllers/currency"));
var currencyRequest_1 = require("./validators/currencyRequest");
var calculationsTypeController = tslib_1.__importStar(require("./controllers/calculationsType"));
var calculationsTypeRequest_1 = require("./validators/calculationsTypeRequest");
var accountOfEmplCalculationsController = tslib_1.__importStar(require("./controllers/accountOfEmplCalculations"));
var accountOfEmplCalculationsRequest_1 = require("./validators/accountOfEmplCalculationsRequest");
var analiticGroup1Controller = tslib_1.__importStar(require("./controllers/analiticGroup1"));
var AnaliticGroup1Request_1 = require("./validators/AnaliticGroup1Request");
var analiticGroup2Controller = tslib_1.__importStar(require("./controllers/analiticGroup2"));
var AnaliticGroup2Request_1 = require("./validators/AnaliticGroup2Request");
var subsectionController = tslib_1.__importStar(require("./controllers/subsection"));
var subsectionRequest_1 = require("./validators/subsectionRequest");
var typesOfActionsController = tslib_1.__importStar(require("./controllers/typesOfActions"));
var typesOfActionsRequest_1 = require("./validators/typesOfActionsRequest");
var cashRegisterController = tslib_1.__importStar(require("./controllers/cashRegister"));
var cashRegisterRequest_1 = require("./validators/cashRegisterRequest");
var cashRegisterEntryController = tslib_1.__importStar(require("./controllers/cashRegisterEntry"));
var cashRegisterEntryRequest_1 = require("./validators/cashRegisterEntryRequest");
var cashRegisterExitController = tslib_1.__importStar(require("./controllers/cashRegisterExit"));
var cashRegisterExitRequest_1 = require("./validators/cashRegisterExitRequest");
var warehouseEntryOrdersController = tslib_1.__importStar(require("./controllers/warehouseEntryOrders"));
var warehouseEntryOrderRequest_1 = require("./validators/warehouseEntryOrderRequest");
var servicesController = tslib_1.__importStar(require("./controllers/services"));
var servicesRequest_1 = require("./validators/servicesRequest");
var warehouseExitOrderController = tslib_1.__importStar(require("./controllers/warehouseExitOrder"));
var warehouseExitOrderRequest_1 = require("./validators/warehouseExitOrderRequest");
var priceOfServicesController = tslib_1.__importStar(require("./controllers/priceOfServices"));
var priceOfServicesRequest_1 = require("./validators/priceOfServicesRequest");
var formulasController = tslib_1.__importStar(require("./controllers/formulas"));
var formulasRequest_1 = require("./validators/formulasRequest");
var structuralSubdivisionController = tslib_1.__importStar(require("./controllers/structuralSubdivision"));
var structuralSubdivisionRequest_1 = require("./validators/structuralSubdivisionRequest");
var billingAccountInBanksController = tslib_1.__importStar(require("./controllers/billingAccountInBanks"));
var billingAccountInBanksRequest_1 = require("./validators/billingAccountInBanksRequest");
var receivedServicesController = tslib_1.__importStar(require("./controllers/receivedServices"));
var receivedServicesRequest_1 = require("./validators/receivedServicesRequest");
var operationsController = tslib_1.__importStar(require("./controllers/operations"));
var operationsRequest_1 = require("./validators/operationsRequest");
var accountProductController = tslib_1.__importStar(require("./controllers/accountProduct"));
var accountProductRequest_1 = require("./validators/accountProductRequest");
var hmxProfitTaxController = tslib_1.__importStar(require("./controllers/hmxProfitTax"));
var hmxProfitTaxRequest_1 = require("./validators/hmxProfitTaxRequest");
var localDbController = tslib_1.__importStar(require("./controllers/localDb"));
var router = express_1.Router();
router.get('/', homeController.index);
router.post('/login', validate.schema(loginRequest_1.loginSchema), authController.login);
router.post('/refresh', validateRefreshToken_1.default, authController.refresh);
router.post('/logout', validateRefreshToken_1.default, authController.logout);
router.get('/users', authenticate_1.authenticate, userController.index);
router.get('/users/me', authenticate_1.authenticate, userController.me);
router.put('/users/me', validate.schema(userDataRequest_1.userDataPUTSchema), authenticate_1.authenticate, userController.update);
router.post('/users/reset', validate.schema(resetRequest_1.resetPOSTSchema), userController.resetPassword);
router.post('/users/check/reset/token', validate.schema(checkTokenRequest_1.checkTokenPOSTSchema), userController.checkToken);
router.post('/users/reset/password', validate.schema(resetPasswordRequest_1.resetPasswordPOSTSchema), userController.checkTokenLong, userController.newPassword);
router.post('/users', authenticate_1.authenticateAdmin, validate.schema(userRequest_1.userPOSTSchema), userController.store);
router.post('/users/admin', validate.schema(userRequest_1.userPOSTSchema), userController.storeAdmin);
router.get('/banks/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), bankController.index);
router.get('/banks', bankController.count);
router.post('/bank', validate.schema(bankRequest_1.bankPOSTSchema), bankController.store);
router.route('/bank/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), bankController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), bankController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), bankController.update);
router.get('/banks-branche', bankBrancheController.index);
router.post('/banks-branche', validate.schema(bankBrancheRequest_1.bankBranchePOSTSchema), bankBrancheController.store);
router.get('/billing-methods/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), billingMethodController.index);
router.get('/billing-methods', billingMethodController.count);
router.post('/billing-method', validate.schema(billingMethodRequest_1.billingMethodPOSTSchema), billingMethodController.store);
router.route('/billing-method/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), billingMethodController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), billingMethodController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), billingMethodController.update);
router.get('/analitic-groups-one/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), analiticGroup1Controller.index);
router.get('/analitic-groups-one', analiticGroup1Controller.count);
router.post('/analitic-group-one', validate.schema(AnaliticGroup1Request_1.analiticGroup1POSTSchema), analiticGroup1Controller.store);
router.route('/analitic-group-one/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), analiticGroup1Controller.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), analiticGroup1Controller.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(AnaliticGroup1Request_1.analiticGroup1POSTSchema), analiticGroup1Controller.update);
router.get('/analitic-groups-two/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), analiticGroup2Controller.index);
router.get('/analitic-groups-two', analiticGroup2Controller.count);
router.post('/analitic-group-two', validate.schema(AnaliticGroup2Request_1.analiticGroup2POSTSchema), analiticGroup2Controller.store);
router.route('/analitic-group-two/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), analiticGroup2Controller.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), analiticGroup2Controller.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(AnaliticGroup2Request_1.analiticGroup2POSTSchema), analiticGroup2Controller.update);
router.get('/classifications/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), classificationController.index);
router.get('/classifications', classificationController.count);
router.post('/classification', validate.schema(classificationRequest_1.classificationPOSTSchema), classificationController.store);
router.route('/classification/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), classificationController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), classificationController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(classificationRequest_1.classificationPOSTSchema), classificationController.update);
router.get('/material-value-groups/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), materialValueGroupController.index);
router.get('/material-value-groups', materialValueGroupController.count);
router.post('/material-value-group', validate.schema(materialValueGroupRequest_1.materialValueGroupPOSTSchema), materialValueGroupController.store);
router.route('/material-value-group/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), materialValueGroupController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), materialValueGroupController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(materialValueGroupRequest_1.materialValueGroupPOSTSchema), materialValueGroupController.update);
router.get('/material-values/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), materialValueController.index);
router.get('/material-values', materialValueController.count);
router.post('/material-value', validate.schema(materialValueRequest_1.materialValuePOSTSchema), materialValueController.store);
router.route('/material-value/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), materialValueController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), materialValueController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(materialValueRequest_1.materialValuePOSTSchema), materialValueController.update);
router.get('/warehouses/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), warehosueController.index);
router.get('/warehouses', warehosueController.count);
router.post('/warehouse', validate.schema(warehouseRequest_1.warehousePOSTSchema), warehosueController.store);
router.route('/warehouse/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), warehosueController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), warehosueController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(warehouseRequest_1.warehousePOSTSchema), warehosueController.update);
router.get('/service-type', serviceTypeController.index);
router.post('/service-type', validate.schema(serviceTypeRequest_1.seviceTypePOSTSchema), serviceTypeController.store);
router.get('/service-name', serviceNameController.index);
router.post('/service-name', validate.schema(serviceNameRequest_1.seviceNamePOSTSchema), serviceNameController.store);
router.get('/billing-accounts/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), billingAccountController.index);
router.get("/billing-accounts", billingAccountController.count);
router.post("/billing-account", validate.schema(billingAccountRequest_1.billingAccountPOSTSchema), billingAccountController.store);
router.route('/billing-account/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), billingAccountController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), billingAccountController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), billingAccountController.update);
router.get('/type-of-actions/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), typesOfActionsController.index);
router.get("/type-of-actions", typesOfActionsController.count);
router.post("/type-of-action", validate.schema(typesOfActionsRequest_1.typeOfActionsPOSTSchema), typesOfActionsController.store);
router.route('/type-of-action/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), typesOfActionsController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), typesOfActionsController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(typesOfActionsRequest_1.typeOfActionsPOSTSchema), typesOfActionsController.update);
router.get('/subdivisions/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), subdivisionController.index);
router.get("/subdivisions", subdivisionController.count);
router.post("/subdivision", validate.schema(subdivisionRequest_1.subdivisionPOSTSchema), subdivisionController.store);
router.route('/subdivision/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), subdivisionController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), subdivisionController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), subdivisionController.update);
router.get('/subsections/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), subsectionController.index);
router.get('/subsections-type/:typeId', validate.params(paginatRequest_1.paginatSchema), subsectionController.indexByType);
router.get("/subsections", subsectionController.count);
router.post("/subsection", validate.schema(subsectionRequest_1.subsectionPOSTSchema), subsectionController.store);
router.route('/subsection/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), subsectionController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), subsectionController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(subsectionRequest_1.subsectionPOSTSchema), subsectionController.update);
router.get('/measurement-units/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), measurementUnitController.index);
router.get("/measurement-units", measurementUnitController.count);
router.post('/measurement-unit', validate.schema(measurementUnitRequest_1.measurementUnitPOSTSchema), measurementUnitController.store);
router.route('/measurement-unit/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), measurementUnitController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), measurementUnitController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(measurementUnitRequest_1.measurementUnitPOSTSchema), measurementUnitController.update);
router.route('/co-workers')
    .get(coWorkersController.index)
    .post(validate.schema(coWorkersRequest_1.coWorkersPOSTSchema), coWorkersController.store);
router.get('/positions/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), positionController.index);
router.get("/positions", positionController.count);
router.post('/position', validate.schema(positionRequest_1.positionPOSTSchema), positionController.store);
router.route('/position/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), positionController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), positionController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(positionRequest_1.positionPOSTSchema), positionController.update);
router.get('/professions/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), professionController.index);
router.get("/professions", professionController.count);
router.post('/profession', validate.schema(professionRequest_1.professionPOSTSchema), professionController.store);
router.route('/profession/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), professionController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), professionController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(professionRequest_1.professionPOSTSchema), professionController.update);
router.get('/expense-accounts/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), expenseAccountController.index);
router.get("/expense-accounts", expenseAccountController.count);
router.post('/expense-account', validate.schema(expenseAccountRequest_1.expenseAccountPOSTSchema), expenseAccountController.store);
router.route('/expense-account/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), expenseAccountController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), expenseAccountController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(expenseAccountRequest_1.expenseAccountPOSTSchema), expenseAccountController.update);
router.get('/type-of-incomes/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), typeOfIncomeController.index);
router.get("/type-of-incomes", typeOfIncomeController.count);
router.post('/type-of-income', validate.schema(typeOfIncomeRequest_1.typeOfIncomePOSTSchema), typeOfIncomeController.store);
router.route('/type-of-income/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), typeOfIncomeController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), typeOfIncomeController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(typeOfIncomeRequest_1.typeOfIncomePOSTSchema), typeOfIncomeController.update);
router.get('/type-of-vacations/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), typeOfVacationController.index);
router.get("/type-of-vacations", typeOfVacationController.count);
router.post('/type-of-vacation', validate.schema(typeOfIncomeRequest_1.typeOfIncomePOSTSchema), typeOfVacationController.store);
router.route('/type-of-vacation/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), typeOfVacationController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), typeOfVacationController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(typeOfVacationRequest_1.typeOfVacationPOSTSchema), typeOfVacationController.update);
router.get('/additions/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), additionController.index);
router.get("/additions", additionController.count);
router.post('/addition', validate.schema(additionRequest_1.additionPOSTSchema), additionController.store);
router.route('/addition/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), additionController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), additionController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(additionRequest_1.additionPOSTSchema), additionController.update);
router.get('/tabels/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), tabelController.index);
router.get('/tabels', tabelController.count);
router.post('/tabel', validate.schema(tabelRequest_1.tabelPOSTSchema), tabelController.store);
router.route('/tabel/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), tabelController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), tabelController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(tabelRequest_1.tabelPOSTSchema), tabelController.update);
router.get('/employees/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), employeeController.index);
router.get("/employees", employeeController.count);
router.post('/employee', validate.schema(employeeRequest_1.employeePOSTSchema), employeeController.store);
router.route('/employee/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), employeeController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), employeeController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(employeeRequest_1.employeePOSTSchema), employeeController.update);
router.get('/contracts/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), contractController.index);
router.get("/contracts", contractController.count);
router.post('/contract', validate.schema(contractRequest_1.contractPOSTSchema), contractController.store);
router.route('/contract/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), contractController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), contractController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(contractRequest_1.contractPOSTSchema), contractController.update);
router.get('/groups/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), groupController.index);
router.get("/groups", groupController.count);
router.post('/group', validate.schema(groupRequest_1.groupPOSTSchema), groupController.store);
router.route('/group/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), groupController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), groupController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(groupRequest_1.groupPOSTSchema), groupController.update);
router.get('/head-positions/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), headPositionController.index);
router.get("/head-positions", headPositionController.count);
router.post('/head-position', validate.schema(headPositionRequest_1.headPositionPOSTSchema), headPositionController.store);
router.route('/head-positions/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), headPositionController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), headPositionController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(headPositionRequest_1.headPositionPOSTSchema), headPositionController.update);
router.get('/account-positions/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), accountantPositionController.index);
router.get("/account-positions", accountantPositionController.count);
router.post('/account-position', validate.schema(accountantPositionRequest_1.accountantPositionPOSTSchema), accountantPositionController.store);
router.route('/accountad-positions/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), accountantPositionController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), accountantPositionController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(accountantPositionRequest_1.accountantPositionPOSTSchema), accountantPositionController.update);
router.get('/partners/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), partnersController.index);
router.get("/partners", partnersController.count);
router.post('/partner', validate.schema(partnersRequest_1.partnersPOSTSchema), partnersController.store);
router.route('/partner/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), partnersController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), partnersController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(partnersRequest_1.partnersPOSTSchema), partnersController.update);
router.get('/currencies/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), currencyController.index);
router.get("/currencies", currencyController.count);
router.post('/currency', validate.schema(currencyRequest_1.currencyPOSTSchema), currencyController.store);
router.route('/currency/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), currencyController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), currencyController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(currencyRequest_1.currencyPOSTSchema), currencyController.update);
router.get('/calculations-types/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), calculationsTypeController.index);
router.get("/calculations-types/", calculationsTypeController.count);
router.post('/calculations-type', validate.schema(calculationsTypeRequest_1.calculationsTypePositionPOSTSchema), calculationsTypeController.store);
router.route('/calculations-type/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), calculationsTypeController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), calculationsTypeController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(calculationsTypeRequest_1.calculationsTypePositionPOSTSchema), calculationsTypeController.update);
router.get('/account-plans/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), accountOfEmplCalculationsController.index);
router.get("/account-plans/", accountOfEmplCalculationsController.count);
router.post('/account-plan', validate.schema(accountOfEmplCalculationsRequest_1.accountOfEmplCalculationsPOSTSchema), accountOfEmplCalculationsController.store);
router.route('/account-plan/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), accountOfEmplCalculationsController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), accountOfEmplCalculationsController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(accountOfEmplCalculationsRequest_1.accountOfEmplCalculationsPOSTSchema), accountOfEmplCalculationsController.update);
router.post('/warehouse-entry-orders-function/', validate.schema(warehouseEntryOrderRequest_1.warehouseEntryOrderProduct), warehouseEntryOrdersController.getWarehouseEntryOrderProductPayload);
router.get('/warehouse-entry-orders/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), warehouseEntryOrdersController.index);
router.get("/warehouse-entry-orders/", warehouseEntryOrdersController.count);
router.post('/warehouse-entry-order/', validate.schema(warehouseEntryOrderRequest_1.warehouseEntryOrderPOSTSchema), warehouseEntryOrdersController.store);
router.route('/warehouse-entry-order/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), warehouseEntryOrdersController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), warehouseEntryOrdersController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(warehouseEntryOrderRequest_1.warehouseEntryOrderPOSTSchema), warehouseEntryOrdersController.update);
router.post('/warehouse-exit-orders-function/', validate.schema(warehouseExitOrderRequest_1.warehouseExitOrderProduct), warehouseExitOrderController.getWarehouseExitOrderProductPayload);
router.get('/warehouse-exit-orders/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), warehouseExitOrderController.index);
router.get("/warehouse-exit-orders/", warehouseExitOrderController.count);
router.post('/warehouse-exit-order/', validate.schema(warehouseExitOrderRequest_1.warehouseExitOrderPOSTSchema), warehouseExitOrderController.store);
router.route('/warehouse-exit-order/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), warehouseExitOrderController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), warehouseExitOrderController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(warehouseExitOrderRequest_1.warehouseExitOrderPOSTSchema), warehouseExitOrderController.update);
router.get('/services/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), servicesController.index);
router.get("/services/", servicesController.count);
router.post('/service/', validate.schema(servicesRequest_1.servicesPOSTSchema), servicesController.store);
router.route('/service/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), servicesController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), servicesController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(servicesRequest_1.servicesPOSTSchema), servicesController.update);
router.get('/cash-registers/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), cashRegisterController.index);
router.get("/cash-registers/", cashRegisterController.count);
router.post('/cash-register/', validate.schema(cashRegisterRequest_1.cashRegisterPOSTSchema), cashRegisterController.store);
router.route('/cash-register/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), cashRegisterController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), cashRegisterController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(cashRegisterRequest_1.cashRegisterPOSTSchema), cashRegisterController.update);
router.get('/price-of-services/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), priceOfServicesController.index);
router.get("/price-of-services/", priceOfServicesController.count);
router.post('/price-of-service/', validate.schema(priceOfServicesRequest_1.priceOfServicesPOSTSchema), priceOfServicesController.store);
router.route('/price-of-service/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), priceOfServicesController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), priceOfServicesController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(priceOfServicesRequest_1.priceOfServicesPOSTSchema), priceOfServicesController.update);
router.get('/formulas/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), formulasController.index);
router.get("/formulas/", formulasController.count);
router.post('/formula/', validate.schema(formulasRequest_1.formulasPOSTSchema), formulasController.store);
router.route('/formula/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), formulasController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), formulasController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(formulasRequest_1.formulasPOSTSchema), formulasController.update);
router.get('/structural-subdivisions/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), structuralSubdivisionController.index);
router.get("/structural-subdivisions/", structuralSubdivisionController.count);
router.post('/structural-subdivision/', validate.schema(structuralSubdivisionRequest_1.structuralSubdivisionPOSTSchema), structuralSubdivisionController.store);
router.route('/structural-subdivision/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), structuralSubdivisionController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), structuralSubdivisionController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(structuralSubdivisionRequest_1.structuralSubdivisionPOSTSchema), structuralSubdivisionController.update);
router.get('/billing-accounts-in-banks/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), billingAccountInBanksController.index);
router.get("/billing-accounts-in-banks/", billingAccountInBanksController.count);
router.post('/billing-account-in-banks/', validate.schema(billingAccountInBanksRequest_1.billingAccountInBanksPOSTSchema), billingAccountInBanksController.store);
router.route('/billing-account-in-banks/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), billingAccountInBanksController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), billingAccountInBanksController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(billingAccountInBanksRequest_1.billingAccountInBanksPOSTSchema), billingAccountInBanksController.update);
router.get('/received-services/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), receivedServicesController.index);
router.get("/received-services/", warehouseExitOrderController.count);
router.post('/received-service/', validate.schema(receivedServicesRequest_1.receivedServicesPOSTSchema), receivedServicesController.store);
router.route('/received-service/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), receivedServicesController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), receivedServicesController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(receivedServicesRequest_1.receivedServicesPOSTSchema), receivedServicesController.update);
router.get('/cash-register-entries/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), cashRegisterEntryController.index);
router.get("/cash-register-entries/", cashRegisterEntryController.count);
router.post('/cash-register-entry/', validate.schema(cashRegisterEntryRequest_1.cashRegisterEntryPOSTSchema), cashRegisterEntryController.store);
router.route('/cash-register-entry/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), receivedServicesController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), receivedServicesController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(cashRegisterEntryRequest_1.cashRegisterEntryPOSTSchema), cashRegisterEntryController.update);
router.get('/cash-register-exits/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), cashRegisterExitController.index);
router.get("/cash-register-exits/", cashRegisterExitController.count);
router.post('/cash-register-exit/', validate.schema(cashRegisterExitRequest_1.cashRegisterExitPOSTSchema), cashRegisterExitController.store);
router.route('/cash-register-exit/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), cashRegisterExitController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), cashRegisterExitController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(cashRegisterExitRequest_1.cashRegisterExitPOSTSchema), cashRegisterExitController.update);
router.get('/operations/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), operationsController.index);
router.get("/operations/", operationsController.count);
router.post('/operation/', validate.schema(operationsRequest_1.operationsPOSTSchema), operationsController.store);
router.route('/operation/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), operationsController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), operationsController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(operationsRequest_1.operationsPOSTSchema), operationsController.update);
router.get('/account-products/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), accountProductController.index);
router.get("/account-products/", accountProductController.count);
router.post('/account-product/', validate.schema(accountProductRequest_1.accountProductPOSTSchema), accountProductController.store);
router.route('/account-product/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), accountProductController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), accountProductController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(accountProductRequest_1.accountProductPOSTSchema), accountProductController.update);
router.get('/hmx-profit-taxs/:limit/:offset', validate.params(paginatRequest_1.paginatSchema), hmxProfitTaxController.index);
router.get("/hmx-profit-taxs/", hmxProfitTaxController.count);
router.post('/hmx-profit-tax/', validate.schema(hmxProfitTaxRequest_1.hmxProfitTaxPOSTSchema), hmxProfitTaxController.store);
router.route('/hmx-profit-tax/:id')
    .get(validate.params(paramRequest_1.paramValidationSchema), hmxProfitTaxController.getOne)
    .delete(validate.params(paramRequest_1.paramValidationSchema), hmxProfitTaxController.destroy)
    .put(validate.params(paramRequest_1.paramValidationSchema), validate.schema(hmxProfitTaxRequest_1.hmxProfitTaxPOSTSchema), hmxProfitTaxController.update);
router.get("/information-by-type/:key", validate.params(informationGetRequest_1.informationGetRequest), localDbController.getOne);
router.get('/generet-code/:table/:colum', userController.getUniqueValue);
exports.default = router;
//# sourceMappingURL=routes.js.map