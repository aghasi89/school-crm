import AccountOfEmplCalculationsDetail from '../domain/entities/AccountOfEmployeeCalculationsDetail';
import AccountOfEmplCalculationsPayload from '../domain/requests/AccountOfEmployeeCalculationsPayload';
export declare function fetchAll(limit: number, offset: number): Promise<AccountOfEmplCalculationsDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: AccountOfEmplCalculationsPayload): Promise<AccountOfEmplCalculationsPayload>;
export declare function getById(id: number): Promise<AccountOfEmplCalculationsDetail>;
export declare function destroy(id: number): Promise<AccountOfEmplCalculationsDetail>;
export declare function update(id: number, params: AccountOfEmplCalculationsPayload): Promise<AccountOfEmplCalculationsDetail>;
