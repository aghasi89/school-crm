"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var AccountOfEmployeeCalculations_1 = tslib_1.__importDefault(require("../models/AccountOfEmployeeCalculations"));
var CurrencyEmploye_1 = tslib_1.__importDefault(require("../models/CurrencyEmploye"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var object = tslib_1.__importStar(require("../utils/object"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _accountOfEmplCalculationss, accountOfEmplCalculationss, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, AccountOfEmployeeCalculations_1.default.fetchAll()];
                case 1:
                    _accountOfEmplCalculationss = _a.sent();
                    if (!limit || limit == 0) {
                        limit = Number(_accountOfEmplCalculationss.count());
                        offset = 0;
                    }
                    console.log(limit, offset);
                    return [4, (_accountOfEmplCalculationss).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({ withRelated: ['currencies', 'calculationsType', 'acumulatedAccount'] })];
                case 2:
                    accountOfEmplCalculationss = _a.sent();
                    res = transform_1.default(accountOfEmplCalculationss.serialize(), function (accountOfEmplCalculations) { return ({
                        id: accountOfEmplCalculations.id,
                        name: accountOfEmplCalculations.name,
                        accountingByPartners: accountOfEmplCalculations.accountingByPartners,
                        acumulatedAccountId: accountOfEmplCalculations.acumulatedAccountId,
                        analyticalGroup1: accountOfEmplCalculations.analyticalGroup1,
                        analyticalGroup2: accountOfEmplCalculations.analyticalGroup2,
                        offBalanceSheet: accountOfEmplCalculations.offBalanceSheet,
                        calculationsTypeId: accountOfEmplCalculations.calculationsTypeId,
                        account: accountOfEmplCalculations.account,
                        isAccumulatedAccount: accountOfEmplCalculations.isAccumulatedAccount,
                        acumulatedAccount: accountOfEmplCalculations.acumulatedAccount,
                        calculationsType: accountOfEmplCalculations.calculationsType
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, AccountOfEmployeeCalculations_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(param) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var currencies, accountOfEmplCalculations, currencies_1, currencies_1_1, billingAccount, e_1_1;
        var e_1, _a;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    currencies = param['currencies'];
                    delete param.currencies;
                    return [4, new AccountOfEmployeeCalculations_1.default(tslib_1.__assign({}, param)).save()];
                case 1:
                    accountOfEmplCalculations = (_b.sent()).serialize();
                    _b.label = 2;
                case 2:
                    _b.trys.push([2, 7, 8, 9]);
                    currencies_1 = tslib_1.__values(currencies), currencies_1_1 = currencies_1.next();
                    _b.label = 3;
                case 3:
                    if (!!currencies_1_1.done) return [3, 6];
                    billingAccount = currencies_1_1.value;
                    billingAccount['employeeCalculationsId'] = accountOfEmplCalculations['id'];
                    return [4, new CurrencyEmploye_1.default(tslib_1.__assign({}, billingAccount)).save()];
                case 4:
                    (_b.sent()).serialize();
                    _b.label = 5;
                case 5:
                    currencies_1_1 = currencies_1.next();
                    return [3, 3];
                case 6: return [3, 9];
                case 7:
                    e_1_1 = _b.sent();
                    e_1 = { error: e_1_1 };
                    return [3, 9];
                case 8:
                    try {
                        if (currencies_1_1 && !currencies_1_1.done && (_a = currencies_1.return)) _a.call(currencies_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                    return [7];
                case 9: return [2, object.camelize(accountOfEmplCalculations)];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var position;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AccountOfEmployeeCalculations_1.default({ id: id }).fetch({ withRelated: ['currencies', 'calculationsType', 'acumulatedAccount'] })];
                case 1:
                    position = (_a.sent());
                    if (position) {
                        return [2, position.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AccountOfEmployeeCalculations_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var currencies, currencies_2, currencies_2_1, billingAccount, error_1, e_2_1, accountOfEmplCalculations;
        var e_2, _a;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    currencies = params['currencies'];
                    delete params['currencies'];
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 12, 13, 14]);
                    currencies_2 = tslib_1.__values(currencies), currencies_2_1 = currencies_2.next();
                    _b.label = 2;
                case 2:
                    if (!!currencies_2_1.done) return [3, 11];
                    billingAccount = currencies_2_1.value;
                    billingAccount['employeeCalculationsId'] = id;
                    _b.label = 3;
                case 3:
                    _b.trys.push([3, 9, , 10]);
                    console.log(billingAccount);
                    if (!(billingAccount['status'] == 'unChanged')) return [3, 4];
                    return [3, 10];
                case 4:
                    if (!(billingAccount['status'] == 'deleted')) return [3, 6];
                    return [4, new CurrencyEmploye_1.default().where({ employee_calculations_id: id, currency_id: billingAccount['currencyId'] }).destroy()];
                case 5:
                    (_b.sent()).serialize();
                    return [3, 8];
                case 6:
                    delete billingAccount['status'];
                    return [4, new CurrencyEmploye_1.default(tslib_1.__assign({}, billingAccount)).save()];
                case 7:
                    (_b.sent()).serialize();
                    _b.label = 8;
                case 8: return [3, 10];
                case 9:
                    error_1 = _b.sent();
                    console.log(error_1);
                    return [3, 10];
                case 10:
                    currencies_2_1 = currencies_2.next();
                    return [3, 2];
                case 11: return [3, 14];
                case 12:
                    e_2_1 = _b.sent();
                    e_2 = { error: e_2_1 };
                    return [3, 14];
                case 13:
                    try {
                        if (currencies_2_1 && !currencies_2_1.done && (_a = currencies_2.return)) _a.call(currencies_2);
                    }
                    finally { if (e_2) throw e_2.error; }
                    return [7];
                case 14: return [4, new AccountOfEmployeeCalculations_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 15:
                    accountOfEmplCalculations = (_b.sent()).serialize();
                    return [2, object.camelize(accountOfEmplCalculations)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=accountOfEmplCalculationsService.js.map