import { AccountProductDetail } from '../domain/entities/AccountProductDetail';
import { AccountProductPayload } from '../domain/requests/AccountProductPayload';
export declare function fetchAll(limit: number, offset: number): Promise<AccountProductDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: AccountProductPayload): Promise<AccountProductDetail>;
export declare function getById(id: number): Promise<AccountProductDetail>;
export declare function destroy(id: number): Promise<AccountProductDetail>;
export declare function update(id: number, params: AccountProductPayload): Promise<AccountProductDetail>;
