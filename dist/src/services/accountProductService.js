"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var AccountProduct_1 = require("../models/AccountProduct");
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _accountProduct, accountProduct, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, AccountProduct_1.AccountProduct.fetchAll()];
                case 1:
                    _accountProduct = _a.sent();
                    return [4, (_accountProduct).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({
                            withRelated: [
                                'partners',
                                'subsections',
                                'analiticGroup1',
                                'analiticGroup2',
                                'aahAccountTypes',
                                'warehouse'
                            ]
                        })];
                case 2:
                    accountProduct = _a.sent();
                    res = transform_1.default(accountProduct.serialize(), function (accountProduct) { return ({
                        id: accountProduct.id,
                        date: accountProduct.date,
                        documentNumber: accountProduct.documentNumber,
                        partnersId: accountProduct.partnersId,
                        partners: accountProduct.partners,
                        linesCode: accountProduct.linesCode,
                        contract: accountProduct.contract,
                        contractDate: accountProduct.contractDate,
                        subsectionId: accountProduct.subsectionId,
                        subsections: accountProduct.subsections,
                        analiticGroup_2Id: accountProduct.analiticGroup_2Id,
                        analiticGroup_1Id: accountProduct.analiticGroup_2Id,
                        analiticGroup2: accountProduct.analiticGroup2,
                        analiticGroup1: accountProduct.analiticGroup1,
                        seria: accountProduct.seria,
                        number: accountProduct.number,
                        dateWriteOff: accountProduct.dateWriteOff,
                        comment: accountProduct.comment,
                        aahAccountTypeId: accountProduct.aahAccountTypeId,
                        aahAccountType: accountProduct.aahAccountType,
                        warehouseId: accountProduct.warehouseId,
                        warehouse: accountProduct.warehouse,
                        address: accountProduct.address,
                        aah: accountProduct.aah
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, AccountProduct_1.AccountProduct.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var accountProduct;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AccountProduct_1.AccountProduct(tslib_1.__assign({}, params)).save()];
                case 1:
                    accountProduct = (_a.sent()).serialize();
                    return [2, accountProduct];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var accountProduct;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AccountProduct_1.AccountProduct({ id: id }).fetch({
                        withRelated: ['partners',
                            'subsections',
                            'analiticGroup1',
                            'analiticGroup2',
                            'aahAccountTypes',
                            'warehouse']
                    })];
                case 1:
                    accountProduct = (_a.sent());
                    if (accountProduct) {
                        return [2, accountProduct.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AccountProduct_1.AccountProduct({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AccountProduct_1.AccountProduct().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=accountProductService.js.map