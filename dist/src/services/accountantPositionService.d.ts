import AccountantPositionDetail from '../domain/entities/AccountantPositionDetail';
import AccountantPositionPayload from '../domain/requests/AccountantPositionPayload';
export declare function fetchAll(limit: number, offset: number): Promise<AccountantPositionDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: AccountantPositionPayload): Promise<AccountantPositionPayload>;
export declare function getById(id: number): Promise<AccountantPositionDetail>;
export declare function destroy(id: number): Promise<AccountantPositionDetail>;
export declare function update(id: number, params: AccountantPositionPayload): Promise<AccountantPositionDetail>;
