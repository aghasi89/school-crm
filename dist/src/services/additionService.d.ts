import AdditionDetail from '../domain/entities/AdditionDetail';
import AdditionPayload from '../domain/requests/AdditionPayload';
export declare function fetchAll(limit: number, offset: number): Promise<AdditionDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: AdditionPayload): Promise<AdditionDetail>;
export declare function getById(id: number): Promise<AdditionDetail>;
export declare function destroy(id: number): Promise<AdditionDetail>;
export declare function update(id: number, params: AdditionPayload): Promise<AdditionDetail>;
