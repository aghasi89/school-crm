"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Addition_1 = tslib_1.__importDefault(require("../models/Addition"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _additions, additions, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Addition_1.default.fetchAll()];
                case 1:
                    _additions = _a.sent();
                    return [4, (_additions).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({ withRelated: ['expenseAccount', 'tabel', 'typeOfIncome', 'typeOfVacation'] })];
                case 2:
                    additions = _a.sent();
                    res = transform_1.default(additions.serialize(), function (addition) { return ({
                        id: addition.id,
                        name: addition.name,
                        expense_account_id: addition.expense_account_id,
                        table_id: addition.table_id,
                        type_of_income_id: addition.type_of_income_id,
                        type_of_vacation_id: addition.type_of_vacation_id,
                        coefficient: addition.coefficient,
                        by_the_employer_mandatory_pension: addition.by_the_employer_mandatory_pension,
                        declining_income: addition.declining_income,
                        is_for_tax_purposes_only: addition.is_for_tax_purposes_only,
                        is_income: addition.is_income,
                        is_mandatory_pension: addition.is_mandatory_pension,
                        is_trade_union: addition.is_trade_union,
                        participates_on_account_of_actual_hours: addition.participates_on_account_of_actual_hours,
                        recalculation: addition.recalculation,
                        expenseAccount: addition.expenseAccount,
                        tabel: addition.tabel,
                        typeOfIncome: addition.typeOfIncome,
                        typeOfVacation: addition.typeOfVacation,
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Addition_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var addition;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Addition_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    addition = (_a.sent()).serialize();
                    return [2, addition];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var addition;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Addition_1.default({ id: id }).fetch({ withRelated: ['expenseAccount', 'tabel', 'typeOfIncome', 'typeOfVacation'] })];
                case 1:
                    addition = (_a.sent());
                    if (addition) {
                        return [2, addition.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Addition_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Addition_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=additionService.js.map