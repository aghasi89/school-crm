import AnaliticGroup1Detail from '../domain/entities/AnaliticGroup1Detail';
import AnaliticGroup1Payload from '../domain/requests/AnaliticGroup1Payload';
export declare function fetchAll(limit: number, offset: number): Promise<AnaliticGroup1Detail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: AnaliticGroup1Payload): Promise<AnaliticGroup1Detail>;
export declare function getById(id: number): Promise<AnaliticGroup1Detail>;
export declare function destroy(id: number): Promise<AnaliticGroup1Detail>;
export declare function update(id: number, params: AnaliticGroup1Payload): Promise<AnaliticGroup1Detail>;
