import AnaliticGroup2Detail from '../domain/entities/AnaliticGroup2Detail';
import AnaliticGroup2Payload from '../domain/requests/AnaliticGroup2Payload';
export declare function fetchAll(limit: number, offset: number): Promise<AnaliticGroup2Detail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: AnaliticGroup2Payload): Promise<AnaliticGroup2Detail>;
export declare function getById(id: number): Promise<AnaliticGroup2Detail>;
export declare function destroy(id: number): Promise<AnaliticGroup2Detail>;
export declare function update(id: number, params: AnaliticGroup2Payload): Promise<AnaliticGroup2Detail>;
