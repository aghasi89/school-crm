"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var AnaliticGroup2_1 = tslib_1.__importDefault(require("../models/AnaliticGroup2"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var object = tslib_1.__importStar(require("../utils/object"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _analiticGroup2s, analiticGroup2s, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, AnaliticGroup2_1.default.fetchAll()];
                case 1:
                    _analiticGroup2s = _a.sent();
                    return [4, (_analiticGroup2s).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({ withRelated: ['parent'] })];
                case 2:
                    analiticGroup2s = _a.sent();
                    console.log(analiticGroup2s.serialize());
                    res = transform_1.default(analiticGroup2s.serialize(), function (analiticGroup2) { return ({
                        id: analiticGroup2.id,
                        code: analiticGroup2.code,
                        analiticGroup2Id: analiticGroup2.analiticGroup2Id,
                        isAccumulate: analiticGroup2.isAccumulate,
                        name: analiticGroup2.name,
                        parent: analiticGroup2.parent
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, AnaliticGroup2_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var analiticGroup2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AnaliticGroup2_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    analiticGroup2 = (_a.sent()).serialize();
                    console.log(analiticGroup2);
                    console.log(object.camelize(analiticGroup2));
                    return [2, object.camelize(analiticGroup2)];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var position;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AnaliticGroup2_1.default({ id: id }).fetch()];
                case 1:
                    position = (_a.sent());
                    if (position) {
                        return [2, position.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AnaliticGroup2_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new AnaliticGroup2_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=analiticGroup2Service.js.map