import JWTPayload from '../domain/misc/JWTPayload';
import LoginPayload from '../domain/requests/LoginPayload';
export declare function login(loginPayload: LoginPayload): Promise<{
    refreshToken: string;
    accessToken: string;
}>;
export declare function refresh(token: string, jwtPayload: JWTPayload): Promise<{
    accessToken: string;
}>;
export declare function logout(token: string): Promise<void>;
