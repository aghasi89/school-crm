"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var User_1 = tslib_1.__importDefault(require("../models/User"));
var jwt = tslib_1.__importStar(require("../utils/jwt"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var bcrypt = tslib_1.__importStar(require("../utils/bcrypt"));
var UserSession_1 = tslib_1.__importDefault(require("../models/UserSession"));
var ForbiddenError_1 = tslib_1.__importDefault(require("../exceptions/ForbiddenError"));
var sessionService = tslib_1.__importStar(require("../services/sessionService"));
var UnauthorizedError_1 = tslib_1.__importDefault(require("../exceptions/UnauthorizedError"));
var errors = config_1.default.errors;
function login(loginPayload) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var email, password, user, isSame, _a, name, roleId, userId, loggedInUser, refreshToken, userSessionPayload, session, accessToken;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    email = loginPayload.email, password = loginPayload.password;
                    logger_1.default.log('info', 'Checking email: %s', email);
                    return [4, new User_1.default({ email: email }).fetch()];
                case 1:
                    user = _b.sent();
                    if (!user) return [3, 4];
                    logger_1.default.log('debug', 'Login: Fetched user by email -', user.attributes);
                    logger_1.default.log('debug', 'Login: Comparing password');
                    return [4, bcrypt.compare(password, user.attributes.password)];
                case 2:
                    isSame = _b.sent();
                    logger_1.default.log('debug', 'Login: Password match status - %s', isSame);
                    if (!isSame) return [3, 4];
                    _a = user.attributes, name = _a.name, roleId = _a.roleId, userId = _a.id;
                    loggedInUser = { name: name, email: email, userId: userId, roleId: roleId };
                    refreshToken = jwt.generateRefreshToken(loggedInUser);
                    userSessionPayload = { userId: userId, token: refreshToken };
                    return [4, sessionService.create(userSessionPayload)];
                case 3:
                    session = _b.sent();
                    accessToken = jwt.generateAccessToken(tslib_1.__assign(tslib_1.__assign({}, loggedInUser), { sessionId: session.id }));
                    return [2, { refreshToken: refreshToken, accessToken: accessToken }];
                case 4: throw new UnauthorizedError_1.default(errors.invalidCredentials);
            }
        });
    });
}
exports.login = login;
function refresh(token, jwtPayload) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var session, accessToken;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'User Session: Fetching session of token - %s', token);
                    return [4, new UserSession_1.default({ token: token, isActive: true }).fetch()];
                case 1:
                    session = _a.sent();
                    if (!session) {
                        throw new ForbiddenError_1.default(errors.sessionNotMaintained);
                    }
                    logger_1.default.log('debug', 'User Session: Fetched session -', session.serialize());
                    logger_1.default.log('info', 'JWT: Generating new access token');
                    accessToken = jwt.generateAccessToken(tslib_1.__assign(tslib_1.__assign({}, jwtPayload), { sessionId: session.id }));
                    return [2, {
                            accessToken: accessToken
                        }];
            }
        });
    });
}
exports.refresh = refresh;
function logout(token) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var session;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'Logout: Logging out user session - %s', token);
                    return [4, sessionService.remove(token)];
                case 1:
                    session = _a.sent();
                    if (!session) {
                        throw new ForbiddenError_1.default(errors.sessionNotMaintained);
                    }
                    return [2];
            }
        });
    });
}
exports.logout = logout;
//# sourceMappingURL=authService.js.map