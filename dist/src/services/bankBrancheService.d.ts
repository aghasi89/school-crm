import BankBrancheDetail from '../domain/entities/BankBrancheDetail';
import BankBranchePayload from '../domain/requests/BankBranchePayload';
export declare function fetchAll(): Promise<BankBrancheDetail[]>;
export declare function insert(params: BankBranchePayload): Promise<BankBrancheDetail>;
