"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var BankBranche_1 = tslib_1.__importDefault(require("../models/BankBranche"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var object = tslib_1.__importStar(require("../utils/object"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
function fetchAll() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var banks, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'Fetching users from database');
                    return [4, BankBranche_1.default.fetchAll()];
                case 1:
                    banks = _a.sent();
                    res = transform_1.default(banks.serialize(), function (bankBranche) { return ({
                        bankId: bankBranche.bankId,
                        brancheCode: bankBranche.brancheCode,
                        brancheAddress: bankBranche.brancheAddress
                    }); });
                    logger_1.default.log('debug', 'Fetched all users successfully:', res);
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var bank;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new BankBranche_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    bank = (_a.sent()).serialize();
                    logger_1.default.log('debug', 'Inserted user successfully:', bank);
                    return [2, object.camelize(bank)];
            }
        });
    });
}
exports.insert = insert;
//# sourceMappingURL=bankBrancheService.js.map