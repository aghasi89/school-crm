import BankDetail from '../domain/entities/BankDetail';
import BankPayload from '../domain/requests/BankPayload';
export declare function fetchAll(limit: number, offset: number): Promise<BankDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: BankPayload): Promise<BankDetail>;
export declare function getById(id: number): Promise<BankDetail>;
export declare function destroy(id: number): Promise<BankDetail>;
export declare function update(id: number, params: BankPayload): Promise<BankDetail>;
