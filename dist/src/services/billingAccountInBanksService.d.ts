import { BillingAccountInBanksDetail } from '../domain/entities/BillingAccountInBanksDetail';
import BillingAccountInBanksPayload from '../domain/requests/BillingAccountInBanksPayload';
export declare function fetchAll(limit: number, offset: number): Promise<BillingAccountInBanksDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: BillingAccountInBanksPayload): Promise<BillingAccountInBanksDetail>;
export declare function getById(id: number): Promise<BillingAccountInBanksDetail>;
export declare function destroy(id: number): Promise<BillingAccountInBanksDetail>;
export declare function update(id: number, params: BillingAccountInBanksPayload): Promise<BillingAccountInBanksDetail>;
