"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var BillingAccountInBanks_1 = tslib_1.__importDefault(require("../models/BillingAccountInBanks"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _billingAccountInBanks, billingAccountInBanks, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, BillingAccountInBanks_1.default.fetchAll()];
                case 1:
                    _billingAccountInBanks = _a.sent();
                    return [4, (_billingAccountInBanks).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({ withRelated: ['currencies', 'accounts'] })];
                case 2:
                    billingAccountInBanks = _a.sent();
                    res = transform_1.default(billingAccountInBanks.serialize(), function (billingAccountInBanks) { return ({
                        id: billingAccountInBanks.id,
                        billingAccount: billingAccountInBanks.billingAccount,
                        name: billingAccountInBanks.name,
                        currencyId: billingAccountInBanks.currencyId,
                        currencies: billingAccountInBanks.currencies,
                        accountId: billingAccountInBanks.accountId,
                        accounts: billingAccountInBanks.accounts,
                        isMain: billingAccountInBanks.isMain,
                        numberOfDocument: billingAccountInBanks.numberOfDocument,
                        eServices: billingAccountInBanks.eServices
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, BillingAccountInBanks_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var billingAccountInBanks;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new BillingAccountInBanks_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    billingAccountInBanks = (_a.sent()).serialize();
                    return [2, billingAccountInBanks];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var billingAccountInBanks;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new BillingAccountInBanks_1.default({ id: id }).fetch({ withRelated: ['currencies', 'accounts'] })];
                case 1:
                    billingAccountInBanks = (_a.sent());
                    if (billingAccountInBanks) {
                        return [2, billingAccountInBanks.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new BillingAccountInBanks_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new BillingAccountInBanks_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=billingAccountInBanksService.js.map