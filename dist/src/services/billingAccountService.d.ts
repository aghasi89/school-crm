import BillingAccountDetail from '../domain/entities/BillingAccountDetail';
import { BillingAccountPayload } from '../domain/requests/BillingAccountPayload';
export declare function fetchAll(limit: number, offset: number): Promise<BillingAccountDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: BillingAccountPayload): Promise<BillingAccountPayload>;
export declare function getById(id: number): Promise<BillingAccountDetail>;
export declare function destroy(id: number): Promise<BillingAccountDetail>;
export declare function update(id: number, params: BillingAccountPayload): Promise<BillingAccountDetail>;
