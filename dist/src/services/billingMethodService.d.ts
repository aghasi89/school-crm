import BillingMethodDetail from '../domain/entities/BillingMethodDetail';
import BillingMethodPayload from '../domain/requests/BillingMethodPayload';
export declare function fetchAll(limit: number, offset: number): Promise<BillingMethodDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: BillingMethodPayload): Promise<BillingMethodPayload>;
export declare function getById(id: number): Promise<BillingMethodDetail>;
export declare function destroy(id: number): Promise<BillingMethodDetail>;
export declare function update(id: number, params: BillingMethodPayload): Promise<BillingMethodDetail>;
