import CalculationsTypeDetail from '../domain/entities/CalculationsTypeDetail';
import CalculationsTypePayload from '../domain/requests/CalculationsTypePayload';
export declare function fetchAll(limit: number, offset: number): Promise<CalculationsTypeDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: CalculationsTypePayload): Promise<CalculationsTypePayload>;
export declare function getById(id: number): Promise<CalculationsTypeDetail>;
export declare function destroy(id: number): Promise<CalculationsTypeDetail>;
export declare function update(id: number, params: CalculationsTypePayload): Promise<CalculationsTypeDetail>;
