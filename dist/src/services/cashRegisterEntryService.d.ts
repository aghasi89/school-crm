import { CashRegisterEntryDetail } from '../domain/entities/CashRegisterEntryDetail';
import { CashRegisterEntryPayload } from '../domain/requests/CashRegisterEntryPayload';
export declare function fetchAll(limit: number, offset: number): Promise<CashRegisterEntryDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: CashRegisterEntryPayload): Promise<CashRegisterEntryDetail>;
export declare function getById(id: number): Promise<CashRegisterEntryDetail>;
export declare function destroy(id: number): Promise<CashRegisterEntryDetail>;
export declare function update(id: number, params: CashRegisterEntryPayload): Promise<CashRegisterEntryDetail>;
