"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var CashRegisterEntry_1 = require("../models/CashRegisterEntry");
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _cashRegisterEntry, cashRegisterEntry, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, CashRegisterEntry_1.CashRegisterEntry.fetchAll()];
                case 1:
                    _cashRegisterEntry = _a.sent();
                    return [4, (_cashRegisterEntry).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({
                            withRelated: [
                                'cashRegister',
                                'correspondentAccount',
                                'entryAccount',
                                'analiticGroup2',
                                'analiticGroup1',
                                'currencies',
                                'partners'
                            ]
                        })];
                case 2:
                    cashRegisterEntry = _a.sent();
                    res = transform_1.default(cashRegisterEntry.serialize(), function (cashRegisterEntry) { return ({
                        id: cashRegisterEntry.id,
                        cashRegisterId: cashRegisterEntry.cashRegisterId,
                        date: cashRegisterEntry.date,
                        hdmN: cashRegisterEntry.hdmN,
                        documentNumber: cashRegisterEntry.documentNumber,
                        correspondentAccountId: cashRegisterEntry.correspondentAccountId,
                        entryAccountId: cashRegisterEntry.entryAccountId,
                        analiticGroup_1Id: cashRegisterEntry.analiticGroup_1Id,
                        analiticGroup_2Id: cashRegisterEntry.analiticGroup_2Id,
                        currencyId: cashRegisterEntry.currencyId,
                        amountCurrency1: cashRegisterEntry.amountCurrency1,
                        amountCurrency2: cashRegisterEntry.amountCurrency2,
                        partnersId: cashRegisterEntry.partnersId,
                        received: cashRegisterEntry.received,
                        basis: cashRegisterEntry.basis,
                        attached: cashRegisterEntry.attached,
                        optiona: cashRegisterEntry.optiona,
                        typicalOperation: cashRegisterEntry.typicalOperation,
                        cashRegister: cashRegisterEntry.cashRegister,
                        accounts: cashRegisterEntry.accounts,
                        analiticGroup1: cashRegisterEntry.analiticGroup1,
                        analiticGroup2: cashRegisterEntry.analiticGroup2,
                        currencies: cashRegisterEntry.currencies,
                        partners: cashRegisterEntry.partners
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, CashRegisterEntry_1.CashRegisterEntry.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var cashRegisterEntry;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegisterEntry_1.CashRegisterEntry(tslib_1.__assign({}, params)).save()];
                case 1:
                    cashRegisterEntry = (_a.sent()).serialize();
                    return [2, cashRegisterEntry];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var cashRegisterEntry;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegisterEntry_1.CashRegisterEntry({ id: id }).fetch({
                        withRelated: [
                            'cashRegister',
                            'correspondentAccount',
                            'entryAccount',
                            'analiticGroup2',
                            'analiticGroup1',
                            'currencies',
                            'partners'
                        ]
                    })];
                case 1:
                    cashRegisterEntry = (_a.sent());
                    if (cashRegisterEntry) {
                        return [2, cashRegisterEntry.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegisterEntry_1.CashRegisterEntry({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegisterEntry_1.CashRegisterEntry().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=cashRegisterEntryService.js.map