import { CashRegisterExitDetail } from '../domain/entities/CashRegisterExitDetail';
import { CashRegisterExitPayload } from '../domain/requests/CashRegisterExitPayload';
export declare function fetchAll(limit: number, offset: number): Promise<CashRegisterExitDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: CashRegisterExitPayload): Promise<CashRegisterExitDetail>;
export declare function getById(id: number): Promise<CashRegisterExitDetail>;
export declare function destroy(id: number): Promise<CashRegisterExitDetail>;
export declare function update(id: number, params: CashRegisterExitPayload): Promise<CashRegisterExitDetail>;
