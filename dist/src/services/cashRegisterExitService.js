"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var CashRegisterExit_1 = require("../models/CashRegisterExit");
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _cashRegisterExit, cashRegisterExit, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, CashRegisterExit_1.CashRegisterExit.fetchAll()];
                case 1:
                    _cashRegisterExit = _a.sent();
                    return [4, (_cashRegisterExit).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({
                            withRelated: [
                                'cashRegister',
                                'correspondentAccount',
                                'exitAccount',
                                'analiticGroup2',
                                'analiticGroup1',
                                'currencies',
                                'partners'
                            ]
                        })];
                case 2:
                    cashRegisterExit = _a.sent();
                    res = transform_1.default(cashRegisterExit.serialize(), function (cashRegisterExit) { return ({
                        id: cashRegisterExit.id,
                        cashRegisterId: cashRegisterExit.cashRegisterId,
                        date: cashRegisterExit.date,
                        documentNumber: cashRegisterExit.documentNumber,
                        correspondentAccountId: cashRegisterExit.correspondentAccountId,
                        exitAccountId: cashRegisterExit.exitAccountId,
                        analiticGroup_1Id: cashRegisterExit.analiticGroup_1Id,
                        analiticGroup_2Id: cashRegisterExit.analiticGroup_2Id,
                        currencyId: cashRegisterExit.currencyId,
                        amountCurrency1: cashRegisterExit.amountCurrency1,
                        amountCurrency2: cashRegisterExit.amountCurrency2,
                        partnersId: cashRegisterExit.partnersId,
                        received: cashRegisterExit.received,
                        npNshPassword: cashRegisterExit.npNshPassword,
                        basis: cashRegisterExit.basis,
                        appendix: cashRegisterExit.appendix,
                        otherInformation: cashRegisterExit.otherInformation,
                        optiona: cashRegisterExit.optiona,
                        typicalOperation: cashRegisterExit.typicalOperation,
                        cashRegister: cashRegisterExit.cashRegister,
                        analiticGroup1: cashRegisterExit.analiticGroup1,
                        analiticGroup2: cashRegisterExit.analiticGroup2,
                        currencies: cashRegisterExit.currencies,
                        partners: cashRegisterExit.partners
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, CashRegisterExit_1.CashRegisterExit.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var cashRegisterExit;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegisterExit_1.CashRegisterExit(tslib_1.__assign({}, params)).save()];
                case 1:
                    cashRegisterExit = (_a.sent()).serialize();
                    return [2, cashRegisterExit];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var cashRegisterExit;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegisterExit_1.CashRegisterExit({ id: id }).fetch({
                        withRelated: [
                            'cashRegister',
                            'correspondentAccount',
                            'exitAccount',
                            'analiticGroup2',
                            'analiticGroup1',
                            'currencies',
                            'partners'
                        ]
                    })];
                case 1:
                    cashRegisterExit = (_a.sent());
                    if (cashRegisterExit) {
                        return [2, cashRegisterExit.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegisterExit_1.CashRegisterExit({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegisterExit_1.CashRegisterExit().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=cashRegisterExitService.js.map