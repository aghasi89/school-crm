import CashRegisterDetail from '../domain/entities/CashRegisterDetail';
import CashRegisterPayload from '../domain/requests/CashRegisterPayload';
export declare function fetchAll(limit: number, offset: number): Promise<CashRegisterDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: CashRegisterPayload): Promise<CashRegisterDetail>;
export declare function getById(id: number): Promise<CashRegisterDetail>;
export declare function destroy(id: number): Promise<CashRegisterDetail>;
export declare function update(id: number, params: CashRegisterPayload): Promise<CashRegisterDetail>;
