"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var CashRegister_1 = tslib_1.__importDefault(require("../models/CashRegister"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var object = tslib_1.__importStar(require("../utils/object"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _cashRegisters, cashRegisters, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'Fetching users from database');
                    return [4, CashRegister_1.default.fetchAll()];
                case 1:
                    _cashRegisters = _a.sent();
                    if (!limit || limit == 0) {
                        limit = Number(_cashRegisters.count());
                        offset = 0;
                    }
                    return [4, (_cashRegisters).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({ withRelated: ['account'] })];
                case 2:
                    cashRegisters = _a.sent();
                    res = transform_1.default(cashRegisters.serialize(), function (cashRegister) { return ({
                        account: cashRegister.account,
                        accountId: cashRegister.accountId,
                        code: cashRegister.code,
                        deoN: cashRegister.deoN,
                        dmoN: cashRegister.dmoN,
                        hdmNonTaxable: cashRegister.hdmNonTaxable,
                        hdmPrintType: cashRegister.hdmPrintType,
                        hdmRegisN: cashRegister.hdmRegisN,
                        hdmTaxable: cashRegister.hdmTaxable,
                        ip: cashRegister.ip,
                        isMain: cashRegister.isMain,
                        name: cashRegister.name,
                        password: cashRegister.password,
                        port: cashRegister.port,
                        id: cashRegister.id
                    }); });
                    logger_1.default.log('debug', 'Fetched all users successfully:', res);
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, CashRegister_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var cashRegister;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    console.log(params, '*********');
                    return [4, new CashRegister_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    cashRegister = (_a.sent()).serialize();
                    logger_1.default.log('debug', 'Inserted user successfully:', cashRegister);
                    return [2, object.camelize(cashRegister)];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var position;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegister_1.default({ id: id }).fetch({ withRelated: ['measurementUnit', 'classification', 'account'] })];
                case 1:
                    position = (_a.sent());
                    if (position) {
                        return [2, position.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegister_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var partners;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CashRegister_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    partners = (_a.sent()).serialize();
                    return [2, object.camelize(partners)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=cashRegisterService.js.map