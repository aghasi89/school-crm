import ClassificationDetail from '../domain/entities/ClassificationDetail';
import ClassificationPayload from '../domain/requests/ClassificationPayload';
export declare function fetchAll(limit: number, offset: number): Promise<ClassificationDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: ClassificationPayload): Promise<ClassificationPayload>;
export declare function getById(id: number): Promise<ClassificationDetail>;
export declare function destroy(id: number): Promise<ClassificationDetail>;
export declare function update(id: number, params: ClassificationPayload): Promise<ClassificationDetail>;
