import { CoWorkersDetail } from '../domain/entities/CoWorkersDetail';
import { CoWorkerPayload } from '../domain/requests/CoWokerPayload';
export declare function fetchAll(limit: number, offset: number): Promise<CoWorkersDetail[]>;
export declare function insert(params: CoWorkerPayload): Promise<CoWorkersDetail>;
export declare function count(): Promise<object>;
export declare function getById(id: number): Promise<CoWorkersDetail>;
export declare function destroy(id: number): Promise<CoWorkersDetail>;
export declare function update(id: number, params: CoWorkerPayload): Promise<CoWorkersDetail>;
