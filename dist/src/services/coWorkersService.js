"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var CoWorkers_1 = tslib_1.__importDefault(require("../models/CoWorkers"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var object = tslib_1.__importStar(require("../utils/object"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _coWorkers, coWorkers, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, CoWorkers_1.default.fetchAll()];
                case 1:
                    _coWorkers = _a.sent();
                    return [4, (_coWorkers).query().limit(limit).offset(offset)];
                case 2:
                    coWorkers = _a.sent();
                    res = transform_1.default(coWorkers, function (coWorker) { return ({
                        id: coWorker.id,
                        code: coWorker.code,
                        hvhh: coWorker.hvhh,
                        name: coWorker.name,
                        creator: coWorker.creator,
                        debetor: coWorker.debetor,
                        legalAddress: coWorker.legalAddress,
                        workAddress: coWorker.workAddress,
                        transferPurpose: coWorker.transferPurpose,
                        inflowAccount: coWorker.inflowAccount,
                        leakageAccount: coWorker.leakageAccount,
                        director: coWorker.director,
                        accountent: coWorker.accountent,
                        bankAccount: coWorker.bankAccount,
                        bankId: coWorker.bankId
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var coWorkers;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CoWorkers_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    coWorkers = (_a.sent()).serialize();
                    return [2, object.camelize(coWorkers)];
            }
        });
    });
}
exports.insert = insert;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, CoWorkers_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var position;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CoWorkers_1.default({ id: id }).fetch()];
                case 1:
                    position = (_a.sent());
                    if (position) {
                        return [2, position.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CoWorkers_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new CoWorkers_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=coWorkersService.js.map