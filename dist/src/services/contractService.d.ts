import ContractDetail from '../domain/entities/ContractDetail';
import ContractPayload from '../domain/requests/ContractPayload';
export declare function fetchAll(limit: number, offset: number): Promise<ContractDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: ContractPayload): Promise<ContractPayload>;
export declare function getById(id: number): Promise<ContractDetail>;
export declare function destroy(id: number): Promise<ContractDetail>;
export declare function update(id: number, params: ContractPayload): Promise<ContractDetail>;
