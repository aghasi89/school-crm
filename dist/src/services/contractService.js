"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Contract_1 = tslib_1.__importDefault(require("../models/Contract"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var object = tslib_1.__importStar(require("../utils/object"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _contracts, contracts, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Contract_1.default.fetchAll()];
                case 1:
                    _contracts = _a.sent();
                    return [4, (_contracts).query().limit(limit).offset(offset)];
                case 2:
                    contracts = _a.sent();
                    res = transform_1.default(contracts, function (contract) { return ({
                        name: contract.name,
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Contract_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(param) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var contract;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Contract_1.default(tslib_1.__assign({}, param)).save()];
                case 1:
                    contract = (_a.sent()).serialize();
                    return [2, object.camelize(contract)];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var position;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Contract_1.default({ id: id }).fetch()];
                case 1:
                    position = (_a.sent());
                    if (position) {
                        return [2, position.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Contract_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var contract;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Contract_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    contract = (_a.sent()).serialize();
                    return [2, object.camelize(contract)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=contractService.js.map