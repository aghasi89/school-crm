import CurrencyDetail from '../domain/entities/CurrencyDetail';
import CurrencyPayload from '../domain/requests/CurrencyPayload';
export declare function fetchAll(limit: number, offset: number): Promise<CurrencyDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: CurrencyPayload): Promise<CurrencyPayload>;
export declare function getById(id: number): Promise<CurrencyDetail>;
export declare function destroy(id: number): Promise<CurrencyDetail>;
export declare function update(id: number, params: CurrencyPayload): Promise<CurrencyDetail>;
