import EmployeeDetail from '../domain/entities/EmployeeDetail';
import EmployeePayload from '../domain/requests/EmployeePayload';
export declare function fetchAll(limit: number, offset: number): Promise<EmployeeDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: EmployeePayload): Promise<EmployeeDetail>;
export declare function getById(id: number): Promise<EmployeeDetail>;
export declare function destroy(id: number): Promise<EmployeeDetail>;
export declare function update(id: number, params: EmployeePayload): Promise<EmployeeDetail>;
