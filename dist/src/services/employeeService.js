"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Employee_1 = tslib_1.__importDefault(require("../models/Employee"));
var EmployeeGeneral_1 = tslib_1.__importDefault(require("../models/EmployeeGeneral"));
var Address_1 = tslib_1.__importDefault(require("../models/Address"));
var OtherInformation_1 = tslib_1.__importDefault(require("../models/OtherInformation"));
var EmployeeAddition_1 = tslib_1.__importDefault(require("../models/EmployeeAddition"));
var EmployeeAccounts_1 = tslib_1.__importDefault(require("../models/EmployeeAccounts"));
var EmployeePosition_1 = tslib_1.__importDefault(require("../models/EmployeePosition"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _employees, employees, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Employee_1.default.fetchAll()];
                case 1:
                    _employees = _a.sent();
                    return [4, (_employees).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({ withRelated: ['general', 'addressies', 'otherInformation', 'employeePosition.positon', 'employeeAccounts', 'employeeAddition'] })];
                case 2:
                    employees = _a.sent();
                    console.log(employees.serialize(), '**********');
                    res = transform_1.default(employees.serialize(), function (employee) { return ({
                        id: employee.id,
                        firstName: employee.firstName,
                        fullName: employee.fullName,
                        lastName: employee.lastName,
                        tabelCounter: employee.tabelCounter,
                        subdivisionId: employee.subdivisionId,
                        addressies: employee.addressies,
                        general: employee.general,
                        otherInformation: employee.otherInformation,
                        employeeAccounts: employee.employeeAccounts,
                        employeeAddition: employee.employeeAddition,
                        employeePosition: employee.employeePosition
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Employee_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var employee, employeeId, _a, _b, key, e_1_1, _c, _d, key, e_2_1, _e, _f, key, e_3_1;
        var e_1, _g, e_2, _h, e_3, _j;
        return tslib_1.__generator(this, function (_k) {
            switch (_k.label) {
                case 0:
                    console.log(params, '***********');
                    return [4, new Employee_1.default(tslib_1.__assign({}, params.selfData)).save()];
                case 1:
                    employee = (_k.sent()).serialize();
                    employeeId = employee.id;
                    params.general.employeeId = employeeId;
                    params.addressies.employeeId = employeeId;
                    params.otherInformation.employeeId = employeeId;
                    return [4, new EmployeeGeneral_1.default(tslib_1.__assign({}, params.general)).save()];
                case 2:
                    (_k.sent()).serialize();
                    return [4, new Address_1.default(tslib_1.__assign({}, params.addressies)).save()];
                case 3:
                    (_k.sent()).serialize();
                    return [4, new OtherInformation_1.default(tslib_1.__assign({}, params.otherInformation)).save()];
                case 4:
                    (_k.sent()).serialize();
                    _k.label = 5;
                case 5:
                    _k.trys.push([5, 10, 11, 12]);
                    _a = tslib_1.__values(params.employeePosition), _b = _a.next();
                    _k.label = 6;
                case 6:
                    if (!!_b.done) return [3, 9];
                    key = _b.value;
                    key.employeeId = employeeId;
                    return [4, new EmployeePosition_1.default(tslib_1.__assign({}, key)).save()];
                case 7:
                    (_k.sent()).serialize();
                    _k.label = 8;
                case 8:
                    _b = _a.next();
                    return [3, 6];
                case 9: return [3, 12];
                case 10:
                    e_1_1 = _k.sent();
                    e_1 = { error: e_1_1 };
                    return [3, 12];
                case 11:
                    try {
                        if (_b && !_b.done && (_g = _a.return)) _g.call(_a);
                    }
                    finally { if (e_1) throw e_1.error; }
                    return [7];
                case 12:
                    _k.trys.push([12, 17, 18, 19]);
                    _c = tslib_1.__values(params.employeeAccounts), _d = _c.next();
                    _k.label = 13;
                case 13:
                    if (!!_d.done) return [3, 16];
                    key = _d.value;
                    key.employeeId = employeeId;
                    return [4, new EmployeeAccounts_1.default(tslib_1.__assign({}, key)).save()];
                case 14:
                    (_k.sent()).serialize();
                    _k.label = 15;
                case 15:
                    _d = _c.next();
                    return [3, 13];
                case 16: return [3, 19];
                case 17:
                    e_2_1 = _k.sent();
                    e_2 = { error: e_2_1 };
                    return [3, 19];
                case 18:
                    try {
                        if (_d && !_d.done && (_h = _c.return)) _h.call(_c);
                    }
                    finally { if (e_2) throw e_2.error; }
                    return [7];
                case 19:
                    _k.trys.push([19, 24, 25, 26]);
                    _e = tslib_1.__values(params.employeeAddition), _f = _e.next();
                    _k.label = 20;
                case 20:
                    if (!!_f.done) return [3, 23];
                    key = _f.value;
                    key.employeeId = employeeId;
                    return [4, new EmployeeAddition_1.default(tslib_1.__assign({}, key)).save()];
                case 21:
                    (_k.sent()).serialize();
                    _k.label = 22;
                case 22:
                    _f = _e.next();
                    return [3, 20];
                case 23: return [3, 26];
                case 24:
                    e_3_1 = _k.sent();
                    e_3 = { error: e_3_1 };
                    return [3, 26];
                case 25:
                    try {
                        if (_f && !_f.done && (_j = _e.return)) _j.call(_e);
                    }
                    finally { if (e_3) throw e_3.error; }
                    return [7];
                case 26: return [2, employee];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var employee;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Employee_1.default({ id: id }).fetch({ withRelated: ['general', 'addressies', 'otherInformation', 'employeePosition.positon', 'employeeAccounts', 'employeeAddition'] })];
                case 1:
                    employee = (_a.sent());
                    if (employee) {
                        return [2, employee.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Employee_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var general, addressies, otherInformation, employeePosition, employeeAccounts, employeeAddition, employeePosition_1, employeePosition_1_1, iterator, error_1, e_4_1, employeeAccounts_1, employeeAccounts_1_1, iterator, error_2, e_5_1, employeeAddition_1, employeeAddition_1_1, iterator, error_3, e_6_1, subdivision;
        var e_4, _a, e_5, _b, e_6, _c;
        return tslib_1.__generator(this, function (_d) {
            switch (_d.label) {
                case 0:
                    general = params.general;
                    addressies = params.addressies;
                    otherInformation = params.otherInformation;
                    return [4, new EmployeeGeneral_1.default().where({ employee_id: id }).save(tslib_1.__assign({}, general), { patch: true })];
                case 1:
                    _d.sent();
                    return [4, new Address_1.default().where({ employee_id: id }).save(tslib_1.__assign({}, addressies), { patch: true })];
                case 2:
                    _d.sent();
                    return [4, new OtherInformation_1.default().where({ employee_id: id }).save(tslib_1.__assign({}, otherInformation), { patch: true })];
                case 3:
                    _d.sent();
                    employeePosition = params.employeePosition;
                    employeeAccounts = params.employeeAccounts;
                    employeeAddition = params.employeeAddition;
                    _d.label = 4;
                case 4:
                    _d.trys.push([4, 16, 17, 18]);
                    employeePosition_1 = tslib_1.__values(employeePosition), employeePosition_1_1 = employeePosition_1.next();
                    _d.label = 5;
                case 5:
                    if (!!employeePosition_1_1.done) return [3, 15];
                    iterator = employeePosition_1_1.value;
                    iterator.employeeId = id;
                    _d.label = 6;
                case 6:
                    _d.trys.push([6, 13, , 14]);
                    console.log(iterator, '*********');
                    if (!(iterator['id'] && iterator.status == 'unChanged')) return [3, 8];
                    delete iterator.status;
                    return [4, new EmployeePosition_1.default().where({ id: iterator['id'] }).save(tslib_1.__assign({}, iterator), { patch: true })];
                case 7:
                    (_d.sent()).serialize();
                    return [3, 12];
                case 8:
                    if (!(iterator['id'] && iterator.status == 'deleted')) return [3, 10];
                    return [4, new EmployeePosition_1.default({ id: iterator['id'] }).destroy()];
                case 9:
                    (_d.sent()).serialize();
                    return [3, 12];
                case 10:
                    if (!(iterator.status == 'new')) return [3, 12];
                    delete iterator.status;
                    return [4, new EmployeePosition_1.default(tslib_1.__assign({}, iterator)).save()];
                case 11:
                    (_d.sent()).serialize();
                    _d.label = 12;
                case 12: return [3, 14];
                case 13:
                    error_1 = _d.sent();
                    console.log(error_1, '99999');
                    return [3, 14];
                case 14:
                    employeePosition_1_1 = employeePosition_1.next();
                    return [3, 5];
                case 15: return [3, 18];
                case 16:
                    e_4_1 = _d.sent();
                    e_4 = { error: e_4_1 };
                    return [3, 18];
                case 17:
                    try {
                        if (employeePosition_1_1 && !employeePosition_1_1.done && (_a = employeePosition_1.return)) _a.call(employeePosition_1);
                    }
                    finally { if (e_4) throw e_4.error; }
                    return [7];
                case 18:
                    _d.trys.push([18, 30, 31, 32]);
                    employeeAccounts_1 = tslib_1.__values(employeeAccounts), employeeAccounts_1_1 = employeeAccounts_1.next();
                    _d.label = 19;
                case 19:
                    if (!!employeeAccounts_1_1.done) return [3, 29];
                    iterator = employeeAccounts_1_1.value;
                    iterator.employeeId = id;
                    _d.label = 20;
                case 20:
                    _d.trys.push([20, 27, , 28]);
                    if (!(iterator['id'] && iterator.status == 'unChanged')) return [3, 22];
                    delete iterator.status;
                    console.log('unChanged');
                    return [4, new EmployeeAccounts_1.default().where({ id: iterator['id'] }).save(tslib_1.__assign({}, iterator), { patch: true })];
                case 21:
                    (_d.sent()).serialize();
                    return [3, 26];
                case 22:
                    if (!(iterator['id'] && iterator.status == 'deleted')) return [3, 24];
                    console.log('deleted');
                    return [4, new EmployeeAccounts_1.default().where({ id: iterator['id'] }).destroy()];
                case 23:
                    (_d.sent()).serialize();
                    return [3, 26];
                case 24:
                    if (!(iterator.status == 'new')) return [3, 26];
                    console.log('new');
                    delete iterator.status;
                    return [4, new EmployeeAccounts_1.default(tslib_1.__assign({}, iterator)).save()];
                case 25:
                    (_d.sent()).serialize();
                    _d.label = 26;
                case 26: return [3, 28];
                case 27:
                    error_2 = _d.sent();
                    console.log(error_2, '154+++++++++++');
                    return [3, 28];
                case 28:
                    employeeAccounts_1_1 = employeeAccounts_1.next();
                    return [3, 19];
                case 29: return [3, 32];
                case 30:
                    e_5_1 = _d.sent();
                    e_5 = { error: e_5_1 };
                    return [3, 32];
                case 31:
                    try {
                        if (employeeAccounts_1_1 && !employeeAccounts_1_1.done && (_b = employeeAccounts_1.return)) _b.call(employeeAccounts_1);
                    }
                    finally { if (e_5) throw e_5.error; }
                    return [7];
                case 32:
                    _d.trys.push([32, 44, 45, 46]);
                    employeeAddition_1 = tslib_1.__values(employeeAddition), employeeAddition_1_1 = employeeAddition_1.next();
                    _d.label = 33;
                case 33:
                    if (!!employeeAddition_1_1.done) return [3, 43];
                    iterator = employeeAddition_1_1.value;
                    iterator.employeeId = id;
                    _d.label = 34;
                case 34:
                    _d.trys.push([34, 41, , 42]);
                    if (!(iterator['id'] && !iterator['isDeleted'])) return [3, 36];
                    return [4, new EmployeeAddition_1.default().where({ id: iterator['id'] }).save(tslib_1.__assign({}, iterator), { patch: true })];
                case 35:
                    (_d.sent()).serialize();
                    return [3, 40];
                case 36:
                    if (!(iterator['id'] && iterator['isDeleted'])) return [3, 38];
                    return [4, new EmployeeAddition_1.default().where({ id: iterator['id'] }).destroy()];
                case 37:
                    (_d.sent()).serialize();
                    return [3, 40];
                case 38: return [4, new EmployeeAddition_1.default(tslib_1.__assign({}, iterator)).save()];
                case 39:
                    (_d.sent()).serialize();
                    _d.label = 40;
                case 40: return [3, 42];
                case 41:
                    error_3 = _d.sent();
                    console.log(error_3, '154-------');
                    return [3, 42];
                case 42:
                    employeeAddition_1_1 = employeeAddition_1.next();
                    return [3, 33];
                case 43: return [3, 46];
                case 44:
                    e_6_1 = _d.sent();
                    e_6 = { error: e_6_1 };
                    return [3, 46];
                case 45:
                    try {
                        if (employeeAddition_1_1 && !employeeAddition_1_1.done && (_c = employeeAddition_1.return)) _c.call(employeeAddition_1);
                    }
                    finally { if (e_6) throw e_6.error; }
                    return [7];
                case 46: return [4, new Employee_1.default().where({ id: id }).save(tslib_1.__assign({}, params.selfData), { patch: true })];
                case 47:
                    subdivision = (_d.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=employeeService.js.map