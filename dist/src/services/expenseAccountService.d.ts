import ExpenseAccountDetail from '../domain/entities/ExpenseAccountDetail';
import ExpenseAccountPayload from '../domain/requests/ExpenseAccountPayload';
export declare function fetchAll(limit: number, offset: number): Promise<ExpenseAccountDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: ExpenseAccountPayload): Promise<ExpenseAccountDetail>;
export declare function getById(id: number): Promise<ExpenseAccountDetail>;
export declare function destroy(id: number): Promise<ExpenseAccountDetail>;
export declare function update(id: number, params: ExpenseAccountPayload): Promise<ExpenseAccountDetail>;
