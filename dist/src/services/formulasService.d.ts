import { FormulasDetail } from '../domain/entities/FormulasDetail';
import FormulasPayload from '../domain/requests/FormulasPayload';
export declare function fetchAll(limit: number, offset: number): Promise<FormulasDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: FormulasPayload): Promise<FormulasDetail>;
export declare function getById(id: number): Promise<FormulasDetail>;
export declare function destroy(id: number): Promise<FormulasDetail>;
export declare function update(id: number, params: FormulasPayload): Promise<FormulasDetail>;
