import GroupDetail from '../domain/entities/GroupDetail';
import GroupPayload from '../domain/requests/GroupPayload';
export declare function fetchAll(limit: number, offset: number): Promise<GroupDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: GroupPayload): Promise<GroupPayload>;
export declare function getById(id: number): Promise<GroupDetail>;
export declare function destroy(id: number): Promise<GroupDetail>;
export declare function update(id: number, params: GroupPayload): Promise<GroupDetail>;
