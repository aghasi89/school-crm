import HeadPositionDetail from '../domain/entities/HeadPositionDetail';
import HeadPositionPayload from '../domain/requests/HeadPositionPayload';
export declare function fetchAll(limit: number, offset: number): Promise<HeadPositionDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: HeadPositionPayload): Promise<HeadPositionPayload>;
export declare function getById(id: number): Promise<HeadPositionDetail>;
export declare function destroy(id: number): Promise<HeadPositionDetail>;
export declare function update(id: number, params: HeadPositionPayload): Promise<HeadPositionDetail>;
