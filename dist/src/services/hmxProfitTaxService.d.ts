import HmxProfitTaxDetail from '../domain/entities/HmxProfitTaxDetail';
import HmxProfitTaxPayload from '../domain/requests/HmxProfitTaxPayload';
export declare function fetchAll(limit: number, offset: number): Promise<HmxProfitTaxDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: HmxProfitTaxPayload): Promise<HmxProfitTaxPayload>;
export declare function getById(id: number): Promise<HmxProfitTaxDetail>;
export declare function destroy(id: number): Promise<HmxProfitTaxDetail>;
export declare function update(id: number, params: HmxProfitTaxPayload): Promise<HmxProfitTaxDetail>;
