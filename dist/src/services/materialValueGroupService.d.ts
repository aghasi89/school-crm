import MaterialValueGroupDetail from '../domain/entities/MaterialValueGroupDetail';
import MaterialValueGroupPayload from '../domain/requests/MaterialValueGroupPayload';
export declare function fetchAll(limit: number, offset: number): Promise<MaterialValueGroupDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: MaterialValueGroupPayload): Promise<MaterialValueGroupDetail>;
export declare function getById(id: number): Promise<MaterialValueGroupDetail>;
export declare function destroy(id: number): Promise<MaterialValueGroupDetail>;
export declare function update(id: number, params: MaterialValueGroupPayload): Promise<MaterialValueGroupDetail>;
