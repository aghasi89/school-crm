import MaterialValueDetail from '../domain/entities/MaterialValueDetail';
import MaterialValuePayload from '../domain/requests/MaterialValuePayload';
export declare function fetchAll(limit: number, offset: number): Promise<MaterialValueDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: MaterialValuePayload): Promise<MaterialValueDetail>;
export declare function getById(id: number): Promise<MaterialValueDetail>;
export declare function destroy(id: number): Promise<MaterialValueDetail>;
export declare function update(id: number, params: MaterialValuePayload): Promise<MaterialValueDetail>;
