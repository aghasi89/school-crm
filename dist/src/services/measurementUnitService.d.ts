import { MeasurementUnitDetail } from '../domain/entities/MeasurementUnitDetail';
import { MeasurementUnitPayload } from '../domain/requests/MeasurementUnitPayload';
export declare function fetchAll(limit: number, offset: number): Promise<MeasurementUnitDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: MeasurementUnitPayload): Promise<MeasurementUnitDetail>;
export declare function getById(id: number): Promise<MeasurementUnitDetail>;
export declare function destroy(id: number): Promise<MeasurementUnitDetail>;
export declare function update(id: number, params: MeasurementUnitPayload): Promise<MeasurementUnitDetail>;
