import OperationsDetail from '../domain/entities/OperationsDetail';
import OperationsPayload from '../domain/requests/OperationsPayload';
export declare function fetchAll(limit: number, offset: number): Promise<OperationsDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: OperationsPayload): Promise<OperationsDetail>;
export declare function getById(id: number): Promise<OperationsDetail>;
export declare function destroy(id: number): Promise<OperationsDetail>;
export declare function update(id: number, params: OperationsPayload): Promise<OperationsDetail>;
