"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Operations_1 = tslib_1.__importDefault(require("../models/Operations"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _operations, operations, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Operations_1.default.fetchAll()];
                case 1:
                    _operations = _a.sent();
                    return [4, (_operations).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({
                            withRelated: [
                                'debit',
                                'dPartners',
                                'dAnaliticGroup2',
                                'dAnaliticGroup1',
                                'credit',
                                'cPartners',
                                'cAnaliticGroup2',
                                'cAnaliticGroup1'
                            ]
                        })];
                case 2:
                    operations = _a.sent();
                    res = transform_1.default(operations.serialize(), function (operations) { return ({
                        id: operations.id,
                        debitId: operations.debitId,
                        dPartnersId: operations.dPartnersId,
                        dAnaliticGroup_2Id: operations.dAnaliticGroup_2Id,
                        dAnaliticGroup_1Id: operations.dAnaliticGroup_1Id,
                        creditId: operations.creditId,
                        cPartnersId: operations.cPartnersId,
                        cAnaliticGroup_2Id: operations.cAnaliticGroup_2Id,
                        cAnaliticGroup_1Id: operations.cAnaliticGroup_1Id,
                        money: operations.money,
                        comment: operations.comment,
                        partners: operations.partners,
                        analiticGroups1: operations.analiticGroups1,
                        analiticGroups2: operations.analiticGroups2
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Operations_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var operations;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Operations_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    operations = (_a.sent()).serialize();
                    return [2, operations];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var operations;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Operations_1.default({ id: id }).fetch({
                        withRelated: [
                            'debit',
                            'dPartners',
                            'dAnaliticGroup2',
                            'dAnaliticGroup1',
                            'credit',
                            'cPartners',
                            'cAnaliticGroup2',
                            'cAnaliticGroup1'
                        ]
                    })];
                case 1:
                    operations = (_a.sent());
                    if (operations) {
                        return [2, operations.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Operations_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Operations_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=operationsService.js.map