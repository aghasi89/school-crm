import PartnersDetail from '../domain/entities/PartnersDetail';
import PartnersPayload from '../domain/requests/PartnersPayload';
export declare function fetchAll(limit: number, offset: number): Promise<PartnersDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: PartnersPayload): Promise<PartnersPayload>;
export declare function getById(id: number): Promise<PartnersDetail>;
export declare function destroy(id: number): Promise<PartnersDetail>;
export declare function update(id: number, params: PartnersPayload): Promise<PartnersDetail>;
