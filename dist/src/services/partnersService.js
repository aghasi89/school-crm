"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Partners_1 = tslib_1.__importDefault(require("../models/Partners"));
var BillingAccount_1 = tslib_1.__importDefault(require("../models/BillingAccount"));
var AdditionalAddressePartners_1 = tslib_1.__importDefault(require("../models/AdditionalAddressePartners"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var object = tslib_1.__importStar(require("../utils/object"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _partnerss, partnerss, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Partners_1.default.fetchAll()];
                case 1:
                    _partnerss = _a.sent();
                    if (!limit || limit == 0) {
                        limit = Number(_partnerss.count());
                        offset = 0;
                    }
                    return [4, (_partnerss).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({ withRelated: ['billingAccounts', 'additionalAddressePartners'] })];
                case 2:
                    partnerss = _a.sent();
                    res = transform_1.default(partnerss.serialize(), function (partners) { return ({
                        id: partners.id,
                        name: partners.name,
                        AAHpayer: partners.AAHpayer,
                        accountantAAH: partners.accountantAAH,
                        accountantPositionId: partners.accountantPositionId,
                        anotherAdditionalInformation: partners.anotherAdditionalInformation,
                        anotherCredentialsNumber: partners.anotherCredentialsNumber,
                        anotherCredentialsDate: partners.anotherCredentialsNumber,
                        anotherDeliveryTime: partners.anotherDeliveryTime,
                        anotherFullname: partners.anotherFullname,
                        contract: partners.contract,
                        certificateNumber: partners.certificateNumber,
                        dateContract: partners.dateContract,
                        email: partners.email,
                        fullName: partners.fullName,
                        groupId: partners.groupId,
                        headAAH: partners.headAAH,
                        headPositionId: partners.headPositionId,
                        hvhh: partners.hvhh,
                        phone: partners.phone,
                        legalAddress: partners.legalAddress,
                        mainPurposeOfPayment: partners.mainPurposeOfPayment,
                        passportNumber: partners.passportNumber,
                        percentageOfSalesDiscount: partners.percentageOfSalesDiscount,
                        practicalAddress: partners.practicalAddress
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Partners_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(param) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var billingAccounts, additionalAddressePartners, partners, partnersId, billingAccounts_1, billingAccounts_1_1, billingAccount, e_1_1, additionalAddressePartners_1, additionalAddressePartners_1_1, additionalAddressePartner, e_2_1;
        var e_1, _a, e_2, _b;
        return tslib_1.__generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    billingAccounts = param['billingAccounts'];
                    additionalAddressePartners = param['additionalAddressePartners'];
                    delete param['additionalAddressePartners'];
                    delete param['billingAccounts'];
                    return [4, new Partners_1.default(tslib_1.__assign({}, param)).save()];
                case 1:
                    partners = (_c.sent()).serialize();
                    partnersId = partners['id'];
                    _c.label = 2;
                case 2:
                    _c.trys.push([2, 7, 8, 9]);
                    billingAccounts_1 = tslib_1.__values(billingAccounts), billingAccounts_1_1 = billingAccounts_1.next();
                    _c.label = 3;
                case 3:
                    if (!!billingAccounts_1_1.done) return [3, 6];
                    billingAccount = billingAccounts_1_1.value;
                    billingAccount['partnersId'] = partnersId;
                    return [4, new BillingAccount_1.default(tslib_1.__assign({}, billingAccount)).save()];
                case 4:
                    (_c.sent()).serialize();
                    _c.label = 5;
                case 5:
                    billingAccounts_1_1 = billingAccounts_1.next();
                    return [3, 3];
                case 6: return [3, 9];
                case 7:
                    e_1_1 = _c.sent();
                    e_1 = { error: e_1_1 };
                    return [3, 9];
                case 8:
                    try {
                        if (billingAccounts_1_1 && !billingAccounts_1_1.done && (_a = billingAccounts_1.return)) _a.call(billingAccounts_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                    return [7];
                case 9:
                    _c.trys.push([9, 14, 15, 16]);
                    additionalAddressePartners_1 = tslib_1.__values(additionalAddressePartners), additionalAddressePartners_1_1 = additionalAddressePartners_1.next();
                    _c.label = 10;
                case 10:
                    if (!!additionalAddressePartners_1_1.done) return [3, 13];
                    additionalAddressePartner = additionalAddressePartners_1_1.value;
                    additionalAddressePartner['partnersId'] = partnersId;
                    return [4, new AdditionalAddressePartners_1.default(tslib_1.__assign({}, additionalAddressePartner)).save()];
                case 11:
                    (_c.sent()).serialize();
                    _c.label = 12;
                case 12:
                    additionalAddressePartners_1_1 = additionalAddressePartners_1.next();
                    return [3, 10];
                case 13: return [3, 16];
                case 14:
                    e_2_1 = _c.sent();
                    e_2 = { error: e_2_1 };
                    return [3, 16];
                case 15:
                    try {
                        if (additionalAddressePartners_1_1 && !additionalAddressePartners_1_1.done && (_b = additionalAddressePartners_1.return)) _b.call(additionalAddressePartners_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                    return [7];
                case 16: return [2, object.camelize(partners)];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var position;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Partners_1.default({ id: id }).fetch({ withRelated: ['billingAccounts', 'additionalAddressePartners'] })];
                case 1:
                    position = (_a.sent());
                    if (position) {
                        return [2, position.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Partners_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var billingAccounts, additionalAddressePartners, partnersId, billingAccounts_2, billingAccounts_2_1, billingAccount, error_1, e_3_1, additionalAddressePartners_2, additionalAddressePartners_2_1, additionalAddressePartner, error_2, e_4_1, partners;
        var e_3, _a, e_4, _b;
        return tslib_1.__generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    billingAccounts = params['billingAccounts'];
                    additionalAddressePartners = params['additionalAddressePartners'];
                    delete params['additionalAddressePartners'];
                    delete params['billingAccounts'];
                    partnersId = id;
                    _c.label = 1;
                case 1:
                    _c.trys.push([1, 13, 14, 15]);
                    billingAccounts_2 = tslib_1.__values(billingAccounts), billingAccounts_2_1 = billingAccounts_2.next();
                    _c.label = 2;
                case 2:
                    if (!!billingAccounts_2_1.done) return [3, 12];
                    billingAccount = billingAccounts_2_1.value;
                    billingAccount['partnersId'] = partnersId;
                    _c.label = 3;
                case 3:
                    _c.trys.push([3, 10, , 11]);
                    console.log(billingAccount);
                    if (!(billingAccount['id'] && !billingAccount['isDeleted'])) return [3, 5];
                    return [4, new BillingAccount_1.default().where({ id: billingAccount['id'] }).save(tslib_1.__assign({}, billingAccount), { patch: true })];
                case 4:
                    (_c.sent()).serialize();
                    return [3, 9];
                case 5:
                    if (!(billingAccount['id'] && billingAccount['isDeleted'])) return [3, 7];
                    return [4, new BillingAccount_1.default({ id: billingAccount['id'] }).destroy()];
                case 6:
                    (_c.sent()).serialize();
                    return [3, 9];
                case 7: return [4, new BillingAccount_1.default(tslib_1.__assign({}, billingAccount)).save()];
                case 8:
                    (_c.sent()).serialize();
                    _c.label = 9;
                case 9: return [3, 11];
                case 10:
                    error_1 = _c.sent();
                    console.log(error_1);
                    return [3, 11];
                case 11:
                    billingAccounts_2_1 = billingAccounts_2.next();
                    return [3, 2];
                case 12: return [3, 15];
                case 13:
                    e_3_1 = _c.sent();
                    e_3 = { error: e_3_1 };
                    return [3, 15];
                case 14:
                    try {
                        if (billingAccounts_2_1 && !billingAccounts_2_1.done && (_a = billingAccounts_2.return)) _a.call(billingAccounts_2);
                    }
                    finally { if (e_3) throw e_3.error; }
                    return [7];
                case 15:
                    _c.trys.push([15, 27, 28, 29]);
                    additionalAddressePartners_2 = tslib_1.__values(additionalAddressePartners), additionalAddressePartners_2_1 = additionalAddressePartners_2.next();
                    _c.label = 16;
                case 16:
                    if (!!additionalAddressePartners_2_1.done) return [3, 26];
                    additionalAddressePartner = additionalAddressePartners_2_1.value;
                    additionalAddressePartner['partnersId'] = partnersId;
                    _c.label = 17;
                case 17:
                    _c.trys.push([17, 24, , 25]);
                    if (!(additionalAddressePartner['id'] && !additionalAddressePartner['isDeleted'])) return [3, 19];
                    return [4, new AdditionalAddressePartners_1.default().where({ id: additionalAddressePartner['id'] }).save(tslib_1.__assign({}, additionalAddressePartner), { patch: true })];
                case 18:
                    (_c.sent()).serialize();
                    return [3, 23];
                case 19:
                    if (!(additionalAddressePartner['id'] && additionalAddressePartner['isDeleted'])) return [3, 21];
                    return [4, new AdditionalAddressePartners_1.default({ id: additionalAddressePartner['id'] }).destroy()];
                case 20:
                    (_c.sent()).serialize();
                    return [3, 23];
                case 21: return [4, new AdditionalAddressePartners_1.default(tslib_1.__assign({}, additionalAddressePartner)).save()];
                case 22:
                    (_c.sent()).serialize();
                    _c.label = 23;
                case 23: return [3, 25];
                case 24:
                    error_2 = _c.sent();
                    console.log(error_2);
                    return [3, 25];
                case 25:
                    additionalAddressePartners_2_1 = additionalAddressePartners_2.next();
                    return [3, 16];
                case 26: return [3, 29];
                case 27:
                    e_4_1 = _c.sent();
                    e_4 = { error: e_4_1 };
                    return [3, 29];
                case 28:
                    try {
                        if (additionalAddressePartners_2_1 && !additionalAddressePartners_2_1.done && (_b = additionalAddressePartners_2.return)) _b.call(additionalAddressePartners_2);
                    }
                    finally { if (e_4) throw e_4.error; }
                    return [7];
                case 29: return [4, new Partners_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 30:
                    partners = (_c.sent()).serialize();
                    return [2, object.camelize(partners)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=partnersService.js.map