import { PositionDetail } from '../domain/entities/PositionDetail';
import { PositionPayload } from '../domain/requests/PositionPayload';
export declare function fetchAll(limit: number, offset: number): Promise<PositionDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: PositionPayload): Promise<PositionDetail>;
export declare function getById(id: number): Promise<PositionDetail>;
export declare function destroy(id: number): Promise<PositionDetail>;
export declare function update(id: number, params: PositionPayload): Promise<PositionDetail>;
