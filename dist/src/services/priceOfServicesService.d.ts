import { PriceOfServicesDetail } from '../domain/entities/PriceOfServicesDetail';
import PriceOfServicesPayload from '../domain/requests/PriceOfServicesPayload';
export declare function fetchAll(limit: number, offset: number): Promise<PriceOfServicesDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: PriceOfServicesPayload): Promise<PriceOfServicesDetail>;
export declare function getById(id: number): Promise<PriceOfServicesDetail>;
export declare function destroy(id: number): Promise<PriceOfServicesDetail>;
export declare function update(id: number, params: PriceOfServicesPayload): Promise<PriceOfServicesDetail>;
