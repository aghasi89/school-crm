"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var PriceOfServices_1 = tslib_1.__importDefault(require("../models/PriceOfServices"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _priceOfServices, priceOfServices, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, PriceOfServices_1.default.fetchAll()];
                case 1:
                    _priceOfServices = _a.sent();
                    return [4, (_priceOfServices).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({ withRelated: ['services'] })];
                case 2:
                    priceOfServices = _a.sent();
                    res = transform_1.default(priceOfServices.serialize(), function (priceOfServices) { return ({
                        id: priceOfServices.id,
                        startDate: priceOfServices.startDate,
                        endDate: priceOfServices.endDate,
                        servicesId: priceOfServices.servicesId,
                        services: priceOfServices.services,
                        type: priceOfServices.type
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, PriceOfServices_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var priceOfServices;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new PriceOfServices_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    priceOfServices = (_a.sent()).serialize();
                    return [2, priceOfServices];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var priceOfServices;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new PriceOfServices_1.default({ id: id }).fetch({ withRelated: ['services'] })];
                case 1:
                    priceOfServices = (_a.sent());
                    if (priceOfServices) {
                        return [2, priceOfServices.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new PriceOfServices_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new PriceOfServices_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=priceOfServicesService.js.map