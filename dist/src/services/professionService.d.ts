import ProfessionDetail from '../domain/entities/ProfessionDetail';
import ProfessionPayload from '../domain/requests/ProfessionPayload';
export declare function fetchAll(limit: number, offset: number): Promise<ProfessionDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: ProfessionPayload): Promise<ProfessionDetail>;
export declare function getById(id: number): Promise<ProfessionDetail>;
export declare function destroy(id: number): Promise<ProfessionDetail>;
export declare function update(id: number, params: ProfessionPayload): Promise<ProfessionDetail>;
