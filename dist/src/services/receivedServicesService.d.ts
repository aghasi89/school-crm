import { ReceivedServicesDetail } from '../domain/entities/ReceivedServicesDetail';
import { ReceivedServicesPayload } from '../domain/requests/ReceivedServicesPayload';
export declare function fetchAll(limit: number, offset: number): Promise<ReceivedServicesDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: ReceivedServicesPayload): Promise<ReceivedServicesDetail>;
export declare function getById(id: number): Promise<ReceivedServicesDetail>;
export declare function destroy(id: number): Promise<ReceivedServicesDetail>;
export declare function update(id: number, params: ReceivedServicesPayload): Promise<ReceivedServicesDetail>;
