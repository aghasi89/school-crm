"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var ReceivedServices_1 = require("../models/ReceivedServices");
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _receivedServices, receivedServices, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, ReceivedServices_1.ReceivedServices.fetchAll()];
                case 1:
                    _receivedServices = _a.sent();
                    if (!limit || limit == 0) {
                        limit = Number(_receivedServices.count());
                        offset = 0;
                    }
                    return [4, (_receivedServices).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({
                            withRelated: [
                                'providers',
                                'providerAccount',
                                'advancePaymentAccount',
                                'analiticGroup2',
                                'analiticGroup1'
                            ]
                        })];
                case 2:
                    receivedServices = _a.sent();
                    res = transform_1.default(receivedServices.serialize(), function (receivedServices) { return ({
                        id: receivedServices.id,
                        currencyId: receivedServices.currencyId,
                        currencyExchangeRate1: receivedServices.currencyExchangeRate1,
                        currencyExchangeRate2: receivedServices.currencyExchangeRate2,
                        previousDayExchangeRate: receivedServices.previousDayExchangeRate,
                        partnersId: receivedServices.partnersId,
                        providerAccountId: receivedServices.providerAccountId,
                        advancePaymentAccountId: receivedServices.advancePaymentAccountId,
                        analiticGroup_1Id: receivedServices.analiticGroup_1Id,
                        analiticGroup_2Id: receivedServices.analiticGroup_2Id,
                        outputMethod: receivedServices.outputMethod,
                        purchaseDocumentNumber: receivedServices.purchaseDocumentNumber,
                        purchaseDocumentDate: receivedServices.purchaseDocumentDate,
                        comment: receivedServices.comment,
                        purchaseTypeOfService: receivedServices.purchaseTypeOfService,
                        calculationTypeAah: receivedServices.calculationTypeAah,
                        includeAahInExpense: receivedServices.includeAahInExpense,
                        formOfReflectionAah: receivedServices.formOfReflectionAah,
                        typicalOperation: receivedServices.typicalOperation,
                        currencies: receivedServices.currencies,
                        partners: receivedServices.partners,
                        analiticGroup1: receivedServices.analiticGroup1,
                        analiticGroup2: receivedServices.analiticGroup2
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, ReceivedServices_1.ReceivedServices.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var receivedServices;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new ReceivedServices_1.ReceivedServices(tslib_1.__assign({}, params)).save()];
                case 1:
                    receivedServices = (_a.sent()).serialize();
                    return [2, receivedServices];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var receivedServices;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new ReceivedServices_1.ReceivedServices({ id: id }).fetch({
                        withRelated: [
                            'providers',
                            'providerAccount',
                            'advancePaymentAccount',
                            'analiticGroup2',
                            'analiticGroup1'
                        ]
                    })];
                case 1:
                    receivedServices = (_a.sent());
                    if (receivedServices) {
                        return [2, receivedServices.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new ReceivedServices_1.ReceivedServices({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var receivedServices;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new ReceivedServices_1.ReceivedServices().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    receivedServices = (_a.sent()).serialize();
                    return [2, object.camelize(receivedServices)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=receivedServicesService.js.map