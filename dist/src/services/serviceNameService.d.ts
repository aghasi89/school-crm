import ServiceNameDetail from '../domain/entities/ServiceNameDetail';
import ServiceNamePayload from '../domain/requests/ServiceNamePayload';
export declare function fetchAll(): Promise<ServiceNameDetail[]>;
export declare function insert(params: ServiceNamePayload): Promise<ServiceNameDetail>;
