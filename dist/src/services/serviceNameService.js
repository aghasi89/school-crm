"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var ServiceName_1 = tslib_1.__importDefault(require("../models/ServiceName"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var object = tslib_1.__importStar(require("../utils/object"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
function fetchAll() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var serviceNames, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'Fetching users from database');
                    return [4, ServiceName_1.default.fetchAll()];
                case 1:
                    serviceNames = _a.sent();
                    res = transform_1.default(serviceNames.serialize(), function (serviceName) { return ({
                        code: serviceName.code,
                        service_type_id: serviceName.service_type_id
                    }); });
                    logger_1.default.log('debug', 'Fetched all users successfully:', res);
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var serviceName;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new ServiceName_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    serviceName = (_a.sent()).serialize();
                    logger_1.default.log('debug', 'Inserted user successfully:', serviceName);
                    return [2, object.camelize(serviceName)];
            }
        });
    });
}
exports.insert = insert;
//# sourceMappingURL=serviceNameService.js.map