import ServiceTypeDetail from '../domain/entities/ServiceTypeDetail';
import ServiceTypePayload from '../domain/requests/ServiceTypePayload';
export declare function fetchAll(): Promise<ServiceTypeDetail[]>;
export declare function insert(params: ServiceTypePayload): Promise<ServiceTypeDetail>;
