"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var ServiceType_1 = tslib_1.__importDefault(require("../models/ServiceType"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var object = tslib_1.__importStar(require("../utils/object"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
function fetchAll() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var serviceTypes, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'Fetching users from database');
                    return [4, ServiceType_1.default.fetchAll()];
                case 1:
                    serviceTypes = _a.sent();
                    res = transform_1.default(serviceTypes.serialize(), function (serviceType) { return ({
                        name: serviceType.name
                    }); });
                    logger_1.default.log('debug', 'Fetched all users successfully:', res);
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var serviceType;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new ServiceType_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    serviceType = (_a.sent()).serialize();
                    logger_1.default.log('debug', 'Inserted user successfully:', serviceType);
                    return [2, object.camelize(serviceType)];
            }
        });
    });
}
exports.insert = insert;
//# sourceMappingURL=serviceTypeService.js.map