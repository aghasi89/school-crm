import ServicesDetail from '../domain/entities/ServicesDetail';
import ServicesPayload from '../domain/requests/ServicesPayload';
export declare function fetchAll(limit: number, offset: number): Promise<ServicesDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: ServicesPayload): Promise<ServicesDetail>;
export declare function getById(id: number): Promise<ServicesDetail>;
export declare function destroy(id: number): Promise<ServicesDetail>;
export declare function update(id: number, params: ServicesPayload): Promise<ServicesDetail>;
