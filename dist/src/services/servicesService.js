"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Services_1 = tslib_1.__importDefault(require("../models/Services"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var object = tslib_1.__importStar(require("../utils/object"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _servicess, servicess, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'Fetching users from database');
                    return [4, Services_1.default.fetchAll()];
                case 1:
                    _servicess = _a.sent();
                    if (!limit || limit == 0) {
                        limit = Number(_servicess.count());
                        offset = 0;
                    }
                    return [4, (_servicess).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({ withRelated: ['measurementUnit', 'classification', 'account'] })];
                case 2:
                    servicess = _a.sent();
                    res = transform_1.default(servicess.serialize(), function (services) { return ({
                        code: services.code,
                        fullName: services.fullName,
                        accountId: services.accountId,
                        barCode: services.barCode,
                        classificationId: services.classificationId,
                        isAah: services.isAah,
                        measurementUnitId: services.measurementUnitId,
                        name: services.name,
                        retailerPrice: services.retailerPrice,
                        wholesalePrice: services.wholesalePrice,
                        account: services.account,
                        classification: services.classification,
                        measurementUnit: services.measurementUnit,
                        id: services.id
                    }); });
                    logger_1.default.log('debug', 'Fetched all users successfully:', res);
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Services_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var services;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    console.log(params, '*********');
                    return [4, new Services_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    services = (_a.sent()).serialize();
                    logger_1.default.log('debug', 'Inserted user successfully:', services);
                    return [2, object.camelize(services)];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var position;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Services_1.default({ id: id }).fetch({ withRelated: ['measurementUnit', 'classification', 'account'] })];
                case 1:
                    position = (_a.sent());
                    if (position) {
                        return [2, position.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Services_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var partners;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new Services_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    partners = (_a.sent()).serialize();
                    return [2, object.camelize(partners)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=servicesService.js.map