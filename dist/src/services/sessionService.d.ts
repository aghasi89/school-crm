import UserSessionDetail from '../domain/entities/UserSessionDetail';
import UserSessionPayload from '../domain/requests/UserSessionPayload';
export declare function create(params: UserSessionPayload): Promise<UserSessionDetail>;
export declare function remove(token: string): Promise<UserSessionDetail>;
