"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var UserSession_1 = tslib_1.__importDefault(require("../models/UserSession"));
var ErrorType_1 = tslib_1.__importDefault(require("../resources/enums/ErrorType"));
var ForbiddenError_1 = tslib_1.__importDefault(require("../exceptions/ForbiddenError"));
var errors = config_1.default.errors;
function create(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var session;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'User Session: Creating session -', params);
                    return [4, new UserSession_1.default(params).save()];
                case 1:
                    session = (_a.sent()).serialize();
                    logger_1.default.log('debug', 'User Session: Session created successfully -', session);
                    return [2, object.camelize(session)];
            }
        });
    });
}
exports.create = create;
function remove(token) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var session, err_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    logger_1.default.log('info', 'User Session: Deactivating token - %s', token);
                    return [4, new UserSession_1.default()
                            .where({ token: token, is_active: true })
                            .save({ isActive: false }, { patch: true })];
                case 1:
                    session = (_a.sent()).serialize();
                    logger_1.default.log('debug', 'User Session: Deactivated session -', session);
                    return [2, object.camelize(session)];
                case 2:
                    err_1 = _a.sent();
                    if (err_1.message === ErrorType_1.default.NO_ROWS_UPDATED_ERROR) {
                        throw new ForbiddenError_1.default(errors.sessionNotMaintained);
                    }
                    throw err_1;
                case 3: return [2];
            }
        });
    });
}
exports.remove = remove;
//# sourceMappingURL=sessionService.js.map