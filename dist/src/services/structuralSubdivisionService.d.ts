import { StructuralSubdivisionDetail } from '../domain/entities/StructuralSubdivisionDetail';
import StructuralSubdivisionPayload from '../domain/requests/StructuralSubdivisionPayload';
export declare function fetchAll(limit: number, offset: number): Promise<StructuralSubdivisionDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: StructuralSubdivisionPayload): Promise<StructuralSubdivisionDetail>;
export declare function getById(id: number): Promise<StructuralSubdivisionDetail>;
export declare function destroy(id: number): Promise<StructuralSubdivisionDetail>;
export declare function update(id: number, params: StructuralSubdivisionPayload): Promise<StructuralSubdivisionDetail>;
