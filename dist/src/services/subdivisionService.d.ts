import { SubdivisionDetail } from '../domain/entities/SubdivisionDetail';
import { SubdivisionPayload } from '../domain/requests/SubdivisionPayload';
export declare function fetchAll(limit: number, offset: number): Promise<SubdivisionDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: SubdivisionPayload): Promise<SubdivisionPayload>;
export declare function getById(id: number): Promise<SubdivisionDetail>;
export declare function destroy(id: number): Promise<SubdivisionDetail>;
export declare function update(id: number, params: SubdivisionPayload): Promise<SubdivisionDetail>;
