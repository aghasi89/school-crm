import { SubsectionDetail } from '../domain/entities/SubsectionDetail';
import { SubsectionPayload } from '../domain/requests/SubsectionPayload';
export declare function fetchAll(limit: number, offset: number): Promise<SubsectionDetail[]>;
export declare function fetchAllByType(id: number): Promise<SubsectionDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: SubsectionPayload): Promise<SubsectionPayload>;
export declare function getById(id: number): Promise<SubsectionDetail>;
export declare function destroy(id: number): Promise<SubsectionDetail>;
export declare function update(id: number, params: SubsectionPayload): Promise<SubsectionDetail>;
