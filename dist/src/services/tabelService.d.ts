import TabelDetail from '../domain/entities/TabelDetail';
import TabelPayload from '../domain/requests/TabelPayload';
export declare function fetchAll(limit: number, offset: number): Promise<TabelDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: TabelPayload): Promise<TabelDetail>;
export declare function getById(id: number): Promise<TabelDetail>;
export declare function destroy(id: number): Promise<TabelDetail>;
export declare function update(id: number, params: TabelPayload): Promise<TabelDetail>;
