import TypeOfIncomeDetail from '../domain/entities/TypeOfIncomeDetail';
import TypeOfIncomePayload from '../domain/requests/TypeOfIncomePayload';
export declare function fetchAll(limit: number, offset: number): Promise<TypeOfIncomeDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: TypeOfIncomePayload): Promise<TypeOfIncomeDetail>;
export declare function getById(id: number): Promise<TypeOfIncomeDetail>;
export declare function destroy(id: number): Promise<TypeOfIncomeDetail>;
export declare function update(id: number, params: TypeOfIncomePayload): Promise<TypeOfIncomeDetail>;
