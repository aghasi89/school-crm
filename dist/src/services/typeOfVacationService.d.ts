import TypeOfVacationDetail from '../domain/entities/TypeOfVacationDetail';
import TypeOfVacationPayload from '../domain/requests/TypeOfVacationPayload';
export declare function fetchAll(limit: number, offset: number): Promise<TypeOfVacationDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: TypeOfVacationPayload): Promise<TypeOfVacationDetail>;
export declare function getById(id: number): Promise<TypeOfVacationDetail>;
export declare function destroy(id: number): Promise<TypeOfVacationDetail>;
export declare function update(id: number, params: TypeOfVacationPayload): Promise<TypeOfVacationDetail>;
