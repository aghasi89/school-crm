import TypeOfActionsDetail from '../domain/entities/TypesOfActionsDetail';
import TypeOfActionsPayload from '../domain/requests/TypesOfActionsPayload';
export declare function fetchAll(limit: number, offset: number): Promise<TypeOfActionsDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(params: TypeOfActionsPayload): Promise<TypeOfActionsDetail>;
export declare function getById(id: number): Promise<TypeOfActionsDetail>;
export declare function destroy(id: number): Promise<TypeOfActionsDetail>;
export declare function update(id: number, params: TypeOfActionsPayload): Promise<TypeOfActionsDetail>;
