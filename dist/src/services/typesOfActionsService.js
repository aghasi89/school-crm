"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var TypesOfActions_1 = tslib_1.__importDefault(require("../models/TypesOfActions"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var object = tslib_1.__importStar(require("../utils/object"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _typeOfActionss, typeOfActionss, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, TypesOfActions_1.default.fetchAll()];
                case 1:
                    _typeOfActionss = _a.sent();
                    return [4, (_typeOfActionss).query().limit(limit).offset(offset)];
                case 2:
                    typeOfActionss = _a.sent();
                    res = transform_1.default(typeOfActionss, function (typeOfActions) { return ({
                        id: typeOfActions.id,
                        name: typeOfActions.name,
                        code: typeOfActions.code
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, TypesOfActions_1.default.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var typeOfActions;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new TypesOfActions_1.default(tslib_1.__assign({}, params)).save()];
                case 1:
                    typeOfActions = (_a.sent()).serialize();
                    return [2, typeOfActions];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var typeOfActions;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new TypesOfActions_1.default({ id: id }).fetch()];
                case 1:
                    typeOfActions = (_a.sent());
                    if (typeOfActions) {
                        return [2, typeOfActions.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new TypesOfActions_1.default({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var subdivision;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new TypesOfActions_1.default().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 1:
                    subdivision = (_a.sent()).serialize();
                    return [2, object.camelize(subdivision)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=typesOfActionsService.js.map