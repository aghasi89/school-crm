import UserDetail from "../domain/entities/UserDetail";
import UserPayload from "../domain/requests/UserPayload";
import ResetPasswordPayload from "../domain/requests/ResetPasswordPayload";
import ResetPayload from "../domain/requests/ResetPayload";
import UniqueValueParamsPayload from "../domain/requests/UniqueValueParamsPayload";
export declare function fetchAll(): Promise<UserDetail[]>;
export declare function me(data: {
    loggedInPayload: any;
}): Promise<UserDetail>;
export declare function update(data: {
    loggedInPayload: any;
}, params: UserPayload): Promise<UserDetail>;
export declare function resetPassword(params: ResetPayload): Promise<string>;
export declare function checkToken(token: string): Promise<any>;
export declare function checkTokenLong(token: string): Promise<any>;
export declare function newPassword(params: ResetPasswordPayload): Promise<UserDetail>;
export declare function insert(params: UserPayload): Promise<UserDetail>;
export declare function insertAdmin(params: UserPayload): Promise<UserDetail>;
export declare function getUniqueValue(params: UniqueValueParamsPayload): Promise<any>;
