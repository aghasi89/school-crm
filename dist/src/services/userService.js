"use strict";
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var User_1 = tslib_1.__importDefault(require("../models/User"));
var logger_1 = tslib_1.__importDefault(require("../utils/logger"));
var bcrypt = tslib_1.__importStar(require("../utils/bcrypt"));
var object = tslib_1.__importStar(require("../utils/object"));
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var Role_1 = tslib_1.__importDefault(require("../resources/enums/Role"));
var knex_1 = tslib_1.__importDefault(require("../config/knex"));
var jwt = tslib_1.__importStar(require("../utils/jwt"));
var UnauthorizedError_1 = tslib_1.__importDefault(require("../exceptions/UnauthorizedError"));
var UniqueUserError_1 = tslib_1.__importDefault(require("../exceptions/UniqueUserError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var ErrorType_1 = tslib_1.__importDefault(require("./../resources/enums/ErrorType"));
var errors = config_1.default.errors;
var mail = tslib_1.__importStar(require("../utils/mail"));
var tokenErrorMessageMap = (_a = {},
    _a[ErrorType_1.default.INVALID] = errors.invalidToken,
    _a[ErrorType_1.default.EXPIRED] = errors.refreshTokenExpired,
    _a);
function fetchAll() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var users, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log("info", "Fetching users from database");
                    return [4, User_1.default.fetchAll()];
                case 1:
                    users = _a.sent();
                    res = transform_1.default(users.serialize(), function (user) { return ({
                        name: user.name,
                        email: user.email,
                        roleId: user.roleId,
                        updatedAt: new Date(user.updatedAt).toLocaleString(),
                        createdAt: new Date(user.updatedAt).toLocaleString()
                    }); });
                    logger_1.default.log("debug", "Fetched all users successfully:", res);
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function me(data) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, users, user, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log("info", "Fetching users from database");
                    id = data.loggedInPayload["userId"];
                    return [4, new User_1.default({ id: id }).fetch({ withRelated: ["role"] })];
                case 1:
                    users = _a.sent();
                    user = users.serialize();
                    res = {
                        name: user.name,
                        email: user.email,
                        roleId: user.roleId,
                        role: user.role,
                        updatedAt: new Date(user.updatedAt).toLocaleString(),
                        createdAt: new Date(user.updatedAt).toLocaleString()
                    };
                    return [2, res];
            }
        });
    });
}
exports.me = me;
function update(data, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var id, users, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log("info", "Fetching users from database");
                    id = data.loggedInPayload["userId"];
                    return [4, new User_1.default({ id: id }).where({ id: id }).save(tslib_1.__assign({}, params))];
                case 1:
                    users = (_a.sent()).serialize();
                    res = {
                        name: users.name,
                        email: users.email,
                        roleId: users.roleId,
                        role: users.role,
                        updatedAt: new Date(users.updatedAt).toLocaleString(),
                        createdAt: new Date(users.updatedAt).toLocaleString()
                    };
                    return [2, res];
            }
        });
    });
}
exports.update = update;
function resetPassword(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var users, user, token, url, err_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log("info", "Fetching users from database");
                    return [4, new User_1.default({ email: params.email }).fetch({ withRelated: ["role"] })];
                case 1:
                    users = _a.sent();
                    if (!users) {
                        throw new UnauthorizedError_1.default(errors.invalidCredentials);
                    }
                    user = users.serialize();
                    token = jwt.generateAccessTokenForReset(user);
                    url = "http://192.168.1.112:5500/auth/verify?verify=" + token;
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 4, , 5]);
                    return [4, mail.send({
                            to: params['email'],
                            subject: 'reset password',
                            markdown: "click this url  <a href=\"" + url + "\">reset!</a>"
                        })];
                case 3:
                    _a.sent();
                    return [2, " ok"];
                case 4:
                    err_1 = _a.sent();
                    console.log(err_1);
                    process.exit(1);
                    return [3, 5];
                case 5: return [2];
            }
        });
    });
}
exports.resetPassword = resetPassword;
function checkToken(token) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var response, _token, tokenErrorMessage;
        return tslib_1.__generator(this, function (_a) {
            try {
                response = jwt.verifyRefreshToken(token);
                _token = jwt.generateAccessTokenForResetLong(response);
                return [2, { token: _token }];
            }
            catch (err) {
                tokenErrorMessage = tokenErrorMessageMap[err.name];
                logger_1.default.log('error', 'JWT: Authentication failed - %s', err.message);
                if (tokenErrorMessage) {
                    logger_1.default.log('error', 'JWT: Token error - %s', tokenErrorMessage);
                    throw (new UnauthorizedError_1.default(tokenErrorMessage));
                }
            }
            return [2];
        });
    });
}
exports.checkToken = checkToken;
function checkTokenLong(token) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var tokenErrorMessage;
        return tslib_1.__generator(this, function (_a) {
            try {
                jwt.verifyRefreshToken(token);
                return [2, 'ok'];
            }
            catch (err) {
                tokenErrorMessage = tokenErrorMessageMap[err.name];
                logger_1.default.log('error', 'JWT: Authentication failed - %s', err.message);
                if (tokenErrorMessage) {
                    logger_1.default.log('error', 'JWT: Token error - %s', tokenErrorMessage);
                    throw (new UnauthorizedError_1.default(tokenErrorMessage));
                }
            }
            return [2];
        });
    });
}
exports.checkTokenLong = checkTokenLong;
function newPassword(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var tokenData, password, user;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log("info", "Inserting user into database:", params);
                    tokenData = jwt.verifyRefreshToken(params['token']);
                    return [4, bcrypt.hash(params.password)];
                case 1:
                    password = _a.sent();
                    return [4, new User_1.default().where({ id: tokenData.data.data.id }).save({ password: password }, { patch: true })];
                case 2:
                    user = (_a.sent()).serialize();
                    logger_1.default.log("debug", "Inserted user successfully:", user);
                    return [2, object.camelize(user)];
            }
        });
    });
}
exports.newPassword = newPassword;
function insert(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var users, password, user;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'Inserting user into database:', params);
                    return [4, new User_1.default({ email: params['email'] }).fetch({ withRelated: ["role"] })];
                case 1:
                    users = _a.sent();
                    console.log(users, '-------------');
                    if (users) {
                        throw "unique email";
                    }
                    return [4, bcrypt.hash(params.password)];
                case 2:
                    password = _a.sent();
                    return [4, new User_1.default(tslib_1.__assign(tslib_1.__assign({}, params), { password: password, roleId: Role_1.default.NORMAL_USER })).save()];
                case 3:
                    user = (_a.sent()).serialize();
                    logger_1.default.log('debug', 'Inserted user successfully:', user);
                    return [2, object.camelize(user)];
            }
        });
    });
}
exports.insert = insert;
function insertAdmin(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var users, password, user;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', 'Inserting user into database:', params);
                    return [4, new User_1.default({ email: params['email'] }).fetch({ withRelated: ["role"] })];
                case 1:
                    users = _a.sent();
                    console.log(users, '-------------');
                    if (users) {
                        throw new UniqueUserError_1.default(errors.uniqueEmail);
                    }
                    return [4, bcrypt.hash(params.password)];
                case 2:
                    password = _a.sent();
                    return [4, new User_1.default(tslib_1.__assign(tslib_1.__assign({}, params), { password: password, roleId: Role_1.default.ADMIN })).save()];
                case 3:
                    user = (_a.sent()).serialize();
                    logger_1.default.log('debug', 'Inserted user successfully:', user);
                    return [2, object.camelize(user)];
            }
        });
    });
}
exports.insertAdmin = insertAdmin;
function getUniqueValue(params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var k, maxColumValue;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, knex_1.default(params.table).select(params.colum).orderBy(params.colum, 'desc').limit(1).first()];
                case 1:
                    k = _a.sent();
                    maxColumValue = JSON.stringify(k['id']) + 1;
                    return [2, { message: { maxColumValue: maxColumValue } }];
            }
        });
    });
}
exports.getUniqueValue = getUniqueValue;
//# sourceMappingURL=userService.js.map