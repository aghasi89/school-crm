import { WarehouseEntryOrderDetail } from '../domain/entities/WarehouseEntryOrderDetail';
import { WarehouseEntryOrderPayload, WarehouseEntryOrderFunctionsPayload, WarehouseEntryOrderProductPayload } from '../domain/requests/WarehouseEntryOrderPayload';
export declare function fetchAll(limit: number, offset: number): Promise<WarehouseEntryOrderDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: WarehouseEntryOrderPayload): Promise<WarehouseEntryOrderPayload>;
export declare function getById(id: number): Promise<WarehouseEntryOrderDetail>;
export declare function destroy(id: number): Promise<WarehouseEntryOrderDetail>;
export declare function getWarehouseEntryOrderProductPayload(params: Array<WarehouseEntryOrderProductPayload>): Array<WarehouseEntryOrderFunctionsPayload>;
export declare function update(id: number, params: WarehouseEntryOrderPayload): Promise<WarehouseEntryOrderDetail>;
