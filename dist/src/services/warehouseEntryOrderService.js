"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var WarehouseEntryOrder_1 = require("../models/WarehouseEntryOrder");
var WarehouseEntryOrder_2 = require("../models/WarehouseEntryOrder");
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var object = tslib_1.__importStar(require("../utils/object"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _warehouseEntryOrders, warehouseEntryOrders, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, WarehouseEntryOrder_1.WarehouseEntryOrder.fetchAll()];
                case 1:
                    _warehouseEntryOrders = _a.sent();
                    if (!limit || limit == 0) {
                        limit = Number(_warehouseEntryOrders.count());
                        offset = 0;
                    }
                    return [4, (_warehouseEntryOrders).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({
                            withRelated: [
                                'warehouse',
                                'partners',
                                'partnersAccount',
                                'prepaidAccount',
                                'analiticGroup2',
                                'analiticGroup1',
                                'warehouseEntryOrderFunctions',
                                'warehouseEntryOrderProduct'
                            ]
                        })];
                case 2:
                    warehouseEntryOrders = _a.sent();
                    res = transform_1.default(warehouseEntryOrders.serialize(), function (warehouseEntryOrder) { return ({
                        id: warehouseEntryOrder.id,
                        accept: warehouseEntryOrder.accept,
                        accountant: warehouseEntryOrder.accountant,
                        allow: warehouseEntryOrder.allow,
                        analiticGroup_1Id: warehouseEntryOrder.analiticGroup_1Id,
                        analiticGroup_2Id: warehouseEntryOrder.analiticGroup_2Id,
                        calculationStyleOfAah: warehouseEntryOrder.calculationStyleOfAah,
                        comment: warehouseEntryOrder.comment,
                        container: warehouseEntryOrder.container,
                        date: warehouseEntryOrder.date,
                        documentDate: warehouseEntryOrder.documentDate,
                        documentN: warehouseEntryOrder.documentN,
                        documentOfTransport: warehouseEntryOrder.documentOfTransport,
                        documentOfTransportDate: warehouseEntryOrder.documentOfTransportDate,
                        includeAahInCost: warehouseEntryOrder.includeAahInCost,
                        mediator: warehouseEntryOrder.mediator,
                        partnersAccountId: warehouseEntryOrder.partnersAccountId,
                        partnersId: warehouseEntryOrder.partnersId,
                        powerOfAttorney: warehouseEntryOrder.powerOfAttorney,
                        prepaidAccountId: warehouseEntryOrder.prepaidAccountId,
                        typeOfAcquisitionOfNa: warehouseEntryOrder.typeOfAcquisitionOfNa,
                        warehouseId: warehouseEntryOrder.warehouseId,
                        warehouse: warehouseEntryOrder.warehouse,
                        partners: warehouseEntryOrder.partners,
                        analiticGroup1: warehouseEntryOrder.analiticGroup1,
                        analiticGroup2: warehouseEntryOrder.analiticGroup2
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, WarehouseEntryOrder_1.WarehouseEntryOrder.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(param) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var warehouseEntryOrderProduct, warehouseEntryOrder, warehouseEntryOrderProduct_1, warehouseEntryOrderProduct_1_1, iterator, e_1_1;
        var e_1, _a;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    warehouseEntryOrderProduct = param.warehouseEntryOrderProduct;
                    delete param.warehouseEntryOrderProduct;
                    delete param.warehouseEntryOrderFunctions;
                    return [4, new WarehouseEntryOrder_1.WarehouseEntryOrder(tslib_1.__assign({}, param)).save()];
                case 1:
                    warehouseEntryOrder = (_b.sent()).serialize();
                    _b.label = 2;
                case 2:
                    _b.trys.push([2, 7, 8, 9]);
                    warehouseEntryOrderProduct_1 = tslib_1.__values(warehouseEntryOrderProduct), warehouseEntryOrderProduct_1_1 = warehouseEntryOrderProduct_1.next();
                    _b.label = 3;
                case 3:
                    if (!!warehouseEntryOrderProduct_1_1.done) return [3, 6];
                    iterator = warehouseEntryOrderProduct_1_1.value;
                    iterator.warehouseEntryOrderId = warehouseEntryOrder.id;
                    return [4, new WarehouseEntryOrder_2.WarehouseEntryOrderProduct(tslib_1.__assign({}, iterator)).save()];
                case 4:
                    (_b.sent()).serialize();
                    _b.label = 5;
                case 5:
                    warehouseEntryOrderProduct_1_1 = warehouseEntryOrderProduct_1.next();
                    return [3, 3];
                case 6: return [3, 9];
                case 7:
                    e_1_1 = _b.sent();
                    e_1 = { error: e_1_1 };
                    return [3, 9];
                case 8:
                    try {
                        if (warehouseEntryOrderProduct_1_1 && !warehouseEntryOrderProduct_1_1.done && (_a = warehouseEntryOrderProduct_1.return)) _a.call(warehouseEntryOrderProduct_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                    return [7];
                case 9: return [2, object.camelize(warehouseEntryOrder)];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var position;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new WarehouseEntryOrder_1.WarehouseEntryOrder({ id: id }).fetch({
                        withRelated: [
                            'warehouse',
                            'partners',
                            'partnersAccount',
                            'prepaidAccount',
                            'analiticGroup2',
                            'analiticGroup1',
                            'warehouseEntryOrderFunctions',
                            'warehouseEntryOrderProduct.materialValue',
                            'warehouseEntryOrderProduct.accounts',
                            'warehouseEntryOrderProduct.classification'
                        ]
                    })];
                case 1:
                    position = (_a.sent());
                    if (position) {
                        return [2, position.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new WarehouseEntryOrder_1.WarehouseEntryOrder({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function getWarehouseEntryOrderProductPayload(params) {
    console.log(params, "getWarehouseEntryOrderProductPayload");
    var res = [{
            debitId: 1,
            dPartnersId: 1,
            dAnaliticGroup_2Id: 1,
            dAnaliticGroup_1Id: 1,
            creditId: 1,
            cPartnersIdisAah: 1,
            cAnaliticGroup_2Id: 1,
            cAnaliticGroup_1Id: 1,
            money: 10,
            comment: "test",
        }];
    return res;
}
exports.getWarehouseEntryOrderProductPayload = getWarehouseEntryOrderProductPayload;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var warehouseEntryOrderProducts, warehouseEntryOrderProducts_1, warehouseEntryOrderProducts_1_1, warehouseEntryOrderProduct, error_1, e_2_1, warehouseEntryOrder;
        var e_2, _a;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    warehouseEntryOrderProducts = params.warehouseEntryOrderProduct;
                    delete params.warehouseEntryOrderProduct;
                    delete params.warehouseEntryOrderFunctions;
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 13, 14, 15]);
                    warehouseEntryOrderProducts_1 = tslib_1.__values(warehouseEntryOrderProducts), warehouseEntryOrderProducts_1_1 = warehouseEntryOrderProducts_1.next();
                    _b.label = 2;
                case 2:
                    if (!!warehouseEntryOrderProducts_1_1.done) return [3, 12];
                    warehouseEntryOrderProduct = warehouseEntryOrderProducts_1_1.value;
                    warehouseEntryOrderProduct['warehouseEntryOrderId'] = id;
                    _b.label = 3;
                case 3:
                    _b.trys.push([3, 10, , 11]);
                    if (!(warehouseEntryOrderProduct['id'] && !warehouseEntryOrderProduct['isDeleted'])) return [3, 5];
                    return [4, new WarehouseEntryOrder_2.WarehouseEntryOrderProduct().where({ id: warehouseEntryOrderProduct['id'] }).save(tslib_1.__assign({}, warehouseEntryOrderProduct), { patch: true })];
                case 4:
                    (_b.sent()).serialize();
                    return [3, 9];
                case 5:
                    if (!(warehouseEntryOrderProduct['id'] && warehouseEntryOrderProduct['isDeleted'])) return [3, 7];
                    return [4, new WarehouseEntryOrder_2.WarehouseEntryOrderProduct({ id: warehouseEntryOrderProduct['id'] }).destroy()];
                case 6:
                    (_b.sent()).serialize();
                    return [3, 9];
                case 7: return [4, new WarehouseEntryOrder_2.WarehouseEntryOrderProduct(tslib_1.__assign({}, warehouseEntryOrderProduct)).save()];
                case 8:
                    (_b.sent()).serialize();
                    _b.label = 9;
                case 9: return [3, 11];
                case 10:
                    error_1 = _b.sent();
                    console.log(error_1);
                    return [3, 11];
                case 11:
                    warehouseEntryOrderProducts_1_1 = warehouseEntryOrderProducts_1.next();
                    return [3, 2];
                case 12: return [3, 15];
                case 13:
                    e_2_1 = _b.sent();
                    e_2 = { error: e_2_1 };
                    return [3, 15];
                case 14:
                    try {
                        if (warehouseEntryOrderProducts_1_1 && !warehouseEntryOrderProducts_1_1.done && (_a = warehouseEntryOrderProducts_1.return)) _a.call(warehouseEntryOrderProducts_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                    return [7];
                case 15: return [4, new WarehouseEntryOrder_1.WarehouseEntryOrder().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 16:
                    warehouseEntryOrder = (_b.sent()).serialize();
                    return [2, object.camelize(warehouseEntryOrder)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=warehouseEntryOrderService.js.map