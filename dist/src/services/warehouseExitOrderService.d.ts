import { WarehouseExitOrderDetail } from '../domain/entities/WarehouseExitOrderDetail';
import { WarehouseExitOrderPayload, WarehouseExitOrderProductPayload, WarehouseExitOrderFunctionsPayload } from '../domain/requests/WarehouseExitOrderPayload';
export declare function fetchAll(limit: number, offset: number): Promise<WarehouseExitOrderDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: WarehouseExitOrderPayload): Promise<WarehouseExitOrderPayload>;
export declare function getById(id: number): Promise<WarehouseExitOrderDetail>;
export declare function destroy(id: number): Promise<WarehouseExitOrderDetail>;
export declare function getWarehouseExitOrderProductPayload(params: Array<WarehouseExitOrderProductPayload>): Array<WarehouseExitOrderFunctionsPayload>;
export declare function update(id: number, params: WarehouseExitOrderPayload): Promise<WarehouseExitOrderDetail>;
