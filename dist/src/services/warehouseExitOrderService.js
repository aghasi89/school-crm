"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var WarehouseExitOrder_1 = require("../models/WarehouseExitOrder");
var WarehouseExitOrder_2 = require("../models/WarehouseExitOrder");
var transform_1 = tslib_1.__importDefault(require("../utils/transform"));
var object = tslib_1.__importStar(require("../utils/object"));
var NotFoundError_1 = tslib_1.__importDefault(require("../exceptions/NotFoundError"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var errors = config_1.default.errors;
function fetchAll(limit, offset) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var _warehouseExitOrders, warehouseExitOrders, res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, WarehouseExitOrder_1.WarehouseExitOrder.fetchAll()];
                case 1:
                    _warehouseExitOrders = _a.sent();
                    if (!limit || limit == 0) {
                        limit = Number(_warehouseExitOrders.count());
                        offset = 0;
                    }
                    return [4, (_warehouseExitOrders).query(function (qb) {
                            qb.limit(limit),
                                qb.offset(offset);
                        }).fetch({
                            withRelated: [
                                'warehouse',
                                'partners',
                                'partnersAccount',
                                'prepaidAccount',
                                'analiticGroup2',
                                'analiticGroup1',
                                'warehouseExitOrderFunctions',
                                'warehouseExitOrderProduct'
                            ]
                        })];
                case 2:
                    warehouseExitOrders = _a.sent();
                    res = transform_1.default(warehouseExitOrders.serialize(), function (warehouseExitOrder) { return ({
                        id: warehouseExitOrder.id,
                        documentNumber: warehouseExitOrder.documentNumber,
                        expenseAccountId: warehouseExitOrder.expenseAccountId,
                        hasRequested: warehouseExitOrder.hasRequested,
                        accountant: warehouseExitOrder.accountant,
                        allow: warehouseExitOrder.allow,
                        analiticGroup_1Id: warehouseExitOrder.analiticGroup_1Id,
                        analiticGroup_2Id: warehouseExitOrder.analiticGroup_2Id,
                        comment: warehouseExitOrder.comment,
                        container: warehouseExitOrder.container,
                        date: warehouseExitOrder.date,
                        mediator: warehouseExitOrder.mediator,
                        powerOfAttorney: warehouseExitOrder.powerOfAttorney,
                        warehouseId: warehouseExitOrder.warehouseId
                    }); });
                    return [2, res];
            }
        });
    });
}
exports.fetchAll = fetchAll;
function count() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var count;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, WarehouseExitOrder_1.WarehouseExitOrder.fetchAll()];
                case 1: return [4, (_a.sent()).count()];
                case 2:
                    count = _a.sent();
                    return [2, { count: count }];
            }
        });
    });
}
exports.count = count;
function insert(param) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var warehouseExitOrderProduct, warehouseExitOrder, warehouseExitOrderProduct_1, warehouseExitOrderProduct_1_1, iterator, e_1_1;
        var e_1, _a;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    warehouseExitOrderProduct = param.warehouseExitOrderProduct;
                    delete param.warehouseExitOrderProduct;
                    delete param.warehouseExitOrderFunctions;
                    console.log(param, '***********');
                    return [4, new WarehouseExitOrder_1.WarehouseExitOrder(tslib_1.__assign({}, param)).save()];
                case 1:
                    warehouseExitOrder = (_b.sent()).serialize();
                    _b.label = 2;
                case 2:
                    _b.trys.push([2, 7, 8, 9]);
                    warehouseExitOrderProduct_1 = tslib_1.__values(warehouseExitOrderProduct), warehouseExitOrderProduct_1_1 = warehouseExitOrderProduct_1.next();
                    _b.label = 3;
                case 3:
                    if (!!warehouseExitOrderProduct_1_1.done) return [3, 6];
                    iterator = warehouseExitOrderProduct_1_1.value;
                    iterator.warehouseExitOrderId = warehouseExitOrder.id;
                    return [4, new WarehouseExitOrder_2.WarehouseExitOrderProduct(tslib_1.__assign({}, iterator)).save()];
                case 4:
                    (_b.sent()).serialize();
                    _b.label = 5;
                case 5:
                    warehouseExitOrderProduct_1_1 = warehouseExitOrderProduct_1.next();
                    return [3, 3];
                case 6: return [3, 9];
                case 7:
                    e_1_1 = _b.sent();
                    e_1 = { error: e_1_1 };
                    return [3, 9];
                case 8:
                    try {
                        if (warehouseExitOrderProduct_1_1 && !warehouseExitOrderProduct_1_1.done && (_a = warehouseExitOrderProduct_1.return)) _a.call(warehouseExitOrderProduct_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                    return [7];
                case 9: return [2, object.camelize(warehouseExitOrder)];
            }
        });
    });
}
exports.insert = insert;
function getById(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var position;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new WarehouseExitOrder_1.WarehouseExitOrder({ id: id }).fetch({
                        withRelated: [
                            'warehouse',
                            'partners',
                            'partnersAccount',
                            'prepaidAccount',
                            'analiticGroup2',
                            'analiticGroup1',
                            'warehouseExitOrderFunctions',
                            'warehouseExitOrderProduct.materialValue',
                            'warehouseExitOrderProduct.accounts',
                            'warehouseExitOrderProduct.classification'
                        ]
                    })];
                case 1:
                    position = (_a.sent());
                    if (position) {
                        return [2, position.serialize()];
                    }
                    else {
                        throw new NotFoundError_1.default(errors.notFound);
                    }
                    return [2];
            }
        });
    });
}
exports.getById = getById;
function destroy(id) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var res;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, new WarehouseExitOrder_1.WarehouseExitOrder({ id: id }).destroy()];
                case 1:
                    res = (_a.sent()).serialize();
                    return [2, res];
            }
        });
    });
}
exports.destroy = destroy;
function getWarehouseExitOrderProductPayload(params) {
    console.log(params, "getWarehouseExitOrderProductPayload");
    var res = [{
            debitId: 1,
            dPartnersId: 1,
            dAnaliticGroup_2Id: 1,
            dAnaliticGroup_1Id: 1,
            creditId: 1,
            cPartnersIdisAah: 1,
            cAnaliticGroup_2Id: 1,
            cAnaliticGroup_1Id: 1,
            money: 10,
            comment: "test",
        }];
    return res;
}
exports.getWarehouseExitOrderProductPayload = getWarehouseExitOrderProductPayload;
function update(id, params) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var warehouseExitOrderProducts, warehouseExitOrderProducts_1, warehouseExitOrderProducts_1_1, warehouseExitOrderProduct, error_1, e_2_1, warehouseExitOrder;
        var e_2, _a;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    warehouseExitOrderProducts = params.warehouseExitOrderProduct;
                    delete params.warehouseExitOrderProduct;
                    delete params.warehouseExitOrderFunctions;
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 13, 14, 15]);
                    warehouseExitOrderProducts_1 = tslib_1.__values(warehouseExitOrderProducts), warehouseExitOrderProducts_1_1 = warehouseExitOrderProducts_1.next();
                    _b.label = 2;
                case 2:
                    if (!!warehouseExitOrderProducts_1_1.done) return [3, 12];
                    warehouseExitOrderProduct = warehouseExitOrderProducts_1_1.value;
                    warehouseExitOrderProduct['warehouseExitOrderId'] = id;
                    _b.label = 3;
                case 3:
                    _b.trys.push([3, 10, , 11]);
                    if (!(warehouseExitOrderProduct['id'] && !warehouseExitOrderProduct['isDeleted'])) return [3, 5];
                    return [4, new WarehouseExitOrder_2.WarehouseExitOrderProduct({ id: warehouseExitOrderProduct['id'] }).where({ id: warehouseExitOrderProduct['id'] }).save(tslib_1.__assign({}, warehouseExitOrderProduct), { patch: true })];
                case 4:
                    (_b.sent()).serialize();
                    return [3, 9];
                case 5:
                    if (!(warehouseExitOrderProduct['id'] && warehouseExitOrderProduct['isDeleted'])) return [3, 7];
                    return [4, new WarehouseExitOrder_2.WarehouseExitOrderProduct().where({ id: warehouseExitOrderProduct['id'] }).destroy()];
                case 6:
                    (_b.sent()).serialize();
                    return [3, 9];
                case 7: return [4, new WarehouseExitOrder_2.WarehouseExitOrderProduct(tslib_1.__assign({}, warehouseExitOrderProduct)).save()];
                case 8:
                    (_b.sent()).serialize();
                    _b.label = 9;
                case 9: return [3, 11];
                case 10:
                    error_1 = _b.sent();
                    console.log(error_1, '8888888888');
                    return [3, 11];
                case 11:
                    warehouseExitOrderProducts_1_1 = warehouseExitOrderProducts_1.next();
                    return [3, 2];
                case 12: return [3, 15];
                case 13:
                    e_2_1 = _b.sent();
                    e_2 = { error: e_2_1 };
                    return [3, 15];
                case 14:
                    try {
                        if (warehouseExitOrderProducts_1_1 && !warehouseExitOrderProducts_1_1.done && (_a = warehouseExitOrderProducts_1.return)) _a.call(warehouseExitOrderProducts_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                    return [7];
                case 15: return [4, new WarehouseExitOrder_1.WarehouseExitOrder().where({ id: id }).save(tslib_1.__assign({}, params), { patch: true })];
                case 16:
                    warehouseExitOrder = (_b.sent()).serialize();
                    return [2, object.camelize(warehouseExitOrder)];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=warehouseExitOrderService.js.map