import WarehouseDetail from '../domain/entities/WarehouseDetail';
import WarehousePayload from '../domain/requests/WarehousePayload';
export declare function fetchAll(limit: number, offset: number): Promise<WarehouseDetail[]>;
export declare function count(): Promise<object>;
export declare function insert(param: WarehousePayload): Promise<WarehousePayload>;
export declare function getById(id: number): Promise<WarehouseDetail>;
export declare function destroy(id: number): Promise<WarehouseDetail>;
export declare function update(id: number, params: WarehousePayload): Promise<WarehouseDetail>;
