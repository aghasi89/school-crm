"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isArray(arr) {
    return arr !== undefined && arr !== null && Array.isArray(arr);
}
exports.isArray = isArray;
//# sourceMappingURL=array.js.map