export declare function hash(value: string): Promise<string>;
export declare function compare(value: string, hashedValue: string): Promise<boolean>;
