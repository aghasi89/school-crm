"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bcrypt_1 = tslib_1.__importDefault(require("bcrypt"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
function hash(value) {
    var saltRounds = config_1.default.auth.saltRounds;
    return bcrypt_1.default.hash(value, saltRounds);
}
exports.hash = hash;
function compare(value, hashedValue) {
    return bcrypt_1.default.compare(value, hashedValue);
}
exports.compare = compare;
//# sourceMappingURL=bcrypt.js.map