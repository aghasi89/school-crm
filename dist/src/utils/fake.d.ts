export declare function generate<T>(factoryCallback: () => Promise<T>, total?: number): Promise<T[]>;
