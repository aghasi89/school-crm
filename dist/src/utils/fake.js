"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
function generate(factoryCallback, total) {
    if (total === void 0) { total = 1; }
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var data, i, _a, _b;
        return tslib_1.__generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    data = [];
                    i = 0;
                    _c.label = 1;
                case 1:
                    if (!(i < total)) return [3, 4];
                    _a = data;
                    _b = i;
                    return [4, factoryCallback()];
                case 2:
                    _a[_b] = _c.sent();
                    _c.label = 3;
                case 3:
                    i++;
                    return [3, 1];
                case 4: return [2, data];
            }
        });
    });
}
exports.generate = generate;
//# sourceMappingURL=fake.js.map