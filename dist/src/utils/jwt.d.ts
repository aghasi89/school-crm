import JWTPayload from '../domain/misc/JWTPayload';
import LoggedInUser from '../domain/misc/LoggedInUser';
export declare function generateAccessToken(data: LoggedInUser): string;
export declare function generateAccessTokenForReset(data: LoggedInUser): string;
export declare function generateAccessTokenForResetLong(data: LoggedInUser): string;
export declare function generateRefreshToken(data: JWTPayload): string;
export declare function verifyAccessToken(token: string): object | string;
export declare function verifyRefreshToken(token: string): object | string;
