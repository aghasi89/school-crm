"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var jsonwebtoken_1 = tslib_1.__importDefault(require("jsonwebtoken"));
var logger_1 = tslib_1.__importDefault(require("./logger"));
var config_1 = tslib_1.__importDefault(require("../config/config"));
var _a = config_1.default.auth, accessTokenDuration = _a.accessTokenDuration, accessTokenSecretKey = _a.accessTokenSecretKey, refreshTokenDuration = _a.refreshTokenDuration, refreshTokenSecretKey = _a.refreshTokenSecretKey, accessTokenDurationReset = _a.accessTokenDurationReset, accessTokenDurationResetLong = _a.accessTokenDurationResetLong;
function generateAccessToken(data) {
    logger_1.default.log('info', 'JWT: Generating access token -', { data: data, expiresIn: accessTokenDuration });
    return jsonwebtoken_1.default.sign({ data: data }, accessTokenSecretKey, { expiresIn: accessTokenDuration });
}
exports.generateAccessToken = generateAccessToken;
function generateAccessTokenForReset(data) {
    logger_1.default.log('info', 'JWT: Generating access token -', { data: data, expiresIn: accessTokenDurationReset });
    return jsonwebtoken_1.default.sign({ data: data }, refreshTokenSecretKey, { expiresIn: accessTokenDurationReset });
}
exports.generateAccessTokenForReset = generateAccessTokenForReset;
function generateAccessTokenForResetLong(data) {
    logger_1.default.log('info', 'JWT: Generating access token -', { data: data, expiresIn: accessTokenDurationResetLong });
    return jsonwebtoken_1.default.sign({ data: data }, refreshTokenSecretKey, { expiresIn: accessTokenDurationResetLong });
}
exports.generateAccessTokenForResetLong = generateAccessTokenForResetLong;
function generateRefreshToken(data) {
    logger_1.default.log('info', 'JWT: Generating refresh token -', { data: data, expiresIn: refreshTokenDuration });
    return jsonwebtoken_1.default.sign({ data: data }, refreshTokenSecretKey, { expiresIn: refreshTokenDuration });
}
exports.generateRefreshToken = generateRefreshToken;
function verifyAccessToken(token) {
    return jsonwebtoken_1.default.verify(token, accessTokenSecretKey);
}
exports.verifyAccessToken = verifyAccessToken;
function verifyRefreshToken(token) {
    return jsonwebtoken_1.default.verify(token, refreshTokenSecretKey);
}
exports.verifyRefreshToken = verifyRefreshToken;
//# sourceMappingURL=jwt.js.map