"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var fs_1 = tslib_1.__importDefault(require("fs"));
var winston_daily_rotate_file_1 = tslib_1.__importDefault(require("winston-daily-rotate-file"));
var winston_1 = require("winston");
var config_1 = tslib_1.__importDefault(require("../config/config"));
var environment = config_1.default.environment, logging = config_1.default.logging;
var combine = winston_1.format.combine, colorize = winston_1.format.colorize, splat = winston_1.format.splat, printf = winston_1.format.printf, timestamp = winston_1.format.timestamp;
var keysToFilter = ['password', 'token'];
var formatter = printf(function (info) {
    var level = info.level, message = info.message, ts = info.timestamp, restMeta = tslib_1.__rest(info, ["level", "message", "timestamp"]);
    var meta = restMeta && Object.keys(restMeta).length
        ? JSON.stringify(restMeta, function (key, value) { return (keysToFilter.includes(key) ? '******' : value); }, 2)
        : restMeta instanceof Object
            ? ''
            : restMeta;
    return "[ " + ts + " ] - [ " + level + " ] " + message + " " + meta;
});
if (!fs_1.default.existsSync(logging.dir)) {
    fs_1.default.mkdirSync(logging.dir);
}
var trans = [];
if (environment === 'development') {
    trans = [new winston_1.transports.Console()];
}
var logger = winston_1.createLogger({
    level: logging.level,
    format: combine(splat(), colorize(), timestamp(), formatter),
    transports: tslib_1.__spread(trans, [
        new winston_daily_rotate_file_1.default({
            maxSize: logging.maxSize,
            maxFiles: logging.maxFiles,
            datePattern: logging.datePattern,
            zippedArchive: true,
            filename: logging.dir + "/" + logging.level + "-%DATE%.log"
        })
    ])
});
exports.default = logger;
//# sourceMappingURL=logger.js.map