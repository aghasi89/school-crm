import MailOptions from '../domain/misc/MailOptions';
declare const transporter: any;
export declare function send(mailOptions: MailOptions): Promise<any>;
export default transporter;
