"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var nodemailer_1 = tslib_1.__importDefault(require("nodemailer"));
var nodemailer_markdown_1 = require("nodemailer-markdown");
var logger_1 = tslib_1.__importDefault(require("./logger"));
var mail_1 = tslib_1.__importDefault(require("../config/mail"));
var smtp = mail_1.default.smtp, from = mail_1.default.from;
var transporter = nodemailer_1.default.createTransport(smtp);
transporter.use('compile', nodemailer_markdown_1.markdown());
function send(mailOptions) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var info, err_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    if (!mailOptions.from) {
                        mailOptions = tslib_1.__assign(tslib_1.__assign({}, mailOptions), { from: from });
                    }
                    logger_1.default.log('debug', 'Mail: Sending email with options -', mailOptions);
                    return [4, transporter.sendMail(mailOptions)];
                case 1:
                    info = _a.sent();
                    return [2, info];
                case 2:
                    err_1 = _a.sent();
                    logger_1.default.log('error', 'Mail: Failed to send email - %s', err_1.message);
                    return [3, 3];
                case 3: return [2];
            }
        });
    });
}
exports.send = send;
exports.default = transporter;
//# sourceMappingURL=mail.js.map