"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var array_1 = require("./array");
var string_1 = require("./string");
function isObject(obj) {
    return obj !== undefined && obj !== null && !Array.isArray(obj) && obj instanceof Object;
}
exports.isObject = isObject;
function camelize(data) {
    var isDate = data instanceof Date;
    if (array_1.isArray(data)) {
        return data.map(function (obj) { return camelize(obj); });
    }
    if (!isDate && isObject(data)) {
        return Object.keys(data).reduce(function (accumulator, current) {
            var _a;
            var key = string_1.camelcase(current);
            var value = camelize(data[current]);
            return Object.assign(accumulator, (_a = {}, _a[key] = value, _a));
        }, {});
    }
    return data;
}
exports.camelize = camelize;
//# sourceMappingURL=object.js.map