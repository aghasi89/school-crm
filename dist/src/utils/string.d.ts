export declare function isString(text: any): boolean;
export declare function capitalize(word: string): string;
export declare function camelcase(text: string, separator?: string): string;
