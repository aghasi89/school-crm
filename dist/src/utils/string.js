"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
function isString(text) {
    return typeof text === 'string';
}
exports.isString = isString;
function capitalize(word) {
    return "" + word.slice(0, 1).toUpperCase() + word.slice(1).toLowerCase();
}
exports.capitalize = capitalize;
function camelcase(text, separator) {
    if (separator === void 0) { separator = '_'; }
    if (!isString(text)) {
        return text;
    }
    var words = text.split(separator);
    return tslib_1.__spread([words[0]], words.slice(1).map(function (word) { return "" + word.slice(0, 1).toUpperCase() + word.slice(1); })).join('');
}
exports.camelcase = camelcase;
//# sourceMappingURL=string.js.map