export default function transform<T>(data: T[], transformCallback: (info: T) => T): T[];
