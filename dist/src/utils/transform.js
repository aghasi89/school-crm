"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function transform(data, transformCallback) {
    return data.map(transformCallback);
}
exports.default = transform;
//# sourceMappingURL=transform.js.map