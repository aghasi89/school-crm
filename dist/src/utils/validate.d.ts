import * as Joi from 'joi';
export default function validate<T>(data: T, schema: Joi.SchemaLike): Promise<void>;
