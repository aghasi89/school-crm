"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var Joi = tslib_1.__importStar(require("joi"));
function validate(data, schema) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, Joi.validate(data, schema, { abortEarly: false })];
                case 1:
                    _a.sent();
                    return [2];
            }
        });
    });
}
exports.default = validate;
//# sourceMappingURL=validate.js.map