"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.analiticGroup2POSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .label('Name')
        .required(),
    code: joi_1.default.number()
        .min(1)
        .required(),
    isAccumulate: joi_1.default.boolean()
        .required()
        .allow(null),
    analiticGroup2Id: joi_1.default.number()
        .min(1)
        .required()
        .allow(null)
});
//# sourceMappingURL=AnaliticGroup2Request.js.map