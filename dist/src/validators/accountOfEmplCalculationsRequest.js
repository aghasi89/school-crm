"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.accountOfEmplCalculationsPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .min(2)
        .max(100)
        .label('name')
        .required(),
    calculationsTypeId: joi_1.default.string()
        .label('calculationsTypeId')
        .required(),
    acumulatedAccountId: joi_1.default.string()
        .label('acumulatedAccountId')
        .allow(null)
        .required(),
    offBalanceSheet: joi_1.default.boolean()
        .label('offBalanceSheet')
        .required(),
    accountingByPartners: joi_1.default.boolean()
        .label('offBalanceSheet')
        .required(),
    analyticalGroup_1: joi_1.default.boolean()
        .label('offBalanceSheet')
        .required(),
    analyticalGroup_2: joi_1.default.boolean()
        .label('offBalanceSheet')
        .required(),
    isAccumulatedAccount: joi_1.default.boolean()
        .label('isAccumulatedAccount')
        .required(),
    account: joi_1.default.string()
        .label('account')
        .required(),
    currencies: joi_1.default.array().items(joi_1.default.object({
        currencyId: joi_1.default.number().min(1),
        status: joi_1.default.string()
    })).required()
});
//# sourceMappingURL=accountOfEmplCalculationsRequest.js.map