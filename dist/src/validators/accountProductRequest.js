"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.accountProductPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    date: joi_1.default.date()
        .label('date')
        .required(),
    documentNumber: joi_1.default.string()
        .label('documentNumber')
        .required(),
    partnersId: joi_1.default.number()
        .label('partnersId')
        .required(),
    linesCode: joi_1.default.string()
        .label('linesCode')
        .required(),
    contract: joi_1.default.string()
        .label('contract')
        .required(),
    contractDate: joi_1.default.date()
        .label('contractDate')
        .required(),
    subsectionId: joi_1.default.number()
        .label('subsectionId')
        .required(),
    analiticGroup_1Id: joi_1.default.number()
        .label('analiticGroup_1Id')
        .required(),
    analiticGroup_2Id: joi_1.default.number()
        .label('analiticGroup_2Id')
        .required(),
    seria: joi_1.default.string()
        .label('seria')
        .required(),
    number: joi_1.default.string()
        .label('number')
        .required(),
    dateWriteOff: joi_1.default.date()
        .label('dateWriteOff')
        .required(),
    comment: joi_1.default.string()
        .label('comment')
        .required(),
    aahAccountTypeId: joi_1.default.number()
        .label('aahAccountTypeId')
        .required(),
    warehouseId: joi_1.default.number()
        .label('warehouseId')
        .required(),
    address: joi_1.default.string()
        .label('address')
        .required(),
    aah: joi_1.default.string()
        .label('aah')
        .required(),
    accountProductsOfProducts: joi_1.default.array().items(joi_1.default.object({
        type: joi_1.default.string()
            .label('type')
            .required(),
        warehouseId: joi_1.default.number()
            .label('warehouseId')
            .required(),
        code: joi_1.default.number()
            .label('code')
            .required(),
        name: joi_1.default.string()
            .label('name')
            .required(),
        point: joi_1.default.number()
            .label('point')
            .required(),
        count: joi_1.default.number()
            .label('count')
            .required(),
        price: joi_1.default.number()
            .label('price')
            .required(),
        money: joi_1.default.number()
            .label('money')
            .required(),
        expenseAccountId: joi_1.default.string()
            .label('expenseAccountId')
            .required(),
        incomeAccountId: joi_1.default.string()
            .label('incomeAccountId')
            .required(),
        batch: joi_1.default.number()
            .label('batch')
            .required(),
        isAah: joi_1.default.number()
            .label('isAah')
            .required(),
    })),
});
//# sourceMappingURL=accountProductRequest.js.map