"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.additionPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .min(4)
        .max(100)
        .label('Name')
        .required(),
    tabel_id: joi_1.default.number()
        .min(1)
        .label('tabel_id')
        .required(),
    type_of_income_id: joi_1.default.number()
        .min(1)
        .label('type_of_income_id')
        .required(),
    type_of_vacation_id: joi_1.default.number()
        .min(1)
        .label('type_of_vacation_id')
        .required(),
    expense_account_id: joi_1.default.number()
        .min(1)
        .label('expense_account_id')
        .required(),
    coefficient: joi_1.default.string()
        .label('expense_account_id')
        .required(),
    recalculation: joi_1.default.boolean()
        .label('expense_account_id')
        .required(),
    is_income: joi_1.default.boolean()
        .label('expense_account_id')
        .required(),
    declining_income: joi_1.default.boolean()
        .label('expense_account_id')
        .required(),
    is_trade_union: joi_1.default.boolean()
        .label('expense_account_id')
        .required(),
    is_for_tax_purposes_only: joi_1.default.boolean()
        .label('expense_account_id')
        .required(),
    is_mandatory_pension: joi_1.default.boolean()
        .label('expense_account_id')
        .required(),
    by_the_employer_mandatory_pension: joi_1.default.boolean()
        .label('expense_account_id')
        .required(),
    participates_on_account_of_actual_hours: joi_1.default.boolean()
        .label('expense_account_id')
        .required()
});
//# sourceMappingURL=additionRequest.js.map