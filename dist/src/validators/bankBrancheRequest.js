"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.bankBranchePOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    branche_code: joi_1.default.string()
        .min(4)
        .max(100)
        .label('Branche code')
        .required(),
    branche_address: joi_1.default.string()
        .min(1)
        .max(100)
        .label('Branche address')
        .required(),
    bank_id: joi_1.default.number()
        .min(1)
        .max(100)
        .label('Bank id')
        .required()
});
//# sourceMappingURL=bankBrancheRequest.js.map