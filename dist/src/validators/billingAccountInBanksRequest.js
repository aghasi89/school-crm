"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.billingAccountInBanksPOSTSchema = joi_1.default
    .object()
    .keys({
    billingAccount: joi_1.default.string()
        .label('billingAccount')
        .required(),
    name: joi_1.default.string()
        .label('name')
        .required(),
    currencyId: joi_1.default.number()
        .min(1)
        .label('currencyId')
        .required(),
    accountId: joi_1.default.number()
        .min(1)
        .label('accountId')
        .required(),
    isMain: joi_1.default.boolean()
        .label('isMain')
        .required(),
    numberOfDocument: joi_1.default.string()
        .label('numberOfDocument')
        .required(),
    eServices: joi_1.default.string()
        .label('eServices')
        .required(),
});
//# sourceMappingURL=billingAccountInBanksRequest.js.map