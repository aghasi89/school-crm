"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.billingAccountPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string().required(),
    isMain: joi_1.default.boolean().required(),
    account: joi_1.default.string().required(),
    serialNumber: joi_1.default.string().required(),
    partnersId: joi_1.default.number().required()
});
//# sourceMappingURL=billingAccountRequest.js.map