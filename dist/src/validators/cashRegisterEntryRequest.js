"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.cashRegisterEntryPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    cashRegisterId: joi_1.default.number()
        .label('cashRegisterId')
        .required(),
    date: joi_1.default.date()
        .label('date')
        .required(),
    hdmN: joi_1.default.string()
        .label('hdmN')
        .required(),
    documentNumber: joi_1.default.string()
        .label('documentNumber')
        .required(),
    correspondentAccountId: joi_1.default.number()
        .label('correspondentAccountId')
        .required(),
    entryAccountId: joi_1.default.number()
        .label('entryAccountId')
        .required(),
    analiticGroup_1Id: joi_1.default.number()
        .label('analiticGroup_1Id')
        .required(),
    analiticGroup_2Id: joi_1.default.number()
        .label('analiticGroup_2Id')
        .required(),
    currencyId: joi_1.default.number()
        .label('currencyId')
        .required(),
    amountCurrency1: joi_1.default.string()
        .label('amountCurrency1')
        .required(),
    amountCurrency2: joi_1.default.string()
        .label('amountCurrency2')
        .required(),
    partnersId: joi_1.default.number()
        .label('partnersId')
        .required(),
    received: joi_1.default.string()
        .label('received')
        .required(),
    basis: joi_1.default.string()
        .label('basis')
        .required(),
    attached: joi_1.default.string()
        .label('attached')
        .required(),
    optiona: joi_1.default.string()
        .label('optiona')
        .required(),
    typicalOperation: joi_1.default.string()
        .label('typicalOperation')
        .required(),
});
//# sourceMappingURL=cashRegisterEntryRequest.js.map