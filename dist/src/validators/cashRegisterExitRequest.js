"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.cashRegisterExitPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    cashRegisterId: joi_1.default.number()
        .label('cashRegisterId')
        .required(),
    date: joi_1.default.date()
        .label('date')
        .required(),
    documentNumber: joi_1.default.string()
        .label('documentNumber')
        .required(),
    correspondentAccountId: joi_1.default.number()
        .label('correspondentAccountId')
        .required(),
    exitAccountId: joi_1.default.number()
        .label('exitAccountId')
        .required(),
    analiticGroup_1Id: joi_1.default.number()
        .label('analiticGroup_1Id')
        .required(),
    analiticGroup_2Id: joi_1.default.number()
        .label('analiticGroup_2Id')
        .required(),
    currencyId: joi_1.default.number()
        .label('currencyId')
        .required(),
    amountCurrency_1: joi_1.default.string()
        .label('amountCurrency_1')
        .required(),
    amountCurrency_2: joi_1.default.string()
        .label('amountCurrency_2')
        .required(),
    partnersId: joi_1.default.number()
        .label('partnersId')
        .required(),
    received: joi_1.default.string()
        .label('received')
        .required(),
    npNshPassword: joi_1.default.string()
        .label('npNshPassword')
        .required(),
    basis: joi_1.default.string()
        .label('basis')
        .required(),
    appendix: joi_1.default.string()
        .label('appendix')
        .required(),
    otherInformation: joi_1.default.string()
        .label('otherInformation')
        .required(),
    optiona: joi_1.default.string()
        .label('optiona')
        .required(),
    typicalOperation: joi_1.default.string()
        .label('typicalOperation')
        .required(),
});
//# sourceMappingURL=cashRegisterExitRequest.js.map