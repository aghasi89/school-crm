"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.cashRegisterPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    code: joi_1.default.string()
        .max(100)
        .label('code')
        .required(),
    name: joi_1.default.string()
        .max(100)
        .label('name')
        .required(),
    isMain: joi_1.default.boolean()
        .label('isMain')
        .required(),
    accountId: joi_1.default.number()
        .label('accountId')
        .required(),
    dmoN: joi_1.default.string()
        .label('dmoN')
        .required(),
    deoN: joi_1.default.string()
        .label('deoN')
        .required(),
    hdmRegisN: joi_1.default.string()
        .label('hdmRegisN')
        .required(),
    ip: joi_1.default.string()
        .label('ip')
        .required(),
    port: joi_1.default.string()
        .label('port')
        .required(),
    password: joi_1.default.string()
        .label('password')
        .required(),
    hdmNonTaxable: joi_1.default.string()
        .label('hdmNonTaxable')
        .required(),
    hdmTaxable: joi_1.default.string()
        .label('hdmTaxable')
        .required(),
    hdmPrintType: joi_1.default.string()
        .label('hdmPrintType')
        .required()
});
//# sourceMappingURL=cashRegisterRequest.js.map