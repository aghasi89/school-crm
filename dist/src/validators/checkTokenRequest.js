"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.checkTokenPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    token: joi_1.default.string()
        .min(4)
        .label('Token')
        .required(),
});
//# sourceMappingURL=checkTokenRequest.js.map