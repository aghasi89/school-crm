"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.classificationPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .min(4)
        .max(100)
        .label('Name')
        .required(),
    code: joi_1.default.number()
        .min(1)
        .required(),
    type: joi_1.default.string()
        .required()
});
//# sourceMappingURL=classificationRequest.js.map