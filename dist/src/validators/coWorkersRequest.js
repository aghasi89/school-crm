"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.coWorkersPOSTSchema = joi_1.default
    .object()
    .keys({
    code: joi_1.default.string().required(),
    hvhh: joi_1.default.string().required(),
    name: joi_1.default.string().required(),
    creditor: joi_1.default.string().required(),
    debetor: joi_1.default.string().required(),
    legal_address: joi_1.default.string().required(),
    work_address: joi_1.default.string().required(),
    transfer_purpose: joi_1.default.string().required(),
    inflow_account: joi_1.default.string().required(),
    leakage_account: joi_1.default.string().required(),
    director: joi_1.default.string().required(),
    accountent: joi_1.default.string().required(),
    bank_account: joi_1.default.string().required(),
    bank_id: joi_1.default.number().required()
});
//# sourceMappingURL=coWorkersRequest.js.map