"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.employeePOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    selfData: joi_1.default.object().keys({
        fullName: joi_1.default.string()
            .min(4)
            .max(100)
            .label('fullName')
            .required(),
        tabelCounter: joi_1.default.number()
            .min(1)
            .label('tabel_counter')
            .required(),
        firstName: joi_1.default.string()
            .label('firstName')
            .required(),
        lastName: joi_1.default.string()
            .min(6)
            .max(100)
            .label('lastName')
            .required(),
        subdivisionId: joi_1.default.number()
            .min(1)
            .label('subdivisionId')
            .required()
    }),
    general: joi_1.default.object().keys({
        professionId: joi_1.default.number()
            .min(1)
            .label('profession_id')
            .allow(null)
            .required(),
        gender: joi_1.default.string()
            .label('gender')
            .required(),
        birthdate: joi_1.default.string()
            .label('birthdate')
            .required(),
        contractId: joi_1.default.number()
            .min(1)
            .label('contract_id')
            .allow(null)
            .required(),
        accountId: joi_1.default.number()
            .min(1)
            .label('account_id')
            .allow(null)
            .required(),
        dateOfAccept: joi_1.default.string()
            .label('date_of_accept')
            .required(),
        releaseDate: joi_1.default.string()
            .label('release_date')
            .required(),
        acceptedCommand: joi_1.default.string()
            .label('accepted_command')
            .required(),
        releaseCommand: joi_1.default.string()
            .min(1)
            .label('release_command')
            .required()
    }).required(),
    addressies: joi_1.default.object().keys({
        state: joi_1.default.string()
            .label('state').required(),
        community: joi_1.default.string()
            .label('community').required(),
        city: joi_1.default.string()
            .label('city').required(),
        street: joi_1.default.string()
            .label('street').required(),
        hhState: joi_1.default.string()
            .label('HHState').required(),
        hhCommunity: joi_1.default.string()
            .label('HHCommunity').required(),
        hhCity: joi_1.default.string()
            .label('HHCity').required(),
        hhStreet: joi_1.default.string()
            .label('HHStreet').required(),
        country: joi_1.default.string()
            .label('country').required(),
        addressee1: joi_1.default.string()
            .label('addressee1').required(),
        addressee2: joi_1.default.string()
            .label('addressee2').required(),
        addressee3: joi_1.default.string()
            .label('addressee3').required(),
        post: joi_1.default.string()
            .label('post').required(),
        placeOfRegistration: joi_1.default.boolean()
            .label('placeOfRegistration').required(),
        sameResidence: joi_1.default.boolean()
            .label('sameResidence').required(),
        hhResidence: joi_1.default.boolean()
            .label('HHResidence').required(),
    }).required(),
    otherInformation: joi_1.default.object().keys({
        bankNumber: joi_1.default.string()
            .label('bankNumber').required(),
        socialCardNumber: joi_1.default.string()
            .label('socialCardNumber').required(),
        documentTypeId: joi_1.default.number()
            .label('documentTypeId').required(),
        passportNumber: joi_1.default.string()
            .label('passportNumber').required(),
        dueDate: joi_1.default.string()
            .label('dueDate').required(),
        byWhom: joi_1.default.string()
            .label('byWhom').required(),
        nationality: joi_1.default.string()
            .label('nationality').required(),
        anotherDocumentNumber: joi_1.default.string()
            .label('anotherDocumentNumber').required(),
        phone: joi_1.default.string()
            .label('phone').required(),
        phone2: joi_1.default.string()
            .label('phone2').allow(null).required(),
        language: joi_1.default.string()
            .label('language').allow(null).required(),
        education: joi_1.default.string()
            .label('education').required(),
    }).required(),
    employeePosition: joi_1.default.array().items(joi_1.default.object({
        id: joi_1.default.number().allow(null),
        positionId: joi_1.default.number().min(1),
        startOfPosition: joi_1.default.date(),
        endOfPosition: joi_1.default.date(),
        status: joi_1.default.string().allow(null)
    })).required(),
    employeeAccounts: joi_1.default.array().items(joi_1.default.object({
        id: joi_1.default.number().allow(null),
        accountOfEmployeeCalculationId: joi_1.default.number().min(1).allow(null),
        type: joi_1.default.string(),
        percent: joi_1.default.string(),
        status: joi_1.default.string().allow(null)
    })).required(),
    employeeAddition: joi_1.default.array().items(joi_1.default.object({
        id: joi_1.default.number().allow(null),
        additionId: joi_1.default.number().min(1).allow(null),
        isMain: joi_1.default.boolean(),
        money: joi_1.default.number(),
        status: joi_1.default.string().allow(null),
        isDeleted: joi_1.default.boolean().allow(null)
    })).required()
});
//# sourceMappingURL=employeeRequest.js.map