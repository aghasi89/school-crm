"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.expenseAccountPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .min(2)
        .max(100)
        .label('Անուն')
        .required(),
    code: joi_1.default.number()
});
//# sourceMappingURL=expenseAccountRequest.js.map