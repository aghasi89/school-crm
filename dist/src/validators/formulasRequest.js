"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.formulasPOSTSchema = joi_1.default
    .object()
    .keys({
    code: joi_1.default.string()
        .label('Code')
        .required(),
    caption: joi_1.default.string()
        .label('Caption')
        .required(),
    expression: joi_1.default.string()
        .label('Expression')
        .required(),
});
//# sourceMappingURL=formulasRequest.js.map