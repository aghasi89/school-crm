"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.hmxProfitTaxPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    code: joi_1.default.string()
        .label('code')
        .required(),
    name: joi_1.default.string()
        .min(2)
        .max(100)
        .label('name')
        .required(),
    yearPrice: joi_1.default.string()
        .label('yearPrice')
        .required(),
});
//# sourceMappingURL=hmxProfitTaxRequest.js.map