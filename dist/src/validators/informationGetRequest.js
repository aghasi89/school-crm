"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.informationGetRequest = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    key: joi_1.default.string()
        .label('key')
        .required()
});
//# sourceMappingURL=informationGetRequest.js.map