"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.materialValueGroupPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .min(4)
        .max(100)
        .label('Name')
        .required(),
    code: joi_1.default.number()
        .min(1)
        .required(),
    materialValueGroupId: joi_1.default.number()
        .min(1)
        .allow(null)
});
//# sourceMappingURL=materialValueGroupRequest.js.map