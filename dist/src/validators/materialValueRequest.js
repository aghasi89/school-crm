"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.materialValuePOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .max(100)
        .label('name')
        .required(),
    measurementUnitId: joi_1.default.number()
        .label('measurementUnitId')
        .required(),
    classificationId: joi_1.default.number()
        .label('classificationId')
        .required(),
    accountId: joi_1.default.number()
        .label('accountId')
        .required(),
    wholesalePrice: joi_1.default.number()
        .label('wholesalePrice')
        .required(),
    retailerPrice: joi_1.default.number()
        .label('retailerPrice')
        .required(),
    characteristic: joi_1.default.string()
        .label('characteristic')
        .required(),
    barCode: joi_1.default.number()
        .label('barCode')
        .required(),
    externalCode: joi_1.default.number()
        .label('externalCode')
        .required(),
    hcbCoefficient: joi_1.default.number()
        .label('hcbCoefficient')
        .required(),
    billingMethodId: joi_1.default.number()
        .label('billingMethodId')
        .required(),
    isAah: joi_1.default.boolean()
        .label('isAah')
        .required(),
    salesRevenueAccountId: joi_1.default.number()
        .label('salesRevenueAccountId')
        .required(),
    retailRevenueAccountId: joi_1.default.number()
        .label('retailRevenueAccountId')
        .required(),
    salesExpenseAccountId: joi_1.default.number()
        .label('salesExpenseAccountId')
        .required(),
    materialValueGroupId: joi_1.default.number()
        .label('materialValueGroupId')
        .required(),
});
//# sourceMappingURL=materialValueRequest.js.map