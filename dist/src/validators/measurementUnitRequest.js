"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.measurementUnitPOSTSchema = joi_1.default
    .object()
    .keys({
    code: joi_1.default.string().required(),
    unit: joi_1.default.string().required(),
    abbreviation: joi_1.default.string().required()
});
//# sourceMappingURL=measurementUnitRequest.js.map