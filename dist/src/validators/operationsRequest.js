"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.operationsPOSTSchema = joi_1.default
    .object()
    .keys({
    debitId: joi_1.default.number().min(1),
    dPartnersId: joi_1.default.number().min(1),
    dAnaliticGroup_2Id: joi_1.default.number().min(1),
    dAnaliticGroup_1Id: joi_1.default.number().min(1),
    creditId: joi_1.default.number().min(1),
    cPartnersId: joi_1.default.number().min(1),
    cAnaliticGroup_2Id: joi_1.default.number().min(1),
    cAnaliticGroup_1Id: joi_1.default.number().min(1),
    money: joi_1.default.number(),
    comment: joi_1.default.string(),
    isDeleted: joi_1.default.boolean(),
    id: joi_1.default.number().min(1)
});
//# sourceMappingURL=operationsRequest.js.map