"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.paginatSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    limit: joi_1.default.number()
        .label('limit')
        .required(),
    offset: joi_1.default.number()
        .label('offset')
        .required()
});
//# sourceMappingURL=paginatRequest.js.map