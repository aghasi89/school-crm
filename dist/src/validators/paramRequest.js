"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.paramValidationSchema = joi_1.default
    .object()
    .keys({
    id: joi_1.default.number().required().label('Id')
});
//# sourceMappingURL=paramRequest.js.map