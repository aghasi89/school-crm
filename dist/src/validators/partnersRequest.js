"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.partnersPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    hvhh: joi_1.default.string()
        .min(4)
        .max(100)
        .label('hvhh')
        .required(),
    name: joi_1.default.string()
        .min(4)
        .max(100)
        .label('Name')
        .required(),
    fullName: joi_1.default.string()
        .min(4)
        .max(100)
        .label('fullName')
        .required(),
    groupId: joi_1.default.number()
        .min(1)
        .label('groupId')
        .required(),
    headPositionId: joi_1.default.number()
        .min(1)
        .label('headPositionId')
        .required(),
    accountantPositionId: joi_1.default.number()
        .min(1)
        .label('accountantPositionId')
        .required(),
    AAHpayer: joi_1.default.boolean()
        .label('AAHpayer')
        .required(),
    legalAddress: joi_1.default.string()
        .label('legalAddress')
        .required(),
    practicalAddress: joi_1.default.string()
        .min(4)
        .max(100)
        .label('practicalAddress')
        .required(),
    headAAH: joi_1.default.string()
        .min(4)
        .max(100)
        .label('headAAH')
        .required(),
    accountantAAH: joi_1.default.string()
        .min(4)
        .max(100)
        .label('accountantAAH')
        .required(),
    certificateNumber: joi_1.default.string()
        .min(1)
        .max(100)
        .label('certificateNumber')
        .required(),
    passportNumber: joi_1.default.string()
        .min(1)
        .max(100)
        .label('passportNumber')
        .required(),
    mainPurposeOfPayment: joi_1.default.string()
        .min(1)
        .max(100)
        .label('certificateNumber')
        .required(),
    phone: joi_1.default.string()
        .min(1)
        .max(100)
        .label('certificateNumber')
        .required(),
    contract: joi_1.default.string()
        .min(1)
        .max(100)
        .label('certificateNumber')
        .required(),
    dateContract: joi_1.default.date()
        .label('dateContract')
        .required(),
    percentageOfSalesDiscount: joi_1.default.string()
        .min(1)
        .max(100)
        .label('percentageOfSalesDiscount')
        .required(),
    anotherAdditionalInformation: joi_1.default.string()
        .min(1)
        .max(100)
        .label('anotherAdditionalInformation')
        .required(),
    anotherDeliveryTime: joi_1.default.string()
        .min(1)
        .max(100)
        .label('anotherDeliveryTime')
        .required(),
    anotherFullname: joi_1.default.string()
        .min(1)
        .max(100)
        .label('anotherFullname')
        .required(),
    anotherCredentialsNumber: joi_1.default.string()
        .min(1)
        .max(100)
        .label('anotherCredentialsNumber')
        .required(),
    anotherCredentialsDate: joi_1.default.string()
        .min(1)
        .max(100)
        .label('anotherCredentialsDate')
        .required(),
    email: joi_1.default.string()
        .min(10)
        .max(100)
        .label('Email')
        .required(),
    additionalAddressePartners: joi_1.default.array().items(joi_1.default.object({
        name: joi_1.default.string().min(1),
        isDeleted: joi_1.default.boolean(),
        id: joi_1.default.number().min(1)
    })).required(),
    billingAccounts: joi_1.default.array().items(joi_1.default.object({
        id: joi_1.default.number().min(1),
        isDeleted: joi_1.default.boolean(),
        name: joi_1.default.string().min(1),
        isMain: joi_1.default.boolean(),
        account: joi_1.default.string(),
    })).required()
});
//# sourceMappingURL=partnersRequest.js.map