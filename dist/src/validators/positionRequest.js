"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.positionPOSTSchema = joi_1.default
    .object()
    .keys({
    name: joi_1.default.string().required(),
    subdivisionId: joi_1.default.number().required()
});
//# sourceMappingURL=positionRequest.js.map