"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.priceOfServicesPOSTSchema = joi_1.default
    .object()
    .keys({
    startDate: joi_1.default.date()
        .label('startDate')
        .required(),
    endDate: joi_1.default.date()
        .label('endDate')
        .required(),
    servicesId: joi_1.default.number()
        .min(1)
        .label('serviceId')
        .required(),
    type: joi_1.default.string()
        .label('Type')
        .required(),
});
//# sourceMappingURL=priceOfServicesRequest.js.map