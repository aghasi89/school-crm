"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.receivedServicesPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    currencyId: joi_1.default.number()
        .label('currencyId')
        .required(),
    currencyExchangeRate1: joi_1.default.string()
        .label('currencyExchangeRate1')
        .required(),
    currencyExchangeRate2: joi_1.default.string()
        .label('currencyExchangeRate2')
        .required(),
    previousDayExchangeRate: joi_1.default.boolean()
        .label('previousDayExchangeRate')
        .required(),
    partnersId: joi_1.default.number()
        .label('partnersId')
        .required(),
    providerAccountId: joi_1.default.string()
        .label('providerAccountId')
        .required(),
    advancePaymentAccountId: joi_1.default.string()
        .label('advancePaymentAccountId')
        .required(),
    analiticGroup_1Id: joi_1.default.number()
        .label('analiticGroup_1Id')
        .required(),
    analiticGroup_2Id: joi_1.default.number()
        .label('analiticGroup_2Id')
        .required(),
    outputMethod: joi_1.default.string()
        .label('outputMethod')
        .required(),
    purchaseDocumentNumber: joi_1.default.number()
        .label('purchaseDocumentNumber')
        .required(),
    purchaseDocumentDate: joi_1.default.date()
        .label('purchaseDocumentDate')
        .required(),
    comment: joi_1.default.string()
        .label('comment')
        .required(),
    purchaseTypeOfService: joi_1.default.string()
        .label('purchaseTypeOfService')
        .required(),
    calculationTypeAah: joi_1.default.string()
        .label('calculationTypeAah')
        .required(),
    includeAahInExpense: joi_1.default.boolean()
        .label('includeAahInExpense')
        .required(),
    formOfReflectionAah: joi_1.default.string()
        .label('formOfReflectionAah')
        .required(),
    typicalOperation: joi_1.default.string()
        .label('typicalOperation')
        .required(),
    receivedServicesDirectory: joi_1.default.array().items(joi_1.default.object({
        serviceId: joi_1.default.number().min(1),
        point: joi_1.default.number(),
        count: joi_1.default.number(),
        price: joi_1.default.number(),
        money: joi_1.default.number(),
        aah: joi_1.default.boolean(),
        account: joi_1.default.string()
    })).required(),
});
//# sourceMappingURL=receivedServicesRequest.js.map