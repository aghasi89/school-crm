"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.resetPasswordPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    token: joi_1.default.string()
        .min(4)
        .label('Token')
        .required(),
    password: joi_1.default.string()
        .min(4)
        .max(100)
        .label('Password')
        .required(),
});
//# sourceMappingURL=resetPasswordRequest.js.map