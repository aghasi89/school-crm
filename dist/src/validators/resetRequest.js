"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.resetPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    email: joi_1.default.string()
        .min(4)
        .max(100)
        .label('Email')
        .required(),
});
//# sourceMappingURL=resetRequest.js.map