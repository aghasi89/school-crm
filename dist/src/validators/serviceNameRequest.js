"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.seviceNamePOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    code: joi_1.default.string()
        .min(4)
        .max(100)
        .label('code')
        .required(),
    service_type_id: joi_1.default.string()
        .min(4)
        .max(100)
        .label('service type id')
        .required(),
});
//# sourceMappingURL=serviceNameRequest.js.map