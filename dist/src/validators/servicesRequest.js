"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.servicesPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    code: joi_1.default.string()
        .max(100)
        .label('code')
        .required(),
    fullName: joi_1.default.string()
        .max(100)
        .label('fullName')
        .required(),
    name: joi_1.default.string()
        .max(100)
        .label('name')
        .required(),
    measurementUnitId: joi_1.default.number()
        .label('measurementUnitId')
        .required(),
    classificationId: joi_1.default.number()
        .label('classificationId')
        .required(),
    accountId: joi_1.default.number()
        .label('accountId')
        .required(),
    wholesalePrice: joi_1.default.number()
        .label('wholesalePrice')
        .required(),
    retailerPrice: joi_1.default.number()
        .label('retailerPrice')
        .required(),
    barCode: joi_1.default.number()
        .label('barCode')
        .required(),
    isAah: joi_1.default.boolean()
        .label('isAah')
        .required()
});
//# sourceMappingURL=servicesRequest.js.map