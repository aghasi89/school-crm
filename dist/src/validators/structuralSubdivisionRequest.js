"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.structuralSubdivisionPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    code: joi_1.default.string()
        .label('code')
        .required(),
    name: joi_1.default.string()
        .label('name')
        .required(),
    partnerId: joi_1.default.number()
        .label('partnerId')
        .required(),
    expenseAccountId: joi_1.default.number()
        .label('expenseAccountId')
        .required(),
});
//# sourceMappingURL=structuralSubdivisionRequest.js.map