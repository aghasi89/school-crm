"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.subsectionPOSTSchema = joi_1.default
    .object()
    .keys({
    name: joi_1.default.string().required(),
    code: joi_1.default.number().required(),
    customerAccountId: joi_1.default.number().required(),
    prepaidAccountReceivedId: joi_1.default.number().required(),
    aahAccountId: joi_1.default.number().required(),
    typesOfActionsId: joi_1.default.number().required()
});
//# sourceMappingURL=subsectionRequest.js.map