"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.tabelPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .min(4)
        .max(100)
        .label('Name')
        .required(),
    hours: joi_1.default.string()
        .min(1)
        .label('hours')
        .required(),
    year: joi_1.default.string()
        .min(1)
        .label('year')
        .required(),
    months: joi_1.default.string()
        .min(1)
        .label('months')
        .required(),
});
//# sourceMappingURL=tabelRequest.js.map