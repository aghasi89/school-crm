"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.userPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .min(4)
        .max(100)
        .label('Name')
        .required(),
    email: joi_1.default.string()
        .min(10)
        .max(100)
        .label('Email')
        .required(),
    password: joi_1.default.string()
        .min(6)
        .max(100)
        .label('Password')
        .required()
});
//# sourceMappingURL=userRequest.js.map