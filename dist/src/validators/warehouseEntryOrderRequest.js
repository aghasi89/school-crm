"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.warehouseEntryOrderPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    date: joi_1.default.date()
        .label('date')
        .required(),
    warehouseId: joi_1.default.number()
        .label('warehouseId')
        .required(),
    partnersId: joi_1.default.number()
        .label('partnersId')
        .required(),
    partnersAccountId: joi_1.default.number()
        .label('partnersAccountId')
        .required(),
    prepaidAccountId: joi_1.default.number()
        .label('prepaidAccountId')
        .required(),
    documentNumber: joi_1.default.number()
        .label('documentNumber')
        .required(),
    name: joi_1.default.string()
        .label('name')
        .required(),
    analiticGroup_2Id: joi_1.default.number()
        .label('analiticGroup_2Id')
        .required(),
    analiticGroup_1Id: joi_1.default.number()
        .label('analiticGroup_1Id')
        .required(),
    documentN: joi_1.default.date()
        .label('documentN')
        .required(),
    documentDate: joi_1.default.date()
        .label('documentDate')
        .required(),
    comment: joi_1.default.string()
        .label('comment')
        .required(),
    powerOfAttorney: joi_1.default.string()
        .label('powerOfAttorney')
        .required(),
    mediator: joi_1.default.string()
        .label('mediator')
        .required(),
    container: joi_1.default.string()
        .label('container')
        .required(),
    accountant: joi_1.default.string()
        .label('accountant')
        .required(),
    allow: joi_1.default.string()
        .label('allow')
        .required(),
    accept: joi_1.default.string()
        .label('accept')
        .required(),
    documentOfTransport: joi_1.default.string()
        .label('documentOfTransport')
        .required(),
    documentOfTransportDate: joi_1.default.date()
        .label('documentOfTransportDate')
        .required(),
    typeOfAcquisitionOfNa: joi_1.default.string()
        .label('typeOfAcquisitionOfNa')
        .required(),
    calculationStyleOfAah: joi_1.default.string()
        .label('typeOfAcquisitionOfNa')
        .required(),
    includeAahInCost: joi_1.default.boolean()
        .label('includeAahInCost')
        .required(),
    warehouseEntryOrderProduct: joi_1.default.array().items(joi_1.default.object({
        materialValueId: joi_1.default.number().min(1),
        isDeleted: joi_1.default.boolean(),
        id: joi_1.default.number().min(1),
        point: joi_1.default.number(),
        count: joi_1.default.number(),
        price: joi_1.default.number(),
        money: joi_1.default.number(),
        isAah: joi_1.default.boolean(),
        accountsId: joi_1.default.number().min(1),
        classificationId: joi_1.default.number().min(1),
    })).required()
});
exports.warehouseEntryOrderProduct = joi_1.default.array().items(joi_1.default.object({
    materialValueId: joi_1.default.number().min(1),
    isDeleted: joi_1.default.boolean(),
    id: joi_1.default.number().min(1),
    point: joi_1.default.number(),
    count: joi_1.default.number(),
    price: joi_1.default.number(),
    money: joi_1.default.number(),
    isAah: joi_1.default.boolean(),
    accountsId: joi_1.default.number().min(1),
    classificationId: joi_1.default.number().min(1),
}));
//# sourceMappingURL=warehouseEntryOrderRequest.js.map