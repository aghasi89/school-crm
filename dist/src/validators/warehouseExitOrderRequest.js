"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.warehouseExitOrderPOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    date: joi_1.default.date()
        .label('date')
        .required(),
    warehouseId: joi_1.default.number()
        .label('warehouseId')
        .required(),
    expenseAccountId: joi_1.default.string()
        .label('expenseAccountId')
        .required(),
    documentNumber: joi_1.default.number()
        .label('documentNumber')
        .required(),
    analiticGroup_2Id: joi_1.default.number()
        .label('analiticGroup_2Id')
        .required(),
    analiticGroup_1Id: joi_1.default.number()
        .label('analiticGroup_1Id')
        .required(),
    comment: joi_1.default.string()
        .label('comment')
        .required(),
    powerOfAttorney: joi_1.default.string()
        .label('powerOfAttorney')
        .required(),
    mediator: joi_1.default.string()
        .label('mediator')
        .required(),
    container: joi_1.default.string()
        .label('container')
        .required(),
    accountant: joi_1.default.string()
        .label('accountant')
        .required(),
    allow: joi_1.default.string()
        .label('allow')
        .required(),
    hasRequested: joi_1.default.string()
        .label('hasRequested')
        .required(),
    warehouseExitOrderProduct: joi_1.default.array().items(joi_1.default.object({
        materialValueId: joi_1.default.number().min(1),
        isDeleted: joi_1.default.boolean(),
        id: joi_1.default.number().min(1),
        point: joi_1.default.number(),
        count: joi_1.default.number(),
        price: joi_1.default.number(),
        money: joi_1.default.number(),
        batch: joi_1.default.number()
    })).required(),
    warehouseExitOrderFunctions: joi_1.default.array().items(joi_1.default.object({
        debitId: joi_1.default.number().min(1),
        dPartnersId: joi_1.default.number().min(1),
        dAnaliticGroup_2Id: joi_1.default.number().min(1),
        dAnaliticGroup_1Id: joi_1.default.number().min(1),
        creditId: joi_1.default.number().min(1),
        cPartnersIdisAah: joi_1.default.number().min(1),
        cAnaliticGroup_2Id: joi_1.default.number().min(1),
        cAnaliticGroup_1Id: joi_1.default.number().min(1),
        money: joi_1.default.number(),
        comment: joi_1.default.string(),
        isDeleted: joi_1.default.boolean(),
        id: joi_1.default.number().min(1)
    })),
});
exports.warehouseExitOrderProduct = joi_1.default.array().items(joi_1.default.object({
    materialValueId: joi_1.default.number().min(1),
    isDeleted: joi_1.default.boolean(),
    id: joi_1.default.number().min(1),
    point: joi_1.default.number(),
    count: joi_1.default.number(),
    price: joi_1.default.number(),
    money: joi_1.default.number(),
    isAah: joi_1.default.boolean(),
    accountsId: joi_1.default.number().min(1),
    classificationId: joi_1.default.number().min(1),
}));
//# sourceMappingURL=warehouseExitOrderRequest.js.map