"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var joi_1 = tslib_1.__importDefault(require("joi"));
exports.warehousePOSTSchema = joi_1.default.object()
    .options({ abortEarly: false })
    .keys({
    name: joi_1.default.string()
        .label('Name')
        .required(),
    code: joi_1.default.number()
        .label('code')
        .required(),
    address: joi_1.default.string()
        .label('address')
        .required(),
    responsible: joi_1.default.string()
        .label('responsible')
        .required()
});
//# sourceMappingURL=warehouseRequest.js.map