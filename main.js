const { app, BrowserWindow } = require('electron')
const path = require('path')
const url = require('url')
const { exec } = require("child_process");

function createWindow() {
    // Создаем окно браузера.
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        //  fullscreen:true,
        webPreferences: {
            nodeIntegration: true
        }
    })

    win.loadURL(
        url.format({
            pathname: path.join(__dirname, `./public/index.html`),
            protocol: 'file:',
            slashes: true,
        })
    )

    win.webContents.openDevTools()

    win.on('closed', () => {
        win = null
    })
    const p = exec('node ./build/main.js')
    console.log(p);

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Некоторые API могут использоваться только после возникновения этого события.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // Для приложений и строки меню в macOS является обычным делом оставаться
    // активными до тех пор, пока пользователь не выйдет окончательно используя Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // На MacOS обычно пересоздают окно в приложении,
    // после того, как на иконку в доке нажали и других открытых окон нету.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})