(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["com-annaniks-shemm-school-views-main-main-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main.view.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main.view.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<div class=\"page_content\">\r\n    <div class=\"top_content\">\r\n\r\n    </div>\r\n    <div class=\"middle_content container\">\r\n        <router-outlet></router-outlet>\r\n    </div>\r\n    <div class=\"bottom_content\">\r\n\r\n    </div>\r\n</div>\r\n\r\n");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: MainRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainRoutingModule", function() { return MainRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _main_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main.view */ "./src/app/com/annaniks/shemm-school/views/main/main.view.ts");




const mainRoutes = [
    {
        path: '', component: _main_view__WEBPACK_IMPORTED_MODULE_3__["MainView"], children: [
            { path: '', loadChildren: () => __webpack_require__.e(/*! import() | home-home-module */ "home-home-module").then(__webpack_require__.bind(null, /*! ./home/home.module */ "./src/app/com/annaniks/shemm-school/views/main/home/home.module.ts")).then(m => m.HomeModule) },
            { path: 'fixed-assets', loadChildren: () => Promise.all(/*! import() | fixed-assets-fixed-assets-module */[__webpack_require__.e("default~enter-password-enter-password-module~fixed-assets-fixed-assets-module~forgot-forgot-module~l~2bbcbf62"), __webpack_require__.e("fixed-assets-fixed-assets-module")]).then(__webpack_require__.bind(null, /*! ./fixed-assets/fixed-assets.module */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/fixed-assets.module.ts")).then(m => m.FixedAssetsModule), data: { title: 'Հիմնական միջոցներ' } },
            { path: 'warehouse', loadChildren: () => Promise.all(/*! import() | warehouse-warehouse-module */[__webpack_require__.e("default~enter-password-enter-password-module~fixed-assets-fixed-assets-module~forgot-forgot-module~l~2bbcbf62"), __webpack_require__.e("warehouse-warehouse-module")]).then(__webpack_require__.bind(null, /*! ./warehouse/warehouse․module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․module.ts")).then(m => m.WarehouseModule), data: { title: 'Պահեստ' } },
            { path: 'main-accounting', loadChildren: () => Promise.all(/*! import() | main-accounting-main-accounting-module */[__webpack_require__.e("default~enter-password-enter-password-module~fixed-assets-fixed-assets-module~forgot-forgot-module~l~2bbcbf62"), __webpack_require__.e("main-accounting-main-accounting-module")]).then(__webpack_require__.bind(null, /*! ./main-accounting/main-accounting.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.module.ts")).then(m => m.MainAccountingModule), data: { title: 'Հաշվապահություն' } },
            { path: 'bank', loadChildren: () => Promise.all(/*! import() | main-bank-main-bank-module */[__webpack_require__.e("default~enter-password-enter-password-module~fixed-assets-fixed-assets-module~forgot-forgot-module~l~2bbcbf62"), __webpack_require__.e("main-bank-main-bank-module")]).then(__webpack_require__.bind(null, /*! ./main-bank/main-bank.module */ "./src/app/com/annaniks/shemm-school/views/main/main-bank/main-bank.module.ts")).then(m => m.MainBankModule), data: { title: 'Բանկ' } },
            { path: 'salary', loadChildren: () => Promise.all(/*! import() | salary-salary-module */[__webpack_require__.e("default~enter-password-enter-password-module~fixed-assets-fixed-assets-module~forgot-forgot-module~l~2bbcbf62"), __webpack_require__.e("salary-salary-module")]).then(__webpack_require__.bind(null, /*! ./salary/salary.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.module.ts")).then(m => m.SalaryModule), data: { title: 'Աշխատավարձ' } }
        ]
    }
];
let MainRoutingModule = class MainRoutingModule {
};
MainRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(mainRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], MainRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main.module.ts ***!
  \*********************************************************************/
/*! exports provided: MainModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainModule", function() { return MainModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _main_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main.view */ "./src/app/com/annaniks/shemm-school/views/main/main.view.ts");
/* harmony import */ var _main_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-routing.module.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _services_often_used_params__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/often-used-params */ "./src/app/com/annaniks/shemm-school/services/often-used-params.ts");








let MainModule = class MainModule {
};
MainModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_main_view__WEBPACK_IMPORTED_MODULE_2__["MainView"]],
        imports: [_main_routing_module__WEBPACK_IMPORTED_MODULE_3__["MainRoutingModule"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"]],
        providers: [_main_service__WEBPACK_IMPORTED_MODULE_5__["MainService"], _services__WEBPACK_IMPORTED_MODULE_6__["ComponentDataService"], _services_often_used_params__WEBPACK_IMPORTED_MODULE_7__["OftenUsedParamsService"]]
    })
], MainModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main.view.scss":
/*!*********************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main.view.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLnZpZXcuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main.view.ts":
/*!*******************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main.view.ts ***!
  \*******************************************************************/
/*! exports provided: MainView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainView", function() { return MainView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let MainView = class MainView {
    constructor() { }
    ngOnInit() {
    }
    ngOnDestroy() { }
};
MainView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'main-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main.view.scss")).default]
    })
], MainView);



/***/ })

}]);
//# sourceMappingURL=com-annaniks-shemm-school-views-main-main-module-es2015.js.map