(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-employees-employees-module~pages-salary-employee-salary-employee-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.html":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.html ***!
  \*********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addEmployee(false)\">\r\n    <tr *ngFor=\"let employee of employees\">\r\n        <td class=\"edit\"> <i (click)=\"addEmployee(true,employee?.id)\" class=\" material-icons\"> edit </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"deleteEmployee(employee?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{employee?.tabelCounter}}</td>\r\n        <td>{{employee?.fullName}}</td>\r\n\r\n        <td>{{employee?.employeePosition[0]?.positon?.name}}</td>\r\n        <td>{{getDate(employee.employeePosition[0]?.startOfPosition)}}</td>\r\n        <td>{{getDate(employee.employeePosition[0]?.endOfPosition)}}</td>\r\n\r\n        <td>{{getDate(employee.general?.birthdate)}}</td>\r\n        <td>{{employee.general?.gender}}</td>\r\n        <td>{{employee.otherInformation?.phone}}</td>\r\n        <td>{{employee.otherInformation?.passportNumber}}</td>\r\n        <td>{{employee.otherInformation?.bankNumber}}</td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.module.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.module.ts ***!
  \*******************************************************************************************************/
/*! exports provided: EmployeesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesModule", function() { return EmployeesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _employees_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employees.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.ts");
/* harmony import */ var _employees_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employees.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _employees_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./employees.service */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");







let EmployeesModule = class EmployeesModule {
};
EmployeesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_employees_view__WEBPACK_IMPORTED_MODULE_2__["EmployeeView"]],
        imports: [_employees_routing_module__WEBPACK_IMPORTED_MODULE_3__["EmployeeRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
        providers: [_employees_service__WEBPACK_IMPORTED_MODULE_5__["EmployeesService"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]],
        exports: [_employees_view__WEBPACK_IMPORTED_MODULE_2__["EmployeeView"]]
    })
], EmployeesModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.routing.module.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.routing.module.ts ***!
  \***************************************************************************************************************/
/*! exports provided: EmployeeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeRoutingModule", function() { return EmployeeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _employees_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employees.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.ts");




let employeesRoutes = [{ path: '', component: _employees_view__WEBPACK_IMPORTED_MODULE_3__["EmployeeView"] }];
let EmployeeRoutingModule = class EmployeeRoutingModule {
};
EmployeeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(employeesRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], EmployeeRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.scss ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy9lbXBsb3llZXMvQzpcXFVzZXJzXFxBbm5hbmlrc1xcRGVza3RvcFxcc2hlbW0tc2Nob29sL3NyY1xcYXBwXFxjb21cXGFubmFuaWtzXFxzaGVtbS1zY2hvb2xcXHZpZXdzXFxtYWluXFxmaXhlZC1hc3NldHNcXHBhZ2VzXFxlbXBsb3llZXNcXGVtcGxveWVlcy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvZW1wbG95ZWVzL2VtcGxveWVlcy52aWV3LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvZW1wbG95ZWVzL2VtcGxveWVlcy52aWV3LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSwgdGgsIHRkIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZToxMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9IiwidGFibGUsIHRoLCB0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.ts ***!
  \*****************************************************************************************************/
/*! exports provided: EmployeeView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeView", function() { return EmployeeView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/modals/index.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");











let EmployeeView = class EmployeeView {
    constructor(_matDialog, _title, _mainService, _router, _activatedRoute, _loadingService, _appService, _datePipe, _urls) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._mainService = _mainService;
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._loadingService = _loadingService;
        this._appService = _appService;
        this._datePipe = _datePipe;
        this._urls = _urls;
        this._modalTitle = "Աշխատակիցներ";
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._paginatorLastPageNumber = 0;
        this._mainUrl = this._urls.employeeOfFixedAssetsMainUrl;
        this._url = this._urls.employeeOfFixedAssetsGetOneUrl;
        this.employees = [];
        this.titles = [
            { title: 'Տաբելային համար' }, { title: 'Ազգանուն, անուն, հայրանուն' },
            // { title: 'Ստորաբաժանում'}, { title: 'Անվանում'},
            { title: 'Պաշտոն' },
            { title: 'Պաշտոնի սկիզբ', isSort: false, arrow: '', min: false, max: false },
            { title: 'Պաշտոնի ավարտ', isSort: false, arrow: '', min: false, max: false },
            // { title: 'Պաշտոնի տեսակը'}, { title: 'Ընդունման ամսաթիվ'},  { title: 'Ազատման ամսաթիվ'},
            { title: 'Ծննդյան ամսաթիվ' }, { title: 'Սեռ' },
            { title: 'Հեռախոս' }, { title: 'Անձնագիր' }, { title: 'Աշխատանկցի հաշիվը բանկում' },
        ];
        this._title.setTitle(this._modalTitle);
    }
    set setIsSalary($event) {
        if ($event) {
            this._mainUrl = this._urls.employeeMainUrl;
            this._url = this._urls.employeeGetOneUrl;
        }
    }
    ngOnInit() {
        this._checkParams();
    }
    getDate(date) {
        return date ? this._datePipe.transform(new Date(date), 'dd/MM/yyyy') : null;
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["forkJoin"])(this._getCount(), this._getEmplyees(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading());
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryparams) => {
            if (queryparams && queryparams.page) {
                this._page = +queryparams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _getCount() {
        return this._mainService.getCount(this._mainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    _getEmplyees(limit, offset) {
        if (this._url) {
            return this._mainService.getByUrl(this._mainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])((data) => {
                this.employees = data.data;
                return data;
            }));
        }
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    deleteEmployee(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._url, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.employees, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        });
    }
    addEmployee(isNew, id) {
        this._openModal(isNew, id);
    }
    _openModal(isNew, id) {
        let newTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_3__["EmployeesModal"], {
            width: '80vw',
            minHeight: '55vh',
            maxHeight: '88vh',
            data: { title: newTitle, url: this._url, id: id }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    ngOnDestroy() {
        this._loadingService.hideLoading();
        this._subscription.unsubscribe();
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
};
EmployeeView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_6__["MainService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["LoadingService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["AppService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_10__["DatePipe"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('isSalary')
], EmployeeView.prototype, "setIsSalary", null);
EmployeeView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'employees-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./employees.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./employees.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/employees/employees.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](8, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], EmployeeView);



/***/ })

}]);
//# sourceMappingURL=default~pages-employees-employees-module~pages-salary-employee-salary-employee-module-es2015.js.map