(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/home/home.view.html": 
        /*!****************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/home/home.view.html ***!
          \****************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"home-container\">\r\n    <div class=\"flexible\">\r\n        <div class=\"header-action\">\r\n            <h2>ՊՈԱԿ կառավարում</h2>\r\n        </div>\r\n        <div class=\"cards\">\r\n            <main-navbar  *ngFor=\"let menuItem of menuItems\" [menuItem]='menuItem'></main-navbar>\r\n        </div>\r\n        \r\n    </div>\r\n    <div class=\"menu-card\">\r\n        <div class=\"menu-items\">\r\n            <div class=\"header-action\">\r\n                <h2>Արագ մեկնարկ</h2>\r\n            </div>\r\n            <div class=\"menu-actions\" *ngFor=\"let item of menuActions\">\r\n                <a class=\"menu-action-item\">\r\n                    <div class=\"menu-action-icon df_jc\"><i class=\"material-icons md-36\">\r\n                            {{item.icon}}\r\n                        </i></div>\r\n                    <div class=\"menu-action-title df_jc\"><span>{{item.navText}}</span></div>\r\n                </a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/home/home-routing.module.ts": 
        /*!**********************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/home/home-routing.module.ts ***!
          \**********************************************************************************/
        /*! exports provided: HomeRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function () { return HomeRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _home_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.view */ "./src/app/com/annaniks/shemm-school/views/main/home/home.view.ts");
            var homeRoutes = [
                { path: '', component: _home_view__WEBPACK_IMPORTED_MODULE_3__["HomeView"] }
            ];
            var HomeRoutingModule = /** @class */ (function () {
                function HomeRoutingModule() {
                }
                return HomeRoutingModule;
            }());
            HomeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(homeRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], HomeRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/home/home.module.ts": 
        /*!**************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/home/home.module.ts ***!
          \**************************************************************************/
        /*! exports provided: HomeModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function () { return HomeModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/home/home-routing.module.ts");
            /* harmony import */ var _home_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.view */ "./src/app/com/annaniks/shemm-school/views/main/home/home.view.ts");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            /* harmony import */ var _components_main_navbar_main_navbar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../components/main-navbar/main-navbar.component */ "./src/app/com/annaniks/shemm-school/components/main-navbar/main-navbar.component.ts");
            var HomeModule = /** @class */ (function () {
                function HomeModule() {
                }
                return HomeModule;
            }());
            HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_home_view__WEBPACK_IMPORTED_MODULE_3__["HomeView"], _components_main_navbar_main_navbar_component__WEBPACK_IMPORTED_MODULE_5__["MainNavbarComponent"]],
                    imports: [_home_routing_module__WEBPACK_IMPORTED_MODULE_2__["HomeRoutingModule"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"]],
                    providers: []
                })
            ], HomeModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/home/home.view.scss": 
        /*!**************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/home/home.view.scss ***!
          \**************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".home-container {\n  display: flex;\n  justify-content: space-between;\n  padding-top: 10px;\n}\n.home-container .flexible {\n  width: 50%;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  display: flex;\n  flex-direction: row;\n  flex-flow: wrap;\n  align-content: flex-start;\n}\n.home-container .flexible .header-action {\n  width: 100%;\n}\n.home-container .flexible .cards {\n  display: flex;\n  flex-wrap: wrap;\n}\n.home-container .flexible .cards main-navbar {\n  flex-basis: 200px;\n}\n.home-container .menu-card {\n  width: 50%;\n}\n.home-container .menu-card .menu-items .header-action {\n  width: 100%;\n  margin-bottom: 5px;\n}\n.home-container .menu-card .menu-items .menu-actions .menu-action-item {\n  display: flex;\n  align-content: center;\n  justify-content: flex-start;\n  width: 100%;\n  height: 64px;\n  margin-bottom: 32px;\n  background: linear-gradient(to right, #3776b4, #4491c5);\n  border-radius: 4px;\n  cursor: pointer;\n}\n.home-container .menu-card .menu-items .menu-actions .menu-action-item .menu-action-icon {\n  width: 44px;\n  height: 44px;\n  margin: auto 10px;\n}\n.home-container .menu-card .menu-items .menu-actions .menu-action-item .menu-action-title span {\n  font-size: 1em;\n  color: white;\n}\n.home-container .menu-card .menu-items .menu-actions:nth-last-child(3) .menu-action-item, .home-container .menu-card .menu-items .menu-actions:nth-last-child(2) .menu-action-item {\n  background: white !important;\n  border: 2px solid #268870;\n  box-sizing: border-box;\n}\n.home-container .menu-card .menu-items .menu-actions:nth-last-child(3) .menu-action-item .menu-action-icon i, .home-container .menu-card .menu-items .menu-actions:nth-last-child(2) .menu-action-item .menu-action-icon i {\n  color: #268870;\n}\n.home-container .menu-card .menu-items .menu-actions:nth-last-child(3) .menu-action-item .menu-action-title span, .home-container .menu-card .menu-items .menu-actions:nth-last-child(2) .menu-action-item .menu-action-title span {\n  font-size: 1em;\n  color: #140b20 !important;\n}\n.home-container .menu-card .menu-items .menu-actions:nth-last-child(1) .menu-action-item {\n  background: white !important;\n  border: 2px solid #268870;\n  box-sizing: border-box;\n  margin-bottom: 0;\n}\n.home-container .menu-card .menu-items .menu-actions:nth-last-child(1) .menu-action-item .menu-action-icon i {\n  color: #f24133;\n}\n.home-container .menu-card .menu-items .menu-actions:nth-last-child(1) .menu-action-item .menu-action-title span {\n  font-size: 1em;\n  color: #140b20 !important;\n}\n@media screen and (max-width: 996px) {\n  .cards {\n    width: 90%;\n  }\n  .cards main-navbar {\n    flex-basis: auto !important;\n    width: 100%;\n  }\n}\nh2 {\n  color: #3776b4;\n  font-size: 1.5rem;\n}\n* {\n  margin: 0;\n  padding: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2hvbWUvQzpcXFVzZXJzXFxBbm5hbmlrc1xcRGVza3RvcFxcc2hlbW0tc2Nob29sL3NyY1xcYXBwXFxjb21cXGFubmFuaWtzXFxzaGVtbS1zY2hvb2xcXHZpZXdzXFxtYWluXFxob21lXFxob21lLnZpZXcuc2NzcyIsInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2hvbWUvaG9tZS52aWV3LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0E7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxpQkFBQTtBQ0ZKO0FESUk7RUFDSSxVQUFBO0VBQ0EsMkJBQUE7RUFBQSx3QkFBQTtFQUFBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0FDRlI7QURHUTtFQUNJLFdBQUE7QUNEWjtBRElRO0VBQ0ksYUFBQTtFQUNBLGVBQUE7QUNGWjtBREdZO0VBQ0ksaUJBQUE7QUNEaEI7QURPSTtFQUNJLFVBQUE7QUNMUjtBRE9ZO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0FDTGhCO0FEU2dCO0VBQ0ksYUFBQTtFQUNBLHFCQUFBO0VBQ0EsMkJBQUE7RUFFQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsdURBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNScEI7QURZb0I7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FDVnhCO0FEZXdCO0VBQ0ksY0FBQTtFQUNBLFlBQUE7QUNiNUI7QURxQm9CO0VBQ0ksNEJBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0FDbkJ4QjtBRHNCNEI7RUFDSSxjQUFBO0FDcEJoQztBRHlCNEI7RUFDSSxjQUFBO0VBQ0EseUJBQUE7QUN2QmhDO0FEOEJvQjtFQUNJLDRCQUFBO0VBQ0EseUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0FDNUJ4QjtBRCtCNEI7RUFDSSxjQUFBO0FDN0JoQztBRGtDNEI7RUFDSSxjQUFBO0VBQ0EseUJBQUE7QUNoQ2hDO0FEMkNBO0VBQ0k7SUFDSSxVQUFBO0VDeENOO0VEeUNNO0lBQ0ksMkJBQUE7SUFDQSxXQUFBO0VDdkNWO0FBQ0Y7QUQyQ0E7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUN6Q0o7QUQ0Q0E7RUFDSSxTQUFBO0VBQ0EsVUFBQTtBQ3pDSiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9ob21lL2hvbWUudmlldy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9zY3NzL3ZhcmlhYmxlXCI7XHJcblxyXG5cclxuLmhvbWUtY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuXHJcbiAgICAuZmxleGlibGUge1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgaGVpZ2h0OiBmaXQtY29udGVudDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgZmxleC1mbG93OiB3cmFwO1xyXG4gICAgICAgIGFsaWduLWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgLmhlYWRlci1hY3Rpb24ge1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jYXJkcyB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICAgICAgbWFpbi1uYXZiYXIge1xyXG4gICAgICAgICAgICAgICAgZmxleC1iYXNpczogMjAwcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC5tZW51LWNhcmQge1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgLm1lbnUtaXRlbXMge1xyXG4gICAgICAgICAgICAuaGVhZGVyLWFjdGlvbiB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLm1lbnUtYWN0aW9ucyB7XHJcbiAgICAgICAgICAgICAgICAubWVudS1hY3Rpb24taXRlbSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHdpZHRoOiA1NjBweDtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDY0cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzNzc2YjQsICM0NDkxYzUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgICAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcblxyXG5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLm1lbnUtYWN0aW9uLWljb24ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDRweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0NHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IGF1dG8gMTBweDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAubWVudS1hY3Rpb24tdGl0bGUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzcGFuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAmOm50aC1sYXN0LWNoaWxkKDMpLFxyXG4gICAgICAgICAgICAgICAgJjpudGgtbGFzdC1jaGlsZCgyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLm1lbnUtYWN0aW9uLWl0ZW0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCAjMjY4ODcwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLm1lbnUtYWN0aW9uLWljb24ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICMyNjg4NzA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5tZW51LWFjdGlvbi10aXRsZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcGFuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDFlbTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzE0MGIyMCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICY6bnRoLWxhc3QtY2hpbGQoMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC5tZW51LWFjdGlvbi1pdGVtIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAycHggc29saWQgIzI2ODg3MDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5tZW51LWFjdGlvbi1pY29uIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjZjI0MTMzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAubWVudS1hY3Rpb24tdGl0bGUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICMxNDBiMjAgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5NnB4KSB7XHJcbiAgICAuY2FyZHMge1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICAgICAgbWFpbi1uYXZiYXIge1xyXG4gICAgICAgICAgICBmbGV4LWJhc2lzOiBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlOyAgICAgICBcclxuICAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG5oMiB7XHJcbiAgICBjb2xvcjogIzM3NzZiNDtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG59XHJcblxyXG4qIHtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbn1cclxuXHJcbiIsIi5ob21lLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG4uaG9tZS1jb250YWluZXIgLmZsZXhpYmxlIHtcbiAgd2lkdGg6IDUwJTtcbiAgaGVpZ2h0OiBmaXQtY29udGVudDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgZmxleC1mbG93OiB3cmFwO1xuICBhbGlnbi1jb250ZW50OiBmbGV4LXN0YXJ0O1xufVxuLmhvbWUtY29udGFpbmVyIC5mbGV4aWJsZSAuaGVhZGVyLWFjdGlvbiB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmhvbWUtY29udGFpbmVyIC5mbGV4aWJsZSAuY2FyZHMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG59XG4uaG9tZS1jb250YWluZXIgLmZsZXhpYmxlIC5jYXJkcyBtYWluLW5hdmJhciB7XG4gIGZsZXgtYmFzaXM6IDIwMHB4O1xufVxuLmhvbWUtY29udGFpbmVyIC5tZW51LWNhcmQge1xuICB3aWR0aDogNTAlO1xufVxuLmhvbWUtY29udGFpbmVyIC5tZW51LWNhcmQgLm1lbnUtaXRlbXMgLmhlYWRlci1hY3Rpb24ge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuLmhvbWUtY29udGFpbmVyIC5tZW51LWNhcmQgLm1lbnUtaXRlbXMgLm1lbnUtYWN0aW9ucyAubWVudS1hY3Rpb24taXRlbSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA2NHB4O1xuICBtYXJnaW4tYm90dG9tOiAzMnB4O1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzNzc2YjQsICM0NDkxYzUpO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5ob21lLWNvbnRhaW5lciAubWVudS1jYXJkIC5tZW51LWl0ZW1zIC5tZW51LWFjdGlvbnMgLm1lbnUtYWN0aW9uLWl0ZW0gLm1lbnUtYWN0aW9uLWljb24ge1xuICB3aWR0aDogNDRweDtcbiAgaGVpZ2h0OiA0NHB4O1xuICBtYXJnaW46IGF1dG8gMTBweDtcbn1cbi5ob21lLWNvbnRhaW5lciAubWVudS1jYXJkIC5tZW51LWl0ZW1zIC5tZW51LWFjdGlvbnMgLm1lbnUtYWN0aW9uLWl0ZW0gLm1lbnUtYWN0aW9uLXRpdGxlIHNwYW4ge1xuICBmb250LXNpemU6IDFlbTtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmhvbWUtY29udGFpbmVyIC5tZW51LWNhcmQgLm1lbnUtaXRlbXMgLm1lbnUtYWN0aW9uczpudGgtbGFzdC1jaGlsZCgzKSAubWVudS1hY3Rpb24taXRlbSwgLmhvbWUtY29udGFpbmVyIC5tZW51LWNhcmQgLm1lbnUtaXRlbXMgLm1lbnUtYWN0aW9uczpudGgtbGFzdC1jaGlsZCgyKSAubWVudS1hY3Rpb24taXRlbSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMyNjg4NzA7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG4uaG9tZS1jb250YWluZXIgLm1lbnUtY2FyZCAubWVudS1pdGVtcyAubWVudS1hY3Rpb25zOm50aC1sYXN0LWNoaWxkKDMpIC5tZW51LWFjdGlvbi1pdGVtIC5tZW51LWFjdGlvbi1pY29uIGksIC5ob21lLWNvbnRhaW5lciAubWVudS1jYXJkIC5tZW51LWl0ZW1zIC5tZW51LWFjdGlvbnM6bnRoLWxhc3QtY2hpbGQoMikgLm1lbnUtYWN0aW9uLWl0ZW0gLm1lbnUtYWN0aW9uLWljb24gaSB7XG4gIGNvbG9yOiAjMjY4ODcwO1xufVxuLmhvbWUtY29udGFpbmVyIC5tZW51LWNhcmQgLm1lbnUtaXRlbXMgLm1lbnUtYWN0aW9uczpudGgtbGFzdC1jaGlsZCgzKSAubWVudS1hY3Rpb24taXRlbSAubWVudS1hY3Rpb24tdGl0bGUgc3BhbiwgLmhvbWUtY29udGFpbmVyIC5tZW51LWNhcmQgLm1lbnUtaXRlbXMgLm1lbnUtYWN0aW9uczpudGgtbGFzdC1jaGlsZCgyKSAubWVudS1hY3Rpb24taXRlbSAubWVudS1hY3Rpb24tdGl0bGUgc3BhbiB7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBjb2xvcjogIzE0MGIyMCAhaW1wb3J0YW50O1xufVxuLmhvbWUtY29udGFpbmVyIC5tZW51LWNhcmQgLm1lbnUtaXRlbXMgLm1lbnUtYWN0aW9uczpudGgtbGFzdC1jaGlsZCgxKSAubWVudS1hY3Rpb24taXRlbSB7XG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMyNjg4NzA7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4uaG9tZS1jb250YWluZXIgLm1lbnUtY2FyZCAubWVudS1pdGVtcyAubWVudS1hY3Rpb25zOm50aC1sYXN0LWNoaWxkKDEpIC5tZW51LWFjdGlvbi1pdGVtIC5tZW51LWFjdGlvbi1pY29uIGkge1xuICBjb2xvcjogI2YyNDEzMztcbn1cbi5ob21lLWNvbnRhaW5lciAubWVudS1jYXJkIC5tZW51LWl0ZW1zIC5tZW51LWFjdGlvbnM6bnRoLWxhc3QtY2hpbGQoMSkgLm1lbnUtYWN0aW9uLWl0ZW0gLm1lbnUtYWN0aW9uLXRpdGxlIHNwYW4ge1xuICBmb250LXNpemU6IDFlbTtcbiAgY29sb3I6ICMxNDBiMjAgIWltcG9ydGFudDtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTk2cHgpIHtcbiAgLmNhcmRzIHtcbiAgICB3aWR0aDogOTAlO1xuICB9XG4gIC5jYXJkcyBtYWluLW5hdmJhciB7XG4gICAgZmxleC1iYXNpczogYXV0byAhaW1wb3J0YW50O1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59XG5oMiB7XG4gIGNvbG9yOiAjMzc3NmI0O1xuICBmb250LXNpemU6IDEuNXJlbTtcbn1cblxuKiB7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/home/home.view.ts": 
        /*!************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/home/home.view.ts ***!
          \************************************************************************/
        /*! exports provided: HomeView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeView", function () { return HomeView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var MENU_ACTIONS = [
                { icon: 'account_balance', navText: 'Մուտքագրեք Ձեր կազմակերպության տվյալները', routerLink: '' },
                { icon: 'bookmarks', navText: 'Մուտքագրեք Ձեր հաշվարկային հաշիվները բանկում', routerLink: '' },
                { icon: 'cached', navText: 'Ավելացրեք նոր հաշիվներ հաշվային պլանում', routerLink: '' },
                { icon: 'get_app', navText: 'Մուտքագրեք հաշիվների սկզբնական մնացորդները', routerLink: '' },
                { icon: 'info', navText: 'Մուտքագրեք տեղեկություններ Ձեր գործընկերների մասին', routerLink: '' },
                { icon: 'output', navText: 'Մուտքագրեք տեղեկությունների սկզբնական մնացորդները', routerLink: '' },
                { icon: 'perm_identity', navText: 'Ավելացրեք Ձեր ծառայությունները', routerLink: '' },
                { icon: 'redeem', navText: 'Ավելացրեք Ձեր ապրանքները և նյութական արժեքները', routerLink: '' },
                { icon: 'info', navText: 'Աշխատավարձի հաշվարկի օգնական', routerLink: '' },
                { icon: 'edit', navText: 'Ապրանքների հրապարակման օգնական ', routerLink: '' },
                { icon: 'edit', navText: 'Դիտեք ուսումնական հոլովակները', routerLink: '' },
            ];
            var MENU_ITEMS = [
                { icon: 'bookmarks', navText: 'Հաշվապահություն', routerLink: '/main-accounting' },
                { icon: 'bookmarks', navText: 'Պահեստ', routerLink: '/warehouse' },
                { icon: 'bookmarks', navText: 'Աշխատավարձ', routerLink: '/salary' },
                { icon: 'bookmarks', navText: 'Հիմնական միջոցներ', routerLink: '/fixed-assets' },
                { icon: 'bookmarks', navText: 'Բանկ', routerLink: '/bank' },
            ];
            var HomeView = /** @class */ (function () {
                function HomeView() {
                    this.menuActions = [];
                    this.menuItems = [];
                    this.menuActions = MENU_ACTIONS;
                    this.menuItems = MENU_ITEMS;
                }
                HomeView.prototype.ngOnInit = function () {
                };
                HomeView.prototype.ngOnDestroy = function () { };
                return HomeView;
            }());
            HomeView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'home-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/home/home.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/home/home.view.scss")).default]
                })
            ], HomeView);
            /***/ 
        })
    }]);
//# sourceMappingURL=home-home-module-es2015.js.map
//# sourceMappingURL=home-home-module-es5.js.map
//# sourceMappingURL=home-home-module-es5.js.map