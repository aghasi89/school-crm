(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main-accounting-main-accounting-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.html":
/*!*******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.html ***!
  \*******************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-items-card [path]=\"'/main-accounting'\" [header]=\"header\" [items]=\"mainAccountigItems\"></app-items-card>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting-routing.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting-routing.ts ***!
  \*************************************************************************************************/
/*! exports provided: MainAccountingRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainAccountingRoutingModule", function() { return MainAccountingRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _main_accounting_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main-accounting.component */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.ts");




const mainAccountingRoutes = [
    { path: '', component: _main_accounting_component__WEBPACK_IMPORTED_MODULE_3__["MainAccountingComponent"] },
    {
        path: 'unit-measurment',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-unit-of-measurement-unit-of-measurment-module */ "pages-unit-of-measurement-unit-of-measurment-module").then(__webpack_require__.bind(null, /*! ./pages/unit-of-measurement/unit-of-measurment.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment.module.ts")).then(m => m.UnitOfMeasurmentModule)
    },
    { path: 'chart-of-accounts', loadChildren: () => __webpack_require__.e(/*! import() | pages-chart-accounts-chart-accounts-module */ "pages-chart-accounts-chart-accounts-module").then(__webpack_require__.bind(null, /*! ./pages/chart-accounts/chart-accounts.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.module.ts")).then(m => m.ChartAccountsModule) },
    { path: 'payment-order', loadChildren: () => __webpack_require__.e(/*! import() | pages-payment-order-payment-order-module */ "pages-payment-order-payment-order-module").then(__webpack_require__.bind(null, /*! ./pages/payment-order/payment-order.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/payment-order/payment-order.module.ts")).then(m => m.PaymentOrderModule) },
    { path: 'partners', loadChildren: () => __webpack_require__.e(/*! import() | pages-partners-partners-module */ "pages-partners-partners-module").then(__webpack_require__.bind(null, /*! ./pages/partners/partners.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.module.ts")).then(m => m.PartnersModule) },
    { path: 'cash-registers', loadChildren: () => __webpack_require__.e(/*! import() | pages-cash-registers-cash-registers-modul */ "pages-cash-registers-cash-registers-modul").then(__webpack_require__.bind(null, /*! ./pages/cash-registers/cash-registers.modul */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.modul.ts")).then(m => m.CashRegistersModule) },
    { path: 'services', loadChildren: () => __webpack_require__.e(/*! import() | pages-services-services-module */ "pages-services-services-module").then(__webpack_require__.bind(null, /*! ./pages/services/services.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.module.ts")).then(m => m.ServicesModule) },
    { path: 'price-of-services', loadChildren: () => __webpack_require__.e(/*! import() | pages-price-of-services-price-of-services-module */ "pages-price-of-services-price-of-services-module").then(__webpack_require__.bind(null, /*! ./pages/price-of-services/price-of-services.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.module.ts")).then(m => m.PriceOfServicesModule) },
    { path: 'currency', loadChildren: () => __webpack_require__.e(/*! import() | pages-currency-currency-module */ "pages-currency-currency-module").then(__webpack_require__.bind(null, /*! ./pages/currency/currency.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.module.ts")).then(m => m.CurrencyModule) },
    { path: 'bank', loadChildren: () => __webpack_require__.e(/*! import() | pages-bank-bank-module */ "pages-bank-bank-module").then(__webpack_require__.bind(null, /*! ./pages/bank/bank.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.module.ts")).then(m => m.BankModule) },
    { path: 'bank-account', loadChildren: () => __webpack_require__.e(/*! import() | pages-bank-account-bank-account-module */ "pages-bank-account-bank-account-module").then(__webpack_require__.bind(null, /*! ./pages/bank-account/bank-account.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.module.ts")).then(m => m.BankAccountModule) },
    { path: 'typical-actions', loadChildren: () => __webpack_require__.e(/*! import() | pages-typical-actions-typical-actions-module */ "pages-typical-actions-typical-actions-module").then(__webpack_require__.bind(null, /*! ./pages/typical-actions/typical-actions.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.module.ts")).then(m => m.TypicalActionsModule) },
    { path: 'formula', loadChildren: () => __webpack_require__.e(/*! import() | pages-formula-formula-module */ "pages-formula-formula-module").then(__webpack_require__.bind(null, /*! ./pages/formula/formula.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/formula/formula.module.ts")).then(m => m.FormulaModule) },
    { path: 'analytical-group-1', loadChildren: () => __webpack_require__.e(/*! import() | pages-analytical-group-analytical-group-module */ "pages-analytical-group-analytical-group-module").then(__webpack_require__.bind(null, /*! ./pages/analytical-group/analytical-group.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/analytical-group/analytical-group.module.ts")).then(m => m.AnalyticalGroupModule), data: { group: '1' } },
    { path: 'analytical-group-2', loadChildren: () => __webpack_require__.e(/*! import() | pages-analytical-group-analytical-group-module */ "pages-analytical-group-analytical-group-module").then(__webpack_require__.bind(null, /*! ./pages/analytical-group/analytical-group.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/analytical-group/analytical-group.module.ts")).then(m => m.AnalyticalGroupModule), data: { group: '2' } },
    { path: 'receive-service', loadChildren: () => __webpack_require__.e(/*! import() | pages-receive-service-receive-service-module */ "pages-receive-service-receive-service-module").then(__webpack_require__.bind(null, /*! ./pages/receive-service/receive-service.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/receive-service/receive-service.module.ts")).then(m => m.ReceiveServiceModule) },
    { path: 'cash-entry-order', loadChildren: () => __webpack_require__.e(/*! import() | pages-cash-entry-order-cash-entry-order-module */ "pages-cash-entry-order-cash-entry-order-module").then(__webpack_require__.bind(null, /*! ./pages/cash-entry-order/cash-entry-order.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-entry-order/cash-entry-order.module.ts")).then(m => m.CashEntryOrderModule) },
    { path: 'cash-exit-order', loadChildren: () => __webpack_require__.e(/*! import() | pages-cash-exit-order-cash-exit-order-module */ "pages-cash-exit-order-cash-exit-order-module").then(__webpack_require__.bind(null, /*! ./pages/cash-exit-order/cash-exit-order.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-exit-order/cash-exit-order.module.ts")).then(m => m.CashExitOrderModule) },
    { path: 'invoice', loadChildren: () => Promise.all(/*! import() | pages-accounting-invoice-accounting-invoice-modue */[__webpack_require__.e("common"), __webpack_require__.e("pages-accounting-invoice-accounting-invoice-modue")]).then(__webpack_require__.bind(null, /*! ./pages/accounting-invoice/accounting-invoice.modue */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.modue.ts")).then(m => m.AccountingInvoiceModule) }
];
let MainAccountingRoutingModule = class MainAccountingRoutingModule {
};
MainAccountingRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(mainAccountingRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], MainAccountingRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.scss ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvbWFpbi1hY2NvdW50aW5nLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: MainAccountingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainAccountingComponent", function() { return MainAccountingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _modals_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modals/index */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");






let MainAccountingComponent = class MainAccountingComponent {
    constructor(_activatedRoute, _router, _matDialog, _title) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._matDialog = _matDialog;
        this._title = _title;
        this._mainAccountigItems = [
            { label: 'Ստացված ծառայություններ', path: 'receive-service' },
            { label: 'Վճարման հանձնարարագիր', path: 'payment-order' },
            { label: 'Դրամարկղ', path: 'cash-registers' },
            { label: 'Դրամարկղի մուտքի օրդեր', path: 'cash-entry-order' },
            { label: 'Դրամարկղի ելքի օրդեր', path: 'cash-exit-order' },
            { label: 'Հաշիվ ապրանքագիր', path: 'invoice' },
            { label: 'Մնացորդներ', modalName: _modals_index__WEBPACK_IMPORTED_MODULE_4__["BalanceModal"] },
            { label: 'Շրջանառություն', modalName: _modals_index__WEBPACK_IMPORTED_MODULE_4__["TunoverModal"] },
            { label: 'T-հաշիվ', modalName: _modals_index__WEBPACK_IMPORTED_MODULE_4__["TAccountModal"] },
            { label: 'Գործընկերոջ T-հաշիվ', modalName: _modals_index__WEBPACK_IMPORTED_MODULE_4__["PartnerTAccountModal"] },
            { label: 'Անալիտիկ խումբ 1', path: 'analytical-group-1' },
            { label: 'Անալիտիկ խումբ 2', path: 'analytical-group-2' },
            { label: 'Հաշվային պլան', path: 'chart-of-accounts' },
            { label: 'Գործընկերներ', path: 'partners' },
            { label: 'Ծառայություններ', path: 'services' },
            { label: 'Ծառայության գին', path: 'price-of-services' },
            { label: 'Չափման միավոր', path: 'unit-measurment' },
            { label: 'Արտայժույթներ', path: 'currency' },
            { label: 'Արտարժույթի փոխարժեքներ', modalName: _modals_index__WEBPACK_IMPORTED_MODULE_4__["CurrencyRatesModal"] },
            { label: 'Բանկեր', path: 'bank' },
            { label: 'Հաշվարկային հաշիվներ բանկում', path: 'bank-account' },
            { label: 'Տիպային գործողություններ', path: 'typical-actions' },
            { label: 'Բանաձևեր', path: 'formula' }
        ];
        this.header = this._activatedRoute.data['_value'].title;
        this._title.setTitle(this.header);
    }
    ngOnInit() {
    }
    openModal(item) {
        if (item.path) {
            this._router.navigate([`/main-accounting/${item.path}`]);
        }
        else {
            if (item && item.modalName) {
                this._matDialog.open(item.modalName, {
                    width: '80vw',
                    minHeight: '90vh',
                    maxHeight: '85vh',
                    panelClass: 'fixed-assets-modal',
                    autoFocus: false,
                    data: { label: item.label, type: item.type }
                });
            }
        }
    }
    get mainAccountigItems() {
        return this._mainAccountigItems;
    }
};
MainAccountingComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] }
];
MainAccountingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-main-accounting',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main-accounting.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main-accounting.component.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.scss")).default]
    })
], MainAccountingComponent);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.module.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.module.ts ***!
  \************************************************************************************************/
/*! exports provided: MainAccountingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainAccountingModule", function() { return MainAccountingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _main_accounting_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main-accounting.component */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting.component.ts");
/* harmony import */ var _main_accounting_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./main-accounting-routing */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/main-accounting-routing.ts");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../components */ "./src/app/com/annaniks/shemm-school/components/index.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _warehouse_pages_material_values_material_values_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../warehouse/pages/material-values/material-values.service */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");









let MainAccountingModule = class MainAccountingModule {
};
MainAccountingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _main_accounting_component__WEBPACK_IMPORTED_MODULE_2__["MainAccountingComponent"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["ServicesReceivedModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["AccountInvoiceModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["RemnantsModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CirculationModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["TAccountModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["PartnerTAccountModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["ChartOfAccountModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["ColleaguesModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["MemorableOrdersModal"],
            _components__WEBPACK_IMPORTED_MODULE_5__["TAccountComponent"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["AddAccountModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CashRegisterLeaveOrder"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CashEntryOrderModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["BalanceModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["TunoverModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["ReceiveServiceModal"],
            _components__WEBPACK_IMPORTED_MODULE_5__["BalanceCommonComponent"],
            _components__WEBPACK_IMPORTED_MODULE_5__["ReceiveServiceCommonComponent"],
            _components__WEBPACK_IMPORTED_MODULE_5__["ReceiveServiceNamesComponent"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CurrencyRatesModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CashOutgoingOrderModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CashWarrantModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["AddCashRegisterModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["AddPriceOfServiceModal"]
        ],
        imports: [_main_accounting_routing__WEBPACK_IMPORTED_MODULE_3__["MainAccountingRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"]],
        entryComponents: [
            _modals__WEBPACK_IMPORTED_MODULE_4__["ServicesReceivedModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["AccountInvoiceModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["RemnantsModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CirculationModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["TAccountModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["PartnerTAccountModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["ChartOfAccountModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["ColleaguesModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["MemorableOrdersModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["AddAccountModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CashRegisterLeaveOrder"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CashEntryOrderModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["BalanceModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["TunoverModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["ReceiveServiceModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CurrencyRatesModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CashOutgoingOrderModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["CashWarrantModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["AddCashRegisterModal"],
            _modals__WEBPACK_IMPORTED_MODULE_4__["AddPriceOfServiceModal"]
        ],
        providers: [_warehouse_pages_material_values_material_values_service__WEBPACK_IMPORTED_MODULE_7__["MaterialValuesService"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["DatePipe"]]
    })
], MainAccountingModule);



/***/ })

}]);
//# sourceMappingURL=main-accounting-main-accounting-module-es2015.js.map