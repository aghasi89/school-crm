(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-accounting-invoice-accounting-invoice-modue"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.html": 
        /*!******************************************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.html ***!
          \******************************************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<invoice-view [mainUrl]=\"mainUrl\" [getOneUrl]=\"getOneUrl\"></invoice-view>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.modue.ts": 
        /*!***************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.modue.ts ***!
          \***************************************************************************************************************************/
        /*! exports provided: AccountingInvoiceModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountingInvoiceModule", function () { return AccountingInvoiceModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _accounting_invoice_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accounting-invoice.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _accounting_invoice_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./accounting-invoice.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.routing.module.ts");
            /* harmony import */ var _warehouse_pages_invoice_invoice_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../warehouse/pages/invoice/invoice.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/invoice/invoice.module.ts");
            var AccountingInvoiceModule = /** @class */ (function () {
                function AccountingInvoiceModule() {
                }
                return AccountingInvoiceModule;
            }());
            AccountingInvoiceModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_accounting_invoice_view__WEBPACK_IMPORTED_MODULE_2__["AccountingInvoiceView"]],
                    imports: [src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _accounting_invoice_routing_module__WEBPACK_IMPORTED_MODULE_4__["AccountingInvoiceRoutingModule"], _warehouse_pages_invoice_invoice_module__WEBPACK_IMPORTED_MODULE_5__["InvoiceModule"]]
                })
            ], AccountingInvoiceModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.routing.module.ts": 
        /*!************************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.routing.module.ts ***!
          \************************************************************************************************************************************/
        /*! exports provided: AccountingInvoiceRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountingInvoiceRoutingModule", function () { return AccountingInvoiceRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _accounting_invoice_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./accounting-invoice.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.ts");
            var accountingInvoiceRoutes = [{ path: '', component: _accounting_invoice_view__WEBPACK_IMPORTED_MODULE_3__["AccountingInvoiceView"] }];
            var AccountingInvoiceRoutingModule = /** @class */ (function () {
                function AccountingInvoiceRoutingModule() {
                }
                return AccountingInvoiceRoutingModule;
            }());
            AccountingInvoiceRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(accountingInvoiceRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], AccountingInvoiceRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.scss": 
        /*!****************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.scss ***!
          \****************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvYWNjb3VudGluZy1pbnZvaWNlL2FjY291bnRpbmctaW52b2ljZS52aWV3LnNjc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.ts": 
        /*!**************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.ts ***!
          \**************************************************************************************************************************/
        /*! exports provided: AccountingInvoiceView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountingInvoiceView", function () { return AccountingInvoiceView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var AccountingInvoiceView = /** @class */ (function () {
                function AccountingInvoiceView(_urls) {
                    this._urls = _urls;
                }
                AccountingInvoiceView.prototype.ngOnInit = function () {
                    this.mainUrl = this._urls.accountingInvoiceMainUrl;
                    this.getOneUrl = this._urls.accountingInvoiceGetOneUrl;
                };
                return AccountingInvoiceView;
            }());
            AccountingInvoiceView.ctorParameters = function () { return [
                { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
            ]; };
            AccountingInvoiceView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'accounting-invoice-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./accounting-invoice.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./accounting-invoice.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/accounting-invoice/accounting-invoice.view.scss")).default]
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
            ], AccountingInvoiceView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-accounting-invoice-accounting-invoice-modue-es2015.js.map
//# sourceMappingURL=pages-accounting-invoice-accounting-invoice-modue-es5.js.map
//# sourceMappingURL=pages-accounting-invoice-accounting-invoice-modue-es5.js.map