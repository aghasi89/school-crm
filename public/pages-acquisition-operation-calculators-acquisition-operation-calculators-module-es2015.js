(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-acquisition-operation-calculators-acquisition-operation-calculators-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.html":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.html ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addOperationFixedAssets(false)\">\r\n    <tr *ngFor=\"let item of operationFixedAssets\">\r\n        <td></td>\r\n        <!-- <td class=\"edit\"> <i (click)=\"addOperationFixedAssets(true,item?.id)\" class=\" material-icons\"> edit </i> </td> -->\r\n        <td class=\"delete\"> <i (click)=\"delete(item?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{item?.documentNumber}}</td>\r\n        <td>{{item?.expenseAccountId}}</td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.module.ts":
/*!*******************************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.module.ts ***!
  \*******************************************************************************************************************************************************/
/*! exports provided: AcquisitionOperationOfFixedAssetsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcquisitionOperationOfFixedAssetsModule", function() { return AcquisitionOperationOfFixedAssetsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _acquisition_operation_calculators_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./acquisition-operation-calculators.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _acquisition_operation_calculators_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./acquisition-operation-calculators.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.routing.module.ts");





let AcquisitionOperationOfFixedAssetsModule = class AcquisitionOperationOfFixedAssetsModule {
};
AcquisitionOperationOfFixedAssetsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_acquisition_operation_calculators_view__WEBPACK_IMPORTED_MODULE_2__["AcquisitionOperationOfFixedAssetsView"]],
        imports: [src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _acquisition_operation_calculators_routing_module__WEBPACK_IMPORTED_MODULE_4__["AcquisitionOperationOfFixedAssetsRoutingModule"]]
    })
], AcquisitionOperationOfFixedAssetsModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.routing.module.ts":
/*!***************************************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.routing.module.ts ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: AcquisitionOperationOfFixedAssetsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcquisitionOperationOfFixedAssetsRoutingModule", function() { return AcquisitionOperationOfFixedAssetsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _acquisition_operation_calculators_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./acquisition-operation-calculators.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.ts");




let routes = [{ path: '', component: _acquisition_operation_calculators_view__WEBPACK_IMPORTED_MODULE_3__["AcquisitionOperationOfFixedAssetsView"] }];
let AcquisitionOperationOfFixedAssetsRoutingModule = class AcquisitionOperationOfFixedAssetsRoutingModule {
};
AcquisitionOperationOfFixedAssetsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AcquisitionOperationOfFixedAssetsRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.scss":
/*!*******************************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.scss ***!
  \*******************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy9hY3F1aXNpdGlvbi1vcGVyYXRpb24tY2FsY3VsYXRvcnMvQzpcXFVzZXJzXFxBbm5hbmlrc1xcRGVza3RvcFxcc2hlbW0tc2Nob29sL3NyY1xcYXBwXFxjb21cXGFubmFuaWtzXFxzaGVtbS1zY2hvb2xcXHZpZXdzXFxtYWluXFxmaXhlZC1hc3NldHNcXHBhZ2VzXFxhY3F1aXNpdGlvbi1vcGVyYXRpb24tY2FsY3VsYXRvcnNcXGFjcXVpc2l0aW9uLW9wZXJhdGlvbi1jYWxjdWxhdG9ycy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvYWNxdWlzaXRpb24tb3BlcmF0aW9uLWNhbGN1bGF0b3JzL2FjcXVpc2l0aW9uLW9wZXJhdGlvbi1jYWxjdWxhdG9ycy52aWV3LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvYWNxdWlzaXRpb24tb3BlcmF0aW9uLWNhbGN1bGF0b3JzL2FjcXVpc2l0aW9uLW9wZXJhdGlvbi1jYWxjdWxhdG9ycy52aWV3LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59IiwidGQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.ts":
/*!*****************************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.ts ***!
  \*****************************************************************************************************************************************************/
/*! exports provided: AcquisitionOperationOfFixedAssetsView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcquisitionOperationOfFixedAssetsView", function() { return AcquisitionOperationOfFixedAssetsView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/modals/index.ts");










let AcquisitionOperationOfFixedAssetsView = class AcquisitionOperationOfFixedAssetsView {
    constructor(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService, _urls) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._loadingService = _loadingService;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._mainService = _mainService;
        this._appService = _appService;
        this._urls = _urls;
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._modalTitle = 'ՀՄ-ի ձեռքբերում և շահագործում';
        // public revaluations = []
        this.operationFixedAssets = [];
        this._paginatorLastPageNumber = 0;
        this.titles = [{ title: 'Փաստաթղթի N' }, { title: 'Ծախսի հաշիվ' }];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["forkJoin"])(this._getOperationFixedAssetsCount(), this._getOperationFixedAssets(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading());
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryparams) => {
            if (queryparams && queryparams.page) {
                this._page = +queryparams.page;
            }
            // this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength)
        });
    }
    _getOperationFixedAssetsCount() {
        return this._mainService.getCount(this._urls.operationFixedAssetsMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    _getOperationFixedAssets(limit, offset) {
        return this._mainService.getByUrl(this._urls.operationFixedAssetsMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this.operationFixedAssets = data.data;
            return data;
        }));
    }
    addOperationFixedAssets(isNew, id) {
        this._openModal(isNew, id);
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    _openModal(isNew, id) {
        let isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_9__["AcquisitionOperationCalculatorsModal"], {
            width: '80vw',
            minHeight: '55vh',
            maxHeight: '85vh',
            autoFocus: false,
            data: { title: isNewTitle, url: this._urls.operationFixedAssetsGetOneUrl, id: id, type: 0 }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    getBooleanVariable(variable) {
        return this._appService.getBooleanVariable(variable);
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._urls.operationFixedAssetsGetOneUrl, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.operationFixedAssets, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        });
    }
    ngOnDestroy() {
        this._loadingService.hideLoading();
        // this._subscription.unsubscribe()
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
};
AcquisitionOperationOfFixedAssetsView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["LoadingService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_7__["MainService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["AppService"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
AcquisitionOperationOfFixedAssetsView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'acquisition-operation-calculators-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./acquisition-operation-calculators.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./acquisition-operation-calculators.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/acquisition-operation-calculators/acquisition-operation-calculators.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], AcquisitionOperationOfFixedAssetsView);



/***/ })

}]);
//# sourceMappingURL=pages-acquisition-operation-calculators-acquisition-operation-calculators-module-es2015.js.map