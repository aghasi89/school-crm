(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-act-putting-into-operation-act-putting-into-operation-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.html":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.html ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addActPuttingIntoOperation(false)\">\r\n    <tr *ngFor=\"let item of actPuttingIntooperations\">\r\n        <td></td>\r\n\r\n        <!-- <td class=\"edit\"> <i (click)=\"addActPuttingIntoOperation(true,item?.id)\" class=\" material-icons\"> edit </i> </td> -->\r\n        <td class=\"delete\"> <i (click)=\"delete(item?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{item?.documentNumber}}</td>\r\n        <td>{{item?.expenseAccountId}}</td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.module.ts":
/*!*****************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.module.ts ***!
  \*****************************************************************************************************************************************/
/*! exports provided: ActPuttingIntoOperationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActPuttingIntoOperationModule", function() { return ActPuttingIntoOperationModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _act_putting_into_operation_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./act-putting-into-operation.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.ts");
/* harmony import */ var _act_putting_into_operation_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./act-putting-into-operation.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");





let ActPuttingIntoOperationModule = class ActPuttingIntoOperationModule {
};
ActPuttingIntoOperationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_act_putting_into_operation_view__WEBPACK_IMPORTED_MODULE_2__["ActPuttingIntoOperationView"]],
        imports: [_act_putting_into_operation_routing_module__WEBPACK_IMPORTED_MODULE_3__["ActPuttingIntoOperationRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
    })
], ActPuttingIntoOperationModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.routing.module.ts":
/*!*************************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.routing.module.ts ***!
  \*************************************************************************************************************************************************/
/*! exports provided: ActPuttingIntoOperationRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActPuttingIntoOperationRoutingModule", function() { return ActPuttingIntoOperationRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _act_putting_into_operation_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./act-putting-into-operation.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.ts");




let actPuttingIntoOperationRoutes = [{ path: '', component: _act_putting_into_operation_view__WEBPACK_IMPORTED_MODULE_3__["ActPuttingIntoOperationView"] }];
let ActPuttingIntoOperationRoutingModule = class ActPuttingIntoOperationRoutingModule {
};
ActPuttingIntoOperationRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(actPuttingIntoOperationRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], ActPuttingIntoOperationRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.scss":
/*!*****************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.scss ***!
  \*****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy9hY3QtcHV0dGluZy1pbnRvLW9wZXJhdGlvbi9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXGZpeGVkLWFzc2V0c1xccGFnZXNcXGFjdC1wdXR0aW5nLWludG8tb3BlcmF0aW9uXFxhY3QtcHV0dGluZy1pbnRvLW9wZXJhdGlvbi52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvYWN0LXB1dHRpbmctaW50by1vcGVyYXRpb24vYWN0LXB1dHRpbmctaW50by1vcGVyYXRpb24udmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL21haW4vZml4ZWQtYXNzZXRzL3BhZ2VzL2FjdC1wdXR0aW5nLWludG8tb3BlcmF0aW9uL2FjdC1wdXR0aW5nLWludG8tb3BlcmF0aW9uLnZpZXcuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRkIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbn0iLCJ0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.ts":
/*!***************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.ts ***!
  \***************************************************************************************************************************************/
/*! exports provided: ActPuttingIntoOperationView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActPuttingIntoOperationView", function() { return ActPuttingIntoOperationView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/modals/index.ts");










let ActPuttingIntoOperationView = class ActPuttingIntoOperationView {
    constructor(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService, _urls) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._loadingService = _loadingService;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._mainService = _mainService;
        this._appService = _appService;
        this._urls = _urls;
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._modalTitle = 'Շահագործման հանձնման ակտ';
        this.actPuttingIntooperations = [];
        this._paginatorLastPageNumber = 0;
        this.titles = [{ title: 'Փաստաթղթի N' }, { title: 'Ծախսի հաշիվ' }];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["forkJoin"])(this._getActPuttingIntoOperationCount(), this._getActPuttingIntoOperation(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading());
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryparams) => {
            if (queryparams && queryparams.page) {
                this._page = +queryparams.page;
            }
            // this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength)
        });
    }
    _getActPuttingIntoOperationCount() {
        return this._mainService.getCount(this._urls.actPuttingIntoOperationMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    _getActPuttingIntoOperation(limit, offset) {
        return this._mainService.getByUrl(this._urls.actPuttingIntoOperationMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this.actPuttingIntooperations = data.data;
            return data;
        }));
    }
    addActPuttingIntoOperation(isNew, id) {
        this._openModal(isNew, id);
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    _openModal(isNew, id) {
        let isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_9__["ActPuttingIntoOperationModal"], {
            width: '80vw',
            minHeight: '55vh',
            maxHeight: '85vh',
            autoFocus: false,
            data: { title: isNewTitle, url: this._urls.actPuttingIntoOperationGetOneUrl, id: id }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    getBooleanVariable(variable) {
        return this._appService.getBooleanVariable(variable);
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._urls.actPuttingIntoOperationGetOneUrl, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.actPuttingIntooperations, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        });
    }
    ngOnDestroy() {
        this._loadingService.hideLoading();
        // this._subscription.unsubscribe()
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
};
ActPuttingIntoOperationView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_3__["LoadingService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_5__["MainService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_3__["AppService"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
ActPuttingIntoOperationView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'act-putting-into-operation-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./act-putting-into-operation.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./act-putting-into-operation.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/act-putting-into-operation/act-putting-into-operation.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], ActPuttingIntoOperationView);



/***/ })

}]);
//# sourceMappingURL=pages-act-putting-into-operation-act-putting-into-operation-module-es2015.js.map