(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-addition-addition-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.html":
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.html ***!
  \*************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addAddition(false)\">\r\n    <tr *ngFor=\"let addition of additions\">\r\n        <td class=\"edit\"> <i (click)=\"addAddition(true,addition?.id)\" class=\" material-icons\"> edit </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(addition?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{addition?.id}}</td>\r\n        <td>{{addition?.name}}</td>\r\n        <td>{{addition?.tabel?.name}}</td>\r\n        <td>{{addition?.coefficient}}</td>\r\n        <td>{{addition?.typeOfIncome?.name}}</td>\r\n        <td>{{addition?.expenseAccount?.name}}</td>\r\n        <td>{{addition?.typeOfVacation?.name}}</td>\r\n        <td> <label class=\"container-checkbox\">\r\n                <input [checked]=\"getBooleanVariable(addition?.recalculation)\" disabled type=\"checkbox\">\r\n                <span class=\"checkmark\"></span>\r\n            </label></td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.module.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: AdditionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdditionModule", function() { return AdditionModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _addition_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addition.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.ts");
/* harmony import */ var _addition_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./addition.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/salary/modals/index.ts");






let AdditionModule = class AdditionModule {
};
AdditionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_addition_view__WEBPACK_IMPORTED_MODULE_2__["AdditionView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddAdditionModal"]],
        imports: [_addition_routing_module__WEBPACK_IMPORTED_MODULE_3__["AdditionRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
        entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddAdditionModal"]]
    })
], AdditionModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.routing.module.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.routing.module.ts ***!
  \*******************************************************************************************************/
/*! exports provided: AdditionRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdditionRoutingModule", function() { return AdditionRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _addition_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./addition.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.ts");




let additionRoutes = [{ path: '', component: _addition_view__WEBPACK_IMPORTED_MODULE_3__["AdditionView"] }];
let AdditionRoutingModule = class AdditionRoutingModule {
};
AdditionRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(additionRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AdditionRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.scss ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy9hZGRpdGlvbi9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXHNhbGFyeVxccGFnZXNcXGFkZGl0aW9uXFxhZGRpdGlvbi52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9zYWxhcnkvcGFnZXMvYWRkaXRpb24vYWRkaXRpb24udmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL21haW4vc2FsYXJ5L3BhZ2VzL2FkZGl0aW9uL2FkZGl0aW9uLnZpZXcuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLCB0aCwgdGQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOjEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIH0iLCJ0YWJsZSwgdGgsIHRkIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.ts ***!
  \*********************************************************************************************/
/*! exports provided: AdditionView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdditionView", function() { return AdditionView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/salary/modals/index.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");










let AdditionView = class AdditionView {
    constructor(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService, _urls) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._loadingService = _loadingService;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._mainService = _mainService;
        this._appService = _appService;
        this._urls = _urls;
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._modalTitle = 'Հավելում';
        this._paginatorLastPageNumber = 0;
        this.additions = [];
        this.titles = [
            { title: 'Կոդ' },
            { title: 'Անվանում' },
            { title: 'Տաբելային կոդ' },
            { title: 'Գործակից' },
            { title: 'Եկամտի տեսակ' },
            { title: 'Ծախսի հաշիվ' },
            { title: 'Մասնակցում է արձակուրդայինի կենսաթոշակը միջինի մեջ' },
            { title: 'Հետհաշվարկ' }
        ];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["forkJoin"])(this._getAdditionCount(), this._getAddition(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading()
        // , () => this._loadingService.hideLoading()
        );
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryparams) => {
            if (queryparams && queryparams.page) {
                this._page = +queryparams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _getAdditionCount() {
        return this._mainService.getCount(this._urls.additionMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    addAddition(isNew, id) {
        this.openModal(isNew, id);
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    _getAddition(limit, offset) {
        return this._mainService.getByUrl(this._urls.additionMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this.additions = data.data;
            return data;
        }));
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    openModal(isNew, id) {
        let isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_6__["AddAdditionModal"], {
            width: '80vw',
            minHeight: '55vh',
            maxHeight: '85vh',
            autoFocus: false,
            data: { title: isNewTitle, url: this._urls.additionGetOneUrl, id: id }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    getBooleanVariable(variable) {
        return this._appService.getBooleanVariable(variable);
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._urls.additionGetOneUrl, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.addAddition, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }
        // , () => { this._loadingService.hideLoading() }
        );
    }
    ngOnDestroy() {
        this._loadingService.hideLoading();
        this._subscription.unsubscribe();
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
};
AdditionView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_4__["LoadingService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_9__["MainService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_4__["AppService"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
AdditionView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'addition-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./addition.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./addition.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], AdditionView);



/***/ })

}]);
//# sourceMappingURL=pages-addition-addition-module-es2015.js.map