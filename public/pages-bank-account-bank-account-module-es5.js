(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-bank-account-bank-account-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.html": 
        /*!******************************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.html ***!
          \******************************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addBankAccount()\">\r\n</app-tables-by-filter>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.module.ts": 
        /*!****************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.module.ts ***!
          \****************************************************************************************************************/
        /*! exports provided: BankAccountModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountModule", function () { return BankAccountModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _bank_account_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bank-account.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.ts");
            /* harmony import */ var _bank_account_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bank-account.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.routing.module.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
            var BankAccountModule = /** @class */ (function () {
                function BankAccountModule() {
                }
                return BankAccountModule;
            }());
            BankAccountModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_bank_account_view__WEBPACK_IMPORTED_MODULE_2__["BankAccountView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddBankAccountModal"]],
                    entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddBankAccountModal"]],
                    imports: [_bank_account_routing_module__WEBPACK_IMPORTED_MODULE_3__["BankAccountRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
                })
            ], BankAccountModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.routing.module.ts": 
        /*!************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.routing.module.ts ***!
          \************************************************************************************************************************/
        /*! exports provided: BankAccountRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountRoutingModule", function () { return BankAccountRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _bank_account_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bank-account.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.ts");
            var bankAccountRoutes = [{ path: '', component: _bank_account_view__WEBPACK_IMPORTED_MODULE_3__["BankAccountView"] }];
            var BankAccountRoutingModule = /** @class */ (function () {
                function BankAccountRoutingModule() {
                }
                return BankAccountRoutingModule;
            }());
            BankAccountRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(bankAccountRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], BankAccountRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.scss": 
        /*!****************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.scss ***!
          \****************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvYmFuay1hY2NvdW50L2JhbmstYWNjb3VudC52aWV3LnNjc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.ts": 
        /*!**************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.ts ***!
          \**************************************************************************************************************/
        /*! exports provided: BankAccountView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountView", function () { return BankAccountView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
            var BankAccountView = /** @class */ (function () {
                function BankAccountView(_matDialog, _title) {
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this.titles = [
                        {
                            title: 'Հաշվարկային հաշիվ',
                            isSort: false,
                            arrow: 'arrow_drop_down',
                            min: false,
                            max: true
                        },
                        {
                            title: 'Անվանում',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Արժույթ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Հաշիվ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Հիմնական',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Փաստաթղթի հերթական համարը',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Էլ․ սպասարկման միջոց',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                    ];
                    this._title.setTitle('Հաշվարկային հաշիվներ բանկում');
                }
                BankAccountView.prototype.ngOnInit = function () { };
                BankAccountView.prototype.addBankAccount = function () {
                    this.openModal();
                };
                BankAccountView.prototype.openModal = function () {
                    this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_4__["AddBankAccountModal"], {
                        width: '600px'
                    });
                };
                return BankAccountView;
            }());
            BankAccountView.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] }
            ]; };
            BankAccountView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'bank-account-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./bank-account.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./bank-account.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank-account/bank-account.view.scss")).default]
                })
            ], BankAccountView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-bank-account-bank-account-module-es2015.js.map
//# sourceMappingURL=pages-bank-account-bank-account-module-es5.js.map
//# sourceMappingURL=pages-bank-account-bank-account-module-es5.js.map