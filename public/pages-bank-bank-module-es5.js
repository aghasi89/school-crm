(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-bank-bank-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.html": 
        /*!**************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.html ***!
          \**************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addBank()\">\r\n    <tr *ngFor=\"let item of banks\">\r\n        <td class=\"edit\"> <i (click)=\"addBank(true, item?.id, item)\" class=\" material-icons\"> edit </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(item?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{item?.code}}</td>\r\n        <td>{{item?.name}}</td>\r\n        <td>{{ item?.swift }}</td>\r\n        \r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.module.ts": 
        /*!************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.module.ts ***!
          \************************************************************************************************/
        /*! exports provided: BankModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankModule", function () { return BankModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _bank_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bank.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.ts");
            /* harmony import */ var _bank_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bank.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.routing.module.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
            var BankModule = /** @class */ (function () {
                function BankModule() {
                }
                return BankModule;
            }());
            BankModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_bank_view__WEBPACK_IMPORTED_MODULE_2__["BankView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddBank"]],
                    imports: [_bank_routing_module__WEBPACK_IMPORTED_MODULE_3__["BankRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
                    entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddBank"]]
                })
            ], BankModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.routing.module.ts": 
        /*!********************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.routing.module.ts ***!
          \********************************************************************************************************/
        /*! exports provided: BankRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankRoutingModule", function () { return BankRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _bank_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bank.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.ts");
            var bankRoutes = [{ path: '', component: _bank_view__WEBPACK_IMPORTED_MODULE_3__["BankView"] }];
            var BankRoutingModule = /** @class */ (function () {
                function BankRoutingModule() {
                }
                return BankRoutingModule;
            }());
            BankRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(bankRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], BankRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.scss": 
        /*!************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.scss ***!
          \************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9iYW5rL0M6XFxVc2Vyc1xcQW5uYW5pa3NcXERlc2t0b3BcXHNoZW1tLXNjaG9vbC9zcmNcXGFwcFxcY29tXFxhbm5hbmlrc1xcc2hlbW0tc2Nob29sXFx2aWV3c1xcbWFpblxcbWFpbi1hY2NvdW50aW5nXFxwYWdlc1xcYmFua1xcYmFuay52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvYmFuay9iYW5rLnZpZXcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9iYW5rL2Jhbmsudmlldy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsIHRoLCB0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59IiwidGFibGUsIHRoLCB0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.ts": 
        /*!**********************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.ts ***!
          \**********************************************************************************************/
        /*! exports provided: BankView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankView", function () { return BankView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
            var BankView = /** @class */ (function () {
                function BankView(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService) {
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this._loadingService = _loadingService;
                    this._activatedRoute = _activatedRoute;
                    this._router = _router;
                    this._mainService = _mainService;
                    this._appService = _appService;
                    this._url = 'banks';
                    this._otherUrl = 'bank';
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this._modalTitle = 'Բանկ';
                    this._paginatorLastPageNumber = 0;
                    this.banks = [];
                    this.titles = [
                        { title: 'Կոդ', isSort: false, arrow: 'arrow_drop_down', min: false, max: true },
                        { title: 'Անվանում', isSort: false, arrow: '', min: false, max: false },
                        { title: 'SWIFT կոդ', isSort: false, arrow: '', min: false, max: false }
                    ];
                    this._title.setTitle('Բանկեր');
                }
                BankView.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                BankView.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["forkJoin"])(this._getBankCount(), this._getBank(limit, offset));
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); }, function () { return _this._loadingService.hideLoading(); });
                };
                BankView.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryparams) {
                        if (queryparams && queryparams.page) {
                            _this._page = +queryparams.page;
                        }
                        _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                    });
                };
                BankView.prototype._getBankCount = function () {
                    var _this = this;
                    return this._mainService.getCount(this._url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                BankView.prototype._getBank = function (limit, offset) {
                    var _this = this;
                    return this._mainService.getByUrl(this._url, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (data) {
                        _this.banks = data.data;
                        return data;
                    }));
                };
                BankView.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                BankView.prototype._resetProperties = function () {
                    this._page = 1;
                };
                BankView.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                BankView.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._mainService.deleteByUrl(this._otherUrl, id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.banks, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    }, function () {
                        _this._loadingService.hideLoading();
                    });
                };
                BankView.prototype.addBank = function (isNew, id, item) {
                    this.openModal(isNew, id, item);
                };
                BankView.prototype.openModal = function (isNew, id, item) {
                    var _this = this;
                    var isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_9__["AddBank"], {
                        width: '500px',
                        data: { title: isNewTitle, url: this._otherUrl, id: id, item: item }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                Object.defineProperty(BankView.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BankView.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BankView.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                BankView.prototype.ngOnDestroy = function () {
                    this._subscription.unsubscribe();
                };
                return BankView;
            }());
            BankView.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__["LoadingService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
                { type: _main_service__WEBPACK_IMPORTED_MODULE_7__["MainService"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__["AppService"] }
            ]; };
            BankView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'bank-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./bank.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./bank.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/bank/bank.view.scss")).default]
                })
            ], BankView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-bank-bank-module-es2015.js.map
//# sourceMappingURL=pages-bank-bank-module-es5.js.map
//# sourceMappingURL=pages-bank-bank-module-es5.js.map