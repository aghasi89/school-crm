(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-billing-method-billing-method-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.html": 
        /*!*********************************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.html ***!
          \*********************************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addBillingMethod()\">\r\n\r\n    <tr *ngFor=\"let billingMethod of billingMethods\">\r\n        <td class=\"edit\"> <i (click)=\"addBillingMethod(true, billingMethod?.id, billingMethod)\" class=\" material-icons\"> edit\r\n            </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(billingMethod?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{ billingMethod?.name }}</td>\r\n        <td>{{ billingMethod?.abbreviation }}</td>\r\n    </tr>\r\n    <tr *ngIf=\"billingMethods.length == 0\">\r\n        <td colspan='4'>Դուք չունեք ստեղծած հաշվառման մեթոդ</td> \r\n    </tr>\r\n\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method-routing.module.ts": 
        /*!**********************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method-routing.module.ts ***!
          \**********************************************************************************************************************/
        /*! exports provided: BillingMethodRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillingMethodRoutingModule", function () { return BillingMethodRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _billing_method_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./billing-method.component */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.ts");
            var billingMethodRoutiongRoutes = [
                { path: '', component: _billing_method_component__WEBPACK_IMPORTED_MODULE_3__["BillingMethodComponent"] }
            ];
            var BillingMethodRoutingModule = /** @class */ (function () {
                function BillingMethodRoutingModule() {
                }
                return BillingMethodRoutingModule;
            }());
            BillingMethodRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(billingMethodRoutiongRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], BillingMethodRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.scss": 
        /*!*******************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.scss ***!
          \*******************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy9iaWxsaW5nLW1ldGhvZC9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXHdhcmVob3VzZVxccGFnZXNcXGJpbGxpbmctbWV0aG9kXFxiaWxsaW5nLW1ldGhvZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy9iaWxsaW5nLW1ldGhvZC9iaWxsaW5nLW1ldGhvZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy9iaWxsaW5nLW1ldGhvZC9iaWxsaW5nLW1ldGhvZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRkIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbn0iLCJ0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.ts": 
        /*!*****************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.ts ***!
          \*****************************************************************************************************************/
        /*! exports provided: BillingMethodComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillingMethodComponent", function () { return BillingMethodComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");
            var BillingMethodComponent = /** @class */ (function () {
                function BillingMethodComponent(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService) {
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this._loadingService = _loadingService;
                    this._activatedRoute = _activatedRoute;
                    this._router = _router;
                    this._mainService = _mainService;
                    this._appService = _appService;
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this._modalTitle = 'Խումբ';
                    this._billingMethodMainUrl = 'billing-methods';
                    this._billingMethodOtherUrl = 'billing-method';
                    this.billingMethods = [];
                    this._paginatorLastPageNumber = 0;
                    this.titles = [
                        { title: 'Կոդ', isSort: false, arrow: '', min: false, max: false },
                        { title: 'Հապավում', isSort: false, arrow: '', min: false, max: false }
                    ];
                    this._title.setTitle(this._modalTitle);
                }
                BillingMethodComponent.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                BillingMethodComponent.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["forkJoin"])(this._getBillingMethodCount(), this._getBillingMethods(limit, offset));
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); });
                };
                BillingMethodComponent.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryparams) {
                        if (queryparams && queryparams.page) {
                            _this._page = +queryparams.page;
                        }
                        _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                    });
                };
                BillingMethodComponent.prototype._getBillingMethodCount = function () {
                    var _this = this;
                    return this._mainService.getCount(this._billingMethodMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                BillingMethodComponent.prototype.addBillingMethod = function (isNew, id, item) {
                    this.openModal(isNew, id, item);
                };
                BillingMethodComponent.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                BillingMethodComponent.prototype._resetProperties = function () {
                    this._page = 1;
                };
                BillingMethodComponent.prototype._getBillingMethods = function (limit, offset) {
                    var _this = this;
                    return this._mainService.getByUrl(this._billingMethodMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) {
                        _this.billingMethods = data.data;
                        return data;
                    }));
                };
                BillingMethodComponent.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                BillingMethodComponent.prototype.openModal = function (isNew, id, item) {
                    var _this = this;
                    var isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_9__["AddBillingMethodModal"], {
                        width: '80vw',
                        minHeight: '48vh',
                        maxHeight: '85vh',
                        data: { title: isNewTitle, url: this._billingMethodOtherUrl, id: id, item: item }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                BillingMethodComponent.prototype.getBooleanVariable = function (variable) {
                    return this._appService.getBooleanVariable(variable);
                };
                BillingMethodComponent.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._mainService.deleteByUrl(this._billingMethodOtherUrl, id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.billingMethods, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    });
                };
                BillingMethodComponent.prototype.ngOnDestroy = function () {
                    this._subscription.unsubscribe();
                };
                Object.defineProperty(BillingMethodComponent.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BillingMethodComponent.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BillingMethodComponent.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                return BillingMethodComponent;
            }());
            BillingMethodComponent.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
                { type: _main_service__WEBPACK_IMPORTED_MODULE_8__["MainService"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["AppService"] }
            ]; };
            BillingMethodComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-billing-method',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./billing-method.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./billing-method.component.scss */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.scss")).default]
                })
            ], BillingMethodComponent);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.module.ts": 
        /*!**************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.module.ts ***!
          \**************************************************************************************************************/
        /*! exports provided: BillingMethodModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillingMethodModule", function () { return BillingMethodModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _billing_method_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./billing-method.component */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.component.ts");
            /* harmony import */ var _billing_method_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./billing-method-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method-routing.module.ts");
            var BillingMethodModule = /** @class */ (function () {
                function BillingMethodModule() {
                }
                return BillingMethodModule;
            }());
            BillingMethodModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_billing_method_component__WEBPACK_IMPORTED_MODULE_3__["BillingMethodComponent"]],
                    imports: [_billing_method_routing_module__WEBPACK_IMPORTED_MODULE_4__["BillingMethodRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"]]
                })
            ], BillingMethodModule);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-billing-method-billing-method-module-es2015.js.map
//# sourceMappingURL=pages-billing-method-billing-method-module-es5.js.map
//# sourceMappingURL=pages-billing-method-billing-method-module-es5.js.map