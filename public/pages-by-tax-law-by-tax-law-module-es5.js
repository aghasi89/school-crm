(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-by-tax-law-by-tax-law-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.html": 
        /*!***********************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.html ***!
          \***********************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addByTaxLaw()\">\r\n  <tr *ngFor=\"let item of addByTaxLawData\">\r\n    <td class=\"edit\"> <i (click)=\"addByTaxLaw(true ,item?.id, item)\" class=\" material-icons\"> edit </i> </td>\r\n    <td class=\"delete\"> <i (click)=\"delete(item?.id)\" class=\" material-icons\"> close </i></td>\r\n    <td>{{item?.code}}</td>\r\n    <td>{{item?.name}}</td>\r\n    <td>{{item?.yearPrice  }}</td>\r\n  </tr>\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n  (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law-routing.module.ts": 
        /*!*****************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law-routing.module.ts ***!
          \*****************************************************************************************************************/
        /*! exports provided: ByTaxLawRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ByTaxLawRoutingModule", function () { return ByTaxLawRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _by_tax_law_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./by-tax-law.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.ts");
            var byTaxLawRoutes = [{ path: '', component: _by_tax_law_view__WEBPACK_IMPORTED_MODULE_3__["ByTaxLawView"] }];
            var ByTaxLawRoutingModule = /** @class */ (function () {
                function ByTaxLawRoutingModule() {
                }
                return ByTaxLawRoutingModule;
            }());
            ByTaxLawRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(byTaxLawRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], ByTaxLawRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.module.ts": 
        /*!*********************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.module.ts ***!
          \*********************************************************************************************************/
        /*! exports provided: ByTaxLawModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ByTaxLawModule", function () { return ByTaxLawModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _by_tax_law_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./by-tax-law.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _by_tax_law_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./by-tax-law-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law-routing.module.ts");
            var ByTaxLawModule = /** @class */ (function () {
                function ByTaxLawModule() {
                }
                return ByTaxLawModule;
            }());
            ByTaxLawModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _by_tax_law_routing_module__WEBPACK_IMPORTED_MODULE_4__["ByTaxLawRoutingModule"]],
                    declarations: [_by_tax_law_view__WEBPACK_IMPORTED_MODULE_2__["ByTaxLawView"]]
                })
            ], ByTaxLawModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.scss": 
        /*!*********************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.scss ***!
          \*********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy9ieS10YXgtbGF3L0M6XFxVc2Vyc1xcQW5uYW5pa3NcXERlc2t0b3BcXHNoZW1tLXNjaG9vbC9zcmNcXGFwcFxcY29tXFxhbm5hbmlrc1xcc2hlbW0tc2Nob29sXFx2aWV3c1xcbWFpblxcZml4ZWQtYXNzZXRzXFxwYWdlc1xcYnktdGF4LWxhd1xcYnktdGF4LWxhdy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvYnktdGF4LWxhdy9ieS10YXgtbGF3LnZpZXcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy9ieS10YXgtbGF3L2J5LXRheC1sYXcudmlldy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsIHRoLCB0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6MTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbn0iLCJ0YWJsZSwgdGgsIHRkIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.ts": 
        /*!*******************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.ts ***!
          \*******************************************************************************************************/
        /*! exports provided: ByTaxLawView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ByTaxLawView", function () { return ByTaxLawView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/modals/index.ts");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var ByTaxLawView = /** @class */ (function () {
                function ByTaxLawView(_urls, _matDialog, _title, _mainService, _router, _activatedRoute, _loadingService, _appService) {
                    this._urls = _urls;
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this._mainService = _mainService;
                    this._router = _router;
                    this._activatedRoute = _activatedRoute;
                    this._loadingService = _loadingService;
                    this._appService = _appService;
                    this._modalTitle = "ՀՄ խումբ շահութահարկի օրենքով";
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this._paginatorLastPageNumber = 0;
                    this.addByTaxLawData = [];
                    this.titles = [
                        { title: 'Կոդ', isSort: false, arrow: '', min: false, max: false },
                        { title: 'Անվանում', isSort: false, arrow: '', min: false, max: false },
                        { title: 'Ամորտիզացիոն տարեկան տոկոսադրույք', isSort: false, arrow: '', min: false, max: false }
                    ];
                    this._title.setTitle(this._modalTitle);
                }
                ByTaxLawView.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                ByTaxLawView.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["forkJoin"])(this._getCount(), this._getByTaxLaw(limit, offset));
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); }, function () { _this._loadingService.hideLoading(); });
                };
                ByTaxLawView.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryparams) {
                        if (queryparams && queryparams.page) {
                            _this._page = +queryparams.page;
                        }
                        _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                    });
                };
                ByTaxLawView.prototype._getCount = function () {
                    var _this = this;
                    return this._mainService.getCount(this._urls.hmxProfitTaxsMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                ByTaxLawView.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                ByTaxLawView.prototype._resetProperties = function () {
                    this._page = 1;
                };
                ByTaxLawView.prototype._getByTaxLaw = function (limit, offset) {
                    var _this = this;
                    return this._mainService.getByUrl(this._urls.hmxProfitTaxsMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])(function (data) {
                        _this.addByTaxLawData = data.data;
                        return data;
                    }));
                };
                ByTaxLawView.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                ByTaxLawView.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._mainService.deleteByUrl(this._urls.hmxProfitTaxGetOneUrl, id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.addByTaxLawData, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    }, function () {
                        _this._loadingService.hideLoading();
                    });
                };
                ByTaxLawView.prototype.addByTaxLaw = function (isNew, id, item) {
                    this.openModal(isNew, id, item);
                };
                ByTaxLawView.prototype.openModal = function (isNew, id, item) {
                    var _this = this;
                    var newTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_2__["AddByTaxLawModal"], {
                        width: '800px',
                        maxHeight: '85vh',
                        data: { title: newTitle, url: this._urls.hmxProfitTaxGetOneUrl, id: id, item: item }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                Object.defineProperty(ByTaxLawView.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ByTaxLawView.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ByTaxLawView.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                ByTaxLawView.prototype.ngOnDestroy = function () {
                    this._loadingService.hideLoading();
                    this._subscription.unsubscribe();
                };
                return ByTaxLawView;
            }());
            ByTaxLawView.ctorParameters = function () { return [
                { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] },
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
                { type: _main_service__WEBPACK_IMPORTED_MODULE_5__["MainService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["LoadingService"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["AppService"] }
            ]; };
            ByTaxLawView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-by-tax-law',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./by-tax-law.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./by-tax-law.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/by-tax-law/by-tax-law.view.scss")).default]
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
            ], ByTaxLawView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-by-tax-law-by-tax-law-module-es2015.js.map
//# sourceMappingURL=pages-by-tax-law-by-tax-law-module-es5.js.map
//# sourceMappingURL=pages-by-tax-law-by-tax-law-module-es5.js.map