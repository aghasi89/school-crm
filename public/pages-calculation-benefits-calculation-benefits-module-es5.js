(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-calculation-benefits-calculation-benefits-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.html": 
        /*!*************************************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.html ***!
          \*************************************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"add(false)\">\r\n    <tr *ngFor=\"let item of calculationBenefits\">\r\n        <td></td>\r\n        <!-- <td class=\"edit\"> <i (click)=\"addMaterialValuesShift(true,item?.id)\" class=\" material-icons\"> edit </i> </td> -->\r\n        <td class=\"delete\"> <i (click)=\"delete(item?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{item?.documentNumber}}</td>\r\n        <td>{{item?.expenseAccountId}}</td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.module.ts": 
        /*!***********************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.module.ts ***!
          \***********************************************************************************************************************/
        /*! exports provided: CalculationBenefitsModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalculationBenefitsModule", function () { return CalculationBenefitsModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _calculation_benefits_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./calculation-benefits.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _calculation_benefits_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./calculation-benefits.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.routing.module.ts");
            var CalculationBenefitsModule = /** @class */ (function () {
                function CalculationBenefitsModule() {
                }
                return CalculationBenefitsModule;
            }());
            CalculationBenefitsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_calculation_benefits_view__WEBPACK_IMPORTED_MODULE_2__["CalculationBenefitsView"]],
                    imports: [src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _calculation_benefits_routing_module__WEBPACK_IMPORTED_MODULE_4__["CalculationBenefitsRoutingModule"]]
                })
            ], CalculationBenefitsModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.routing.module.ts": 
        /*!*******************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.routing.module.ts ***!
          \*******************************************************************************************************************************/
        /*! exports provided: CalculationBenefitsRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalculationBenefitsRoutingModule", function () { return CalculationBenefitsRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _calculation_benefits_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./calculation-benefits.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.ts");
            var calculationBenefitsRoutes = [{ path: '', component: _calculation_benefits_view__WEBPACK_IMPORTED_MODULE_3__["CalculationBenefitsView"] }];
            var CalculationBenefitsRoutingModule = /** @class */ (function () {
                function CalculationBenefitsRoutingModule() {
                }
                return CalculationBenefitsRoutingModule;
            }());
            CalculationBenefitsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(calculationBenefitsRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], CalculationBenefitsRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.scss": 
        /*!***********************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.scss ***!
          \***********************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy9jYWxjdWxhdGlvbi1iZW5lZml0cy9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXHNhbGFyeVxccGFnZXNcXGNhbGN1bGF0aW9uLWJlbmVmaXRzXFxjYWxjdWxhdGlvbi1iZW5lZml0cy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9zYWxhcnkvcGFnZXMvY2FsY3VsYXRpb24tYmVuZWZpdHMvY2FsY3VsYXRpb24tYmVuZWZpdHMudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL21haW4vc2FsYXJ5L3BhZ2VzL2NhbGN1bGF0aW9uLWJlbmVmaXRzL2NhbGN1bGF0aW9uLWJlbmVmaXRzLnZpZXcuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRkIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbn0iLCJ0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.ts": 
        /*!*********************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.ts ***!
          \*********************************************************************************************************************/
        /*! exports provided: CalculationBenefitsView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalculationBenefitsView", function () { return CalculationBenefitsView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/salary/modals/index.ts");
            var CalculationBenefitsView = /** @class */ (function () {
                function CalculationBenefitsView(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService, _urls) {
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this._loadingService = _loadingService;
                    this._activatedRoute = _activatedRoute;
                    this._router = _router;
                    this._mainService = _mainService;
                    this._appService = _appService;
                    this._urls = _urls;
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this._modalTitle = 'Նպաստների հաշվարկ';
                    this.calculationBenefits = [];
                    this._paginatorLastPageNumber = 0;
                    this.titles = [{ title: 'Փաստաթղթի N' }, { title: 'Ծախսի հաշիվ' }];
                    this._title.setTitle(this._modalTitle);
                }
                CalculationBenefitsView.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                CalculationBenefitsView.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["forkJoin"])(this._getcalculationBenefitsCount(), this._getcalculationBenefits(limit, offset));
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); });
                };
                CalculationBenefitsView.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryparams) {
                        if (queryparams && queryparams.page) {
                            _this._page = +queryparams.page;
                        }
                        // this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength)
                    });
                };
                CalculationBenefitsView.prototype._getcalculationBenefitsCount = function () {
                    var _this = this;
                    return this._mainService.getCount(this._urls.calculationBenefitsMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                CalculationBenefitsView.prototype._getcalculationBenefits = function (limit, offset) {
                    var _this = this;
                    return this._mainService.getByUrl(this._urls.calculationBenefitsMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (data) {
                        _this.calculationBenefits = data.data;
                        return data;
                    }));
                };
                CalculationBenefitsView.prototype.add = function (isNew, id) {
                    this._openModal(isNew, id);
                };
                CalculationBenefitsView.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                CalculationBenefitsView.prototype._resetProperties = function () {
                    this._page = 1;
                };
                CalculationBenefitsView.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                CalculationBenefitsView.prototype._openModal = function (isNew, id) {
                    var _this = this;
                    var isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_9__["CalculationBenefitsModal"], {
                        width: '80vw',
                        minHeight: '55vh',
                        maxHeight: '85vh',
                        autoFocus: false,
                        data: { title: isNewTitle, url: this._urls.calculationBenefitsGetOneUrl, id: id }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                CalculationBenefitsView.prototype.getBooleanVariable = function (variable) {
                    return this._appService.getBooleanVariable(variable);
                };
                CalculationBenefitsView.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._mainService.deleteByUrl(this._urls.calculationBenefitsGetOneUrl, id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.calculationBenefits, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    });
                };
                CalculationBenefitsView.prototype.ngOnDestroy = function () {
                    this._loadingService.hideLoading();
                    // this._subscription.unsubscribe()
                };
                Object.defineProperty(CalculationBenefitsView.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(CalculationBenefitsView.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(CalculationBenefitsView.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                return CalculationBenefitsView;
            }());
            CalculationBenefitsView.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
                { type: _main_service__WEBPACK_IMPORTED_MODULE_7__["MainService"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["AppService"] },
                { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
            ]; };
            CalculationBenefitsView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'calculation-benefits-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./calculation-benefits.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./calculation-benefits.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.view.scss")).default]
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
            ], CalculationBenefitsView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-calculation-benefits-calculation-benefits-module-es2015.js.map
//# sourceMappingURL=pages-calculation-benefits-calculation-benefits-module-es5.js.map
//# sourceMappingURL=pages-calculation-benefits-calculation-benefits-module-es5.js.map