(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-cash-registers-cash-registers-modul"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.html":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.html ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addCashRegister()\">\r\n    <tr *ngFor=\"let item of cashRegisters\">\r\n        <td class=\"edit\" (click)=\"addCashRegister(true, item?.id, item)\">\r\n            <i class=\"material-icons\">edit</i>\r\n        </td>\r\n        <td class=\"delete\" (click)=delete(item.id)>\r\n            <i class=\"material-icons\">close</i>\r\n        </td>\r\n        <td>{{item?.code}}</td>\r\n        <td>{{item?.name}}</td>\r\n        <td>{{item?.accountId}}</td>\r\n        <td>\r\n            <label class=\"container-checkbox\">\r\n                <input [checked]=\"getBooleanVariable(item?.isMain)\" disabled type=\"checkbox\">\r\n                <span class=\"checkmark\"></span>\r\n            </label>\r\n        </td>\r\n        <td>{{item?.dmoN}}</td>\r\n        <td>{{item?.deoN}}</td>\r\n        <td>{{item?.hdmRegisN}}</td>\r\n        <td>{{item?.ip}}</td>\r\n        <td>{{item?.port}}</td>\r\n\r\n        <td>{{item?.password}}</td>\r\n        <td>{{item?.hdmTaxable}}</td>\r\n        <td>{{item?.hdmNonTaxable}}</td>\r\n        <td>{{item?.hdmPrintType}}</td>\r\n\r\n    </tr>\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers-routing.module.ts":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers-routing.module.ts ***!
  \****************************************************************************************************************************/
/*! exports provided: CashRegistrationRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CashRegistrationRoutingModule", function() { return CashRegistrationRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _cash_registers_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cash-registers.component */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.ts");




const cashRegistrationRoutes = [
    { path: '', component: _cash_registers_component__WEBPACK_IMPORTED_MODULE_3__["CashRegistersComponent"] }
];
let CashRegistrationRoutingModule = class CashRegistrationRoutingModule {
};
CashRegistrationRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(cashRegistrationRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], CashRegistrationRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.scss":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.scss ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9jYXNoLXJlZ2lzdGVycy9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXG1haW4tYWNjb3VudGluZ1xccGFnZXNcXGNhc2gtcmVnaXN0ZXJzXFxjYXNoLXJlZ2lzdGVycy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9jYXNoLXJlZ2lzdGVycy9jYXNoLXJlZ2lzdGVycy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9jYXNoLXJlZ2lzdGVycy9jYXNoLXJlZ2lzdGVycy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRkIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbn0iLCJ0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: CashRegistersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CashRegistersComponent", function() { return CashRegistersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");










let CashRegistersComponent = class CashRegistersComponent {
    constructor(_matDialog, _title, _activatedRoute, _router, _appService, _loadingService, _mainService) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._appService = _appService;
        this._loadingService = _loadingService;
        this._mainService = _mainService;
        this.titles = [
            { title: 'Կոդ', isSort: false, arrow: '', min: false, max: false },
            { title: 'Անվանում', isSort: false, arrow: '', min: false, max: false },
            { title: 'Հաշիվ', isSort: false, arrow: '', min: false, max: false },
            { title: 'Հիմնական', isSort: false, arrow: '', min: false, max: false },
            { title: 'ԴՄՕ-ի հերթական համար', isSort: false, arrow: '', min: false, max: false },
            { title: 'ԴԵՕ-ի հերթական համար', isSort: false, arrow: '', min: false, max: false },
            { title: 'ՀԴՄ գրանցում', isSort: false, arrow: '', min: false, max: false },
            { title: 'IP հասցե', isSort: false, arrow: '', min: false, max: false },
            { title: 'Պորտ', isSort: false, arrow: '', min: false, max: false },
            { title: 'Գաղտնաբառ', isSort: false, arrow: '', min: false, max: false },
            { title: 'ԱԱՀ-ով հարկվող ՀԴՄ բաժին', isSort: false, arrow: '', min: false, max: false },
            { title: 'ԱԱՀ-ով չհարկվող ՀԴՄ բաժին', isSort: false, arrow: '', min: false, max: false },
            { title: 'ՀԴՄ տպման ռեժիմ', isSort: false, arrow: '', min: false, max: false }
        ];
        this.cashRegisters = [];
        this._modalTitle = 'Դրամարկղ';
        this._otherUrl = 'cash-registers';
        this._cashRegisterUrl = 'cash-register';
        this._paginatorLastPageNumber = 0;
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryParams) => {
            if (queryParams && queryParams.page) {
                this._page = +queryParams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _resetProperties() {
        this._page = 1;
    }
    addCashRegister(isNew, id, item) {
        this.openModal(isNew, id, item);
    }
    openModal(isNew, id, item) {
        let isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_9__["AddCashRegisterModal"], {
            width: '50vw',
            maxHeight: '85vh',
            data: { title: isNewTitle, url: this._cashRegisterUrl, id: id, item: item }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["forkJoin"])(this._getCashRegistersCount(), this._getRegisters(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading(), () => this._loadingService.hideLoading());
    }
    _getCashRegistersCount() {
        return this._mainService.getCount(this._otherUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    _getRegisters(limit, offset) {
        return this._mainService.getByUrl(this._otherUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this.cashRegisters = data.data;
            return data;
        }));
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._cashRegisterUrl, id).subscribe(() => {
            let page = this._appService.setAfterDeletedPage(this.cashRegisters, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }, () => {
            this._loadingService.hideLoading();
        });
    }
    getBooleanVariable(variable) {
        return this._appService.getBooleanVariable(variable);
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
};
CashRegistersComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["AppService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["LoadingService"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_7__["MainService"] }
];
CashRegistersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cash-registers',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./cash-registers.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./cash-registers.component.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.scss")).default]
    })
], CashRegistersComponent);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.modul.ts":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.modul.ts ***!
  \*******************************************************************************************************************/
/*! exports provided: CashRegistersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CashRegistersModule", function() { return CashRegistersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _cash_registers_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cash-registers.component */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers.component.ts");
/* harmony import */ var _cash_registers_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cash-registers-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/cash-registers/cash-registers-routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");





let CashRegistersModule = class CashRegistersModule {
};
CashRegistersModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_cash_registers_component__WEBPACK_IMPORTED_MODULE_2__["CashRegistersComponent"]],
        imports: [src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], _cash_registers_routing_module__WEBPACK_IMPORTED_MODULE_3__["CashRegistrationRoutingModule"]],
        providers: []
    })
], CashRegistersModule);



/***/ })

}]);
//# sourceMappingURL=pages-cash-registers-cash-registers-modul-es2015.js.map