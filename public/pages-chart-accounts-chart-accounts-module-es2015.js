(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-chart-accounts-chart-accounts-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.html":
/*!**********************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.html ***!
  \**********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addChartAccount(false)\">\r\n    <tr *ngFor=\"let item of chartAccounts\">\r\n        <td class=\"edit\"> <i (click)=\"addChartAccount(true,item?.id)\" class=\" material-icons\"> edit </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(item?.id)\" class=\" material-icons\"> close </i></td>\r\n        <!-- <td>{{item?.code}}</td> -->\r\n        <td>{{item?.account}}</td>\r\n        <td>{{item?.name}}</td>\r\n        <td>{{item?.calculationsTypeId}}</td>\r\n        <td>{{item?.acumulatedAccountId}}</td>\r\n        <td>\r\n            <label class=\"container-checkbox\">\r\n                <input [checked]=\"getBooleanVariable(item?.offBalanceSheet)\" disabled type=\"checkbox\">\r\n                <span class=\"checkmark\"></span>\r\n            </label>\r\n        </td>\r\n        <td>\r\n            <label class=\"container-checkbox\">\r\n                <input [checked]=\"getBooleanVariable(item?.accountingByPartners)\" disabled type=\"checkbox\">\r\n                <span class=\"checkmark\"></span>\r\n            </label>\r\n        </td>\r\n        <td>\r\n            <label class=\"container-checkbox\">\r\n                <input [checked]=\"getBooleanVariable(item?.analyticalGroup1)\" disabled type=\"checkbox\">\r\n                <span class=\"checkmark\"></span>\r\n            </label>\r\n        </td>\r\n        <td>\r\n            <label class=\"container-checkbox\">\r\n                <input [checked]=\"getBooleanVariable(item?.analyticalGroup2)\" disabled type=\"checkbox\">\r\n                <span class=\"checkmark\"></span>\r\n            </label>\r\n        </td>\r\n        <td></td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.module.ts":
/*!********************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.module.ts ***!
  \********************************************************************************************************************/
/*! exports provided: ChartAccountsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartAccountsModule", function() { return ChartAccountsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _chart_accounts_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chart-accounts.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.ts");
/* harmony import */ var _chart_accounts_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chart-accounts.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _chart_accounts_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chart-accounts.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.service.ts");






let ChartAccountsModule = class ChartAccountsModule {
};
ChartAccountsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_chart_accounts_view__WEBPACK_IMPORTED_MODULE_2__["ChartAccountsView"]],
        imports: [_chart_accounts_routing_module__WEBPACK_IMPORTED_MODULE_3__["ChartAccountsRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
        providers: [_chart_accounts_service__WEBPACK_IMPORTED_MODULE_5__["ChartAccountsService"]]
    })
], ChartAccountsModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.routing.module.ts":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.routing.module.ts ***!
  \****************************************************************************************************************************/
/*! exports provided: ChartAccountsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartAccountsRoutingModule", function() { return ChartAccountsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _chart_accounts_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chart-accounts.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.ts");




let chartAccountsRoutes = [{ path: '', component: _chart_accounts_view__WEBPACK_IMPORTED_MODULE_3__["ChartAccountsView"] }];
let ChartAccountsRoutingModule = class ChartAccountsRoutingModule {
};
ChartAccountsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(chartAccountsRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], ChartAccountsRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.scss":
/*!********************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.scss ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9jaGFydC1hY2NvdW50cy9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXG1haW4tYWNjb3VudGluZ1xccGFnZXNcXGNoYXJ0LWFjY291bnRzXFxjaGFydC1hY2NvdW50cy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvY2hhcnQtYWNjb3VudHMvY2hhcnQtYWNjb3VudHMudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL21haW4vbWFpbi1hY2NvdW50aW5nL3BhZ2VzL2NoYXJ0LWFjY291bnRzL2NoYXJ0LWFjY291bnRzLnZpZXcuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLCB0aCwgdGQge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgcGFkZGluZzogNXB4O1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufSIsInRhYmxlLCB0aCwgdGQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.ts ***!
  \******************************************************************************************************************/
/*! exports provided: ChartAccountsView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartAccountsView", function() { return ChartAccountsView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _chart_accounts_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./chart-accounts.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.service.ts");










let ChartAccountsView = class ChartAccountsView {
    constructor(_matDialog, _title, _activatedRoute, _router, _chartAccountService, _appService, _loadingService, _urls) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._chartAccountService = _chartAccountService;
        this._appService = _appService;
        this._loadingService = _loadingService;
        this._urls = _urls;
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._modalTitle = 'Հաշվային պլան';
        this._paginatorLastPageNumber = 0;
        this.chartAccounts = [];
        this.titles = [
            { title: 'Հաշիվ' }, { title: 'Անվանում' },
            { title: 'Տեսակ' }, { title: 'Կուտակիչ' },
            { title: 'Արտահաշիվ' }, { title: 'Գործընկ․' },
            { title: 'Ան․խումբ 1' }, { title: 'Ան․խումբ 2' },
            { title: 'Նկարագրություն' }
        ];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    addChartAccount(isNew, id) {
        this._openModal(isNew, id);
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["forkJoin"])(this._getAccountsPlanCount(), this._getAccountsPlan(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading(), () => this._loadingService.hideLoading());
    }
    _getAccountsPlanCount() {
        return this._chartAccountService.getAccountsCount().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    _getAccountsPlan(limit, offset) {
        return this._chartAccountService.getAccounts(limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this.chartAccounts = data.data;
            return data;
        }));
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryParams) => {
            if (queryParams && queryParams.page) {
                this._page = +queryParams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    _openModal(isNew, id) {
        let isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_3__["AddAccountModal"], {
            width: '80vw',
            minHeight: '55vh',
            maxHeight: '85vh',
            data: { title: isNewTitle, url: this._urls.accountPlanGetOneUrl, id: id }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    getBooleanVariable(variable) {
        return this._appService.getBooleanVariable(variable);
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    delete(id) {
        this._loadingService.showLoading();
        this._chartAccountService.deleteAccount(id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.chartAccounts, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }
        // , () => { this._loadingService.hideLoading() }
        );
    }
    ngOnDestroy() {
        this._loadingService.hideLoading();
        this._subscription.unsubscribe();
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
};
ChartAccountsView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _chart_accounts_service__WEBPACK_IMPORTED_MODULE_9__["ChartAccountsService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["AppService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
ChartAccountsView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'chart-accounts-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chart-accounts.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chart-accounts.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/chart-accounts/chart-accounts.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], ChartAccountsView);



/***/ })

}]);
//# sourceMappingURL=pages-chart-accounts-chart-accounts-module-es2015.js.map