(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-currency-currency-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.html": 
        /*!**********************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.html ***!
          \**********************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addCurrency(false)\">\r\n    <tr *ngFor=\"let currency of currencies\">\r\n        <td class=\"edit\"> <i (click)=\"addCurrency(true,currency?.id)\" class=\" material-icons\"> edit </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(currency?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{currency.currency}}</td>\r\n        <td>{{currency.name}}</td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.module.ts": 
        /*!********************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.module.ts ***!
          \********************************************************************************************************/
        /*! exports provided: CurrencyModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrencyModule", function () { return CurrencyModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _currency_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./currency.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _currency_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./currency.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.routing.module.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
            /* harmony import */ var _currency_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./currency.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.service.ts");
            var CurrencyModule = /** @class */ (function () {
                function CurrencyModule() {
                }
                return CurrencyModule;
            }());
            CurrencyModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_currency_view__WEBPACK_IMPORTED_MODULE_2__["CurrencyView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddCurrencyModal"]],
                    imports: [src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _currency_routing_module__WEBPACK_IMPORTED_MODULE_4__["CurrencyRoutingModule"]],
                    entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddCurrencyModal"]],
                    providers: [_currency_service__WEBPACK_IMPORTED_MODULE_6__["CurrencyService"]]
                })
            ], CurrencyModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.routing.module.ts": 
        /*!****************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.routing.module.ts ***!
          \****************************************************************************************************************/
        /*! exports provided: CurrencyRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrencyRoutingModule", function () { return CurrencyRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _currency_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./currency.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.ts");
            var currencyRoutes = [{ path: '', component: _currency_view__WEBPACK_IMPORTED_MODULE_3__["CurrencyView"] }];
            var CurrencyRoutingModule = /** @class */ (function () {
                function CurrencyRoutingModule() {
                }
                return CurrencyRoutingModule;
            }());
            CurrencyRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(currencyRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], CurrencyRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.scss": 
        /*!********************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.scss ***!
          \********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9jdXJyZW5jeS9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXG1haW4tYWNjb3VudGluZ1xccGFnZXNcXGN1cnJlbmN5XFxjdXJyZW5jeS52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvY3VycmVuY3kvY3VycmVuY3kudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ0FSIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9jdXJyZW5jeS9jdXJyZW5jeS52aWV3LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuICAgIHRhYmxlLCB0aCwgdGQge1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgICBmb250LXNpemU6MTJweFxyXG4gICAgfSIsInRhYmxlLCB0aCwgdGQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.ts": 
        /*!******************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.ts ***!
          \******************************************************************************************************/
        /*! exports provided: CurrencyView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrencyView", function () { return CurrencyView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var CurrencyView = /** @class */ (function () {
                function CurrencyView(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService) {
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this._loadingService = _loadingService;
                    this._activatedRoute = _activatedRoute;
                    this._router = _router;
                    this._mainService = _mainService;
                    this._appService = _appService;
                    this.titles = [{ title: 'Կոդ' }, { title: 'Անվանում' }];
                    this._url = 'currencies';
                    this._otherUrl = 'currency';
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this._modalTitle = 'Արտայժույթներ';
                    this._paginatorLastPageNumber = 0;
                    this.currencies = [];
                    this._title.setTitle(this._modalTitle);
                }
                CurrencyView.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                CurrencyView.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["forkJoin"])(this._getCurrencyCount(), this._getCurrency(limit, offset));
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); });
                };
                CurrencyView.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryparams) {
                        if (queryparams && queryparams.page) {
                            _this._page = +queryparams.page;
                        }
                        _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                    });
                };
                CurrencyView.prototype._getCurrencyCount = function () {
                    var _this = this;
                    return this._mainService.getCount(this._url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                CurrencyView.prototype._getCurrency = function (limit, offset) {
                    var _this = this;
                    return this._mainService.getByUrl(this._url, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])(function (data) {
                        _this.currencies = data.data;
                        return data;
                    }));
                };
                CurrencyView.prototype.addCurrency = function (isNew, id) {
                    this._openModal(isNew, id);
                };
                CurrencyView.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                CurrencyView.prototype._resetProperties = function () {
                    this._page = 1;
                };
                CurrencyView.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                CurrencyView.prototype.getBooleanVariable = function (variable) {
                    return this._appService.getBooleanVariable(variable);
                };
                CurrencyView.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._mainService.deleteByUrl(this._otherUrl, id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.currencies, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    });
                };
                CurrencyView.prototype._openModal = function (isNew, id) {
                    var _this = this;
                    var isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_3__["AddCurrencyModal"], {
                        width: '500px',
                        data: { title: isNewTitle, url: this._otherUrl, id: id }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                CurrencyView.prototype.ngOnDestroy = function () {
                    this._subscription.unsubscribe();
                };
                Object.defineProperty(CurrencyView.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(CurrencyView.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(CurrencyView.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                return CurrencyView;
            }());
            CurrencyView.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["LoadingService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
                { type: _main_service__WEBPACK_IMPORTED_MODULE_7__["MainService"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["AppService"] }
            ]; };
            CurrencyView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'currency-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./currency.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./currency.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/currency/currency.view.scss")).default]
                })
            ], CurrencyView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-currency-currency-module-es2015.js.map
//# sourceMappingURL=pages-currency-currency-module-es5.js.map
//# sourceMappingURL=pages-currency-currency-module-es5.js.map