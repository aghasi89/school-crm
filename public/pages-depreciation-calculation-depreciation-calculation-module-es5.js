(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-depreciation-calculation-depreciation-calculation-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.html": 
        /*!***************************************************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.html ***!
          \***************************************************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addWearCalcuation(false)\">\r\n    <tr *ngFor=\"let item of depreciationCalculations\">\r\n        <td></td>\r\n        <!-- <td class=\"edit\"> <i (click)=\"addWearCalcuation(true,item?.id)\" class=\" material-icons\"> edit </i> </td> -->\r\n        <td class=\"delete\"> <i (click)=\"delete(item.id)\" class=\" material-icons\"> close </i></td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.module.ts": 
        /*!*************************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.module.ts ***!
          \*************************************************************************************************************************************/
        /*! exports provided: DepreciationCalculationModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepreciationCalculationModule", function () { return DepreciationCalculationModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _depreciation_calculation_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./depreciation-calculation.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _depreciation_calculation_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./depreciation-calculation.routing */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.routing.ts");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            var DepreciationCalculationModule = /** @class */ (function () {
                function DepreciationCalculationModule() {
                }
                return DepreciationCalculationModule;
            }());
            DepreciationCalculationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _depreciation_calculation_routing__WEBPACK_IMPORTED_MODULE_4__["DepreciationCalculationRoutes"]],
                    declarations: [_depreciation_calculation_view__WEBPACK_IMPORTED_MODULE_2__["DepreciationCalculationView"]],
                    providers: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]],
                    entryComponents: []
                })
            ], DepreciationCalculationModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.routing.ts": 
        /*!**************************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.routing.ts ***!
          \**************************************************************************************************************************************/
        /*! exports provided: DepreciationCalculationRoutes */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepreciationCalculationRoutes", function () { return DepreciationCalculationRoutes; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _depreciation_calculation_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./depreciation-calculation.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.ts");
            var routes = [
                { path: '', component: _depreciation_calculation_view__WEBPACK_IMPORTED_MODULE_2__["DepreciationCalculationView"] },
            ];
            var DepreciationCalculationRoutes = _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.scss": 
        /*!*************************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.scss ***!
          \*************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvZGVwcmVjaWF0aW9uLWNhbGN1bGF0aW9uL2RlcHJlY2lhdGlvbi1jYWxjdWxhdGlvbi52aWV3LnNjc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.ts": 
        /*!***********************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.ts ***!
          \***********************************************************************************************************************************/
        /*! exports provided: DepreciationCalculationView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DepreciationCalculationView", function () { return DepreciationCalculationView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/modals/index.ts");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            /* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var DepreciationCalculationView = /** @class */ (function () {
                function DepreciationCalculationView(_matDialog, _title, _mainService, _router, _activatedRoute, _loadingService, _appService, _datePipe, _urls) {
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this._mainService = _mainService;
                    this._router = _router;
                    this._activatedRoute = _activatedRoute;
                    this._loadingService = _loadingService;
                    this._appService = _appService;
                    this._datePipe = _datePipe;
                    this._urls = _urls;
                    this.depreciationCalculations = [];
                    this._modalTitle = 'Մաշվածքի հաշիվ';
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this._paginatorLastPageNumber = 0;
                    this.titles = [
                        { title: 'Փաստաթղթի համար' }
                    ];
                    this._title.setTitle(this._modalTitle);
                }
                DepreciationCalculationView.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                DepreciationCalculationView.prototype.getDate = function (date) {
                    return date ? this._datePipe.transform(new Date(date), 'dd/MM/yyyy') : null;
                };
                DepreciationCalculationView.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["forkJoin"])(this._getDepreciationCalculationCount(), this._getDepreciationCalculations(limit, offset)
                    // TODO: GET DEPRECIATION 
                    );
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); }, function () { _this._loadingService.hideLoading(); });
                };
                DepreciationCalculationView.prototype._getDepreciationCalculationCount = function () {
                    var _this = this;
                    return this._mainService.getCount(this._urls.depreciationCalculationMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                DepreciationCalculationView.prototype._getDepreciationCalculations = function (limit, offset) {
                    var _this = this;
                    return this._mainService.getByUrl(this._urls.depreciationCalculationMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["map"])(function (data) {
                        _this.depreciationCalculations = data.data;
                        return data;
                    }));
                };
                DepreciationCalculationView.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryparams) {
                        if (queryparams && queryparams.page) {
                            _this._page = +queryparams.page;
                        }
                        _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                    });
                };
                DepreciationCalculationView.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                DepreciationCalculationView.prototype._resetProperties = function () {
                    this._page = 1;
                };
                DepreciationCalculationView.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                DepreciationCalculationView.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._mainService.deleteByUrl(this._urls.depreciationCalculationGetOneUrl, id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.depreciationCalculations, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    });
                };
                DepreciationCalculationView.prototype.addWearCalcuation = function (isNew, id) {
                    this._openModal(isNew, id);
                };
                DepreciationCalculationView.prototype._openModal = function (isNew, id) {
                    var _this = this;
                    var newTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_7__["CalculationOfWearModal"], {
                        width: '80vw',
                        minHeight: '55vh',
                        maxHeight: '85vh',
                        autoFocus: false,
                        data: { title: newTitle, url: this._urls.depreciationCalculationGetOneUrl, id: id }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                DepreciationCalculationView.prototype.ngOnDestroy = function () {
                    this._loadingService.hideLoading();
                    this._subscription.unsubscribe();
                };
                Object.defineProperty(DepreciationCalculationView.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(DepreciationCalculationView.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(DepreciationCalculationView.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                return DepreciationCalculationView;
            }());
            DepreciationCalculationView.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
                { type: _main_service__WEBPACK_IMPORTED_MODULE_4__["MainService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
                { type: _services__WEBPACK_IMPORTED_MODULE_9__["LoadingService"] },
                { type: _services__WEBPACK_IMPORTED_MODULE_9__["AppService"] },
                { type: _angular_common__WEBPACK_IMPORTED_MODULE_8__["DatePipe"] },
                { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
            ]; };
            DepreciationCalculationView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-depreciation-calculation',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./depreciation-calculation.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./depreciation-calculation.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/depreciation-calculation/depreciation-calculation.view.scss")).default]
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](8, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
            ], DepreciationCalculationView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-depreciation-calculation-depreciation-calculation-module-es2015.js.map
//# sourceMappingURL=pages-depreciation-calculation-depreciation-calculation-module-es5.js.map
//# sourceMappingURL=pages-depreciation-calculation-depreciation-calculation-module-es5.js.map