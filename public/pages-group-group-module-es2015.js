(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-group-group-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.html":
/*!***************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.html ***!
  \***************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addMaterialvalueGroup(false)\">\r\n\r\n    <tr *ngFor=\"let materialvalueGroup of materialvalueGroups\">\r\n        <td class=\"edit\"> <i (click)=\"addMaterialvalueGroup(true, materialvalueGroup?.id, materialvalueGroup)\" class=\" material-icons\"> edit\r\n            </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(materialvalueGroup?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{ materialvalueGroup?.code }}</td>\r\n        <td>{{ materialvalueGroup?.name }}</td>\r\n        <td>{{ materialvalueGroup?.materialValueGroupId }}</td>\r\n    </tr>\r\n\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group-routing.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group-routing.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: GroupRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupRoutingModule", function() { return GroupRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _group_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./group.component */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.ts");




const groupRoutes = [
    { path: '', component: _group_component__WEBPACK_IMPORTED_MODULE_3__["GroupComponent"] }
];
let GroupRoutingModule = class GroupRoutingModule {
};
GroupRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(groupRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], GroupRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.scss ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy9ncm91cC9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXHdhcmVob3VzZVxccGFnZXNcXGdyb3VwXFxncm91cC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy9ncm91cC9ncm91cC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy9ncm91cC9ncm91cC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLCB0aCwgdGQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxufSIsInRhYmxlLCB0aCwgdGQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: GroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupComponent", function() { return GroupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");










let GroupComponent = class GroupComponent {
    constructor(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._loadingService = _loadingService;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._mainService = _mainService;
        this._appService = _appService;
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._modalTitle = 'Խումբ';
        this._materialvalueGroupMainUrl = 'material-value-groups';
        this._materialvalueGroupOtherUrl = 'material-value-group';
        this.materialvalueGroups = [];
        this._paginatorLastPageNumber = 0;
        this.titles = [
            { title: 'Կոդ', isSort: false, arrow: '', min: false, max: false },
            { title: 'Անվանում', isSort: false, arrow: '', min: false, max: false },
            { title: 'Խումբ', isSort: false, arrow: '', min: false, max: false },
        ];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["forkJoin"])(this._getMaterialvalueGroupCount(), this._getMaterialvalueGroups(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading(), () => this._loadingService.hideLoading());
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryparams) => {
            if (queryparams && queryparams.page) {
                this._page = +queryparams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _getMaterialvalueGroupCount() {
        return this._mainService.getCount(this._materialvalueGroupMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    addMaterialvalueGroup(isNew, id, item) {
        this.openModal(isNew, id, item);
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    _getMaterialvalueGroups(limit, offset) {
        return this._mainService.getByUrl(this._materialvalueGroupMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this.materialvalueGroups = data.data;
            return data;
        }));
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    openModal(isNew, id, item) {
        let isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_9__["MaterialValueGroupModal"], {
            width: '700px',
            maxHeight: '85vh',
            data: { title: isNewTitle, url: this._materialvalueGroupOtherUrl, id: id, item, array: this.materialvalueGroups }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    getBooleanVariable(variable) {
        return this._appService.getBooleanVariable(variable);
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._materialvalueGroupOtherUrl, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.materialvalueGroups, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }, () => {
            this._loadingService.hideLoading();
        });
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
};
GroupComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_4__["LoadingService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_7__["MainService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_4__["AppService"] }
];
GroupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-group',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./group.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./group.component.scss */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.scss")).default]
    })
], GroupComponent);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.module.ts ***!
  \********************************************************************************************/
/*! exports provided: GroupModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupModule", function() { return GroupModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _group_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./group-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group-routing.module.ts");
/* harmony import */ var _group_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./group.component */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.component.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");





let GroupModule = class GroupModule {
};
GroupModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_group_component__WEBPACK_IMPORTED_MODULE_3__["GroupComponent"]],
        imports: [_group_routing_module__WEBPACK_IMPORTED_MODULE_2__["GroupRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
    })
], GroupModule);



/***/ })

}]);
//# sourceMappingURL=pages-group-group-module-es2015.js.map