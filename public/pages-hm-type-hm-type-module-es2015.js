(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-hm-type-hm-type-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.html":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.html ***!
  \*****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addHmType()\">\r\n    <tr *ngFor=\"let item of hmTypes\">\r\n        <td class=\"edit\"> <i (click)=\"addHmType(true ,item?.id, item)\" class=\" material-icons\"> edit </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(item?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{item?.code}}</td>\r\n        <td>{{item?.name}}</td>\r\n        <td>{{item?.initialAccountId  }}</td>\r\n        <td>{{item?.staleAccountId}}</td>\r\n        <td>{{item?.usefullServiceDuration}}</td>\r\n        <td>{{item?.hmxProfitTaxId}}</td>\r\n        <td>{{item?.parentId  }}</td>\r\n      </tr>\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n  (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type-routing.module.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type-routing.module.ts ***!
  \***********************************************************************************************************/
/*! exports provided: HmTypeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HmTypeRoutingModule", function() { return HmTypeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _hm_type_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./hm-type.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.ts");



const routes = [
    { path: '', component: _hm_type_view__WEBPACK_IMPORTED_MODULE_2__["HmTypeView"] },
];
const HmTypeRoutingModule = _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes);


/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.module.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.module.ts ***!
  \***************************************************************************************************/
/*! exports provided: HmTypeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HmTypeModule", function() { return HmTypeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _hm_type_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./hm-type.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _hm_type_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./hm-type-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type-routing.module.ts");





let HmTypeModule = class HmTypeModule {
};
HmTypeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _hm_type_routing_module__WEBPACK_IMPORTED_MODULE_4__["HmTypeRoutingModule"]],
        declarations: [_hm_type_view__WEBPACK_IMPORTED_MODULE_2__["HmTypeView"]]
    })
], HmTypeModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.scss ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy9obS10eXBlL0M6XFxVc2Vyc1xcQW5uYW5pa3NcXERlc2t0b3BcXHNoZW1tLXNjaG9vbC9zcmNcXGFwcFxcY29tXFxhbm5hbmlrc1xcc2hlbW0tc2Nob29sXFx2aWV3c1xcbWFpblxcZml4ZWQtYXNzZXRzXFxwYWdlc1xcaG0tdHlwZVxcaG0tdHlwZS52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvaG0tdHlwZS9obS10eXBlLnZpZXcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy9obS10eXBlL2htLXR5cGUudmlldy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsIHRoLCB0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6MTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbn0iLCJ0YWJsZSwgdGgsIHRkIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.ts ***!
  \*************************************************************************************************/
/*! exports provided: HmTypeView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HmTypeView", function() { return HmTypeView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _modals_add_hm_type_add_hm_type_modal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals/add-hm-type/add-hm-type.modal */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/modals/add-hm-type/add-hm-type.modal.ts");










let HmTypeView = class HmTypeView {
    constructor(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService, _urls) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._loadingService = _loadingService;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._mainService = _mainService;
        this._appService = _appService;
        this._urls = _urls;
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._modalTitle = 'ՀՄ տեսակ';
        this.hmTypes = [];
        this._paginatorLastPageNumber = 0;
        this.titles = [
            { title: 'Կոդ' },
            { title: 'Անվանում' },
            { title: 'Սկզբնական արժեքի հաշիվ' },
            { title: 'Մաշվածության հաշիվ' },
            { title: 'Օգտակար ծառայության ժամկետ (ամիս)' },
            { title: 'ՀՄ խումբ շահութահարկի օրենքով' },
            { title: 'Կուտակիչ' }
        ];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["forkJoin"])(this._getHmTypesCount(), this._getHmTypes(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading(), () => this._loadingService.hideLoading());
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryparams) => {
            if (queryparams && queryparams.page) {
                this._page = +queryparams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _getHmTypesCount() {
        return this._mainService.getCount(this._urls.hmTypesMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    _getHmTypes(limit, offset) {
        return this._mainService.getByUrl(this._urls.hmTypesMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this.hmTypes = data.data;
            return data;
        }));
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    addHmType(isNew, id, item) {
        this.openModal(isNew, id, item);
    }
    openModal(isNew, id, item) {
        let isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals_add_hm_type_add_hm_type_modal__WEBPACK_IMPORTED_MODULE_9__["AddHmTypeModal"], {
            width: '750px',
            maxHeight: '85vh',
            data: { title: isNewTitle, url: this._urls.hmTypeGetOneUrl, id: id, item }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._urls.hmTypeGetOneUrl, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.hmTypes, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }, () => {
            this._loadingService.hideLoading();
        });
    }
    ngOnDestroy() {
        this._loadingService.hideLoading();
        this._subscription.unsubscribe();
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
};
HmTypeView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["LoadingService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_7__["MainService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["AppService"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
HmTypeView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-hm-type',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./hm-type.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./hm-type.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hm-type/hm-type.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], HmTypeView);



/***/ })

}]);
//# sourceMappingURL=pages-hm-type-hm-type-module-es2015.js.map