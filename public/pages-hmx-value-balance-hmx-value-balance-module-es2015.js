(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-hmx-value-balance-hmx-value-balance-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.html":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.html ***!
  \*************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addHmxValueBalance()\">\r\n  <tr *ngFor=\"let item of addHmxValueBalanceData\">\r\n    <td class=\"edit\"> <i (click)=\"addHmxValueBalance(true ,item?.id, item)\" class=\" material-icons\"> edit </i> </td>\r\n    <td class=\"delete\"> <i (click)=\"delete(item?.id)\" class=\" material-icons\"> close </i></td>\r\n    <td>{{ item?.code }}</td>\r\n    <td>{{ item?.date }}</td>\r\n    <td>{{ item?.valueBeginningOfYear }}</td>\r\n  </tr>\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n  (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance-routing.module.ts":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance-routing.module.ts ***!
  \*******************************************************************************************************************************/
/*! exports provided: HmxValueBalanceRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HmxValueBalanceRoutingModule", function() { return HmxValueBalanceRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _hmx_value_balance_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./hmx-value-balance.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.ts");




const hmxValueBalanRoutes = [{ path: '', component: _hmx_value_balance_view__WEBPACK_IMPORTED_MODULE_3__["HmxValueBalanceView"] }];
let HmxValueBalanceRoutingModule = class HmxValueBalanceRoutingModule {
};
HmxValueBalanceRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(hmxValueBalanRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HmxValueBalanceRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.module.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.module.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: HmxValueBalanceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HmxValueBalanceModule", function() { return HmxValueBalanceModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _hmx_value_balance_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./hmx-value-balance.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _hmx_value_balance_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./hmx-value-balance-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance-routing.module.ts");





let HmxValueBalanceModule = class HmxValueBalanceModule {
};
HmxValueBalanceModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _hmx_value_balance_routing_module__WEBPACK_IMPORTED_MODULE_4__["HmxValueBalanceRoutingModule"]],
        declarations: [_hmx_value_balance_view__WEBPACK_IMPORTED_MODULE_2__["HmxValueBalanceView"]]
    })
], HmxValueBalanceModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.scss":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.scss ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy9obXgtdmFsdWUtYmFsYW5jZS9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXGZpeGVkLWFzc2V0c1xccGFnZXNcXGhteC12YWx1ZS1iYWxhbmNlXFxobXgtdmFsdWUtYmFsYW5jZS52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvaG14LXZhbHVlLWJhbGFuY2UvaG14LXZhbHVlLWJhbGFuY2Uudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL21haW4vZml4ZWQtYXNzZXRzL3BhZ2VzL2hteC12YWx1ZS1iYWxhbmNlL2hteC12YWx1ZS1iYWxhbmNlLnZpZXcuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLCB0aCwgdGQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOjEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59IiwidGFibGUsIHRoLCB0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.ts":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.ts ***!
  \*********************************************************************************************************************/
/*! exports provided: HmxValueBalanceView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HmxValueBalanceView", function() { return HmxValueBalanceView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/modals/index.ts");










let HmxValueBalanceView = class HmxValueBalanceView {
    constructor(_urls, _matDialog, _title, _mainService, _router, _activatedRoute, _loadingService, _appService) {
        this._urls = _urls;
        this._matDialog = _matDialog;
        this._title = _title;
        this._mainService = _mainService;
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._loadingService = _loadingService;
        this._appService = _appService;
        this._modalTitle = "ՀՄ խմբի հաշվեկշռային արժեքներ";
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._paginatorLastPageNumber = 0;
        this.addHmxValueBalanceData = [];
        this.titles = [
            { title: 'Կոդ', isSort: false, arrow: '', min: false, max: false },
            { title: 'Տարի', isSort: false, arrow: '', min: false, max: false },
            { title: 'Հաշվեկշռային արժեք տարվա սկզբին', isSort: false, arrow: '', min: false, max: false }
        ];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["forkJoin"])(this._getCount(), this._getEmplyees(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading(), () => { this._loadingService.hideLoading(); });
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryparams) => {
            if (queryparams && queryparams.page) {
                this._page = +queryparams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _getCount() {
        return this._mainService.getCount(this._urls.hmxValueBalansMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    _getEmplyees(limit, offset) {
        return this._mainService.getByUrl(this._urls.hmxValueBalansMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])((data) => {
            this.addHmxValueBalanceData = data.data;
            return data;
        }));
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._urls.hmxValueBalansGetOneUrl, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.addHmxValueBalanceData, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }, () => {
            this._loadingService.hideLoading();
        });
    }
    addHmxValueBalance(isNew, id, item) {
        this.openModal(isNew, id, item);
    }
    openModal(isNew, id, item) {
        let newTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_9__["AddHmxValueBalanceModal"], {
            width: '800px',
            maxHeight: '85vh',
            data: { title: newTitle, url: this._urls.hmxValueBalansGetOneUrl, id: id, item }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
    ngOnDestroy() {
        this._loadingService.hideLoading();
        this._subscription.unsubscribe();
    }
};
HmxValueBalanceView.ctorParameters = () => [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_5__["MainService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["LoadingService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["AppService"] }
];
HmxValueBalanceView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-hmx-value-balance',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./hmx-value-balance.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./hmx-value-balance.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/hmx-value-balance/hmx-value-balance.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], HmxValueBalanceView);



/***/ })

}]);
//# sourceMappingURL=pages-hmx-value-balance-hmx-value-balance-module-es2015.js.map