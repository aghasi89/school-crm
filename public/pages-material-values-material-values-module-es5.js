(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-material-values-material-values-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.html": 
        /*!******************************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.html ***!
          \******************************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addMaterialValues()\">\r\n    <tr  *ngFor=\"let item of materialValues\" >\r\n        <td class=\"edit\" (click)=\"addMaterialValues(true, item?.id, item)\">\r\n            <i class=\"material-icons\">edit</i>\r\n        </td>\r\n        <td class=\"delete\" (click)=delete(item.id)>\r\n            <i class=\"material-icons\">close</i>\r\n        </td>\r\n        <td>{{item?.name}}</td>                         <!--  anvanum -->\r\n        <td>{{item?.measurementUnit?.unit}}</td>            <!--  chapman miavor -->\r\n        <td>{{item?.materialValueGroup?.name}}</td>         <!--  xumb -->\r\n        <td>{{item?.classification?.code}}</td>             <!--  dasakargum -->\r\n        <td>{{item?.account?.name}}</td>                    <!--  hashiv -->\r\n        <td>{{item?.wholesalePrice}}</td>               <!--  mecacax gin -->\r\n        <td>{{item?.retailerPrice}}</td>                <!--  manracax gin -->\r\n        \r\n        <td>{{item?.salesRevenueAccountId}}</td>        <!--  vacharqic hasuyti hashiv -->\r\n        <td>{{item?.retailRevenueAccountId}}</td>       <!--  vacharqic vacharqic hasuyti hashiv -->\r\n \r\n        <td>{{item?.characteristic}}</td>               <!--  bnutagir -->\r\n        <td>{{item?.barCode}}</td>                      <!--  gcikavor kod -->\r\n        <td>{{item?.externalCode}}</td>                 <!--  artaqin kod -->\r\n        <td>{{item?.hcbCoefficient}}</td>               <!--  hbc gorcakic -->\r\n        <td>{{item?.billingMethodId}}</td>              <!--  hashvarman metod -->\r\n        <td><label class=\"container-checkbox\">          <!--  isAah -->\r\n            <input [checked]=\"getBooleanVariable(item?.isAah)\" disabled type=\"checkbox\">\r\n            <span class=\"checkmark\"></span>\r\n        </label></td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>\r\n\r\n\r\n");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.module.ts": 
        /*!****************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.module.ts ***!
          \****************************************************************************************************************/
        /*! exports provided: MaterialValuesModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialValuesModule", function () { return MaterialValuesModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _material_values_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./material-values.view */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.ts");
            /* harmony import */ var _material_values_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./material-values.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.routing.module.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");
            /* harmony import */ var _material_values_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./material-values.service */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.service.ts");
            /* harmony import */ var _main_accounting_pages_unit_of_measurement_unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../main-accounting/pages/unit-of-measurement/unit-of-measurement.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.service.ts");
            var MaterialValuesModule = /** @class */ (function () {
                function MaterialValuesModule() {
                }
                return MaterialValuesModule;
            }());
            MaterialValuesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_material_values_view__WEBPACK_IMPORTED_MODULE_2__["MaterialValuesView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddMaterialValueModal"]],
                    imports: [_material_values_routing_module__WEBPACK_IMPORTED_MODULE_3__["MaterialValuesRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
                    entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddMaterialValueModal"]],
                    providers: [_material_values_service__WEBPACK_IMPORTED_MODULE_6__["MaterialValuesService"], _main_accounting_pages_unit_of_measurement_unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_7__["UnitOfMeasurementService"]]
                })
            ], MaterialValuesModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.routing.module.ts": 
        /*!************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.routing.module.ts ***!
          \************************************************************************************************************************/
        /*! exports provided: MaterialValuesRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialValuesRoutingModule", function () { return MaterialValuesRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _material_values_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./material-values.view */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.ts");
            var materialValuesRoutes = [{ path: '', component: _material_values_view__WEBPACK_IMPORTED_MODULE_3__["MaterialValuesView"] }];
            var MaterialValuesRoutingModule = /** @class */ (function () {
                function MaterialValuesRoutingModule() {
                }
                return MaterialValuesRoutingModule;
            }());
            MaterialValuesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(materialValuesRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], MaterialValuesRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.scss": 
        /*!****************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.scss ***!
          \****************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy9tYXRlcmlhbC12YWx1ZXMvQzpcXFVzZXJzXFxBbm5hbmlrc1xcRGVza3RvcFxcc2hlbW0tc2Nob29sL3NyY1xcYXBwXFxjb21cXGFubmFuaWtzXFxzaGVtbS1zY2hvb2xcXHZpZXdzXFxtYWluXFx3YXJlaG91c2VcXHBhZ2VzXFxtYXRlcmlhbC12YWx1ZXNcXG1hdGVyaWFsLXZhbHVlcy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi93YXJlaG91c2UvcGFnZXMvbWF0ZXJpYWwtdmFsdWVzL21hdGVyaWFsLXZhbHVlcy52aWV3LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi93YXJlaG91c2UvcGFnZXMvbWF0ZXJpYWwtdmFsdWVzL21hdGVyaWFsLXZhbHVlcy52aWV3LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59IiwidGQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.ts": 
        /*!**************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.ts ***!
          \**************************************************************************************************************/
        /*! exports provided: MaterialValuesView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialValuesView", function () { return MaterialValuesView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var _material_values_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./material-values.service */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.service.ts");
            /* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
            var MaterialValuesView = /** @class */ (function () {
                function MaterialValuesView(_matDialog, _materialValuesService, _mainService, _router, _activatedRoute, _title, _loadingService, _appService) {
                    this._matDialog = _matDialog;
                    this._materialValuesService = _materialValuesService;
                    this._mainService = _mainService;
                    this._router = _router;
                    this._activatedRoute = _activatedRoute;
                    this._title = _title;
                    this._loadingService = _loadingService;
                    this._appService = _appService;
                    this.titles = [
                        {
                            title: 'Անվանում',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Չափման միավոր',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Խումբ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'ԱՏԳԱԱ դասակարգիչ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Հաշիվ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Մեծածախ գին դրամով',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Մանրածախ գին դրամով',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Վաճառքից հասույթի հաշիվ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Մանրածախ վաճառքից հասույթի հաշիվ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Բնութագիր',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Գծիկավոր կոդ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Արտաքին կոդ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'ՀԾԲ գործակից',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'Հաշվառման մեթոդ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        },
                        {
                            title: 'ԱԱՀ',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        }
                    ];
                    this.materialValues = [];
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this._paginatorLastPageNumber = 0;
                    this._materialValuesCount = 0;
                    this._materialValueUrl = 'material-values';
                    this._modalTitle = 'Նյութական արժեք';
                    this._title.setTitle(this._modalTitle);
                }
                MaterialValuesView.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                MaterialValuesView.prototype.addMaterialValues = function (isNew, id, item) {
                    this.openModal(isNew, id, item);
                };
                MaterialValuesView.prototype.openModal = function (isNew, id, item) {
                    var _this = this;
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_3__["ClassificationModal"], {
                        width: '80vw',
                        minHeight: '55vh',
                        maxHeight: '85vh',
                        data: {
                            url: 'material-value',
                            id: id,
                            item: item
                        }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page } });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                MaterialValuesView.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryParams) {
                        if (queryParams && queryParams.page) {
                            _this._page = +queryParams.page;
                        }
                        _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                    });
                };
                MaterialValuesView.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["forkJoin"])(this.getMaterialValuesCount(), this.getMaterialValues(limit, offset));
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); }, function () { return _this._loadingService.hideLoading(); });
                };
                MaterialValuesView.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._materialValuesService.deleteMaterialValue(id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.materialValues, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    }, function () {
                        _this._loadingService.hideLoading();
                    });
                };
                MaterialValuesView.prototype.getMaterialValuesCount = function () {
                    var _this = this;
                    return this._mainService.getCount(this._materialValueUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                MaterialValuesView.prototype.getMaterialValues = function (limit, offset) {
                    var _this = this;
                    return this._mainService.getByUrl(this._materialValueUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
                        _this.materialValues = data.data;
                        return data;
                    }));
                };
                MaterialValuesView.prototype.getBooleanVariable = function (variable) {
                    return this._appService.getBooleanVariable(variable);
                };
                MaterialValuesView.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                MaterialValuesView.prototype._resetProperties = function () {
                    this._page = 1;
                };
                MaterialValuesView.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                MaterialValuesView.prototype.ngOnDestroy = function () {
                    this._subscription.unsubscribe();
                };
                Object.defineProperty(MaterialValuesView.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(MaterialValuesView.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(MaterialValuesView.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                return MaterialValuesView;
            }());
            MaterialValuesView.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
                { type: _material_values_service__WEBPACK_IMPORTED_MODULE_9__["MaterialValuesService"] },
                { type: _main_service__WEBPACK_IMPORTED_MODULE_10__["MainService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__["Title"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__["LoadingService"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__["AppService"] }
            ]; };
            MaterialValuesView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'material-values-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./material-values.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./material-values.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.view.scss")).default]
                })
            ], MaterialValuesView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-material-values-material-values-module-es2015.js.map
//# sourceMappingURL=pages-material-values-material-values-module-es5.js.map
//# sourceMappingURL=pages-material-values-material-values-module-es5.js.map