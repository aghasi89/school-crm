(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-partners-partners-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.html":
/*!**********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.html ***!
  \**********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addPartner(false)\">\r\n    <tr *ngFor=\"let partner of partners\">\r\n        <td class=\"edit\"> <i (click)=\"addPartner(true,partner?.id)\" class=\" material-icons\"> edit </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(partner?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{partner?.id}}</td>\r\n        <td>{{partner?.name}}</td>\r\n        <td>{{partner?.hvhh}}</td>\r\n        <td>{{partner?.email}}</td>\r\n        <td>{{isPhoneStartWithCode(partner?.phone)}}</td>\r\n        <td>{{partner?.contract}}</td>\r\n        <td>{{partner?.legalAddress}}</td>\r\n        <td>{{partner?.practicalAddress}}</td>\r\n\r\n    </tr>\r\n</app-tables-by-filter>\r\n<div class=\"bottom-content\">\r\n    <span class=\"web_name\">b2b.armsoft հարթակ</span>\r\n    <div class=\"more-info\">\r\n        <div class=\"circle\"></div>\r\n        <div>- Գործընկերը հրապարակել է ապրանքացանկ b2b.armsoft-ում</div>\r\n        <div class=\"info-text\">\r\n            <span> <i class=\"material-icons\">\r\n                    cloud_queue\r\n                </i>\r\n            </span>\r\n            <a>\r\n                Դիտել գործընկերոջ ապրանքացանկը\r\n            </a>\r\n        </div>\r\n        <div class=\"info-text\"> <span> <i class=\"material-icons\">\r\n                    cloud_queue\r\n                </i>\r\n            </span>\r\n            <a>\r\n                Ապրանքացանկ հրապարակած գործընկերների տեղեկատու\r\n            </a>\r\n        </div>\r\n        <div class=\"info-text\">\r\n            <span> <i class=\"material-icons\">\r\n                    cloud_queue\r\n                </i>\r\n            </span>\r\n            <a>\r\n                Դիտել հոլովակը\r\n            </a>\r\n        </div>\r\n    </div>\r\n</div>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.module.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.module.ts ***!
  \********************************************************************************************************/
/*! exports provided: PartnersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartnersModule", function() { return PartnersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _partners_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./partners.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.ts");
/* harmony import */ var _partners_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./partners.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _partners_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./partners.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");







let PartnersModule = class PartnersModule {
};
PartnersModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_partners_view__WEBPACK_IMPORTED_MODULE_2__["PartnersView"]],
        imports: [_partners_routing_module__WEBPACK_IMPORTED_MODULE_3__["PartnersRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
        entryComponents: [],
        providers: [_partners_service__WEBPACK_IMPORTED_MODULE_5__["PartnerService"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]]
    })
], PartnersModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.routing.module.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.routing.module.ts ***!
  \****************************************************************************************************************/
/*! exports provided: PartnersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartnersRoutingModule", function() { return PartnersRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _partners_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./partners.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.ts");




let partnersRoutes = [{ path: '', component: _partners_view__WEBPACK_IMPORTED_MODULE_3__["PartnersView"] }];
let PartnersRoutingModule = class PartnersRoutingModule {
};
PartnersRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(partnersRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PartnersRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.service.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.service.ts ***!
  \*********************************************************************************************************/
/*! exports provided: PartnerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartnerService", function() { return PartnerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");



let PartnerService = class PartnerService {
    constructor(_mainService) {
        this._mainService = _mainService;
    }
    addPartner(body) {
        return this._mainService.addByUrl('partner', body);
    }
    updatePartner(id, body) {
        return this._mainService.updateByUrl('partner', id, body);
    }
};
PartnerService.ctorParameters = () => [
    { type: _main_service__WEBPACK_IMPORTED_MODULE_2__["MainService"] }
];
PartnerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], PartnerService);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.scss ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n\n.bottom-content {\n  margin-top: 20px;\n  border: 1px solid grey;\n  padding: 10px;\n  font-size: 12px;\n}\n\n.bottom-content .web_name {\n  color: grey;\n}\n\n.bottom-content .more-info {\n  display: flex;\n  align-items: center;\n  flex-wrap: wrap;\n}\n\n.bottom-content .more-info .circle {\n  height: 10px;\n  width: 10px;\n  border-radius: 50%;\n  background: green;\n  margin-right: 7px;\n}\n\n.bottom-content .info-text {\n  margin-left: 7px;\n  display: flex;\n  align-items: center;\n}\n\n.bottom-content .info-text i {\n  color: red;\n}\n\n.bottom-content .info-text a {\n  margin-left: 3px;\n  cursor: pointer;\n  text-decoration: underline;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9wYXJ0bmVycy9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXG1haW4tYWNjb3VudGluZ1xccGFnZXNcXHBhcnRuZXJzXFxwYXJ0bmVycy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvcGFydG5lcnMvcGFydG5lcnMudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FEQ0E7RUFDSSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7QUNFSjs7QURESTtFQUNJLFdBQUE7QUNHUjs7QURESTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNHUjs7QURGUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FDSVo7O0FEREk7RUFDSSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQ0dSOztBREZRO0VBQ0ksVUFBQTtBQ0laOztBREZPO0VBQ0ssZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7QUNJWiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvcGFydG5lcnMvcGFydG5lcnMudmlldy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsIHRoLCB0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6MTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfVxyXG4uYm90dG9tLWNvbnRlbnR7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgZ3JleTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAud2ViX25hbWV7XHJcbiAgICAgICAgY29sb3I6IGdyZXk7XHJcbiAgICB9XHJcbiAgICAubW9yZS1pbmZve1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAgICAgLmNpcmNsZXtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICB3aWR0aDogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBncmVlbjtcclxuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA3cHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmluZm8tdGV4dHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogN3B4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBpe1xyXG4gICAgICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgIH1cclxuICAgICAgIGF7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAzcHg7XHJcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcbiAgICAgICAgfVxyXG4gICAgIH1cclxufVxyXG4iLCJ0YWJsZSwgdGgsIHRkIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5ib3R0b20tY29udGVudCB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5ib3R0b20tY29udGVudCAud2ViX25hbWUge1xuICBjb2xvcjogZ3JleTtcbn1cbi5ib3R0b20tY29udGVudCAubW9yZS1pbmZvIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC13cmFwOiB3cmFwO1xufVxuLmJvdHRvbS1jb250ZW50IC5tb3JlLWluZm8gLmNpcmNsZSB7XG4gIGhlaWdodDogMTBweDtcbiAgd2lkdGg6IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZDogZ3JlZW47XG4gIG1hcmdpbi1yaWdodDogN3B4O1xufVxuLmJvdHRvbS1jb250ZW50IC5pbmZvLXRleHQge1xuICBtYXJnaW4tbGVmdDogN3B4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmJvdHRvbS1jb250ZW50IC5pbmZvLXRleHQgaSB7XG4gIGNvbG9yOiByZWQ7XG59XG4uYm90dG9tLWNvbnRlbnQgLmluZm8tdGV4dCBhIHtcbiAgbWFyZ2luLWxlZnQ6IDNweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.ts ***!
  \******************************************************************************************************/
/*! exports provided: PartnersView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartnersView", function() { return PartnersView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");










let PartnersView = class PartnersView {
    constructor(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService, _urls) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._loadingService = _loadingService;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._mainService = _mainService;
        this._appService = _appService;
        this._urls = _urls;
        this.titles = [
            { title: 'Կոդ' }, { title: 'Անվանում' }, { title: 'ՀՎՀՀ' }, { title: 'Էլ․ փոստ' }, { title: 'Հեռախոսահամար' },
            { title: 'Պայմանագիր' }, { title: 'Իրավ․ հասցե' }, { title: 'Գործ․ հասցե' }
        ];
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._modalTitle = 'Գործընկերներ';
        this._paginatorLastPageNumber = 0;
        this.partners = [];
        this._url = this._urls.partnerMainUrl;
        this._otherUrl = this._urls.partnerGetOneUrl;
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    isPhoneStartWithCode(phone) {
        return phone.startsWith('+374') ? phone : '+374' + phone;
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["forkJoin"])(this._getPartnerCount(), this._getPartners(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading());
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryparams) => {
            if (queryparams && queryparams.page) {
                this._page = +queryparams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _getPartnerCount() {
        return this._mainService.getCount(this._url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    _getPartners(limit, offset) {
        return this._mainService.getByUrl(this._url, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((data) => {
            this.partners = data.data;
            return data;
        }));
    }
    addAddition(isNew, id) {
        this._openModal(isNew, id);
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    _openModal(isNew, id) {
        let isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_3__["AddPartnerModal"], {
            width: '80vw',
            minHeight: '55vh',
            maxHeight: '85vh',
            data: { title: isNewTitle, url: this._otherUrl, id: id }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    getBooleanVariable(variable) {
        return this._appService.getBooleanVariable(variable);
    }
    addPartner(isNew, id) {
        this._openModal(isNew, id);
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._otherUrl, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.partners, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        });
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
    ngOnDestroy() {
        this._loadingService.hideLoading();
        this._subscription.unsubscribe();
    }
};
PartnersView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__["LoadingService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_9__["MainService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__["AppService"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
PartnersView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'partners-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./partners.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./partners.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/partners/partners.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], PartnersView);



/***/ })

}]);
//# sourceMappingURL=pages-partners-partners-module-es2015.js.map