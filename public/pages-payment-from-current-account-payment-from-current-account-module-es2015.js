(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-payment-from-current-account-payment-from-current-account-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.html":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.html ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"add(false)\">\r\n    <tr *ngFor=\"let item of paymentFromCurrentAccount\">\r\n        <td></td>\r\n        <!-- <td class=\"edit\"> <i (click)=\"addMaterialValuesShift(true,item?.id)\" class=\" material-icons\"> edit </i> </td> -->\r\n        <td class=\"delete\"> <i (click)=\"delete(item?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{item?.documentNumber}}</td>\r\n        <td>{{item?.expenseAccountId}}</td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.module.ts":
/*!***************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.module.ts ***!
  \***************************************************************************************************************************************/
/*! exports provided: PaymentFromCurrentAccountModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentFromCurrentAccountModule", function() { return PaymentFromCurrentAccountModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _payment_from_current_account_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./payment-from-current-account.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.ts");
/* harmony import */ var _payment_from_current_account_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./payment-from-current-account.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");





let PaymentFromCurrentAccountModule = class PaymentFromCurrentAccountModule {
};
PaymentFromCurrentAccountModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_payment_from_current_account_view__WEBPACK_IMPORTED_MODULE_2__["PaymentFromCurrentAccountView"]],
        imports: [_payment_from_current_account_routing_module__WEBPACK_IMPORTED_MODULE_3__["PaymentFromCurrentAccountRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
    })
], PaymentFromCurrentAccountModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.routing.module.ts":
/*!***********************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.routing.module.ts ***!
  \***********************************************************************************************************************************************/
/*! exports provided: PaymentFromCurrentAccountRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentFromCurrentAccountRoutingModule", function() { return PaymentFromCurrentAccountRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _payment_from_current_account_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./payment-from-current-account.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.ts");




let paymentFromCurrentAccountRoutes = [{ path: '', component: _payment_from_current_account_view__WEBPACK_IMPORTED_MODULE_3__["PaymentFromCurrentAccountView"] }];
let PaymentFromCurrentAccountRoutingModule = class PaymentFromCurrentAccountRoutingModule {
};
PaymentFromCurrentAccountRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(paymentFromCurrentAccountRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PaymentFromCurrentAccountRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.scss":
/*!***************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.scss ***!
  \***************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy9wYXltZW50LWZyb20tY3VycmVudC1hY2NvdW50L0M6XFxVc2Vyc1xcQW5uYW5pa3NcXERlc2t0b3BcXHNoZW1tLXNjaG9vbC9zcmNcXGFwcFxcY29tXFxhbm5hbmlrc1xcc2hlbW0tc2Nob29sXFx2aWV3c1xcbWFpblxcc2FsYXJ5XFxwYWdlc1xccGF5bWVudC1mcm9tLWN1cnJlbnQtYWNjb3VudFxccGF5bWVudC1mcm9tLWN1cnJlbnQtYWNjb3VudC52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9zYWxhcnkvcGFnZXMvcGF5bWVudC1mcm9tLWN1cnJlbnQtYWNjb3VudC9wYXltZW50LWZyb20tY3VycmVudC1hY2NvdW50LnZpZXcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy9wYXltZW50LWZyb20tY3VycmVudC1hY2NvdW50L3BheW1lbnQtZnJvbS1jdXJyZW50LWFjY291bnQudmlldy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxufSIsInRkIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.ts":
/*!*************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.ts ***!
  \*************************************************************************************************************************************/
/*! exports provided: PaymentFromCurrentAccountView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentFromCurrentAccountView", function() { return PaymentFromCurrentAccountView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/salary/modals/index.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");










let PaymentFromCurrentAccountView = class PaymentFromCurrentAccountView {
    constructor(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService, _urls) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._loadingService = _loadingService;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._mainService = _mainService;
        this._appService = _appService;
        this._urls = _urls;
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this._modalTitle = 'Վճարումներ հաշվարկային հաշվից';
        this.paymentFromCurrentAccount = [];
        this._paginatorLastPageNumber = 0;
        this.titles = [{ title: 'Փաստաթղթի N' }, { title: 'Ծախսի հաշիվ' }];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["forkJoin"])(this._getPaymentFromCurrentAccountCount(), this._getPaymentFromCurrentAccount(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading());
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryparams) => {
            if (queryparams && queryparams.page) {
                this._page = +queryparams.page;
            }
            // this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength)
        });
    }
    _getPaymentFromCurrentAccountCount() {
        return this._mainService.getCount(this._urls.materialValuesShiftMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    _getPaymentFromCurrentAccount(limit, offset) {
        return this._mainService.getByUrl(this._urls.materialValuesShiftMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((data) => {
            this.paymentFromCurrentAccount = data.data;
            return data;
        }));
    }
    add(isNew, id) {
        this._openModal(isNew, id);
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _resetProperties() {
        this._page = 1;
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    _openModal(isNew, id) {
        let isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_2__["PaymentFromCurrencyAccountModal"], {
            width: '80vw',
            minHeight: '55vh',
            maxHeight: '85vh',
            autoFocus: false,
            data: { title: isNewTitle, url: this._urls.materialValuesShiftGetOneUrl, id: id }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    getBooleanVariable(variable) {
        return this._appService.getBooleanVariable(variable);
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._urls.materialValuesShiftGetOneUrl, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.paymentFromCurrentAccount, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        });
    }
    ngOnDestroy() {
        this._loadingService.hideLoading();
        // this._subscription.unsubscribe()
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
};
PaymentFromCurrentAccountView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["LoadingService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_9__["MainService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["AppService"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
PaymentFromCurrentAccountView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'payment-from-current-account-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./payment-from-current-account.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./payment-from-current-account.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], PaymentFromCurrentAccountView);



/***/ })

}]);
//# sourceMappingURL=pages-payment-from-current-account-payment-from-current-account-module-es2015.js.map