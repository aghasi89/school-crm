(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-position-position-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.html":
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.html ***!
  \*************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-name-code-table [url]=\"getOneUrl\" [mainUrl]=\"mainUrl\"  [title]=\"'Պաշտոն'\">\r\n</app-name-code-table>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.module.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: PositionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PositionModule", function() { return PositionModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _position_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./position.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.ts");
/* harmony import */ var _position_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./position.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");





let PositionModule = class PositionModule {
};
PositionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_position_view__WEBPACK_IMPORTED_MODULE_2__["PositionView"]],
        entryComponents: [],
        imports: [_position_routing_module__WEBPACK_IMPORTED_MODULE_3__["PositionRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
    })
], PositionModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.routing.module.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.routing.module.ts ***!
  \*******************************************************************************************************/
/*! exports provided: PositionRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PositionRoutingModule", function() { return PositionRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _position_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./position.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.ts");




let postionRoutes = [{ path: '', component: _position_view__WEBPACK_IMPORTED_MODULE_3__["PositionView"] }];
let PositionRoutingModule = class PositionRoutingModule {
};
PositionRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(postionRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PositionRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.scss ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy9wb3NpdGlvbi9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXHNhbGFyeVxccGFnZXNcXHBvc2l0aW9uXFxwb3NpdGlvbi52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9zYWxhcnkvcGFnZXMvcG9zaXRpb24vcG9zaXRpb24udmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL21haW4vc2FsYXJ5L3BhZ2VzL3Bvc2l0aW9uL3Bvc2l0aW9uLnZpZXcuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLCB0aCwgdGQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOjEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIH0iLCJ0YWJsZSwgdGgsIHRkIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.ts ***!
  \*********************************************************************************************/
/*! exports provided: PositionView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PositionView", function() { return PositionView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PositionView = class PositionView {
    constructor(_urls) {
        this._urls = _urls;
    }
    ngOnInit() {
        this.mainUrl = this._urls.positionMainUrl;
        this.getOneUrl = this._urls.positionGetOneUrl;
    }
};
PositionView.ctorParameters = () => [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
PositionView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'position-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./position.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./position.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], PositionView);



/***/ })

}]);
//# sourceMappingURL=pages-position-position-module-es2015.js.map