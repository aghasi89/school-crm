(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-price-of-services-price-of-services-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.html":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.html ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addPriceOfService()\">\r\n\r\n    <tr *ngFor=\"let item of priceOfServicesDetail\">\r\n        <td class=\"edit\" (click)=\"addPriceOfService(true, item?.id, item)\">\r\n            <i class=\"material-icons\">edit</i>\r\n        </td>\r\n        <td class=\"delete\" (click)=\"delete(item?.id)\">\r\n            <i class=\"material-icons\">close</i>\r\n        </td>\r\n        <td>{{item?.startDate | date: 'yyyy-MM-dd'}}</td>\r\n        <td>{{item?.endDate | date: 'yyyy-MM-dd'}}</td>\r\n        <td>{{item?.type}}</td>\r\n \r\n    </tr>\r\n\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services-routing.module.ts":
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services-routing.module.ts ***!
  \**********************************************************************************************************************************/
/*! exports provided: PriceOfServicesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PriceOfServicesRoutingModule", function() { return PriceOfServicesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _price_of_services_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./price-of-services.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.ts");




const priceOfServicesRoutes = [
    { path: '', component: _price_of_services_view__WEBPACK_IMPORTED_MODULE_3__["PriceOfServicesView"] }
];
let PriceOfServicesRoutingModule = class PriceOfServicesRoutingModule {
};
PriceOfServicesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(priceOfServicesRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PriceOfServicesRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.module.ts":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.module.ts ***!
  \**************************************************************************************************************************/
/*! exports provided: PriceOfServicesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PriceOfServicesModule", function() { return PriceOfServicesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _price_of_services_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./price-of-services.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.ts");
/* harmony import */ var _price_of_services_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./price-of-services-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services-routing.module.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");






let PriceOfServicesModule = class PriceOfServicesModule {
};
PriceOfServicesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_price_of_services_view__WEBPACK_IMPORTED_MODULE_3__["PriceOfServicesView"]],
        imports: [_price_of_services_routing_module__WEBPACK_IMPORTED_MODULE_4__["PriceOfServicesRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"]],
        exports: [],
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]]
    })
], PriceOfServicesModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.scss":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.scss ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9wcmljZS1vZi1zZXJ2aWNlcy9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXG1haW4tYWNjb3VudGluZ1xccGFnZXNcXHByaWNlLW9mLXNlcnZpY2VzXFxwcmljZS1vZi1zZXJ2aWNlcy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvcHJpY2Utb2Ytc2VydmljZXMvcHJpY2Utb2Ytc2VydmljZXMudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL21haW4vbWFpbi1hY2NvdW50aW5nL3BhZ2VzL3ByaWNlLW9mLXNlcnZpY2VzL3ByaWNlLW9mLXNlcnZpY2VzLnZpZXcuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLCB0aCwgdGQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOjEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIH0iLCJ0YWJsZSwgdGgsIHRkIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.ts":
/*!************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.ts ***!
  \************************************************************************************************************************/
/*! exports provided: PriceOfServicesView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PriceOfServicesView", function() { return PriceOfServicesView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");










let PriceOfServicesView = class PriceOfServicesView {
    constructor(_matDialog, _router, _activatedRoute, _title, _loadingService, _appService, _mainService) {
        this._matDialog = _matDialog;
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._title = _title;
        this._loadingService = _loadingService;
        this._appService = _appService;
        this._mainService = _mainService;
        this._url = 'price-of-services';
        this._otherUrl = 'price-of-service';
        this._paginatorLastPageNumber = 0;
        this._modalTitle = 'Ծառայության գին';
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this.priceOfServicesDetail = [];
        this.titles = [
            { title: 'Սկիզբ', isSort: false, arrow: '', min: false, max: false },
            { title: 'Ավարտ', isSort: false, arrow: '', min: false, max: false },
            { title: 'Տիպ', isSort: false, arrow: '', min: false, max: false }
        ];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    addPriceOfService(isNew, id, item) {
        this.openModal(isNew, id, item);
    }
    openModal(isNew, id, item) {
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_2__["AddPriceOfServiceModal"], {
            width: '50vw',
            maxHeight: '85vh',
            autoFocus: false,
            data: { title: this._modalTitle, url: this._otherUrl, id: id, item, label: this._modalTitle }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page } });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    _getPriceOfServices(limit, offset) {
        return this._mainService.getByUrl(this._url, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])((data) => {
            this.priceOfServicesDetail = data.data;
            return data;
        }));
    }
    _getPriceOfServicesCount() {
        return this._mainService.getCount(this._url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryParams) => {
            if (queryParams && queryParams.page) {
                this._page = +queryParams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _resetProperties() {
        this._page = 1;
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["forkJoin"])(this._getPriceOfServicesCount(), this._getPriceOfServices(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading(), () => this._loadingService.hideLoading());
    }
    delete(id) {
        this._loadingService.showLoading();
        this._mainService.deleteByUrl(this._otherUrl, id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.priceOfServicesDetail, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }, () => {
            this._loadingService.hideLoading();
        });
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
};
PriceOfServicesView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["LoadingService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["AppService"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_8__["MainService"] }
];
PriceOfServicesView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-price-of-services',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./price-of-services.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./price-of-services.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/price-of-services/price-of-services.view.scss")).default]
    })
], PriceOfServicesView);



/***/ })

}]);
//# sourceMappingURL=pages-price-of-services-price-of-services-module-es2015.js.map