(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-profession-profession-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.html": 
        /*!*****************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.html ***!
          \*****************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-name-code-table [url]=\"getOneUrl\" [mainUrl]=\"mainUrl\" [title]=\"'Մանագիտություն'\">\r\n</app-name-code-table>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.module.ts": 
        /*!***************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.module.ts ***!
          \***************************************************************************************************/
        /*! exports provided: ProfessionModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfessionModule", function () { return ProfessionModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _profession_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profession.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.ts");
            /* harmony import */ var _profession_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profession.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.routing.module.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            var ProfessionModule = /** @class */ (function () {
                function ProfessionModule() {
                }
                return ProfessionModule;
            }());
            ProfessionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_profession_view__WEBPACK_IMPORTED_MODULE_2__["ProfessionView"]],
                    imports: [_profession_routing_module__WEBPACK_IMPORTED_MODULE_3__["ProfessionRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
                })
            ], ProfessionModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.routing.module.ts": 
        /*!***********************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.routing.module.ts ***!
          \***********************************************************************************************************/
        /*! exports provided: ProfessionRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfessionRoutingModule", function () { return ProfessionRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _profession_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profession.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.ts");
            var professionRoutes = [{ path: '', component: _profession_view__WEBPACK_IMPORTED_MODULE_3__["ProfessionView"] }];
            var ProfessionRoutingModule = /** @class */ (function () {
                function ProfessionRoutingModule() {
                }
                return ProfessionRoutingModule;
            }());
            ProfessionRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(professionRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], ProfessionRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.scss": 
        /*!***************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.scss ***!
          \***************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy9wcm9mZXNzaW9uL0M6XFxVc2Vyc1xcQW5uYW5pa3NcXERlc2t0b3BcXHNoZW1tLXNjaG9vbC9zcmNcXGFwcFxcY29tXFxhbm5hbmlrc1xcc2hlbW0tc2Nob29sXFx2aWV3c1xcbWFpblxcc2FsYXJ5XFxwYWdlc1xccHJvZmVzc2lvblxccHJvZmVzc2lvbi52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9zYWxhcnkvcGFnZXMvcHJvZmVzc2lvbi9wcm9mZXNzaW9uLnZpZXcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy9wcm9mZXNzaW9uL3Byb2Zlc3Npb24udmlldy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsIHRoLCB0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6MTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfSIsInRhYmxlLCB0aCwgdGQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.ts": 
        /*!*************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.ts ***!
          \*************************************************************************************************/
        /*! exports provided: ProfessionView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfessionView", function () { return ProfessionView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var ProfessionView = /** @class */ (function () {
                function ProfessionView(_urls) {
                    this._urls = _urls;
                }
                ProfessionView.prototype.ngOnInit = function () {
                    this.mainUrl = this._urls.professionMainUrl;
                    this.getOneUrl = this._urls.professionGetOneUrl;
                };
                return ProfessionView;
            }());
            ProfessionView.ctorParameters = function () { return [
                { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
            ]; };
            ProfessionView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'profession-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./profession.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./profession.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.view.scss")).default]
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
            ], ProfessionView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-profession-profession-module-es2015.js.map
//# sourceMappingURL=pages-profession-profession-module-es5.js.map
//# sourceMappingURL=pages-profession-profession-module-es5.js.map