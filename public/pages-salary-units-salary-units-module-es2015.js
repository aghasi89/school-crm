(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-salary-units-salary-units-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.html":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.html ***!
  \*********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-name-code-table [url]=\"getOneUrl\" [mainUrl]=\"mainUrl\"   [title]=\"'Ստորաբաժանում'\">\r\n</app-name-code-table>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.module.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.module.ts ***!
  \*******************************************************************************************************/
/*! exports provided: SalaryUnitsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalaryUnitsModule", function() { return SalaryUnitsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _salary_units_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./salary-units.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.ts");
/* harmony import */ var _salary_units_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./salary-units.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");





let SalaryUnitsModule = class SalaryUnitsModule {
};
SalaryUnitsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_salary_units_view__WEBPACK_IMPORTED_MODULE_2__["SalaryUnitsView"]],
        imports: [_salary_units_routing_module__WEBPACK_IMPORTED_MODULE_3__["SalaryUnitsRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
    })
], SalaryUnitsModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.routing.module.ts":
/*!***************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.routing.module.ts ***!
  \***************************************************************************************************************/
/*! exports provided: SalaryUnitsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalaryUnitsRoutingModule", function() { return SalaryUnitsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _salary_units_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./salary-units.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.ts");




let salaryUnitsRoutes = [{ path: '', component: _salary_units_view__WEBPACK_IMPORTED_MODULE_3__["SalaryUnitsView"] }];
let SalaryUnitsRoutingModule = class SalaryUnitsRoutingModule {
};
SalaryUnitsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(salaryUnitsRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], SalaryUnitsRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.scss ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy9zYWxhcnktdW5pdHMvQzpcXFVzZXJzXFxBbm5hbmlrc1xcRGVza3RvcFxcc2hlbW0tc2Nob29sL3NyY1xcYXBwXFxjb21cXGFubmFuaWtzXFxzaGVtbS1zY2hvb2xcXHZpZXdzXFxtYWluXFxzYWxhcnlcXHBhZ2VzXFxzYWxhcnktdW5pdHNcXHNhbGFyeS11bml0cy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9zYWxhcnkvcGFnZXMvc2FsYXJ5LXVuaXRzL3NhbGFyeS11bml0cy52aWV3LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9zYWxhcnkvcGFnZXMvc2FsYXJ5LXVuaXRzL3NhbGFyeS11bml0cy52aWV3LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSwgdGgsIHRkIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZToxMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9IiwidGFibGUsIHRoLCB0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.ts ***!
  \*****************************************************************************************************/
/*! exports provided: SalaryUnitsView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalaryUnitsView", function() { return SalaryUnitsView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SalaryUnitsView = class SalaryUnitsView {
    constructor(_urls) {
        this._urls = _urls;
    }
    ngOnInit() {
        this.mainUrl = this._urls.subdivisionMainUrl;
        this.getOneUrl = this._urls.subdivisionGetOneUrl;
    }
};
SalaryUnitsView.ctorParameters = () => [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
];
SalaryUnitsView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'salary-units-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./salary-units.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./salary-units.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.view.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
], SalaryUnitsView);



/***/ })

}]);
//# sourceMappingURL=pages-salary-units-salary-units-module-es2015.js.map