(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-services-services-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.html":
/*!**********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.html ***!
  \**********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addServices(true)\">\r\n        <tr  *ngFor=\"let item of servicesValue\" >\r\n            <td class=\"edit\" (click)=\"addServices(true, item?.id, item)\">\r\n                <i class=\"material-icons\">edit</i>\r\n            </td>\r\n            <td class=\"delete\" (click)=delete(item.id)>\r\n                <i class=\"material-icons\">close</i>\r\n            </td>\r\n            <td>{{item?.code}}</td>      \r\n            <td>{{item?.name}}</td>                         \r\n            <td>{{item?.fullName}}</td>                         \r\n            <td>{{item?.measurementUnitId}}</td>                         \r\n            <td>{{item?.classificationId}}</td>                         \r\n            <td>{{item?.accountId}}</td>                         \r\n            <td>{{item?.wholesalePrice}}</td>                         \r\n            <td>{{item?.retailerPrice}}</td>                         \r\n            <td>{{item?.barCode}}</td>                \r\n            <td><label class=\"container-checkbox\">          \r\n                <input [checked]=\"getBooleanVariable(item?.isAah)\" disabled type=\"checkbox\">\r\n                <span class=\"checkmark\"></span>\r\n            </label></td>\r\n        </tr>\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>\r\n\r\n\r\n<div class=\"bottom-content\">\r\n    <span class=\"web_name\">b2b.armsoft հարթակ</span>\r\n    <div class=\"more-info\">\r\n        <div class=\"text\">\r\n            <span>\r\n                Նշեք փեր ծառայությունների ցանկը և հրապարակեք b2b.armsoft հարթակ\r\n            </span>\r\n        </div>\r\n        <div class=\"info-text\">\r\n            <span> <i class=\"material-icons\">\r\n                    cloud_queue\r\n                </i>\r\n            </span>\r\n            <a>\r\n                Հրապարակել\r\n            </a>\r\n        </div>\r\n    </div>\r\n</div>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.module.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.module.ts ***!
  \********************************************************************************************************/
/*! exports provided: ServicesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesModule", function() { return ServicesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.ts");
/* harmony import */ var _services_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.routing.module.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
/* harmony import */ var _services_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.service.ts");







let ServicesModule = class ServicesModule {
};
ServicesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_services_view__WEBPACK_IMPORTED_MODULE_2__["ServicesView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddServiceModal"]],
        entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddServiceModal"]],
        imports: [_services_routing_module__WEBPACK_IMPORTED_MODULE_3__["ServicesRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
        providers: [_services_service__WEBPACK_IMPORTED_MODULE_6__["ServicesService"]]
    })
], ServicesModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.routing.module.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.routing.module.ts ***!
  \****************************************************************************************************************/
/*! exports provided: ServicesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesRoutingModule", function() { return ServicesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.ts");




let servicesRoutes = [{ path: '', component: _services_view__WEBPACK_IMPORTED_MODULE_3__["ServicesView"] }];
let ServicesRoutingModule = class ServicesRoutingModule {
};
ServicesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(servicesRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], ServicesRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.scss ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n\n.bottom-content {\n  margin-top: 20px;\n  border: 1px solid grey;\n  padding: 10px;\n}\n\n.bottom-content .web_name {\n  color: grey;\n}\n\n.bottom-content .more-info {\n  display: flex;\n  align-items: center;\n  flex-wrap: wrap;\n}\n\n.bottom-content .more-info .text {\n  margin-left: 20px;\n}\n\n.bottom-content .info-text {\n  margin-left: 7px;\n  display: flex;\n  align-items: center;\n}\n\n.bottom-content .info-text i {\n  color: red;\n}\n\n.bottom-content .info-text a {\n  margin-left: 2px;\n  cursor: pointer;\n  text-decoration: underline;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9zZXJ2aWNlcy9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXG1haW4tYWNjb3VudGluZ1xccGFnZXNcXHNlcnZpY2VzXFxzZXJ2aWNlcy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvc2VydmljZXMvc2VydmljZXMudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FER0E7RUFDSSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtBQ0FKOztBRENJO0VBQ0ksV0FBQTtBQ0NSOztBRENJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0NSOztBREFRO0VBQ0ksaUJBQUE7QUNFWjs7QURFSTtFQUNJLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDQVI7O0FEQ1E7RUFDSSxVQUFBO0FDQ1o7O0FEQ1E7RUFDSSxnQkFBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtBQ0NaIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy9zZXJ2aWNlcy9zZXJ2aWNlcy52aWV3LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59XHJcblxyXG5cclxuLmJvdHRvbS1jb250ZW50e1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgLndlYl9uYW1le1xyXG4gICAgICAgIGNvbG9yOiBncmV5O1xyXG4gICAgfVxyXG4gICAgLm1vcmUtaW5mb3tcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgICAgIC50ZXh0e1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmluZm8tdGV4dHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogN3B4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBpe1xyXG4gICAgICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhe1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMnB4O1xyXG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iLCJ0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG4uYm90dG9tLWNvbnRlbnQge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xuICBwYWRkaW5nOiAxMHB4O1xufVxuLmJvdHRvbS1jb250ZW50IC53ZWJfbmFtZSB7XG4gIGNvbG9yOiBncmV5O1xufVxuLmJvdHRvbS1jb250ZW50IC5tb3JlLWluZm8ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmbGV4LXdyYXA6IHdyYXA7XG59XG4uYm90dG9tLWNvbnRlbnQgLm1vcmUtaW5mbyAudGV4dCB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuLmJvdHRvbS1jb250ZW50IC5pbmZvLXRleHQge1xuICBtYXJnaW4tbGVmdDogN3B4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmJvdHRvbS1jb250ZW50IC5pbmZvLXRleHQgaSB7XG4gIGNvbG9yOiByZWQ7XG59XG4uYm90dG9tLWNvbnRlbnQgLmluZm8tdGV4dCBhIHtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.ts ***!
  \******************************************************************************************************/
/*! exports provided: ServicesView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesView", function() { return ServicesView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _services_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.service.ts");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");











let ServicesView = class ServicesView {
    constructor(_matDialog, _title, _router, _activatedRoute, _appService, _servicesService, _mainService, _loadingService) {
        this._matDialog = _matDialog;
        this._title = _title;
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._appService = _appService;
        this._servicesService = _servicesService;
        this._mainService = _mainService;
        this._loadingService = _loadingService;
        this.titles = [
            {
                title: 'Կոդ',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'Անվանում',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'Լրիվ անվանում',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'Չափման միավոր',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'ԱԴԳՏ դասակարգիչ',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'Հաշիվ',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'Մեծածախ գին դրամով',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'Մանրածախ գին դրամով',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'Գծիկվոր կոդ',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'ԱԱՀ',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            }
        ];
        this._modalTitle = 'Ծառայություններ';
        this._otherUrl = 'service';
        this._paginatorLastPageNumber = 0;
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this.servicesValue = [];
        this._serviceUrl = 'services';
        this._title.setTitle('Ծառայություններ');
    }
    ngOnInit() {
        this._checkParams();
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryParams) => {
            if (queryParams && queryParams.page) {
                this._page = +queryParams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _resetProperties() {
        this._page = 1;
    }
    addServices(isNew, id, item) {
        this.openModal(isNew, id, item);
    }
    openModal(isNew, id, item) {
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_3__["AddServiceModal"], {
            width: '50vw',
            maxHeight: '85vh',
            autoFocus: false,
            data: { title: this._modalTitle, url: this._otherUrl, id: id, item }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page } });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["forkJoin"])(this.getServiceValuesCount(), this.getServiceValues(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading(), () => this._loadingService.hideLoading());
    }
    getServiceValuesCount() {
        return this._mainService.getCount(this._serviceUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    getServiceValues(limit, offset) {
        return this._mainService.getByUrl(this._serviceUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["map"])((data) => {
            this.servicesValue = data.data;
            return data;
        }));
    }
    delete(id) {
        this._loadingService.showLoading();
        this._servicesService.deleteService(id).subscribe(() => {
            let page = this._appService.setAfterDeletedPage(this.servicesValue, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }, () => {
            this._loadingService.hideLoading();
        });
    }
    getBooleanVariable(variable) {
        return this._appService.getBooleanVariable(variable);
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
};
ServicesView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["AppService"] },
    { type: _services_service__WEBPACK_IMPORTED_MODULE_8__["ServicesService"] },
    { type: _main_service__WEBPACK_IMPORTED_MODULE_9__["MainService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["LoadingService"] }
];
ServicesView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'services-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./services.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./services.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/services/services.view.scss")).default]
    })
], ServicesView);



/***/ })

}]);
//# sourceMappingURL=pages-services-services-module-es2015.js.map