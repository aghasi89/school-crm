var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-timecard-timecard-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.html": 
        /*!*************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.html ***!
          \*************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addTimeCard(false)\">\r\n    <tr *ngFor=\"let timeCard of timeCards\">\r\n        <td class=\"edit\"> <i (click)=\"addTimeCard(true,timeCard?.id)\" class=\" material-icons\"> edit </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(timeCard?.id)\" class=\" material-icons\"> close </i></td>\r\n\r\n        <td>{{timeCard?.id}}</td>\r\n        <td>{{timeCard?.name}}</td>\r\n        <td> <label class=\"container-checkbox\">\r\n                <input [checked]=\"false\" type=\"checkbox\">\r\n                <span class=\"checkmark\"></span>\r\n            </label></td>\r\n        <td>{{timeCard?.year}}</td>\r\n        <td>{{timeCard?.hours}}</td>\r\n        <td>{{timeCard?.months?.january}}</td>\r\n        <td>{{timeCard?.months?.febrary}}</td>\r\n        <td>{{timeCard?.months?.march}}</td>\r\n        <td>{{timeCard?.months?.april}}</td>\r\n        <td>{{timeCard?.months?.may}}</td>\r\n        <td>{{timeCard?.months?.june}}</td>\r\n        <td>{{timeCard?.months?.july}}</td>\r\n        <td>{{timeCard?.months?.august}}</td>\r\n        <td>{{timeCard?.months?.september}}</td>\r\n        <td>{{timeCard?.months?.october}}</td>\r\n        <td>{{timeCard?.months?.november}}</td>\r\n        <td>{{timeCard?.months?.december}}</td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.module.ts": 
        /*!***********************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.module.ts ***!
          \***********************************************************************************************/
        /*! exports provided: TimeCardModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeCardModule", function () { return TimeCardModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _timecard_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./timecard.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.ts");
            /* harmony import */ var _timecard_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./timecard.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.routing.module.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/salary/modals/index.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_components__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/components */ "./src/app/com/annaniks/shemm-school/components/index.ts");
            /* harmony import */ var _timecard_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./timecard.service */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.service.ts");
            var TimeCardModule = /** @class */ (function () {
                function TimeCardModule() {
                }
                return TimeCardModule;
            }());
            TimeCardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_timecard_view__WEBPACK_IMPORTED_MODULE_2__["TimeCardView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddTimecardModal"], src_app_com_annaniks_shemm_school_components__WEBPACK_IMPORTED_MODULE_6__["MonthComponent"]],
                    imports: [_timecard_routing_module__WEBPACK_IMPORTED_MODULE_3__["TimeCardRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
                    providers: [_timecard_service__WEBPACK_IMPORTED_MODULE_7__["TimeCardService"]],
                    entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddTimecardModal"]]
                })
            ], TimeCardModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.routing.module.ts": 
        /*!*******************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.routing.module.ts ***!
          \*******************************************************************************************************/
        /*! exports provided: TimeCardRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeCardRoutingModule", function () { return TimeCardRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _timecard_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./timecard.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.ts");
            var timeCardRoutes = [{ path: '', component: _timecard_view__WEBPACK_IMPORTED_MODULE_3__["TimeCardView"] }];
            var TimeCardRoutingModule = /** @class */ (function () {
                function TimeCardRoutingModule() {
                }
                return TimeCardRoutingModule;
            }());
            TimeCardRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(timeCardRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], TimeCardRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.scss": 
        /*!***********************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.scss ***!
          \***********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n\ntd {\n  text-align: center !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy90aW1lY2FyZC9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXHNhbGFyeVxccGFnZXNcXHRpbWVjYXJkXFx0aW1lY2FyZC52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9zYWxhcnkvcGFnZXMvdGltZWNhcmQvdGltZWNhcmQudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FEQ0U7RUFDRSw2QkFBQTtBQ0VKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9wYWdlcy90aW1lY2FyZC90aW1lY2FyZC52aWV3LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSwgdGgsIHRkIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIGZvbnQtc2l6ZToxMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcbiAgdGR7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcclxuICB9IiwidGFibGUsIHRoLCB0ZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG50ZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.ts": 
        /*!*********************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.ts ***!
          \*********************************************************************************************/
        /*! exports provided: TimeCardView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeCardView", function () { return TimeCardView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/salary/modals/index.ts");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
            var TimeCardView = /** @class */ (function () {
                function TimeCardView(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService, _urls) {
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this._loadingService = _loadingService;
                    this._activatedRoute = _activatedRoute;
                    this._router = _router;
                    this._mainService = _mainService;
                    this._appService = _appService;
                    this._urls = _urls;
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this._modalTitle = 'Տաբելներ';
                    this._paginatorLastPageNumber = 0;
                    this.timeCards = [];
                    this.titles = [
                        { title: 'Կոդ' }, { title: 'Անվանում' }, { title: 'Ավտոմատ համեմատել b2b.armsoft հարթակի հետ' },
                        { title: 'Տարի' }, { title: 'Ժամերի քանակ' }, { title: 'Հունվար' }, { title: 'Փետրվար' },
                        { title: 'Մարտ' }, { title: 'Ապրիլ' }, { title: 'Մայիս' }, { title: 'Հունիս' },
                        { title: 'Հուլիս' }, { title: 'Օգոստոս' }, { title: 'Սեպտեմբեր' }, { title: 'Հոկտեմբեր' },
                        { title: 'Նոյեմբեր' }, { title: 'Դեկտեմբեր' },
                    ];
                    this._title.setTitle(this._modalTitle);
                }
                TimeCardView.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                TimeCardView.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["forkJoin"])(this._getTimeCardCount(), this._getTimeCard(limit, offset));
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); });
                };
                TimeCardView.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryparams) {
                        if (queryparams && queryparams.page) {
                            _this._page = +queryparams.page;
                        }
                        _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                    });
                };
                TimeCardView.prototype._getTimeCardCount = function () {
                    var _this = this;
                    return this._mainService.getCount(this._urls.tableMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                TimeCardView.prototype.addAddition = function (isNew, id) {
                    this.openModal(isNew, id);
                };
                TimeCardView.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                TimeCardView.prototype._resetProperties = function () {
                    this._page = 1;
                };
                TimeCardView.prototype._getTimeCard = function (limit, offset) {
                    var _this = this;
                    return this._mainService.getByUrl(this._urls.tableMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (data) {
                        _this.timeCards = data.data;
                        _this._replaceJsonParams();
                        return data;
                    }));
                };
                TimeCardView.prototype._replaceJsonParams = function () {
                    var e_1, _a;
                    try {
                        for (var _b = __values(this.timeCards), _c = _b.next(); !_c.done; _c = _b.next()) {
                            var item = _c.value;
                            if (item && item.months)
                                item['months'] = JSON.parse(item.months);
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                };
                TimeCardView.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                TimeCardView.prototype.getBooleanVariable = function (variable) {
                    return this._appService.getBooleanVariable(variable);
                };
                TimeCardView.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._mainService.deleteByUrl(this._urls.tableGetOneUrl, id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.timeCards, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    });
                };
                TimeCardView.prototype.addTimeCard = function (isNew, id) {
                    this.openModal(isNew, id);
                };
                TimeCardView.prototype.openModal = function (isNew, id) {
                    var _this = this;
                    var isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_4__["AddTimecardModal"], {
                        width: '80vw',
                        minHeight: '55vh',
                        maxHeight: '85vh',
                        data: { title: isNewTitle, url: this._urls.tableGetOneUrl, id: id }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                TimeCardView.prototype.ngOnDestroy = function () {
                    this._loadingService.hideLoading();
                    this._subscription.unsubscribe();
                };
                Object.defineProperty(TimeCardView.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(TimeCardView.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(TimeCardView.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                return TimeCardView;
            }());
            TimeCardView.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["LoadingService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
                { type: _main_service__WEBPACK_IMPORTED_MODULE_9__["MainService"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_7__["AppService"] },
                { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
            ]; };
            TimeCardView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'timecard-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./timecard.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./timecard.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.view.scss")).default]
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
            ], TimeCardView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-timecard-timecard-module-es2015.js.map
//# sourceMappingURL=pages-timecard-timecard-module-es5.js.map
//# sourceMappingURL=pages-timecard-timecard-module-es5.js.map