(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-types-types-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.html": 
        /*!**********************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.html ***!
          \**********************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addType(false)\">\r\n    <tr *ngFor=\"let type of types\">\r\n        <td class=\"edit\"> <i (click)=\"addType(true,type?.id)\" class=\" material-icons\"> edit </i> </td>\r\n        <td class=\"delete\"> <i (click)=\"delete(type?.id)\" class=\" material-icons\"> close </i></td>\r\n        <td>{{type?.code}}</td>\r\n        <td>{{type?.name}}</td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.module.ts": 
        /*!********************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.module.ts ***!
          \********************************************************************************************/
        /*! exports provided: TypesModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypesModule", function () { return TypesModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _types_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./types.view */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.ts");
            /* harmony import */ var _types_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./types.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.routing.module.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");
            var TypesModule = /** @class */ (function () {
                function TypesModule() {
                }
                return TypesModule;
            }());
            TypesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_types_view__WEBPACK_IMPORTED_MODULE_2__["TypesView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddTypesModal"]],
                    entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddTypesModal"]],
                    imports: [_types_routing_module__WEBPACK_IMPORTED_MODULE_3__["TypesRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
                })
            ], TypesModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.routing.module.ts": 
        /*!****************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.routing.module.ts ***!
          \****************************************************************************************************/
        /*! exports provided: TypesRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypesRoutingModule", function () { return TypesRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _types_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./types.view */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.ts");
            var typesRoutes = [{ path: '', component: _types_view__WEBPACK_IMPORTED_MODULE_3__["TypesView"] }];
            var TypesRoutingModule = /** @class */ (function () {
                function TypesRoutingModule() {
                }
                return TypesRoutingModule;
            }());
            TypesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(typesRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], TypesRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.scss": 
        /*!********************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.scss ***!
          \********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy90eXBlcy9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXHdhcmVob3VzZVxccGFnZXNcXHR5cGVzXFx0eXBlcy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi93YXJlaG91c2UvcGFnZXMvdHlwZXMvdHlwZXMudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL21haW4vd2FyZWhvdXNlL3BhZ2VzL3R5cGVzL3R5cGVzLnZpZXcuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlLCB0aCwgdGQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOjEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIH0iLCJ0YWJsZSwgdGgsIHRkIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.ts": 
        /*!******************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.ts ***!
          \******************************************************************************************/
        /*! exports provided: TypesView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypesView", function () { return TypesView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");
            var TypesView = /** @class */ (function () {
                function TypesView(_matDialog, _title, _loadingService, _activatedRoute, _router, _mainService, _appService, _urls) {
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this._loadingService = _loadingService;
                    this._activatedRoute = _activatedRoute;
                    this._router = _router;
                    this._mainService = _mainService;
                    this._appService = _appService;
                    this._urls = _urls;
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this._modalTitle = 'Տեսակ';
                    this.types = [];
                    this._paginatorLastPageNumber = 0;
                    this.titles = [{ title: 'Կոդ' }, { title: 'Անվանում' }];
                    this._title.setTitle(this._modalTitle);
                }
                TypesView.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                TypesView.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["forkJoin"])(this._getTypesCount(), this._getTypes(limit, offset));
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); });
                };
                TypesView.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryparams) {
                        if (queryparams && queryparams.page) {
                            _this._page = +queryparams.page;
                        }
                        _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                    });
                };
                TypesView.prototype._getTypesCount = function () {
                    var _this = this;
                    return this._mainService.getCount(this._urls.typesMainUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                TypesView.prototype._getTypes = function (limit, offset) {
                    var _this = this;
                    return this._mainService.getByUrl(this._urls.typesMainUrl, limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (data) {
                        _this.types = data.data;
                        return data;
                    }));
                };
                TypesView.prototype.addType = function (isNew, id) {
                    this.openModal(isNew, id);
                };
                TypesView.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                TypesView.prototype._resetProperties = function () {
                    this._page = 1;
                };
                TypesView.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                TypesView.prototype.openModal = function (isNew, id) {
                    var _this = this;
                    var isNewTitle = isNew ? this._modalTitle : this._modalTitle + ' (Նոր)';
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_9__["AddTypesModal"], {
                        width: '500px',
                        data: { title: isNewTitle, url: this._urls.typesGetOneUrl, id: id }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                TypesView.prototype.getBooleanVariable = function (variable) {
                    return this._appService.getBooleanVariable(variable);
                };
                TypesView.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._mainService.deleteByUrl(this._urls.typesGetOneUrl, id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.types, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    });
                };
                TypesView.prototype.ngOnDestroy = function () {
                    this._loadingService.hideLoading();
                    this._subscription.unsubscribe();
                };
                Object.defineProperty(TypesView.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(TypesView.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(TypesView.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                return TypesView;
            }());
            TypesView.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["LoadingService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
                { type: _main_service__WEBPACK_IMPORTED_MODULE_7__["MainService"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_5__["AppService"] },
                { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
            ]; };
            TypesView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'types-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./types.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./types.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.view.scss")).default]
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](7, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
            ], TypesView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-types-types-module-es2015.js.map
//# sourceMappingURL=pages-types-types-module-es5.js.map
//# sourceMappingURL=pages-types-types-module-es5.js.map