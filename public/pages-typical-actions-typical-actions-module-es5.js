(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-typical-actions-typical-actions-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.html": 
        /*!************************************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.html ***!
          \************************************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addTypicalAction()\">\r\n</app-tables-by-filter>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.module.ts": 
        /*!**********************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.module.ts ***!
          \**********************************************************************************************************************/
        /*! exports provided: TypicalActionsModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypicalActionsModule", function () { return TypicalActionsModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _typical_actions_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./typical-actions.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.ts");
            /* harmony import */ var _typical_actions_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./typical-actions.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.routing.module.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
            var TypicalActionsModule = /** @class */ (function () {
                function TypicalActionsModule() {
                }
                return TypicalActionsModule;
            }());
            TypicalActionsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_typical_actions_view__WEBPACK_IMPORTED_MODULE_2__["TypicalActionView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddTypicalActionsModal"]],
                    imports: [_typical_actions_routing_module__WEBPACK_IMPORTED_MODULE_3__["TypicalActionRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
                    entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddTypicalActionsModal"]]
                })
            ], TypicalActionsModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.routing.module.ts": 
        /*!******************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.routing.module.ts ***!
          \******************************************************************************************************************************/
        /*! exports provided: TypicalActionRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypicalActionRoutingModule", function () { return TypicalActionRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _typical_actions_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./typical-actions.view */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.ts");
            var typicalActionRoutes = [{ path: '', component: _typical_actions_view__WEBPACK_IMPORTED_MODULE_3__["TypicalActionView"] }];
            var TypicalActionRoutingModule = /** @class */ (function () {
                function TypicalActionRoutingModule() {
                }
                return TypicalActionRoutingModule;
            }());
            TypicalActionRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(typicalActionRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], TypicalActionRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.scss": 
        /*!**********************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.scss ***!
          \**********************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvdHlwaWNhbC1hY3Rpb25zL3R5cGljYWwtYWN0aW9ucy52aWV3LnNjc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.ts": 
        /*!********************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.ts ***!
          \********************************************************************************************************************/
        /*! exports provided: TypicalActionView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypicalActionView", function () { return TypicalActionView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
            var TypicalActionView = /** @class */ (function () {
                function TypicalActionView(_matDialog, _title) {
                    this._matDialog = _matDialog;
                    this._title = _title;
                    this.titles = [
                        {
                            title: 'Կոդ',
                            isSort: false,
                            arrow: 'arrow_drop_down',
                            min: false,
                            max: true
                        },
                        {
                            title: 'Անվանում',
                            isSort: false,
                            arrow: '',
                            min: false,
                            max: false
                        }
                    ];
                    this._title.setTitle('Տիպային գործողություններ');
                }
                TypicalActionView.prototype.ngOnInit = function () { };
                TypicalActionView.prototype.addTypicalAction = function () {
                    this.openModal();
                };
                TypicalActionView.prototype.openModal = function () {
                    this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_4__["AddTypicalActionsModal"], {
                        width: '80vw',
                        minHeight: '60vh',
                        maxHeight: '85vh',
                        autoFocus: false
                    });
                };
                return TypicalActionView;
            }());
            TypicalActionView.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] }
            ]; };
            TypicalActionView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'typical-actions-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./typical-actions.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./typical-actions.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/typical-actions/typical-actions.view.scss")).default]
                })
            ], TypicalActionView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-typical-actions-typical-actions-module-es2015.js.map
//# sourceMappingURL=pages-typical-actions-typical-actions-module-es5.js.map
//# sourceMappingURL=pages-typical-actions-typical-actions-module-es5.js.map