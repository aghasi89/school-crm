(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-unit-of-measurement-unit-of-measurment-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.html":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.html ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addUnitOfMeasurement()\">\r\n\r\n    <tr *ngFor=\"let item of unitOfMeasurements\">\r\n        <td class=\"edit\" (click)=\"addUnitOfMeasurement(true, item?.id, item)\">\r\n            <i class=\"material-icons\">edit</i>\r\n        </td>\r\n        <td class=\"delete\" (click)=\"delete(item?.id)\">\r\n            <i class=\"material-icons\">close</i>\r\n        </td>\r\n        <td>{{item?.code}}</td>\r\n        <td>{{item?.unit}}</td>\r\n        <td>{{item?.abbreviation}}</td>\r\n \r\n    </tr>\r\n\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.scss":
/*!***********************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.scss ***!
  \***********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy91bml0LW9mLW1lYXN1cmVtZW50L0M6XFxVc2Vyc1xcQW5uYW5pa3NcXERlc2t0b3BcXHNoZW1tLXNjaG9vbC9zcmNcXGFwcFxcY29tXFxhbm5hbmlrc1xcc2hlbW0tc2Nob29sXFx2aWV3c1xcbWFpblxcbWFpbi1hY2NvdW50aW5nXFxwYWdlc1xcdW5pdC1vZi1tZWFzdXJlbWVudFxcdW5pdC1vZi1tZWFzdXJlbWVudC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy91bml0LW9mLW1lYXN1cmVtZW50L3VuaXQtb2YtbWVhc3VyZW1lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvdW5pdC1vZi1tZWFzdXJlbWVudC91bml0LW9mLW1lYXN1cmVtZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsIHRoLCB0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6MTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfSIsInRhYmxlLCB0aCwgdGQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.ts":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.ts ***!
  \*********************************************************************************************************************************/
/*! exports provided: UnitOfMeasurementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitOfMeasurementComponent", function() { return UnitOfMeasurementComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./unit-of-measurement.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");










let UnitOfMeasurementComponent = class UnitOfMeasurementComponent {
    constructor(_matDialog, _router, _activatedRoute, _title, _loadingService, _unitOfMeasurementService, _appService) {
        this._matDialog = _matDialog;
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._title = _title;
        this._loadingService = _loadingService;
        this._unitOfMeasurementService = _unitOfMeasurementService;
        this._appService = _appService;
        this._url = 'measurement-units';
        this._otherUrl = 'measurement-unit';
        this._paginatorLastPageNumber = 0;
        this._modalTitle = 'Չափման միավոր';
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this.unitOfMeasurements = [];
        this.titles = [
            { title: 'Կոդ', isSort: false, arrow: '', min: false, max: false },
            { title: 'Անվանում', isSort: false, arrow: '', min: false, max: false },
            { title: 'Հապավում', isSort: false, arrow: '', min: false, max: false }
        ];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    addUnitOfMeasurement(isNew, id, item) {
        this.openModal(isNew, id, item);
    }
    openModal(isNew, id, item) {
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_3__["AddMeasurmentModal"], {
            width: '500px',
            maxHeight: '85vh',
            autoFocus: false,
            data: { title: this._modalTitle, url: this._otherUrl, id: id, item }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page } });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    _getMeasurementsPlan(limit, offset) {
        return this._unitOfMeasurementService.getMeasurement(limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])((data) => {
            this.unitOfMeasurements = data.data;
            return data;
        }));
    }
    _getMeasurementsPlanCount() {
        return this._unitOfMeasurementService.getMeasurementsCount().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryParams) => {
            if (queryParams && queryParams.page) {
                this._page = +queryParams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _resetProperties() {
        this._page = 1;
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["forkJoin"])(this._getMeasurementsPlanCount(), this._getMeasurementsPlan(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading(), () => this._loadingService.hideLoading());
    }
    delete(id) {
        this._loadingService.showLoading();
        this._unitOfMeasurementService.deleteMeasurement(id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.unitOfMeasurements, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }, () => {
            this._loadingService.hideLoading();
        });
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
};
UnitOfMeasurementComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
    { type: _unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_8__["UnitOfMeasurementService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["AppService"] }
];
UnitOfMeasurementComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-unit-of-measurement',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./unit-of-measurement.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./unit-of-measurement.component.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.scss")).default]
    })
], UnitOfMeasurementComponent);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment-routing.module.ts":
/*!*************************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment-routing.module.ts ***!
  \*************************************************************************************************************************************/
/*! exports provided: UnitOfMesaurmentRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitOfMesaurmentRoutingModule", function() { return UnitOfMesaurmentRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _unit_of_measurement_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./unit-of-measurement.component */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.ts");




const unitOfMesaurmentRouter = [
    {
        path: '',
        component: _unit_of_measurement_component__WEBPACK_IMPORTED_MODULE_3__["UnitOfMeasurementComponent"]
    }
];
let UnitOfMesaurmentRoutingModule = class UnitOfMesaurmentRoutingModule {
};
UnitOfMesaurmentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(unitOfMesaurmentRouter)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], UnitOfMesaurmentRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment.module.ts":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment.module.ts ***!
  \*****************************************************************************************************************************/
/*! exports provided: UnitOfMeasurmentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitOfMeasurmentModule", function() { return UnitOfMeasurmentModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _unit_of_measurement_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./unit-of-measurement.component */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.ts");
/* harmony import */ var _unit_of_measurment_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./unit-of-measurment-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment-routing.module.ts");
/* harmony import */ var _unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./unit-of-measurement.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.service.ts");






let UnitOfMeasurmentModule = class UnitOfMeasurmentModule {
};
UnitOfMeasurmentModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_unit_of_measurement_component__WEBPACK_IMPORTED_MODULE_3__["UnitOfMeasurementComponent"]],
        imports: [_unit_of_measurment_routing_module__WEBPACK_IMPORTED_MODULE_4__["UnitOfMesaurmentRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"]],
        exports: [],
        providers: [_unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_5__["UnitOfMeasurementService"]]
    })
], UnitOfMeasurmentModule);



/***/ })

}]);
//# sourceMappingURL=pages-unit-of-measurement-unit-of-measurment-module-es2015.js.map