(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-unit-of-measurement-unit-of-measurment-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.html": 
        /*!*************************************************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.html ***!
          \*************************************************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter [titles]=\"titles\" (add)=\"addUnitOfMeasurement()\">\r\n\r\n    <tr *ngFor=\"let item of unitOfMeasurements\">\r\n        <td class=\"edit\" (click)=\"addUnitOfMeasurement(true, item?.id, item)\">\r\n            <i class=\"material-icons\">edit</i>\r\n        </td>\r\n        <td class=\"delete\" (click)=\"delete(item?.id)\">\r\n            <i class=\"material-icons\">close</i>\r\n        </td>\r\n        <td>{{item?.code}}</td>\r\n        <td>{{item?.unit}}</td>\r\n        <td>{{item?.abbreviation}}</td>\r\n \r\n    </tr>\r\n\r\n</app-tables-by-filter>\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.scss": 
        /*!***********************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.scss ***!
          \***********************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy91bml0LW9mLW1lYXN1cmVtZW50L0M6XFxVc2Vyc1xcQW5uYW5pa3NcXERlc2t0b3BcXHNoZW1tLXNjaG9vbC9zcmNcXGFwcFxcY29tXFxhbm5hbmlrc1xcc2hlbW0tc2Nob29sXFx2aWV3c1xcbWFpblxcbWFpbi1hY2NvdW50aW5nXFxwYWdlc1xcdW5pdC1vZi1tZWFzdXJlbWVudFxcdW5pdC1vZi1tZWFzdXJlbWVudC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL21haW4tYWNjb3VudGluZy9wYWdlcy91bml0LW9mLW1lYXN1cmVtZW50L3VuaXQtb2YtbWVhc3VyZW1lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9tYWluLWFjY291bnRpbmcvcGFnZXMvdW5pdC1vZi1tZWFzdXJlbWVudC91bml0LW9mLW1lYXN1cmVtZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsIHRoLCB0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6MTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfSIsInRhYmxlLCB0aCwgdGQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.ts": 
        /*!*********************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.ts ***!
          \*********************************************************************************************************************************/
        /*! exports provided: UnitOfMeasurementComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitOfMeasurementComponent", function () { return UnitOfMeasurementComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/modals/index.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var _unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./unit-of-measurement.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.service.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var UnitOfMeasurementComponent = /** @class */ (function () {
                function UnitOfMeasurementComponent(_matDialog, _router, _activatedRoute, _title, _loadingService, _unitOfMeasurementService, _appService) {
                    this._matDialog = _matDialog;
                    this._router = _router;
                    this._activatedRoute = _activatedRoute;
                    this._title = _title;
                    this._loadingService = _loadingService;
                    this._unitOfMeasurementService = _unitOfMeasurementService;
                    this._appService = _appService;
                    this._url = 'measurement-units';
                    this._otherUrl = 'measurement-unit';
                    this._paginatorLastPageNumber = 0;
                    this._modalTitle = 'Չափման միավոր';
                    this._pageLength = 10;
                    this._count = 0;
                    this._page = 1;
                    this.unitOfMeasurements = [];
                    this.titles = [
                        { title: 'Կոդ', isSort: false, arrow: '', min: false, max: false },
                        { title: 'Անվանում', isSort: false, arrow: '', min: false, max: false },
                        { title: 'Հապավում', isSort: false, arrow: '', min: false, max: false }
                    ];
                    this._title.setTitle(this._modalTitle);
                }
                UnitOfMeasurementComponent.prototype.ngOnInit = function () {
                    this._checkParams();
                };
                UnitOfMeasurementComponent.prototype.lastPage = function ($event) {
                    if ($event)
                        this._paginatorLastPageNumber = $event;
                };
                UnitOfMeasurementComponent.prototype.onPageChange = function ($event) {
                    if ($event.isArrow)
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
                };
                UnitOfMeasurementComponent.prototype.addUnitOfMeasurement = function (isNew, id, item) {
                    this.openModal(isNew, id, item);
                };
                UnitOfMeasurementComponent.prototype.openModal = function (isNew, id, item) {
                    var _this = this;
                    var dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_3__["AddMeasurmentModal"], {
                        width: '500px',
                        maxHeight: '85vh',
                        autoFocus: false,
                        data: { title: this._modalTitle, url: this._otherUrl, id: id, item: item }
                    });
                    dialog.afterClosed().subscribe(function (data) {
                        if (data) {
                            if (data.value) {
                                if (data.id) {
                                    _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                                }
                                else {
                                    var page = _this._appService.getPaginatorLastPage(_this._count, _this._pageLength, _this._paginatorLastPageNumber);
                                    _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page } });
                                    _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                                }
                            }
                        }
                    });
                };
                UnitOfMeasurementComponent.prototype._getMeasurementsPlan = function (limit, offset) {
                    var _this = this;
                    return this._unitOfMeasurementService.getMeasurement(limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])(function (data) {
                        _this.unitOfMeasurements = data.data;
                        return data;
                    }));
                };
                UnitOfMeasurementComponent.prototype._getMeasurementsPlanCount = function () {
                    var _this = this;
                    return this._unitOfMeasurementService.getMeasurementsCount().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])(function (data) {
                        _this._count = data.data.count;
                        return data;
                    }));
                };
                UnitOfMeasurementComponent.prototype._checkParams = function () {
                    var _this = this;
                    this._resetProperties();
                    this._activatedRoute.queryParams.subscribe(function (queryParams) {
                        if (queryParams && queryParams.page) {
                            _this._page = +queryParams.page;
                        }
                        _this._combineObservable(_this._pageLength, (_this._page - 1) * _this._pageLength);
                    });
                };
                UnitOfMeasurementComponent.prototype._resetProperties = function () {
                    this._page = 1;
                };
                UnitOfMeasurementComponent.prototype._combineObservable = function (limit, offset) {
                    var _this = this;
                    this._loadingService.showLoading();
                    var combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["forkJoin"])(this._getMeasurementsPlanCount(), this._getMeasurementsPlan(limit, offset));
                    this._subscription = combine.subscribe(function () { return _this._loadingService.hideLoading(); }, function () { return _this._loadingService.hideLoading(); });
                };
                UnitOfMeasurementComponent.prototype.delete = function (id) {
                    var _this = this;
                    this._loadingService.showLoading();
                    this._unitOfMeasurementService.deleteMeasurement(id).subscribe(function (data) {
                        var page = _this._appService.setAfterDeletedPage(_this.unitOfMeasurements, _this._page);
                        _this._router.navigate([], { relativeTo: _this._activatedRoute, queryParams: { page: page }, });
                        _this._combineObservable(_this._pageLength, (page - 1) * _this._pageLength);
                        _this._loadingService.hideLoading();
                    }, function () {
                        _this._loadingService.hideLoading();
                    });
                };
                Object.defineProperty(UnitOfMeasurementComponent.prototype, "page", {
                    get: function () {
                        return this._page;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(UnitOfMeasurementComponent.prototype, "pageLength", {
                    get: function () {
                        return this._pageLength;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(UnitOfMeasurementComponent.prototype, "count", {
                    get: function () {
                        return this._count;
                    },
                    enumerable: true,
                    configurable: true
                });
                UnitOfMeasurementComponent.prototype.ngOnDestroy = function () {
                    this._subscription.unsubscribe();
                };
                return UnitOfMeasurementComponent;
            }());
            UnitOfMeasurementComponent.ctorParameters = function () { return [
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
                { type: _unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_8__["UnitOfMeasurementService"] },
                { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_6__["AppService"] }
            ]; };
            UnitOfMeasurementComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-unit-of-measurement',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./unit-of-measurement.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./unit-of-measurement.component.scss */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.scss")).default]
                })
            ], UnitOfMeasurementComponent);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment-routing.module.ts": 
        /*!*************************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment-routing.module.ts ***!
          \*************************************************************************************************************************************/
        /*! exports provided: UnitOfMesaurmentRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitOfMesaurmentRoutingModule", function () { return UnitOfMesaurmentRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _unit_of_measurement_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./unit-of-measurement.component */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.ts");
            var unitOfMesaurmentRouter = [
                {
                    path: '',
                    component: _unit_of_measurement_component__WEBPACK_IMPORTED_MODULE_3__["UnitOfMeasurementComponent"]
                }
            ];
            var UnitOfMesaurmentRoutingModule = /** @class */ (function () {
                function UnitOfMesaurmentRoutingModule() {
                }
                return UnitOfMesaurmentRoutingModule;
            }());
            UnitOfMesaurmentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(unitOfMesaurmentRouter)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], UnitOfMesaurmentRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment.module.ts": 
        /*!*****************************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment.module.ts ***!
          \*****************************************************************************************************************************/
        /*! exports provided: UnitOfMeasurmentModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitOfMeasurmentModule", function () { return UnitOfMeasurmentModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _unit_of_measurement_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./unit-of-measurement.component */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.component.ts");
            /* harmony import */ var _unit_of_measurment_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./unit-of-measurment-routing.module */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurment-routing.module.ts");
            /* harmony import */ var _unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./unit-of-measurement.service */ "./src/app/com/annaniks/shemm-school/views/main/main-accounting/pages/unit-of-measurement/unit-of-measurement.service.ts");
            var UnitOfMeasurmentModule = /** @class */ (function () {
                function UnitOfMeasurmentModule() {
                }
                return UnitOfMeasurmentModule;
            }());
            UnitOfMeasurmentModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_unit_of_measurement_component__WEBPACK_IMPORTED_MODULE_3__["UnitOfMeasurementComponent"]],
                    imports: [_unit_of_measurment_routing_module__WEBPACK_IMPORTED_MODULE_4__["UnitOfMesaurmentRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_2__["SharedModule"]],
                    exports: [],
                    providers: [_unit_of_measurement_service__WEBPACK_IMPORTED_MODULE_5__["UnitOfMeasurementService"]]
                })
            ], UnitOfMeasurmentModule);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-unit-of-measurement-unit-of-measurment-module-es2015.js.map
//# sourceMappingURL=pages-unit-of-measurement-unit-of-measurment-module-es5.js.map
//# sourceMappingURL=pages-unit-of-measurement-unit-of-measurment-module-es5.js.map