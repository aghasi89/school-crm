(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-unit-unit-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.html": 
        /*!***********************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.html ***!
          \***********************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-name-code-table [url]=\"getOneUrl\" [mainUrl]=\"mainUrl\"   [title]=\"'Ստորաբաժանում'\">\r\n</app-name-code-table>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.module.ts": 
        /*!*********************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.module.ts ***!
          \*********************************************************************************************/
        /*! exports provided: UnitModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitModule", function () { return UnitModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _unit_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./unit.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.ts");
            /* harmony import */ var _unit_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./unit.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.routing.module.ts");
            /* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            var UnitModule = /** @class */ (function () {
                function UnitModule() {
                }
                return UnitModule;
            }());
            UnitModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_unit_view__WEBPACK_IMPORTED_MODULE_2__["UnitView"]],
                    imports: [_unit_routing_module__WEBPACK_IMPORTED_MODULE_3__["UnitRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
                })
            ], UnitModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.routing.module.ts": 
        /*!*****************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.routing.module.ts ***!
          \*****************************************************************************************************/
        /*! exports provided: UnitRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitRoutingModule", function () { return UnitRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _unit_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./unit.view */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.ts");
            var unitRoutes = [{ path: '', component: _unit_view__WEBPACK_IMPORTED_MODULE_3__["UnitView"] }];
            var UnitRoutingModule = /** @class */ (function () {
                function UnitRoutingModule() {
                }
                return UnitRoutingModule;
            }());
            UnitRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(unitRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], UnitRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.scss": 
        /*!*********************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.scss ***!
          \*********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("table, th, td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy91bml0L0M6XFxVc2Vyc1xcQW5uYW5pa3NcXERlc2t0b3BcXHNoZW1tLXNjaG9vbC9zcmNcXGFwcFxcY29tXFxhbm5hbmlrc1xcc2hlbW0tc2Nob29sXFx2aWV3c1xcbWFpblxcZml4ZWQtYXNzZXRzXFxwYWdlc1xcdW5pdFxcdW5pdC52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi9maXhlZC1hc3NldHMvcGFnZXMvdW5pdC91bml0LnZpZXcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL2ZpeGVkLWFzc2V0cy9wYWdlcy91bml0L3VuaXQudmlldy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUsIHRoLCB0ZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6MTJweDtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfSIsInRhYmxlLCB0aCwgdGQge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.ts": 
        /*!*******************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.ts ***!
          \*******************************************************************************************/
        /*! exports provided: UnitView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitView", function () { return UnitView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var UnitView = /** @class */ (function () {
                function UnitView(_urls) {
                    this._urls = _urls;
                }
                UnitView.prototype.ngOnInit = function () {
                    this.mainUrl = this._urls.subdivisionOfFixedAssetsMainUrl;
                    this.getOneUrl = this._urls.subdivisionOfFixedAssetsGetOneUrl;
                };
                return UnitView;
            }());
            UnitView.ctorParameters = function () { return [
                { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['URL_NAMES',] }] }
            ]; };
            UnitView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'unit-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./unit.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./unit.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/fixed-assets/pages/unit/unit.view.scss")).default]
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('URL_NAMES'))
            ], UnitView);
            /***/ 
        })
    }]);
//# sourceMappingURL=pages-unit-unit-module-es2015.js.map
//# sourceMappingURL=pages-unit-unit-module-es5.js.map
//# sourceMappingURL=pages-unit-unit-module-es5.js.map