(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-warehouses-warehouses-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.html":
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.html ***!
  \********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-tables-by-filter class=\"table-container\" [titles]=\"titles\" (add)=\"addWarehouse()\">\r\n    <tr  *ngFor=\"let item of warehouses\" >\r\n        <td class=\"edit\" (click)=\"addWarehouse(true, item?.id, item)\">\r\n            <i class=\"material-icons\">edit</i>\r\n        </td>\r\n        <td class=\"delete\" (click)=delete(item.id)>\r\n            <i class=\"material-icons\">close</i>\r\n        </td>\r\n        <td>{{item?.code}}</td>\r\n        <td>{{item?.name}}</td>\r\n        <td>{{item?.responsible}}</td>\r\n        <td>{{item?.address}}</td>\r\n    </tr>\r\n</app-tables-by-filter>\r\n\r\n<app-paginator (getLastPage)=\"lastPage($event)\" [count]=\"count\" [pageLength]=\"pageLength\" [currentPage]=\"page\"\r\n    (paginate)=\"onPageChange($event)\">\r\n</app-paginator>\r\n\r\n");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.module.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.module.ts ***!
  \******************************************************************************************************/
/*! exports provided: WarehousesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehousesModule", function() { return WarehousesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _warehouses_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./warehouses.view */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.ts");
/* harmony import */ var src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _warehouses_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./warehouses.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.routing.module.ts");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");
/* harmony import */ var _warehouses_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./warehouses.service */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.service.ts");







let WarehousesModule = class WarehousesModule {
};
WarehousesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_warehouses_view__WEBPACK_IMPORTED_MODULE_2__["WarehousesView"], _modals__WEBPACK_IMPORTED_MODULE_5__["AddWarehouseModal"]],
        imports: [_warehouses_routing_module__WEBPACK_IMPORTED_MODULE_4__["WarehousesRoutingModule"], src_app_com_annaniks_shemm_school_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"]],
        providers: [_warehouses_service__WEBPACK_IMPORTED_MODULE_6__["WarehousesService"]],
        entryComponents: [_modals__WEBPACK_IMPORTED_MODULE_5__["AddWarehouseModal"]]
    })
], WarehousesModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.routing.module.ts":
/*!**************************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.routing.module.ts ***!
  \**************************************************************************************************************/
/*! exports provided: WarehousesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehousesRoutingModule", function() { return WarehousesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _warehouses_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./warehouses.view */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.ts");




let warehouseRoutes = [{ path: '', component: _warehouses_view__WEBPACK_IMPORTED_MODULE_3__["WarehousesView"] }];
let WarehousesRoutingModule = class WarehousesRoutingModule {
};
WarehousesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(warehouseRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], WarehousesRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.service.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.service.ts ***!
  \*******************************************************************************************************/
/*! exports provided: WarehousesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehousesService", function() { return WarehousesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _main_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../main.service */ "./src/app/com/annaniks/shemm-school/views/main/main.service.ts");



let WarehousesService = class WarehousesService {
    constructor(_mainService) {
        this._mainService = _mainService;
    }
    getWarehouses(limit, offset) {
        return this._mainService.getByUrl('warehouses', limit, offset);
    }
    getWarehousesCount() {
        return this._mainService.getCount('warehouses');
    }
    deleteWarehouse(id) {
        return this._mainService.deleteByUrl('warehouse', id);
    }
};
WarehousesService.ctorParameters = () => [
    { type: _main_service__WEBPACK_IMPORTED_MODULE_2__["MainService"] }
];
WarehousesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], WarehousesService);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.scss":
/*!******************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.scss ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("td {\n  border: 1px solid;\n  border-collapse: collapse;\n  padding: 5px;\n  color: black;\n  font-size: 12px;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy93YXJlaG91c2VzL0M6XFxVc2Vyc1xcQW5uYW5pa3NcXERlc2t0b3BcXHNoZW1tLXNjaG9vbC9zcmNcXGFwcFxcY29tXFxhbm5hbmlrc1xcc2hlbW0tc2Nob29sXFx2aWV3c1xcbWFpblxcd2FyZWhvdXNlXFxwYWdlc1xcd2FyZWhvdXNlc1xcd2FyZWhvdXNlcy52aWV3LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi93YXJlaG91c2UvcGFnZXMvd2FyZWhvdXNlcy93YXJlaG91c2VzLnZpZXcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3dhcmVob3VzZS9wYWdlcy93YXJlaG91c2VzL3dhcmVob3VzZXMudmlldy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxufSIsInRkIHtcbiAgYm9yZGVyOiAxcHggc29saWQ7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.ts ***!
  \****************************************************************************************************/
/*! exports provided: WarehousesView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehousesView", function() { return WarehousesView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/com/annaniks/shemm-school/services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
/* harmony import */ var _warehouses_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./warehouses.service */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.service.ts");










let WarehousesView = class WarehousesView {
    constructor(_matDialog, _router, _activatedRoute, _title, _loadingService, _warehousesService, _appService) {
        this._matDialog = _matDialog;
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._title = _title;
        this._loadingService = _loadingService;
        this._warehousesService = _warehousesService;
        this._appService = _appService;
        this._url = 'warehouse';
        this._paginatorLastPageNumber = 0;
        this._modalTitle = 'Պահեստ';
        this._pageLength = 10;
        this._count = 0;
        this._page = 1;
        this.warehouses = [];
        this.titles = [
            {
                title: 'Կոդ',
                isSort: true,
                arrow: '',
                min: false,
                max: true
            },
            {
                title: 'Անվանում',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'Պահեստապետ',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
            {
                title: 'Հասցե',
                isSort: false,
                arrow: '',
                min: false,
                max: false
            },
        ];
        this._title.setTitle(this._modalTitle);
    }
    ngOnInit() {
        this._checkParams();
    }
    lastPage($event) {
        if ($event)
            this._paginatorLastPageNumber = $event;
    }
    onPageChange($event) {
        if ($event.isArrow)
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page: $event.pageNumber }, });
    }
    addWarehouse(isNew, id, item) {
        this.openModal(isNew, id, item);
    }
    openModal(isNew, id, item) {
        let dialog = this._matDialog.open(_modals__WEBPACK_IMPORTED_MODULE_3__["AddWarehouseModal"], {
            width: '500px',
            maxHeight: '85vh',
            data: { title: this._modalTitle, url: this._url, id: id, item }
        });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                if (data.value) {
                    if (data.id) {
                        this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
                    }
                    else {
                        let page = this._appService.getPaginatorLastPage(this._count, this._pageLength, this._paginatorLastPageNumber);
                        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page } });
                        this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
                    }
                }
            }
        });
    }
    _checkParams() {
        this._resetProperties();
        this._activatedRoute.queryParams.subscribe((queryParams) => {
            if (queryParams && queryParams.page) {
                this._page = +queryParams.page;
            }
            this._combineObservable(this._pageLength, (this._page - 1) * this._pageLength);
        });
    }
    _resetProperties() {
        this._page = 1;
    }
    _getAccountsPlan(limit, offset) {
        return this._warehousesService.getWarehouses(limit, offset).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((data) => {
            this.warehouses = data.data;
            return data;
        }));
    }
    _getAccountsPlanCount() {
        return this._warehousesService.getWarehousesCount().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((data) => {
            this._count = data.data.count;
            return data;
        }));
    }
    _combineObservable(limit, offset) {
        this._loadingService.showLoading();
        const combine = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["forkJoin"])(this._getAccountsPlanCount(), this._getAccountsPlan(limit, offset));
        this._subscription = combine.subscribe(() => this._loadingService.hideLoading(), () => this._loadingService.hideLoading());
    }
    delete(id) {
        this._loadingService.showLoading();
        this._warehousesService.deleteWarehouse(id).subscribe((data) => {
            let page = this._appService.setAfterDeletedPage(this.warehouses, this._page);
            this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { page }, });
            this._combineObservable(this._pageLength, (page - 1) * this._pageLength);
            this._loadingService.hideLoading();
        }, () => {
            this._loadingService.hideLoading();
        });
    }
    get page() {
        return this._page;
    }
    get pageLength() {
        return this._pageLength;
    }
    get count() {
        return this._count;
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
};
WarehousesView.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__["LoadingService"] },
    { type: _warehouses_service__WEBPACK_IMPORTED_MODULE_9__["WarehousesService"] },
    { type: src_app_com_annaniks_shemm_school_services__WEBPACK_IMPORTED_MODULE_8__["AppService"] }
];
WarehousesView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'warehouses-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./warehouses.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./warehouses.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.view.scss")).default]
    })
], WarehousesView);



/***/ })

}]);
//# sourceMappingURL=pages-warehouses-warehouses-module-es2015.js.map