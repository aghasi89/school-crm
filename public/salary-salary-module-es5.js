var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["salary-salary-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/components/avarage-salary-data/avarage-salary-data.component.html": 
        /*!***************************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/components/avarage-salary-data/avarage-salary-data.component.html ***!
          \***************************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<!-- <div class=\"table-content\">\r\n    <table>\r\n        <tr>\r\n            <th></th>\r\n            <th>Տարի/Ամիս</th>\r\n            <th>Գումար</th>\r\n            <th>Հաշվի առնել</th>\r\n            <th>1/12 մասով</th>\r\n            <th>Նպաստի գումար</th>\r\n        </tr>\r\n        <tr *ngFor=\"let arr of [1,2]\">\r\n            <td></td>\r\n            <td>2016/12</td>\r\n            <td>0.00</td>\r\n            <td>\r\n                <label class=\"container-checkbox\">\r\n                    <input type=\"checkbox\">\r\n                    <span class=\"checkmark\"></span>\r\n                </label>\r\n            </td>\r\n            <td>0.00</td>\r\n            <td>0.00</td>\r\n        </tr>\r\n        <tr class=\"all_sum\">\r\n            <td class=\"td\" colspan=\"2\"> </td>\r\n            <td class=\"td\"><input readOnly=true [(ngModel)]=\"totalAmount\" type=\"number\"></td>\r\n            <td class=\"td\" colspan=\"1\"> </td>\r\n            <td class=\"td\"><input readOnly=true [(ngModel)]=\"totalForOneMonthAmount\" type=\"number\"></td>\r\n            <td class=\"td\"><input readOnly=true [(ngModel)]=\"totalAllowanceAmount\" type=\"number\"></td>\r\n\r\n        </tr>\r\n    </table>\r\n</div> -->\r\n<app-table (add)=\"addRow($event)\" (delete)=\"deleteAll($event)\">\r\n    <table\r\n        *ngIf=\"avarageSalaryGroupArray && avarageSalaryGroupArray.controls && avarageSalaryGroupArray.controls.length\">\r\n        <tr>\r\n            <th></th>\r\n            <th>Տարի/Ամիս</th>\r\n            <th>Գումար</th>\r\n            <th>Հաշվի առնել</th>\r\n            <th>1/12 մասով</th>\r\n\r\n            <th>Մեկնաբանություն</th>\r\n        </tr>\r\n        <tr class=\"row\" *ngFor=\"let item of avarageSalaryGroupArray.controls; let i = index\" [formGroup]=\"item\">\r\n            <td>\r\n                <div (click)=\"remove(i)\" class=\"close\">\r\n                    <i class=\"material-icons\">\r\n                        close\r\n                    </i>\r\n                </div>\r\n            </td>\r\n            <td>\r\n                <div class=\"row-content\">\r\n                    <p-calendar formControlName=\"date\" [placeholder]=\"'Ամիս/Տարի'\" view=\"month\" dateFormat=\"yy/mm\"\r\n                        [yearNavigator]=\"true\" yearRange=\"2000:2020\" [locale]=\"calendarConfig\"> </p-calendar>\r\n                    <span class=\"validate_error\"\r\n                        *ngIf=\"item.get('date').hasError('required') && item.get('date').touched\"><i\r\n                            class=\"material-icons\">\r\n                            close\r\n                        </i></span>\r\n                </div>\r\n            </td>          \r\n            <td>\r\n                <div class=\"row-content\">\r\n                    <input type=\"number\" (ngModelChange)=\"change('amount',totalAmount)\" formControlName=\"amount\">\r\n                    <span class=\"validate_error\"\r\n                        *ngIf=\"item.get('amount').hasError('required') && item.get('amount').touched\">\r\n                        <i class=\"material-icons\">\r\n                            close\r\n                        </i>\r\n                    </span>\r\n                </div>\r\n            </td>\r\n            <td>\r\n                <div class=\"row-content\">\r\n                    <label class=\"container-checkbox\">\r\n                        <input formControlName=\"isConsider\" type=\"checkbox\">\r\n                        <span class=\"checkmark\"></span>\r\n                    </label>\r\n\r\n                    <span class=\"validate_error\"\r\n                        *ngIf=\"item.get('isConsider').hasError('required') && item.get('isConsider').touched\"><i\r\n                            class=\"material-icons\">\r\n                            close\r\n                        </i>\r\n                    </span>\r\n                </div>\r\n            </td>\r\n            <td>\r\n                <div class=\"row-content\">\r\n                    <input formControlName=\"monthPart\"  (ngModelChange)=\"change('monthPart',totalMonthPart)\" type=\"number\">\r\n                    <span class=\"validate_error\"\r\n                        *ngIf=\"item.get('monthPart').hasError('required') && item.get('monthPart').touched\"><i\r\n                            class=\"material-icons\">\r\n                            close\r\n                        </i></span>\r\n                </div>\r\n            </td>\r\n            <td>\r\n                <div class=\"row-content\">\r\n                    <textarea formControlName=\"comment\"></textarea>\r\n                    <span class=\"validate_error\"\r\n                        *ngIf=\"item.get('comment').hasError('required') && item.get('comment').touched\"><i\r\n                            class=\"material-icons\">\r\n                            close\r\n                        </i></span>\r\n                </div>\r\n            </td>\r\n        </tr>\r\n        <tr class=\"all_sum\">\r\n            <td class=\"td\" colspan=\"2\"> </td>\r\n            <td class=\"td\"><input readOnly=true [formControl]=\"totalAmount\" type=\"number\"></td>\r\n            <td class=\"td\" colspan=\"1\"> </td>\r\n            <td class=\"td\"><input readOnly=true [formControl]=\"totalMonthPart\" type=\"number\"></td>\r\n            <td class=\"td\" colspan=\"1\"> </td>\r\n\r\n        </tr>\r\n    </table>\r\n</app-table>\r\n<div class=\"card\">\r\n    <app-card [header]=\"'Բանաձև'\">\r\n        <div class=\"formula-content\">\r\n            <div class=\"title\">Միջին ամսական`</div>\r\n            <div class=\"result\"></div>\r\n        </div>\r\n\r\n        <div class=\"formula-content\">\r\n            <div class=\"title\">Միջին օրական`</div>\r\n            <div class=\"result\"></div>\r\n        </div>\r\n    </app-card>\r\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.html": 
        /*!********************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.html ***!
          \********************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<app-items-card [path]=\"'/salary'\" [header]=\"header\" [items]=\"\"></app-items-card>\r\n<div class=\"salary-container\">\r\n    <div class=\"salary\" *ngFor=\"let salary of salaryItems\">\r\n        <div (click)=\"openOrClose(salary)\" class=\"items\">\r\n            <span [ngStyle]=\"setArrowStyle(salary)\" class=\"arrow\"><i class=\"material-icons\">\r\n                keyboard_arrow_down\r\n            </i></span>\r\n            <span class=\"title\">{{salary.title}}</span>\r\n        </div>\r\n        <div *ngIf=\"salary.isOpen\" class=\"item-container\">\r\n            <div class=\"item\" (click)=\"openModal(item)\" *ngFor=\"let item of salary.items\">{{item.label}}</div>\r\n        </div>\r\n    </div>\r\n</div>");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/components/avarage-salary-data/avarage-salary-data.component.scss": 
        /*!*************************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/components/avarage-salary-data/avarage-salary-data.component.scss ***!
          \*************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (":host::ng-deep app-modal-dropdown input {\n  height: 32px;\n  background: none;\n  border: 1px solid grey !important;\n  border-right: none !important;\n}\n\n:host::ng-deep app-table .table-content {\n  overflow-x: inherit !important;\n}\n\n.formula-content {\n  margin-top: 10px;\n  display: flex;\n  align-items: center;\n}\n\n.formula-content .result {\n  margin-left: 10px;\n}\n\n.row input[type=text], .row input[type=number], .row p-dopdown, .row textarea {\n  border: none !important;\n  height: 100%;\n  resize: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC9jb21wb25lbnRzL2F2YXJhZ2Utc2FsYXJ5LWRhdGEvQzpcXFVzZXJzXFxBbm5hbmlrc1xcRGVza3RvcFxcc2hlbW0tc2Nob29sL3NyY1xcYXBwXFxjb21cXGFubmFuaWtzXFxzaGVtbS1zY2hvb2xcXGNvbXBvbmVudHNcXGF2YXJhZ2Utc2FsYXJ5LWRhdGFcXGF2YXJhZ2Utc2FsYXJ5LWRhdGEuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvY29tcG9uZW50cy9hdmFyYWdlLXNhbGFyeS1kYXRhL2F2YXJhZ2Utc2FsYXJ5LWRhdGEuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR007RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQ0FBQTtFQUNBLDZCQUFBO0FDRlI7O0FEU007RUFDRSw4QkFBQTtBQ05SOztBRFVBO0VBQ0UsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUNQRjs7QURRRTtFQUNFLGlCQUFBO0FDTko7O0FEV0k7RUFDRSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDUk4iLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL2NvbXBvbmVudHMvYXZhcmFnZS1zYWxhcnktZGF0YS9hdmFyYWdlLXNhbGFyeS1kYXRhLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbiAgOmhvc3Q6Om5nLWRlZXB7XHJcbiAgICBhcHAtbW9kYWwtZHJvcGRvd257XHJcbiAgICAgIGlucHV0e1xyXG4gICAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyZXkgIWltcG9ydGFudDtcclxuICAgICAgICBib3JkZXItcmlnaHQ6IG5vbmUgIWltcG9ydGFudDtcclxuICBcclxuICAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgOmhvc3Q6Om5nLWRlZXB7XHJcbiAgICBhcHAtdGFibGV7XHJcbiAgICAgIC50YWJsZS1jb250ZW50e1xyXG4gICAgICAgIG92ZXJmbG93LXg6IGluaGVyaXQgIWltcG9ydGFudDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuLmZvcm11bGEtY29udGVudHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAucmVzdWx0e1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgfVxyXG59XHJcblxyXG4gIC5yb3d7XHJcbiAgICBpbnB1dFt0eXBlPVwidGV4dFwiXSwgaW5wdXRbdHlwZT1cIm51bWJlclwiXSwgcC1kb3Bkb3duLHRleHRhcmVhe1xyXG4gICAgICBib3JkZXI6bm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIHJlc2l6ZTogbm9uZTtcclxuICAgIH1cclxuICB9XHJcbiIsIjpob3N0OjpuZy1kZWVwIGFwcC1tb2RhbC1kcm9wZG93biBpbnB1dCB7XG4gIGhlaWdodDogMzJweDtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JleSAhaW1wb3J0YW50O1xuICBib3JkZXItcmlnaHQ6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuOmhvc3Q6Om5nLWRlZXAgYXBwLXRhYmxlIC50YWJsZS1jb250ZW50IHtcbiAgb3ZlcmZsb3cteDogaW5oZXJpdCAhaW1wb3J0YW50O1xufVxuXG4uZm9ybXVsYS1jb250ZW50IHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb3JtdWxhLWNvbnRlbnQgLnJlc3VsdCB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4ucm93IGlucHV0W3R5cGU9dGV4dF0sIC5yb3cgaW5wdXRbdHlwZT1udW1iZXJdLCAucm93IHAtZG9wZG93biwgLnJvdyB0ZXh0YXJlYSB7XG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCU7XG4gIHJlc2l6ZTogbm9uZTtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/components/avarage-salary-data/avarage-salary-data.component.ts": 
        /*!***********************************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/components/avarage-salary-data/avarage-salary-data.component.ts ***!
          \***********************************************************************************************************/
        /*! exports provided: AvarageSalaryDataComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvarageSalaryDataComponent", function () { return AvarageSalaryDataComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services */ "./src/app/com/annaniks/shemm-school/services/index.ts");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            var AvarageSalaryDataComponent = /** @class */ (function () {
                function AvarageSalaryDataComponent(_fb, _matDialog, _appService, _componentDataService, calendarConfig) {
                    this._fb = _fb;
                    this._matDialog = _matDialog;
                    this._appService = _appService;
                    this._componentDataService = _componentDataService;
                    this.calendarConfig = calendarConfig;
                    this.totalAmount = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](0);
                    this.totalMonthPart = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](0);
                    this._validate();
                }
                Object.defineProperty(AvarageSalaryDataComponent.prototype, "setFormGroup", {
                    set: function ($event) {
                        var _this = this;
                        if ($event && $event.length) {
                            this.avarageSalaryGroupArray = this._fb.array([]);
                            $event.forEach(function (element) {
                                _this.avarageSalaryGroupArray.push(element);
                                _this._appService.markFormGroupTouched(element);
                            });
                            this.change('amount', this.totalAmount);
                            this.change('monthPart', this.totalMonthPart);
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                AvarageSalaryDataComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._subscription = this._componentDataService.getState().subscribe(function (data) {
                        if (data.isSend) {
                            if (_this.avarageSalaryGroupArray) {
                                var isValid = _this.avarageSalaryGroupArray && _this.avarageSalaryGroupArray.valid ? true : false;
                                _this._componentDataService.sendData(_this.avarageSalaryGroupArray.controls, 'avarageSalary', isValid);
                            }
                        }
                    });
                };
                AvarageSalaryDataComponent.prototype.remove = function (index) {
                    var formArray = this.avarageSalaryGroupArray;
                    formArray.removeAt(index);
                    this.change('amount', this.totalAmount);
                    this.change('monthPart', this.totalMonthPart);
                };
                AvarageSalaryDataComponent.prototype.deleteAll = function (event) {
                    if (event && (this.avarageSalaryGroupArray && this.avarageSalaryGroupArray.length)) {
                        var index = this.avarageSalaryGroupArray.length - 1;
                        this.avarageSalaryGroupArray.removeAt(index);
                        this.change('amount', this.totalAmount);
                        this.change('monthPart', this.totalMonthPart);
                    }
                };
                AvarageSalaryDataComponent.prototype.change = function (controlName, formControl) {
                    var e_1, _a;
                    var sum = 0;
                    if (this.avarageSalaryGroupArray && this.avarageSalaryGroupArray.controls) {
                        try {
                            for (var _b = __values(this.avarageSalaryGroupArray.controls), _c = _b.next(); !_c.done; _c = _b.next()) {
                                var item = _c.value;
                                if (item.get(controlName).value) {
                                    sum += (item.get(controlName).value) ? +item.get(controlName).value : 0;
                                }
                            }
                        }
                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                        finally {
                            try {
                                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                            }
                            finally { if (e_1) throw e_1.error; }
                        }
                    }
                    formControl.setValue(sum);
                };
                AvarageSalaryDataComponent.prototype._validate = function () {
                    this.avarageSalaryGroupArray = this._fb.array([this._fb.group({
                            date: [null],
                            amount: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                            isConsider: [false],
                            monthPart: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                            comment: [null],
                        })]);
                };
                AvarageSalaryDataComponent.prototype.addRow = function (event) {
                    if (event) {
                        var formArray = this.avarageSalaryGroupArray;
                        formArray.push(this._fb.group({
                            date: [null],
                            amount: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                            isConsider: [false],
                            monthPart: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                            comment: [null]
                        }));
                    }
                };
                AvarageSalaryDataComponent.prototype.setValue = function (event, controlName, form) {
                    form.get(controlName).setValue(event);
                };
                AvarageSalaryDataComponent.prototype.setModalParams = function (title, codeKeyName) {
                    var modalParams = { tabs: ['Կոդ', 'Անվանում'], title: title, keys: [codeKeyName, 'name'] };
                    return modalParams;
                };
                AvarageSalaryDataComponent.prototype.setInputValue = function (controlName, property, form) {
                    return this._appService.setInputValue(form, controlName, property);
                };
                AvarageSalaryDataComponent.prototype.ngOnDestroy = function () {
                    this._subscription.unsubscribe();
                };
                Object.defineProperty(AvarageSalaryDataComponent.prototype, "avarageMonth", {
                    get: function () {
                        this._avarageMonth = +this.totalAmount.value / 12;
                        return this._avarageMonth + " = " + this.totalAmount.value + " / 12";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(AvarageSalaryDataComponent.prototype, "avaragDaily", {
                    get: function () {
                        this._avarageDaily = +this._avarageBenefit / 5.00;
                        return this._avarageDaily + " = " + this._avarageBenefit + " / 5.00";
                    },
                    enumerable: true,
                    configurable: true
                });
                return AvarageSalaryDataComponent;
            }());
            AvarageSalaryDataComponent.ctorParameters = function () { return [
                { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
                { type: _services__WEBPACK_IMPORTED_MODULE_3__["AppService"] },
                { type: _services__WEBPACK_IMPORTED_MODULE_3__["ComponentDataService"] },
                { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['CALENDAR_CONFIG',] }] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('group')
            ], AvarageSalaryDataComponent.prototype, "setFormGroup", null);
            AvarageSalaryDataComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-avarage-salary-data',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./avarage-salary-data.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/components/avarage-salary-data/avarage-salary-data.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./avarage-salary-data.component.scss */ "./src/app/com/annaniks/shemm-school/components/avarage-salary-data/avarage-salary-data.component.scss")).default]
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('CALENDAR_CONFIG'))
            ], AvarageSalaryDataComponent);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.module.ts": 
        /*!******************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/salary.module.ts ***!
          \******************************************************************************/
        /*! exports provided: SalaryModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalaryModule", function () { return SalaryModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _salary_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./salary.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.ts");
            /* harmony import */ var _salary_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./salary.routing.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.routing.module.ts");
            /* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modals */ "./src/app/com/annaniks/shemm-school/views/main/salary/modals/index.ts");
            /* harmony import */ var _modals_select_employees_select_employees_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modals/select-employees/select-employees.modal */ "./src/app/com/annaniks/shemm-school/views/main/salary/modals/select-employees/select-employees.modal.ts");
            /* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../components */ "./src/app/com/annaniks/shemm-school/components/index.ts");
            /* harmony import */ var _components_avarage_salary_data_avarage_salary_data_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../components/avarage-salary-data/avarage-salary-data.component */ "./src/app/com/annaniks/shemm-school/components/avarage-salary-data/avarage-salary-data.component.ts");
            /* harmony import */ var _salary_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./salary.service */ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.service.ts");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            var SalaryModule = /** @class */ (function () {
                function SalaryModule() {
                }
                return SalaryModule;
            }());
            SalaryModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [
                        _salary_view__WEBPACK_IMPORTED_MODULE_2__["SalaryView"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["SettlementDateModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["CalculationVacationTimeModal"],
                        _modals_select_employees_select_employees_modal__WEBPACK_IMPORTED_MODULE_6__["SelectEmployeeModal"],
                        _components__WEBPACK_IMPORTED_MODULE_7__["VacationTimeCommonComponent"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["CalculationBenefitsModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["BenefitsModal"],
                        _components__WEBPACK_IMPORTED_MODULE_7__["BenefitsCommonComponent"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["FinalySettlementModal"],
                        _components__WEBPACK_IMPORTED_MODULE_7__["FinalySettlementCommonComponent"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["PaymentFromCurrencyAccountModal"],
                        _components__WEBPACK_IMPORTED_MODULE_7__["CurrencyAccountCommonComponent"],
                        _components__WEBPACK_IMPORTED_MODULE_7__["CurrencyAccountOperationComponent"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["InvestmentDataModal"],
                        _components_avarage_salary_data_avarage_salary_data_component__WEBPACK_IMPORTED_MODULE_8__["AvarageSalaryDataComponent"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["SalaryDocumentJournalModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["SelectDocumentModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["PaymentListModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["ShortDataModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["ConfirmAdditionRetentionModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["SelectAdditionRetentionModal"]
                    ],
                    entryComponents: [
                        _modals__WEBPACK_IMPORTED_MODULE_5__["SettlementDateModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["CalculationVacationTimeModal"],
                        _modals_select_employees_select_employees_modal__WEBPACK_IMPORTED_MODULE_6__["SelectEmployeeModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["CalculationBenefitsModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["BenefitsModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["FinalySettlementModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["PaymentFromCurrencyAccountModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["InvestmentDataModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["SalaryDocumentJournalModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["SelectDocumentModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["PaymentListModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["ShortDataModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["ConfirmAdditionRetentionModal"],
                        _modals__WEBPACK_IMPORTED_MODULE_5__["SelectAdditionRetentionModal"]
                    ],
                    imports: [_salary_routing_module__WEBPACK_IMPORTED_MODULE_3__["SalaryRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
                    providers: [_salary_service__WEBPACK_IMPORTED_MODULE_9__["SalaryService"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["DatePipe"]]
                })
            ], SalaryModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.routing.module.ts": 
        /*!**************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/salary.routing.module.ts ***!
          \**************************************************************************************/
        /*! exports provided: SalaryRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalaryRoutingModule", function () { return SalaryRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _salary_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./salary.view */ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.ts");
            var salaryRoutes = [
                { path: '', component: _salary_view__WEBPACK_IMPORTED_MODULE_3__["SalaryView"] },
                { path: 'subdivision', loadChildren: function () { return __webpack_require__.e(/*! import() | pages-salary-units-salary-units-module */ "pages-salary-units-salary-units-module").then(__webpack_require__.bind(null, /*! ./pages/salary-units/salary-units.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-units/salary-units.module.ts")).then(function (m) { return m.SalaryUnitsModule; }); } },
                { path: 'position', loadChildren: function () { return __webpack_require__.e(/*! import() | pages-position-position-module */ "pages-position-position-module").then(__webpack_require__.bind(null, /*! ./pages/position/position.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/position/position.module.ts")).then(function (m) { return m.PositionModule; }); } },
                { path: 'profession', loadChildren: function () { return __webpack_require__.e(/*! import() | pages-profession-profession-module */ "pages-profession-profession-module").then(__webpack_require__.bind(null, /*! ./pages/profession/profession.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/profession/profession.module.ts")).then(function (m) { return m.ProfessionModule; }); } },
                { path: 'addition', loadChildren: function () { return __webpack_require__.e(/*! import() | pages-addition-addition-module */ "pages-addition-addition-module").then(__webpack_require__.bind(null, /*! ./pages/addition/addition.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/addition/addition.module.ts")).then(function (m) { return m.AdditionModule; }); } },
                { path: 'timecard', loadChildren: function () { return __webpack_require__.e(/*! import() | pages-timecard-timecard-module */ "pages-timecard-timecard-module").then(__webpack_require__.bind(null, /*! ./pages/timecard/timecard.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/timecard/timecard.module.ts")).then(function (m) { return m.TimeCardModule; }); } },
                { path: 'employees', loadChildren: function () { return Promise.all(/*! import() | pages-salary-employee-salary-employee-module */ [__webpack_require__.e("default~pages-employees-employees-module~pages-salary-employee-salary-employee-module"), __webpack_require__.e("pages-salary-employee-salary-employee-module")]).then(__webpack_require__.bind(null, /*! ./pages/salary-employee/salary-employee.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/salary-employee/salary-employee.module.ts")).then(function (m) { return m.SalaryEmployeeModule; }); } },
                { path: 'payment-from-current-account', loadChildren: function () { return __webpack_require__.e(/*! import() | pages-payment-from-current-account-payment-from-current-account-module */ "pages-payment-from-current-account-payment-from-current-account-module").then(__webpack_require__.bind(null, /*! ./pages/payment-from-current-account/payment-from-current-account.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/payment-from-current-account/payment-from-current-account.module.ts")).then(function (m) { return m.PaymentFromCurrentAccountModule; }); } },
                { path: 'calculation-benefits', loadChildren: function () { return __webpack_require__.e(/*! import() | pages-calculation-benefits-calculation-benefits-module */ "pages-calculation-benefits-calculation-benefits-module").then(__webpack_require__.bind(null, /*! ./pages/calculation-benefits/calculation-benefits.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-benefits/calculation-benefits.module.ts")).then(function (m) { return m.CalculationBenefitsModule; }); } },
                { path: 'calculation-vacation-time', loadChildren: function () { return __webpack_require__.e(/*! import() | pages-calculation-vacation-time-calculation-vacation-time-module */ "pages-calculation-vacation-time-calculation-vacation-time-module").then(__webpack_require__.bind(null, /*! ./pages/calculation-vacation-time/calculation-vacation-time.module */ "./src/app/com/annaniks/shemm-school/views/main/salary/pages/calculation-vacation-time/calculation-vacation-time.module.ts")).then(function (m) { return m.CalculationVacationTimeModule; }); } }
            ];
            var SalaryRoutingModule = /** @class */ (function () {
                function SalaryRoutingModule() {
                }
                return SalaryRoutingModule;
            }());
            SalaryRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(salaryRoutes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], SalaryRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.service.ts": 
        /*!*******************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/salary.service.ts ***!
          \*******************************************************************************/
        /*! exports provided: SalaryService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalaryService", function () { return SalaryService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var SalaryService = /** @class */ (function () {
                function SalaryService(_httpClient) {
                    this._httpClient = _httpClient;
                }
                return SalaryService;
            }());
            SalaryService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            SalaryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
            ], SalaryService);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.scss": 
        /*!******************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.scss ***!
          \******************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".salary-container {\n  display: flex;\n  flex-wrap: wrap;\n  justify-content: space-between;\n}\n.salary-container .salary {\n  margin-top: 15px;\n  width: 33%;\n}\n.salary-container .items {\n  display: flex;\n  align-items: center;\n  color: grey;\n  margin-left: 10px;\n  cursor: pointer;\n}\n.salary-container .items .arrow {\n  display: flex;\n  align-items: center;\n}\n.salary-container .items .title {\n  text-transform: capitalize;\n}\n.salary-container .items:first-child {\n  margin-left: 0 !important;\n}\n.salary-container .item-container {\n  margin-top: 5px;\n  margin-left: 5px;\n}\n.salary-container .item-container .item {\n  margin-top: 5px;\n  font-size: 15px;\n  line-height: 25px;\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXG1haW5cXHNhbGFyeVxcc2FsYXJ5LnZpZXcuc2NzcyIsInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9tYWluL3NhbGFyeS9zYWxhcnkudmlldy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLGVBQUE7RUFDQSw4QkFBQTtBQ0NKO0FEQUk7RUFDSSxnQkFBQTtFQUNBLFVBQUE7QUNFUjtBREFJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ0VSO0FERFE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUNHWjtBRERRO0VBQ0ksMEJBQUE7QUNHWjtBREFJO0VBQ0kseUJBQUE7QUNFUjtBREFJO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FDRVI7QUREUTtFQUNJLGVBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDR1oiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL21haW4vc2FsYXJ5L3NhbGFyeS52aWV3LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2FsYXJ5LWNvbnRhaW5lcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAuc2FsYXJ5e1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAgd2lkdGg6IDMzJTtcclxuICAgIH1cclxuICAgIC5pdGVtc3tcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgY29sb3I6IGdyZXk7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgIC5hcnJvd3tcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnRpdGxle1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuaXRlbXM6Zmlyc3QtY2hpbGR7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDAgIWltcG9ydGFudDtcclxuICAgIH1cclxuICAgIC5pdGVtLWNvbnRhaW5lcntcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgICAgICAuaXRlbXtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiIsIi5zYWxhcnktY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uc2FsYXJ5LWNvbnRhaW5lciAuc2FsYXJ5IHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgd2lkdGg6IDMzJTtcbn1cbi5zYWxhcnktY29udGFpbmVyIC5pdGVtcyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGNvbG9yOiBncmV5O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLnNhbGFyeS1jb250YWluZXIgLml0ZW1zIC5hcnJvdyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uc2FsYXJ5LWNvbnRhaW5lciAuaXRlbXMgLnRpdGxlIHtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG4uc2FsYXJ5LWNvbnRhaW5lciAuaXRlbXM6Zmlyc3QtY2hpbGQge1xuICBtYXJnaW4tbGVmdDogMCAhaW1wb3J0YW50O1xufVxuLnNhbGFyeS1jb250YWluZXIgLml0ZW0tY29udGFpbmVyIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBtYXJnaW4tbGVmdDogNXB4O1xufVxuLnNhbGFyeS1jb250YWluZXIgLml0ZW0tY29udGFpbmVyIC5pdGVtIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiAyNXB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.ts": 
        /*!****************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.ts ***!
          \****************************************************************************/
        /*! exports provided: SalaryView */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalaryView", function () { return SalaryView; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm2015/dialog.js");
            /* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modals */ "./src/app/com/annaniks/shemm-school/views/main/salary/modals/index.ts");
            var SalaryView = /** @class */ (function () {
                function SalaryView(_activatedRoute, _router, _title, _matDialog) {
                    this._activatedRoute = _activatedRoute;
                    this._router = _router;
                    this._title = _title;
                    this._matDialog = _matDialog;
                    this._salaryItems = [
                        {
                            title: 'Փաստաթղթեր',
                            isOpen: true,
                            items: [
                                { label: 'Հաստատել հավելում/պահումը', modalName: _modals__WEBPACK_IMPORTED_MODULE_5__["ConfirmAdditionRetentionModal"] },
                                { label: 'Արձակուրդային հաշվարկ', path: 'calculation-vacation-time' },
                                { label: 'Նպաստների հաշվարկ', path: 'calculation-benefits' },
                                { label: 'Վճարումներ', },
                                { label: 'Վճարումներ հաշվարկային հաշվից', path: 'payment-from-current-account' },
                                { label: 'Աշխատավարձի ձևակերպումներ', },
                                { label: 'Վերջնահաշվարկ', modalName: _modals__WEBPACK_IMPORTED_MODULE_5__["FinalySettlementModal"] },
                                { label: 'Ներդրումային տվյալներ', modalName: _modals__WEBPACK_IMPORTED_MODULE_5__["InvestmentDataModal"] }
                            ]
                        },
                        {
                            title: 'գործողություններ',
                            isOpen: true,
                            items: [
                                { label: 'Հաշվարկի ամսաթիվ', modalName: _modals__WEBPACK_IMPORTED_MODULE_5__["SettlementDateModal"], isSmallModal: true },
                                { label: 'Նշանակել հաստիքը' },
                                { label: 'Աշխատաժամանակի հաշվարկի տեղեկագիր' },
                                { label: 'Տաբելներ', path: 'timecard' },
                                { label: 'Ավելացնել դասացուցակ' }
                            ]
                        },
                        {
                            title: 'հաշվետվություններ',
                            isOpen: true,
                            items: [
                                { label: 'Եկամտային հարկի հաշվարկ' },
                                { label: 'Աշխատողի և քաղ․ պայման․ մատուցողի գրանցման հայտ' },
                                { label: 'Ավելացնել նախահաշիվ' },
                                { label: 'Հաշվարկային թերթիկ', modalName: _modals__WEBPACK_IMPORTED_MODULE_5__["PaymentListModal"], isSmallModal: true }
                            ]
                        },
                        {
                            title: 'տեղեկատուներ',
                            isOpen: true,
                            items: [
                                { label: 'Աշխատակիցներ', path: 'employees' },
                                { label: 'Ստորաբաժանումներ', path: 'subdivision' },
                                { label: 'Պաշտոններ', path: 'position' },
                                { label: 'Մասնագիտություններ', path: 'profession' },
                                { label: 'Հավելումներ', path: 'addition' },
                                { label: 'Պահումներ' },
                                { label: 'Համակարգային հավելում/պահումներ' },
                                { label: 'Աշխատանքային օրացույց', sitePath: 'https://www.irtek.am/views/calendarw.aspx' },
                                { label: 'Ամփոփ տվյալներ', modalName: _modals__WEBPACK_IMPORTED_MODULE_5__["ShortDataModal"], isSmallModal: true },
                            ]
                        },
                        {
                            title: 'մատյաններ',
                            isOpen: true,
                            items: [
                                { label: 'Աշխատավարձերի փաստաթղթի մատյան', modalName: _modals__WEBPACK_IMPORTED_MODULE_5__["SalaryDocumentJournalModal"] },
                                { label: 'Աշխատանք աշխատակիցների հետ' },
                                { label: 'Ավելացնել փոխարինումներ' }
                            ]
                        }
                    ];
                    this.header = this._activatedRoute.data['_value'].title;
                    this._title.setTitle(this.header);
                }
                SalaryView.prototype.ngOnInit = function () { };
                SalaryView.prototype.openOrClose = function (salary) {
                    salary.isOpen = !salary.isOpen;
                };
                SalaryView.prototype.openModal = function (item) {
                    if (item && item.sitePath) {
                        window.location.href = item.sitePath;
                        return;
                    }
                    if (item && item.path) {
                        this._router.navigate(["/salary/" + item.path]);
                    }
                    else {
                        if (item && item.modalName) {
                            var label = item.longTitle ? item.longTitle : item.label;
                            var param1 = {
                                width: '80vw',
                                minHeight: '55vh',
                                maxHeight: '85vh',
                                data: { label: label, type: item.type },
                                autoFocus: false
                            };
                            var param2 = {
                                width: '700px',
                                minHeight: '320px',
                                data: { label: label, type: item.type },
                                autoFocus: false
                            };
                            var modalParamms = item.isSmallModal ? param2 : param1;
                            this._matDialog.open(item.modalName, modalParamms);
                        }
                    }
                };
                SalaryView.prototype.setArrowStyle = function (salary) {
                    var style = {};
                    if (salary.isOpen) {
                        style['transform'] = "rotate(180deg)";
                    }
                    return style;
                };
                Object.defineProperty(SalaryView.prototype, "salaryItems", {
                    get: function () {
                        return this._salaryItems;
                    },
                    enumerable: true,
                    configurable: true
                });
                SalaryView.prototype.ngOnDestroy = function () { };
                return SalaryView;
            }());
            SalaryView.ctorParameters = function () { return [
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
                { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] }
            ]; };
            SalaryView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'salary-view',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./salary.view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./salary.view.scss */ "./src/app/com/annaniks/shemm-school/views/main/salary/salary.view.scss")).default]
                })
            ], SalaryView);
            /***/ 
        })
    }]);
//# sourceMappingURL=salary-salary-module-es2015.js.map
//# sourceMappingURL=salary-salary-module-es5.js.map
//# sourceMappingURL=salary-salary-module-es5.js.map