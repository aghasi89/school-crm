(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["verify-verify-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.html": 
        /*!*************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.html ***!
          \*************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\r\n<div  *ngIf='errText' class=\"bordered\">\r\n    <p>You seccessfuly registered</p>\r\n    <button (click)=\"navigateLogin()\">Go Home</button>\r\n</div>\r\n<!-- <app-loading *ngIf=\"errMsg\"></app-loading> -->\r\n\r\n");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify-routing.module.ts": 
        /*!**************************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/auth/verify/verify-routing.module.ts ***!
          \**************************************************************************************/
        /*! exports provided: VerifyRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyRoutingModule", function () { return VerifyRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _verify_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./verify.component */ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.ts");
            var verifyRouter = [{
                    path: '',
                    component: _verify_component__WEBPACK_IMPORTED_MODULE_3__["VerifyComponent"]
                }];
            var VerifyRoutingModule = /** @class */ (function () {
                function VerifyRoutingModule() {
                }
                return VerifyRoutingModule;
            }());
            VerifyRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(verifyRouter)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], VerifyRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.scss": 
        /*!***********************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.scss ***!
          \***********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".bordered {\n  padding: 25px 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tL2FubmFuaWtzL3NoZW1tLXNjaG9vbC92aWV3cy9hdXRoL3ZlcmlmeS9DOlxcVXNlcnNcXEFubmFuaWtzXFxEZXNrdG9wXFxzaGVtbS1zY2hvb2wvc3JjXFxhcHBcXGNvbVxcYW5uYW5pa3NcXHNoZW1tLXNjaG9vbFxcdmlld3NcXGF1dGhcXHZlcmlmeVxcdmVyaWZ5LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL2F1dGgvdmVyaWZ5L3ZlcmlmeS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb20vYW5uYW5pa3Mvc2hlbW0tc2Nob29sL3ZpZXdzL2F1dGgvdmVyaWZ5L3ZlcmlmeS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ib3JkZXJlZCB7XHJcbiAgICBwYWRkaW5nOiAyNXB4IDUwcHg7XHJcbn0iLCIuYm9yZGVyZWQge1xuICBwYWRkaW5nOiAyNXB4IDUwcHg7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.ts": 
        /*!*********************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.ts ***!
          \*********************************************************************************/
        /*! exports provided: VerifyComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyComponent", function () { return VerifyComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _verify_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./verify.service */ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify.service.ts");
            /* harmony import */ var ngx_cookie__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie */ "./node_modules/ngx-cookie/fesm2015/ngx-cookie.js");
            var VerifyComponent = /** @class */ (function () {
                function VerifyComponent(_route, _router, _verifyService, _cookieService) {
                    this._route = _route;
                    this._router = _router;
                    this._verifyService = _verifyService;
                    this._cookieService = _cookieService;
                    this.getVerifyKey = '';
                    this.errMsg = false;
                    this.errText = false;
                }
                VerifyComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._route.queryParams.subscribe(function (param) {
                        _this.getVerifyKey = param['verify'];
                        if (param['verify']) {
                            var sendingRefreshKey = {
                                token: param['verify']
                            };
                            _this._verifyService.checkParams(sendingRefreshKey).subscribe(function (data) {
                                _this._cookieService.put('token', data.data.token);
                                _this.navigateEnterPassword();
                            }, function (error) {
                                _this.errText = true;
                            });
                        }
                    });
                };
                VerifyComponent.prototype.navigateEnterPassword = function () {
                    var _this = this;
                    this.errMsg = true;
                    setTimeout(function () {
                        _this._router.navigate(['auth/forgot/enter-password']);
                    }, 3000);
                };
                VerifyComponent.prototype.navigateLogin = function () {
                    this._router.navigate(['auth/login']);
                };
                return VerifyComponent;
            }());
            VerifyComponent.ctorParameters = function () { return [
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
                { type: _verify_service__WEBPACK_IMPORTED_MODULE_3__["VerifyService"] },
                { type: ngx_cookie__WEBPACK_IMPORTED_MODULE_4__["CookieService"] }
            ]; };
            VerifyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-verify',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./verify.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./verify.component.scss */ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.scss")).default]
                })
            ], VerifyComponent);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify.module.ts": 
        /*!******************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/auth/verify/verify.module.ts ***!
          \******************************************************************************/
        /*! exports provided: VerifyModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyModule", function () { return VerifyModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _verify_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./verify.component */ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify.component.ts");
            /* harmony import */ var _verify_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./verify-routing.module */ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify-routing.module.ts");
            /* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
            /* harmony import */ var _verify_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./verify.service */ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify.service.ts");
            var VerifyModule = /** @class */ (function () {
                function VerifyModule() {
                }
                return VerifyModule;
            }());
            VerifyModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_verify_component__WEBPACK_IMPORTED_MODULE_2__["VerifyComponent"]],
                    imports: [_verify_routing_module__WEBPACK_IMPORTED_MODULE_3__["VerifyRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
                    providers: [_verify_service__WEBPACK_IMPORTED_MODULE_5__["VerifyService"]]
                })
            ], VerifyModule);
            /***/ 
        }),
        /***/ "./src/app/com/annaniks/shemm-school/views/auth/verify/verify.service.ts": 
        /*!*******************************************************************************!*\
          !*** ./src/app/com/annaniks/shemm-school/views/auth/verify/verify.service.ts ***!
          \*******************************************************************************/
        /*! exports provided: VerifyService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyService", function () { return VerifyService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
            var VerifyService = /** @class */ (function () {
                function VerifyService(_httpClient) {
                    this._httpClient = _httpClient;
                }
                VerifyService.prototype.checkParams = function (data) {
                    return this._httpClient.post('users/check/reset/token', data);
                };
                return VerifyService;
            }());
            VerifyService.ctorParameters = function () { return [
                { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
            ]; };
            VerifyService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
            ], VerifyService);
            /***/ 
        })
    }]);
//# sourceMappingURL=verify-verify-module-es2015.js.map
//# sourceMappingURL=verify-verify-module-es5.js.map
//# sourceMappingURL=verify-verify-module-es5.js.map