(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["warehouse-warehouse-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.html":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.html ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-items-card [path]=\"'/warehouse'\" [header]=\"header\" [items]=\"warehouseItems\"></app-items-card>\r\n");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․module.ts ***!
  \************************************************************************************/
/*! exports provided: WarehouseModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehouseModule", function() { return WarehouseModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _warehouse_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./warehouse․view */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.ts");
/* harmony import */ var _warehouse_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./warehouse․routing.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․routing.module.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/shared.module */ "./src/app/com/annaniks/shemm-school/shared/shared.module.ts");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../components */ "./src/app/com/annaniks/shemm-school/components/index.ts");
/* harmony import */ var _modals_classification_classification_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modals/classification/classification.modal */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/classification/classification.modal.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");









let WarehouseModule = class WarehouseModule {
};
WarehouseModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _warehouse_view__WEBPACK_IMPORTED_MODULE_2__["WarehouseView"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["AvailabilityCertificateModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["EnterVaultModal"],
            _components__WEBPACK_IMPORTED_MODULE_6__["EnterVaultCommonComponent"],
            _components__WEBPACK_IMPORTED_MODULE_6__["EnterVaultAdditionallyComponent"],
            _components__WEBPACK_IMPORTED_MODULE_6__["MaterialAssetsListComponent"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["OutVaultModal"],
            _components__WEBPACK_IMPORTED_MODULE_6__["OutVaultCommonComponent"],
            _components__WEBPACK_IMPORTED_MODULE_6__["OutVaultAdditionallyComponent"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["MaterialValuesShiftModal"],
            _components__WEBPACK_IMPORTED_MODULE_6__["MaterialValueShiftCommonComponent"],
            _components__WEBPACK_IMPORTED_MODULE_6__["MaterialValueShiftAdditionallyComponent"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["MaterialValuesTAccountModal"],
            _components__WEBPACK_IMPORTED_MODULE_6__["AvailableCertificateComponent"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["MaterialValuesInventoryModal"],
            _components__WEBPACK_IMPORTED_MODULE_6__["MaterialValuesInventoryCommonComponent"],
            _components__WEBPACK_IMPORTED_MODULE_6__["CommissionMemberComponent"],
            _modals_classification_classification_modal__WEBPACK_IMPORTED_MODULE_7__["ClassificationModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["MaterialValueGroupModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["AddBillingMethodModal"]
        ],
        imports: [_warehouse_routing_module__WEBPACK_IMPORTED_MODULE_3__["WarehouseRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
        entryComponents: [
            _modals__WEBPACK_IMPORTED_MODULE_5__["AvailabilityCertificateModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["EnterVaultModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["OutVaultModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["MaterialValuesShiftModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["MaterialValuesTAccountModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["MaterialValuesInventoryModal"],
            _modals_classification_classification_modal__WEBPACK_IMPORTED_MODULE_7__["ClassificationModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["MaterialValueGroupModal"],
            _modals__WEBPACK_IMPORTED_MODULE_5__["AddBillingMethodModal"]
        ],
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_8__["DatePipe"]]
    })
], WarehouseModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․routing.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․routing.module.ts ***!
  \********************************************************************************************/
/*! exports provided: WarehouseRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehouseRoutingModule", function() { return WarehouseRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _warehouse_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./warehouse․view */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.ts");




let warehouseRoutes = [
    { path: '', component: _warehouse_view__WEBPACK_IMPORTED_MODULE_3__["WarehouseView"] },
    { path: 'material-values', loadChildren: () => __webpack_require__.e(/*! import() | pages-material-values-material-values-module */ "pages-material-values-material-values-module").then(__webpack_require__.bind(null, /*! ./pages/material-values/material-values.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.module.ts")).then(m => m.MaterialValuesModule) },
    { path: 'warehouses', loadChildren: () => __webpack_require__.e(/*! import() | pages-warehouses-warehouses-module */ "pages-warehouses-warehouses-module").then(__webpack_require__.bind(null, /*! ./pages/warehouses/warehouses.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.module.ts")).then(m => m.WarehousesModule) },
    { path: 'subsection', loadChildren: () => __webpack_require__.e(/*! import() | pages-subsection-subsection-module */ "pages-subsection-subsection-module").then(__webpack_require__.bind(null, /*! ./pages/subsection/subsection.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/subsection/subsection.module.ts")).then(m => m.SubsectionModule) },
    { path: 'material-value-group', loadChildren: () => __webpack_require__.e(/*! import() | pages-group-group-module */ "pages-group-group-module").then(__webpack_require__.bind(null, /*! ./pages/group/group.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/group/group.module.ts")).then(m => m.GroupModule) },
    { path: 'billing-method', loadChildren: () => __webpack_require__.e(/*! import() | pages-billing-method-billing-method-module */ "pages-billing-method-billing-method-module").then(__webpack_require__.bind(null, /*! ./pages/billing-method/billing-method.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/billing-method/billing-method.module.ts")).then(m => m.BillingMethodModule) },
    { path: 'material-values', loadChildren: () => __webpack_require__.e(/*! import() | pages-material-values-material-values-module */ "pages-material-values-material-values-module").then(__webpack_require__.bind(null, /*! ./pages/material-values/material-values.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values/material-values.module.ts")).then(m => m.MaterialValuesModule) },
    { path: 'warehouses', loadChildren: () => __webpack_require__.e(/*! import() | pages-warehouses-warehouses-module */ "pages-warehouses-warehouses-module").then(__webpack_require__.bind(null, /*! ./pages/warehouses/warehouses.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/warehouses/warehouses.module.ts")).then(m => m.WarehousesModule) },
    { path: 'subsection', loadChildren: () => __webpack_require__.e(/*! import() | pages-subsection-subsection-module */ "pages-subsection-subsection-module").then(__webpack_require__.bind(null, /*! ./pages/subsection/subsection.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/subsection/subsection.module.ts")).then(m => m.SubsectionModule) },
    { path: 'types', loadChildren: () => __webpack_require__.e(/*! import() | pages-types-types-module */ "pages-types-types-module").then(__webpack_require__.bind(null, /*! ./pages/types/types.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/types/types.module.ts")).then(m => m.TypesModule) },
    { path: 'classifier', loadChildren: () => __webpack_require__.e(/*! import() | pages-classifier-classifier-module */ "pages-classifier-classifier-module").then(__webpack_require__.bind(null, /*! ./pages/classifier/classifier.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/classifier/classifier.module.ts")).then(m => m.ClassifierModule) },
    { path: 'entry-orders', loadChildren: () => __webpack_require__.e(/*! import() | pages-enter-vault-enter-vault-module */ "pages-enter-vault-enter-vault-module").then(__webpack_require__.bind(null, /*! ./pages/enter-vault/enter-vault.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/enter-vault/enter-vault.module.ts")).then(m => m.EnterValutModule) },
    { path: 'exit-orders', loadChildren: () => __webpack_require__.e(/*! import() | pages-exit-order-exit-order-module */ "pages-exit-order-exit-order-module").then(__webpack_require__.bind(null, /*! ./pages/exit-order/exit-order.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/exit-order/exit-order.module.ts")).then(m => m.ExitOrderModule) },
    { path: 'invoice', loadChildren: () => __webpack_require__.e(/*! import() | pages-invoice-invoice-module */ "common").then(__webpack_require__.bind(null, /*! ./pages/invoice/invoice.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/invoice/invoice.module.ts")).then(m => m.InvoiceModule) },
    { path: 'materail-values-shift', loadChildren: () => __webpack_require__.e(/*! import() | pages-material-values-shift-material-values-shift-module */ "pages-material-values-shift-material-values-shift-module").then(__webpack_require__.bind(null, /*! ./pages/material-values-shift/material-values-shift.module */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/pages/material-values-shift/material-values-shift.module.ts")).then(m => m.MaterialValuewShiftModule) }
];
let WarehouseRoutingModule = class WarehouseRoutingModule {
};
WarehouseRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(warehouseRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        declarations: []
    })
], WarehouseRoutingModule);



/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.scss":
/*!************************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.scss ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbS9hbm5hbmlrcy9zaGVtbS1zY2hvb2wvdmlld3MvbWFpbi93YXJlaG91c2Uvd2FyZWhvdXNl4oCkdmlldy5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.ts ***!
  \**********************************************************************************/
/*! exports provided: WarehouseView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehouseView", function() { return WarehouseView; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _modals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modals */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/modals/index.ts");





let WarehouseView = class WarehouseView {
    constructor(_activatedRoute, _router, _title) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._title = _title;
        this._warehouseItems = [
            { label: 'Հաշիվ-ապրանքագիր', path: 'invoice' },
            { label: 'Պահեստի մուտքի օրդեր', path: 'entry-orders' },
            // { label: 'Պահեստի մուտքի օրդեր', modalName: EnterVaultModal },
            { label: 'Պահեստի ելքի օրդեր', path: 'exit-orders' },
            // { label: 'Պահեստի ելքի օրդեր', modalName: OutVaultModal },
            { label: 'ՆԱ տեղաշարժ', longTitle: 'Նյութական արժեքների տեղաշարժ', path: 'materail-values-shift' },
            { label: 'ՆԱ գույքագրում', modalName: _modals__WEBPACK_IMPORTED_MODULE_4__["MaterialValuesInventoryModal"], longTitle: 'Նյութական արժեքների գույքագրում' },
            { label: 'Տեղեկանք ՆԱ առկայության մասին', modalName: _modals__WEBPACK_IMPORTED_MODULE_4__["AvailabilityCertificateModal"] },
            { label: 'ՆԱ T-հաշիվ', modalName: _modals__WEBPACK_IMPORTED_MODULE_4__["MaterialValuesTAccountModal"] },
            { label: 'Նյութական արժեքներ', path: 'material-values' },
            { label: 'Պահեստներ', path: 'warehouses' },
            { label: 'Ենթաբաժիններ', path: 'subsection' },
            { label: 'Խումբ', modalName: _modals__WEBPACK_IMPORTED_MODULE_4__["MaterialValueGroupModal"], path: 'material-value-group' },
            { label: 'Հաշվառման մեթոդ', path: 'billing-method' },
            { label: 'Տեսակներ', path: 'types' },
            { label: 'ԱՏԳԱԱ և ԱԴԳՏ դասակարգիչ', path: 'classifier' },
        ];
        this.header = this._activatedRoute.data['_value'].title;
        this._title.setTitle(this.header);
    }
    ngOnInit() { }
    get warehouseItems() {
        return this._warehouseItems;
    }
};
WarehouseView.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] }
];
WarehouseView = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'warehouse-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./warehouse․view.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./warehouse․view.scss */ "./src/app/com/annaniks/shemm-school/views/main/warehouse/warehouse․view.scss")).default]
    })
], WarehouseView);



/***/ })

}]);
//# sourceMappingURL=warehouse-warehouse-module-es2015.js.map