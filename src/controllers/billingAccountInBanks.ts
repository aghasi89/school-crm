import { Request, Response, NextFunction } from 'express';
import * as billingAccountInBanksService from '../services/billingAccountInBanksService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  BillingAccountInBanksPayload  from '../domain/requests/BillingAccountInBanksPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await billingAccountInBanksService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.billingAccount.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await billingAccountInBanksService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.billingAccount.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const billingAccountInBanksPayload = req.body as BillingAccountInBanksPayload;

        const billingAccountInBanks = await billingAccountInBanksService.insert(billingAccountInBanksPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: billingAccountInBanks,
            message: messages.billingAccount.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await billingAccountInBanksService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.billingAccount.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await billingAccountInBanksService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.billingAccount.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const billingAccountInBanksPayload = req.body as BillingAccountInBanksPayload;

        const response = (await billingAccountInBanksService.update(id,billingAccountInBanksPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.billingAccount.edit
        })

    } catch (error) {
        next(error);
    }
}