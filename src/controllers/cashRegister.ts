import * as HttpStatus from 'http-status-codes';
import { Request, Response, NextFunction } from 'express';

import config from '../config/config';
import * as cashRegisterService from '../services/cashRegisterService';
import CashRegisterPayload from '../domain/requests/CashRegisterPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

/**
 * Handle /users GET request.
 *
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
export async function index(req: Request, res: Response, next: NextFunction) {
  try {
    const params = Object(req.params) as PaginetPayload;

    const response = await cashRegisterService.fetchAll(params.limit,params.offset);

    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: response,
      message: messages.cashRegister.fetchAll
    });
  } catch (err) {
    next(err);
  }
}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

  try {
      const response = await cashRegisterService.count();
      
      res.status(HttpStatus.OK).json({
          code:HttpStatus.OK,
          data:response,
          message:messages.cashRegister.count
      })

  } catch (error) {
      next(error);
  }

}


/**
 * Handle /users POST request.
 *
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
export async function store(req: Request, res: Response, next: NextFunction) {
  try {
    const cashRegisterPayload = req.body as CashRegisterPayload;

    const response = await cashRegisterService.insert(cashRegisterPayload);

    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: response,
      message: messages.cashRegister.insert
    });
  } catch (err) {
    next(err);
  }
}


export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

  try {
      const id: number = Number(req.params.id);

      const response = await cashRegisterService.getById(id);

      res.status(HttpStatus.OK).json({
          code: HttpStatus.OK,
          data: response,
          message: messages.cashRegister.fetch
      })

  } catch (error) {
      next(error);
  }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

  try {
      const id: number = Number(req.params.id);

      const response = (await cashRegisterService.destroy(id));

      res.status(HttpStatus.OK).json({
          code: HttpStatus.OK,
          data: response,
          message: messages.cashRegister.delete
      })

  } catch (error) {
      next(error);
  }
}


export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

  try {
      const id: number = Number(req.params.id);
      const partnersPayload = req.body as CashRegisterPayload;

      const response = (await cashRegisterService.update(id,partnersPayload));

      res.status(HttpStatus.OK).json({
          code: HttpStatus.OK,
          data: response,
          message: messages.cashRegister.edit
      })

  } catch (error) {
      next(error);
  }
}