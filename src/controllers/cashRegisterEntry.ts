import { Request, Response, NextFunction } from 'express';
import * as cashRegisterEntryService from '../services/cashRegisterEntryService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {CashRegisterEntryPayload}  from '../domain/requests/CashRegisterEntryPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await cashRegisterEntryService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.cashRegisterEntry.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await cashRegisterEntryService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.cashRegisterEntry.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const cashRegisterEntryPayload = req.body as CashRegisterEntryPayload;

        const cashRegisterEntry = await cashRegisterEntryService.insert(cashRegisterEntryPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: cashRegisterEntry,
            message: messages.cashRegisterEntry.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await cashRegisterEntryService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.cashRegisterEntry.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await cashRegisterEntryService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.cashRegisterEntry.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const cashRegisterEntryPayload = req.body as CashRegisterEntryPayload;

        const response = (await cashRegisterEntryService.update(id,cashRegisterEntryPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.cashRegisterEntry.edit
        })

    } catch (error) {
        next(error);
    }
}