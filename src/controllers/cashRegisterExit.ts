import { Request, Response, NextFunction } from 'express';
import * as cashRegisterExitService from '../services/cashRegisterExitService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {CashRegisterExitPayload}  from '../domain/requests/CashRegisterExitPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await cashRegisterExitService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.cashRegisterExit.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await cashRegisterExitService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.cashRegisterExit.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const cashRegisterExitPayload = req.body as CashRegisterExitPayload;

        const cashRegisterExit = await cashRegisterExitService.insert(cashRegisterExitPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: cashRegisterExit,
            message: messages.cashRegisterExit.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await cashRegisterExitService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.cashRegisterExit.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await cashRegisterExitService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.cashRegisterExit.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const cashRegisterExitPayload = req.body as CashRegisterExitPayload;

        const response = (await cashRegisterExitService.update(id,cashRegisterExitPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.cashRegisterExit.edit
        })

    } catch (error) {
        next(error);
    }
}