import { Request, Response, NextFunction } from 'express';
import * as hmAcquisOperatService from '../services/hmAcquisOperatService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {HmAcquisOperatPayload}  from '../domain/requests/HmAcquisOperatPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await hmAcquisOperatService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmAcquisOperat.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await hmAcquisOperatService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.hmAcquisOperat.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const hmAcquisOperatPayload = req.body as HmAcquisOperatPayload;

        const hmAcquisOperat = await hmAcquisOperatService.insert(hmAcquisOperatPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: hmAcquisOperat,
            message: messages.hmAcquisOperat.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await hmAcquisOperatService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmAcquisOperat.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await hmAcquisOperatService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmAcquisOperat.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const hmAcquisOperatPayload = req.body as HmAcquisOperatPayload;

        const response = (await hmAcquisOperatService.update(id,hmAcquisOperatPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmAcquisOperat.edit
        })

    } catch (error) {
        next(error);
    }
}