import { Request, Response, NextFunction } from 'express';
import * as hmReconstructionService from '../services/hmReconstructionService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {HmReconstructionPayload}  from '../domain/requests/HmReconstructionPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await hmReconstructionService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmReconstruction.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await hmReconstructionService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.hmReconstruction.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const hmReconstructionPayload = req.body as HmReconstructionPayload;

        const hmReconstruction = await hmReconstructionService.insert(hmReconstructionPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: hmReconstruction,
            message: messages.hmReconstruction.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await hmReconstructionService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmReconstruction.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await hmReconstructionService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmReconstruction.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const hmReconstructionPayload = req.body as HmReconstructionPayload;

        const response = (await hmReconstructionService.update(id,hmReconstructionPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmReconstruction.edit
        })

    } catch (error) {
        next(error);
    }
}