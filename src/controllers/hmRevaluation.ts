import { Request, Response, NextFunction } from 'express';
import * as hmRevaluationService from '../services/hmRevaluationService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {HmRevaluationPayload}  from '../domain/requests/HmRevaluationPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await hmRevaluationService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmRevaluation.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await hmRevaluationService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.hmRevaluation.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const hmRevaluationPayload = req.body as HmRevaluationPayload;

        const hmRevaluation = await hmRevaluationService.insert(hmRevaluationPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: hmRevaluation,
            message: messages.hmRevaluation.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await hmRevaluationService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmRevaluation.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await hmRevaluationService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmRevaluation.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const hmRevaluationPayload = req.body as HmRevaluationPayload;

        const response = (await hmRevaluationService.update(id,hmRevaluationPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmRevaluation.edit
        })

    } catch (error) {
        next(error);
    }
}