import * as HttpStatus from 'http-status-codes';
import { Request, Response, NextFunction } from 'express';
import * as hmxValueOfBalanceService from '../services/hmxValueOfBalancService';
import config from '../config/config'; 
import HmxValueOfBalancePayload  from '../domain/requests/HmxValueOfBalancePayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';


const { messages } = config;

export async function index(req:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await hmxValueOfBalanceService.fetchAll(params.limit,params.offset);
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.hmxValueOfBalance.fetchAll
        })

    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await hmxValueOfBalanceService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.hmxValueOfBalance.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req:Request,res:Response,next:NextFunction):Promise<void>{
    
    try {
        const hmxValueOfBalancePayload = req.body as HmxValueOfBalancePayload;
        
        const response = await hmxValueOfBalanceService.insert(hmxValueOfBalancePayload);

        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.hmxValueOfBalance.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await hmxValueOfBalanceService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.position.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await hmxValueOfBalanceService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.position.delete
        })

    } catch (error) {
        next(error);
    }
}


export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const hmxValueOfBalancePayload = req.body as HmxValueOfBalancePayload;

        const response = (await hmxValueOfBalanceService.update(id,hmxValueOfBalancePayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.hmxValueOfBalance.edit
        })

    } catch (error) {
        next(error);
    }
}