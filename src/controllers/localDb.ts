import { Request, Response, NextFunction } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as localDbService from '../services/localDbService';
/**
 * Handle / GET request, responds API information.
 *
 * @param {Request} req
 * @param {Response} res
 */
export function getOne(req: Request, res: Response, next: NextFunction): any {

    try {
        const key: string = req.params.key;

        console.log(key)
        const response =  localDbService.getByKey(key);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: 'info'
        })

    } catch (error) {
        next(error);
    }
}