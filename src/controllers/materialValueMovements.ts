import { Request, Response, NextFunction } from 'express';
import * as materialValueMovementsService from '../services/materialValueMovementsService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {MaterialValueMovementsPayload}  from '../domain/requests/MaterialValueMovementsPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await materialValueMovementsService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.materialValueMovements.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await materialValueMovementsService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.materialValueMovements.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const materialValueMovementsPayload = req.body as MaterialValueMovementsPayload;

        const materialValueMovements = await materialValueMovementsService.insert(materialValueMovementsPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: materialValueMovements,
            message: messages.materialValueMovements.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await materialValueMovementsService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.materialValueMovements.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await materialValueMovementsService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.materialValueMovements.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const materialValueMovementsPayload = req.body as MaterialValueMovementsPayload;

        const response = (await materialValueMovementsService.update(id,materialValueMovementsPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.materialValueMovements.edit
        })

    } catch (error) {
        next(error);
    }
}