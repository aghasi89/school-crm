import { Request, Response, NextFunction } from 'express';
import * as operationsHandoverActService from '../services/operationsHandoverActService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {OperationsHandoverActPayload}  from '../domain/requests/OperationsHandoverActPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await operationsHandoverActService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.operationsHandoverAct.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await operationsHandoverActService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.operationsHandoverAct.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const operationsHandoverActPayload = req.body as OperationsHandoverActPayload;

        const operationsHandoverAct = await operationsHandoverActService.insert(operationsHandoverActPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: operationsHandoverAct,
            message: messages.operationsHandoverAct.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await operationsHandoverActService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.operationsHandoverAct.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await operationsHandoverActService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.operationsHandoverAct.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const operationsHandoverActPayload = req.body as OperationsHandoverActPayload;

        const response = (await operationsHandoverActService.update(id,operationsHandoverActPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.operationsHandoverAct.edit
        })

    } catch (error) {
        next(error);
    }
}