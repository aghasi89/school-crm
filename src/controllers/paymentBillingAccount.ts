import { Request, Response, NextFunction } from 'express';
import * as paymentBillingAccountService from '../services/paymentBillingAccountService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {PaymentBillingAccountPayload}  from '../domain/requests/PaymentBillingAccountPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await paymentBillingAccountService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.paymentBillingAccount.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await paymentBillingAccountService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.paymentBillingAccount.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const paymentBillingAccountPayload = req.body as PaymentBillingAccountPayload;

        const paymentBillingAccount = await paymentBillingAccountService.insert(paymentBillingAccountPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: paymentBillingAccount,
            message: messages.paymentBillingAccount.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await paymentBillingAccountService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.paymentBillingAccount.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await paymentBillingAccountService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.paymentBillingAccount.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const paymentBillingAccountPayload = req.body as PaymentBillingAccountPayload;

        const response = (await paymentBillingAccountService.update(id,paymentBillingAccountPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.paymentBillingAccount.edit
        })

    } catch (error) {
        next(error);
    }
}