import { Request, Response, NextFunction } from 'express';
import * as paymentOrderService from '../services/paymentOrderService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {PaymentOrderPayload}  from '../domain/requests/PaymentOrderPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await paymentOrderService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.paymentOrder.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await paymentOrderService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.paymentOrder.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const paymentOrderPayload = req.body as PaymentOrderPayload;

        const paymentOrder = await paymentOrderService.insert(paymentOrderPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: paymentOrder,
            message: messages.paymentOrder.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getPaymentOrderOperation(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        // const warehouseExitOrderPayload = req.body as Array<ReceivedServicesDirectoryPayload>;

        const response = (await paymentOrderService.getPaymentOrderOperation(req.body));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseExitOrder.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await paymentOrderService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.paymentOrder.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await paymentOrderService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.paymentOrder.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const paymentOrderPayload = req.body as PaymentOrderPayload;

        const response = (await paymentOrderService.update(id,paymentOrderPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.paymentOrder.edit
        })

    } catch (error) {
        next(error);
    }
}