import { Request, Response, NextFunction } from 'express';
import * as priceOfServicesService from '../services/priceOfServicesService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  PriceOfServicesPayload  from '../domain/requests/PriceOfServicesPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await priceOfServicesService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.position.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await priceOfServicesService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.subdivision.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const priceOfServicesPayload = req.body as PriceOfServicesPayload;

        const priceOfServices = await priceOfServicesService.insert(priceOfServicesPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: priceOfServices,
            message: messages.position.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await priceOfServicesService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.position.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await priceOfServicesService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.position.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const priceOfServicesPayload = req.body as PriceOfServicesPayload;

        const response = (await priceOfServicesService.update(id,priceOfServicesPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.partners.edit
        })

    } catch (error) {
        next(error);
    }
}