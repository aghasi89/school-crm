import { Request, Response, NextFunction } from 'express';
import * as receivedServicesService from '../services/receivedServicesService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {ReceivedServicesPayload,ReceivedServicesDirectoryPayload}  from '../domain/requests/ReceivedServicesPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await receivedServicesService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.receivedServices.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await receivedServicesService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.receivedServices.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const receivedServicesPayload = req.body as ReceivedServicesPayload;

        const receivedServices = await receivedServicesService.insert(receivedServicesPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: receivedServices,
            message: messages.receivedServices.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await receivedServicesService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.receivedServices.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await receivedServicesService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.receivedServices.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function getReceivedServicesDirectoryPayload(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const warehouseExitOrderPayload = req.body as Array<ReceivedServicesDirectoryPayload>;

        const response = (await receivedServicesService.getReceivedServicesDirectoryPayload(warehouseExitOrderPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseExitOrder.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const receivedServicesPayload = req.body as ReceivedServicesPayload;

        const response = (await receivedServicesService.update(id,receivedServicesPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.receivedServices.edit
        })

    } catch (error) {
        next(error);
    }
}