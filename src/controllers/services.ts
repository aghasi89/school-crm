import * as HttpStatus from 'http-status-codes';
import { Request, Response, NextFunction } from 'express';

import config from '../config/config';
import * as servicesService from '../services/servicesService';
import ServicesPayload from '../domain/requests/ServicesPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

/**
 * Handle /users GET request.
 *
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
export async function index(req: Request, res: Response, next: NextFunction) {
  try {
    const params = Object(req.params) as PaginetPayload;

    const response = await servicesService.fetchAll(params.limit,params.offset);

    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: response,
      message: messages.services.fetchAll
    });
  } catch (err) {
    next(err);
  }
}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

  try {
      const response = await servicesService.count();
      
      res.status(HttpStatus.OK).json({
          code:HttpStatus.OK,
          data:response,
          message:messages.services.count
      })

  } catch (error) {
      next(error);
  }

}


/**
 * Handle /users POST request.
 *
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
export async function store(req: Request, res: Response, next: NextFunction) {
  try {
    const servicesPayload = req.body as ServicesPayload;

    const response = await servicesService.insert(servicesPayload);

    res.status(HttpStatus.OK).json({
      code: HttpStatus.OK,
      data: response,
      message: messages.services.insert
    });
  } catch (err) {
    next(err);
  }
}


export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

  try {
      const id: number = Number(req.params.id);

      const response = await servicesService.getById(id);

      res.status(HttpStatus.OK).json({
          code: HttpStatus.OK,
          data: response,
          message: messages.services.fetch
      })

  } catch (error) {
      next(error);
  }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

  try {
      const id: number = Number(req.params.id);

      const response = (await servicesService.destroy(id));

      res.status(HttpStatus.OK).json({
          code: HttpStatus.OK,
          data: response,
          message: messages.services.delete
      })

  } catch (error) {
      next(error);
  }
}


export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

  try {
      const id: number = Number(req.params.id);
      const partnersPayload = req.body as ServicesPayload;

      const response = (await servicesService.update(id,partnersPayload));

      res.status(HttpStatus.OK).json({
          code: HttpStatus.OK,
          data: response,
          message: messages.services.edit
      })

  } catch (error) {
      next(error);
  }
}