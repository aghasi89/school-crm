import { Request, Response, NextFunction } from 'express';
import * as structuralSubdivisionService from '../services/structuralSubdivisionService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  StructuralSubdivisionPayload  from '../domain/requests/StructuralSubdivisionPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await structuralSubdivisionService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.position.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await structuralSubdivisionService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.subdivision.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const structuralSubdivisionPayload = req.body as StructuralSubdivisionPayload;

        const structuralSubdivision = await structuralSubdivisionService.insert(structuralSubdivisionPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: structuralSubdivision,
            message: messages.position.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await structuralSubdivisionService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.position.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await structuralSubdivisionService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.position.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const structuralSubdivisionPayload = req.body as StructuralSubdivisionPayload;

        const response = (await structuralSubdivisionService.update(id,structuralSubdivisionPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.partners.edit
        })

    } catch (error) {
        next(error);
    }
}