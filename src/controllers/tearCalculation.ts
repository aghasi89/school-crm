import { Request, Response, NextFunction } from 'express';
import * as tearCalculationService from '../services/tearCalculationService';
import * as HttpStatus from 'http-status-codes';
import config from '../config/config';
import  {TearCalculationPayload}  from '../domain/requests/TearCalculationPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';

const { messages } = config;

export async function index(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await tearCalculationService.fetchAll(params.limit,params.offset);
        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.tearCalculation.fetchAll
        })
    } catch (error) {
        next(error);
    }

}

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await tearCalculationService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.tearCalculation.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const tearCalculationPayload = req.body as TearCalculationPayload;

        const tearCalculation = await tearCalculationService.insert(tearCalculationPayload);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: tearCalculation,
            message: messages.tearCalculation.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await tearCalculationService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.tearCalculation.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await tearCalculationService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.tearCalculation.delete
        })

    } catch (error) {
        next(error);
    }
}

export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const tearCalculationPayload = req.body as TearCalculationPayload;

        const response = (await tearCalculationService.update(id,tearCalculationPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.tearCalculation.edit
        })

    } catch (error) {
        next(error);
    }
}