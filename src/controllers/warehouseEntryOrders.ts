import * as HttpStatus from 'http-status-codes';
import { Request, Response, NextFunction } from 'express';
import * as warehouseEntryOrderService from '../services/warehouseEntryOrderService';
import config from '../config/config'; 
import { WarehouseEntryOrderPayload, WarehouseEntryOrderProductPayload } from '../domain/requests/WarehouseEntryOrderPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';


const { messages } = config;

export async function index(req:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await warehouseEntryOrderService.fetchAll(params.limit,params.offset);
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.warehouseEntryOrder.fetchAll
        })

    } catch (error) {
        next(error);
    }

}


// export async function indexByType(req:Request,res:Response,next:NextFunction):Promise<void>{

    

//     try {
//         const typeId: number = Number(req.params.typeId);

//         const response = await warehouseEntryOrderService.fetchAllByType(typeId);
        
//         res.status(HttpStatus.OK).json({
//             code:HttpStatus.OK,
//             data:response,
//             message:messages.warehouseEntryOrder.fetchAll
//         })

//     } catch (error) {
//         next(error);
//     }

// }

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await warehouseEntryOrderService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.warehouseEntryOrder.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req:Request,res:Response,next:NextFunction):Promise<void>{
    
    try {
        const warehouseEntryOrderPayload = req.body as WarehouseEntryOrderPayload;
        
        const response = await warehouseEntryOrderService.insert(warehouseEntryOrderPayload);

        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.warehouseEntryOrder.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await warehouseEntryOrderService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseEntryOrder.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await warehouseEntryOrderService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseEntryOrder.delete
        })

    } catch (error) {
        next(error);
    }
}


export async function getWarehouseEntryOrderProductPayload(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const warehouseEntryOrderPayload = req.body as Array<WarehouseEntryOrderProductPayload>;

        const response = (await warehouseEntryOrderService.getWarehouseEntryOrderProductPayload(warehouseEntryOrderPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseEntryOrder.delete
        })

    } catch (error) {
        next(error);
    }
}


export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const warehouseEntryOrderPayload = req.body as WarehouseEntryOrderPayload;

        const response = (await warehouseEntryOrderService.update(id,warehouseEntryOrderPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseEntryOrder.edit
        })

    } catch (error) {
        next(error);
    }
}