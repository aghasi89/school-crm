import * as HttpStatus from 'http-status-codes';
import { Request, Response, NextFunction } from 'express';
import * as warehouseExitOrderService from '../services/warehouseExitOrderService';
import config from '../config/config'; 
import { WarehouseExitOrderPayload, WarehouseExitOrderProductPayload } from '../domain/requests/WarehouseExitOrderPayload';
import PaginetPayload  from '../domain/requests/PaginetPayload';


const { messages } = config;

export async function index(req:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const params = Object(req.params) as PaginetPayload;

        const response = await warehouseExitOrderService.fetchAll(params.limit,params.offset);
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.warehouseExitOrder.fetchAll
        })

    } catch (error) {
        next(error);
    }

}


// export async function indexByType(req:Request,res:Response,next:NextFunction):Promise<void>{

    

//     try {
//         const typeId: number = Number(req.params.typeId);

//         const response = await warehouseExitOrderService.fetchAllByType(typeId);
        
//         res.status(HttpStatus.OK).json({
//             code:HttpStatus.OK,
//             data:response,
//             message:messages.warehouseExitOrder.fetchAll
//         })

//     } catch (error) {
//         next(error);
//     }

// }

export async function count(_:Request,res:Response,next:NextFunction):Promise<void>{

    

    try {
        const response = await warehouseExitOrderService.count();
        
        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.warehouseExitOrder.count
        })

    } catch (error) {
        next(error);
    }

}

export async function store(req:Request,res:Response,next:NextFunction):Promise<void>{
    
    try {
        const warehouseExitOrderPayload = req.body as WarehouseExitOrderPayload;
        
        const response = await warehouseExitOrderService.insert(warehouseExitOrderPayload);

        res.status(HttpStatus.OK).json({
            code:HttpStatus.OK,
            data:response,
            message:messages.warehouseExitOrder.insert
        })

    } catch (error) {
        next(error);
    }
}

export async function getOne(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = await warehouseExitOrderService.getById(id);

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseExitOrder.fetch
        })

    } catch (error) {
        next(error);
    }
}


export async function destroy(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);

        const response = (await warehouseExitOrderService.destroy(id));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseExitOrder.delete
        })

    } catch (error) {
        next(error);
    }
}


export async function getWarehouseExitOrderProductPayload(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const warehouseExitOrderPayload = req.body as Array<WarehouseExitOrderProductPayload>;

        const response = (await warehouseExitOrderService.getWarehouseExitOrderProductPayload(warehouseExitOrderPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseExitOrder.delete
        })

    } catch (error) {
        next(error);
    }
}


export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {

    try {
        const id: number = Number(req.params.id);
        const warehouseExitOrderPayload = req.body as WarehouseExitOrderPayload;

        const response = (await warehouseExitOrderService.update(id,warehouseExitOrderPayload));

        res.status(HttpStatus.OK).json({
            code: HttpStatus.OK,
            data: response,
            message: messages.warehouseExitOrder.edit
        })

    } catch (error) {
        next(error);
    }
}