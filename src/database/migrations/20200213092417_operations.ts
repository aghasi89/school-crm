import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.OPERATIONS, (table) => {
      table.increments('id');
      table.integer('debit_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();

      table.integer('d_partners_id')
        .unsigned()
        .references('id')
        .inTable(Table.PARTNERS)
        .onDelete('CASCADE').nullable();
      table
        .integer('d_analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');

      table
        .integer('d_analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');

      table.integer('credit_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();

      table.integer('c_partners_id')
        .unsigned()
        .references('id')
        .inTable(Table.PARTNERS)
        .onDelete('CASCADE').nullable();

      table
        .integer('c_analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');

      table
        .integer('c_analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');


      table.decimal('money').nullable();
      table.text('comment')
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.OPERATIONS)
}
