import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.WAREHOUSE_ENTRY_ORDER, function (table) {
      table.increments('id');
      table.date('date')
      table
        .integer('warehouse_id')
        .unsigned()
        .references('id')
        .inTable(Table.WAREHOUSE)
        .onDelete('CASCADE').nullable();
      table.integer('document_number').unsigned().unique()
      table
        .integer('partners_id')
        .unsigned()
        .references('id')
        .inTable(Table.PARTNERS)
        .onDelete('CASCADE').nullable();
      table.string('name');
      table
        .integer('partners_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();
      table
        .integer('prepaid_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();
      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');
      table.text('document_n')
      table.date('document_date')
      table.text('comment')
      table.text('power_of_attorney')
      table.text('mediator')
      table.text('container')
      table.text('accountant')
      table.text('allow')
      table.text('accept')
      table.text('document_of_transport')
      table.date('document_of_transport_date')
      table.text('type_of_acquisition_of_na')
      table.text('calculation_style_of_aah')
      table.boolean('include_aah_in_cost')
    })
    .createTable(Table.WAREHOUSE_ENTRY_ORDER_PRODUCT, (table) => {
      table.increments('id');
      table
        .integer('warehouse_entry_order_id')
        .unsigned()
        .references('id')
        .inTable(Table.WAREHOUSE_ENTRY_ORDER)
        .onDelete('CASCADE').nullable();
      table
        .integer('material_value_id')
        .unsigned()
        .references('id')
        .inTable(Table.MATERIAL_VALUE)
        .onDelete('CASCADE').nullable();
      table.integer('point')
      table.integer('count')
      table.decimal('price')
      table.decimal('money')
      table.boolean('is_aah').nullable()
      table
        .integer('accounts_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();
      table
        .integer('classification_id')
        .unsigned()
        .references('id')
        .inTable(Table.CLASSIFICATION)
        .onDelete('CASCADE').nullable();
    })
    .createTable(Table.WAREHOUSE_ENTRY_ORDER_FUNCTIONS, (table) => {
      table.increments('id');
      table
        .integer('warehouse_entry_order_id')
        .unsigned()
        .references('id')
        .inTable(Table.WAREHOUSE_ENTRY_ORDER)
        .onDelete('CASCADE').nullable();


      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.WAREHOUSE_ENTRY_ORDER)
}
