import * as Knex from 'knex';
import Table from '../../resources/enums/Table';

export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.CASH_REGISTER, function (table) {
      table.increments('id');
      table.string('code').unique()
      table.string('name');
      table.string('account_id').references('accounts.account').index().onDelete('CASCADE');
      table.boolean('is_main');
      table.string('dmo_n');
      table.string('deo_n');
      table.string('hdm_regis_n')
      table.string('ip')
      table.string('port')
      table.string('password')
      table.string('hdm_non_taxable')
      table.string('hdm_taxable')
      table.string('hdm_print_type')
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.CASH_REGISTER)
}
