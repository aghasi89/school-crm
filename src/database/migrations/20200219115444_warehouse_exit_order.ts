import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.WAREHOUSE_EXIT_ORDER, function (table) {
      table.increments('id');
      table.date('date')
      table
        .integer('warehouse_id')
        .unsigned()
        .references('id')
        .inTable(Table.WAREHOUSE)
        .onDelete('CASCADE').nullable();
      table.integer('document_number').unsigned().unique()
      table
        .string('expense_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();
      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');

      table.text('comment')


      table.text('power_of_attorney')
      table.text('mediator')
      table.text('container')
      table.text('accountant')
      table.text('allow')
      table.text('has_requested')


    })
    .createTable(Table.WAREHOUSE_EXIT_ORDER_PRODUCT, (table) => {
      table.increments('id');
      table
        .integer('warehouse_exit_order_id')
        .unsigned()
        .references('id')
        .inTable(Table.WAREHOUSE_EXIT_ORDER)
        .onDelete('CASCADE').nullable();
      table
        .integer('material_value_id')
        .unsigned()
        .references('id')
        .inTable(Table.MATERIAL_VALUE)
        .onDelete('CASCADE').nullable();
      table.integer('point')
      table.integer('count')
      table.decimal('money')
      table.integer('batch').nullable()

    })
    .createTable(Table.WAREHOUSE_EXIT_ORDER_FUNCTIONS, (table) => {
      table.increments('id');
      table
        .integer('warehouse_exit_order_id')
        .unsigned()
        .references('id')
        .inTable(Table.WAREHOUSE_ENTRY_ORDER)
        .onDelete('CASCADE').nullable();
      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.WAREHOUSE_EXIT_ORDER)
    .dropTable(Table.WAREHOUSE_EXIT_ORDER_PRODUCT)
    .dropTable(Table.WAREHOUSE_EXIT_ORDER_FUNCTIONS)
}
