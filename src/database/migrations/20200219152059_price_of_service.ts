import * as Knex from 'knex';
import Table from '../../resources/enums/Table';

export function up(knex: Knex): Promise<any> {

  return knex.schema
    .createTable(Table.PRICE_OF_SERVICES, function (table) {
      table.increments('id');
      table.date('start_date').notNullable();
      table.date('end_date').notNullable();
      table
      .integer('services_id')
      .unsigned()
      .notNullable()
      .references('id')
      .inTable(Table.SERVICES)
      .onDelete('CASCADE');
      table.text('type').notNullable();

    })
}

export function down(knex: Knex) {
  return knex.schema.dropTable(Table.PRICE_OF_SERVICES);
}
