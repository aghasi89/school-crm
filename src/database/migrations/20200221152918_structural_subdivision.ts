import * as Knex from 'knex';
import Table from '../../resources/enums/Table';

export function up(knex: Knex): Promise<any> {

  return knex.schema
    .createTable(Table.STRUCTURAL_SUBDIVISION, function (table) {
      table.increments('id');
      table.string('code').unique();
      table.string('name');
      table
      .integer('partner_id')
      .unsigned()
      .notNullable()
      .references('id')
      .inTable(Table.PARTNERS)
      .onDelete('CASCADE');
      table
      .string('expense_account_id')
      .references('account')
      .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
      .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema.dropTable(Table.STRUCTURAL_SUBDIVISION);
}