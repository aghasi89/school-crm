import * as Knex from 'knex';
import Table from '../../resources/enums/Table';

export function up(knex: Knex): Promise<any> {

  return knex.schema
    .createTable(Table.BILLING_ACCOUNT_IN_BANKS, function (table) {
      table.increments('id');
      table.string('billing_account');
      table.string('name');
      table
      .integer('currency_id')
      .unsigned()
      .notNullable()
      .references('id')
      .inTable(Table.CURRENCY)
      .onDelete('CASCADE');
      table
      .string('account_id')
      .references('account')
      .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
      .onDelete('CASCADE');
      table.boolean('is_main');
      table.string('number_of_document'),
      table.string('e_services')

    })
}

export function down(knex: Knex) {
  return knex.schema.dropTable(Table.BILLING_ACCOUNT_IN_BANKS);
}