import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.MATERIAL_VALUE_MOVEMENTS, function (table) {
      table.increments('id');
      table.date('date')
      table
        .integer('warehouse_exit_order_id')
        .unsigned()
        .references('id')
        .inTable(Table.WAREHOUSE_EXIT_ORDER)
        .onDelete('CASCADE').nullable();
      table
        .integer('warehouse_entry_order_id')
        .unsigned()
        .references('id')
        .inTable(Table.WAREHOUSE_ENTRY_ORDER)
        .onDelete('CASCADE').nullable();
      table.integer('document_number').unsigned().unique()

      table.string('print_at_sale_prices').nullable()
      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');

      table.text('comment')




      table.text('accountant')
      table.text('mediator')
      table.text('allow')
      table.text('shipper_registration_book_info')

      table.text('output_method')
      table.text('seria')


      table.text('date_of_discharge').nullable()

      table.text('has_requested')

      table.text('additional_comment')

    })
    .createTable(Table.MATERIAL_VALUE_MOVEMENTS_PRODUCT, (table) => {
      table.increments('id');
      table
        .integer('material_movements_id')
        .unsigned()
        .references('id')
        .inTable(Table.MATERIAL_VALUE_MOVEMENTS)
        .onDelete('CASCADE').nullable();
      table
        .integer('material_value_id')
        .unsigned()
        .references('id')
        .inTable(Table.MATERIAL_VALUE)
        .onDelete('CASCADE').nullable();
      table.integer('point')
      table.integer('count')
      table.decimal('money')
      table.integer('batch').nullable()

    })
    .createTable(Table.MATERIAL_VALUE_MOVEMENTS_FUNCTIONS, (table) => {
      table.increments('id');
      table
        .integer('material_movements_id')
        .unsigned()
        .references('id')
        .inTable(Table.MATERIAL_VALUE_MOVEMENTS)
        .onDelete('CASCADE').nullable();
      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.MATERIAL_VALUE_MOVEMENTS)
    .dropTable(Table.MATERIAL_VALUE_MOVEMENTS_PRODUCT)
    .dropTable(Table.MATERIAL_VALUE_MOVEMENTS_FUNCTIONS)
}
