import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.RECEIVED_SERVICES, function (table) {
      table.increments('id');
      table.date('date');
      table.integer('document_number').unsigned().unique();
      
      table
        .integer('currency_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable(Table.CURRENCY)
        .onDelete('CASCADE');
      table.text('currency_exchange_rate_1');
      table.text('currency_exchange_rate_2');
      table.boolean('previous_day_exchange_rate');
      table
        .integer('partners_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable(Table.PARTNERS)
        .onDelete('CASCADE');
      table
        .string('provider_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();
      table
        .string('advance_payment_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table.text('output_method');
      table.integer('purchase_document_number');
      table.date('purchase_document_date');
      table.text('comment');
      table.text('purchase_type_of_service');
      table.text('calculation_type_aah');
      table.boolean('include_aah_in_expense');
      table.text('form_of_reflection_aah');
      table.text('typical_operation')
    })
    .createTable(Table.RECEIVED_SERVICES_DIRECTORY, (table) => {
      table.increments('id');
      table.integer('received_service_id')
        .unsigned()
        .references('id')
        .inTable(Table.RECEIVED_SERVICES)
        .onDelete('CASCADE').nullable();
      table.integer('service_id')
        .unsigned()
        .references('id')
        .inTable(Table.SERVICES)
        .onDelete('CASCADE').nullable();
      table.integer('point');
      table.integer('count');
      table.decimal('price');
      table.decimal('money');
      table.boolean('aah');
      table.text('account').nullable()

    })
    .createTable(Table.RECEIVED_SERVICES_OPERATIONS, (table) => {
      table.increments('id');
      table.integer('received_service_id')
        .unsigned()
        .references('id')
        .inTable(Table.RECEIVED_SERVICES)
        .onDelete('CASCADE').nullable();

      table.integer('operation_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.RECEIVED_SERVICES)
    .dropTable(Table.RECEIVED_SERVICES_DIRECTORY)
    .dropTable(Table.RECEIVED_SERVICES_OPERATIONS)
}