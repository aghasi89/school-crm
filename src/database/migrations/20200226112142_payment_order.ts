import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.PAYMENT_ORDER, function (table) {
      table.increments('id');
      table.date('date')
      table.date('date_of_formulation').nullable()
      table.integer('document_number').unsigned().unique();
      table.string('payment_purpose'),

      table.text('payer_name').nullable()
      table.text('payer_hvhh').nullable()

      table.text('provide_name').nullable()
      table.text('provide_hvhh').nullable()
      table
        .integer('provide_partners_id')
        .unsigned()
        .references('id')
        .inTable(Table.PARTNERS)
        .onDelete('CASCADE').nullable();

      table
        .integer('d_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.BILLING_ACCOUNT_IN_BANKS)
        .onDelete('CASCADE').nullable();
      table.text('d_bank').nullable()
      table
        .integer('d_currency_id')
        .unsigned()
        .references('id')
        .inTable(Table.CURRENCY)
        .onDelete('CASCADE').nullable()

      table
        .integer('c_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.BILLING_ACCOUNT)
        .onDelete('CASCADE').nullable();
      table.text('c_bank').nullable()
      table
        .integer('c_correspondent_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()
      table
        .integer('c_currency_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()


      table.text('amount_currency_1').nullable();
      table.text('amount_currency_2').nullable();

      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');
      table.text('typical_operation').nullable()

    })

    .createTable(Table.PAYMENT_ORDER_FUNCTIONS, (table) => {
      table.increments('id');
      table
        .integer('payment_order_id')
        .unsigned()
        .references('id')
        .inTable(Table.PAYMENT_ORDER)
        .onDelete('CASCADE').nullable();
      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.MATERIAL_VALUE_MOVEMENTS)
    .dropTable(Table.MATERIAL_VALUE_MOVEMENTS_PRODUCT)
    .dropTable(Table.MATERIAL_VALUE_MOVEMENTS_FUNCTIONS)
}
