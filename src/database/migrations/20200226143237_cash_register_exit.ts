import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.CASH_REGISTER_EXIT, function (table) {
      table.increments('id');
      table
        .integer('cash_register_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable(Table.CASH_REGISTER)
        .onDelete('CASCADE');
      table.date('date')
      table.string('document_number')
      table
        .integer('correspondent_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()
      table
        .integer('exit_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()
      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');
      table
        .integer('currency_id')
        .unsigned()
        .references('id')
        .inTable(Table.CURRENCY)
        .onDelete('CASCADE').nullable()
      table.text('amount_currency1').nullable();
      table.text('amount_currency2').nullable();
      table
        .integer('partners_id')
        .unsigned()
        .references('id')
        .inTable(Table.PARTNERS)
        .onDelete('CASCADE').nullable();
      table.text('received').nullable();

      table.text('np_nsh_password').nullable();
      table.text('basis').nullable();
      table.text('appendix').nullable();
      table.text('other_information').nullable()
      table.text('optiona').nullable()
      table.text('typical_operation').nullable()
    })

    .createTable(Table.CASH_REGISTER_EXIT_FUNCTIONS, (table) => {
      table.increments('id');
      table
        .integer('cash_register_exit_id')
        .unsigned()
        .references('id')
        .inTable(Table.CASH_REGISTER_EXIT)
        .onDelete('CASCADE').nullable();
      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.CASH_REGISTER_ENTRY)
    .dropTable(Table.CASH_REGISTER_ENTRY_FUNCTIONS)
}
