import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.PAYMENTS_BILLING_ACCOUNT, function (table) {
      table.increments('id');
      table.date('date');
      table.integer('document_number').unsigned().unique();
      table.date('calculation_date');
      table.string('payment_type').nullable();
      table
        .integer('subdivision_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.SUBDIVISION)
        .onDelete('CASCADE').nullable();
      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');
      table
        .integer('settlement_account_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.BILLING_ACCOUNT_IN_BANKS)
        .onDelete('CASCADE').nullable();


      table.text('comment')


      table.date('date_of_formulation').nullable()
      table
        .integer('correspondent_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()
      table
        .integer('exit_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()



    })
    .createTable(Table.PAYMENTS_BILLING_ACCOUNT_LIST, (table) => {
      table.increments('id');
      table
        .integer('payments_billing_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.PAYMENTS_BILLING_ACCOUNT)
        .onDelete('CASCADE').nullable();
      table
        .integer('subdivision_id')
        .unsigned()
        .references('id')
        .inTable(Table.SUBDIVISION)
        .onDelete('CASCADE').nullable();

      table
        .integer('employee_id')
        .unsigned()
        .references('id')
        .inTable(Table.EMPLOYEE)
        .onDelete('CASCADE').nullable();
      table.decimal('pay_money')
      table.decimal('is_cash')

    })
    .createTable(Table.PAYMENTS_BILLING_ACCOUNT_OPERATIONS, (table) => {
      table.increments('id');
      table
        .integer('payments_billing_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.PAYMENTS_BILLING_ACCOUNT)
        .onDelete('CASCADE').nullable();
      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.PAYMENTS_BILLING_ACCOUNT)
    .dropTable(Table.PAYMENTS_BILLING_ACCOUNT_LIST)
    .dropTable(Table.PAYMENTS_BILLING_ACCOUNT_OPERATIONS)
}
