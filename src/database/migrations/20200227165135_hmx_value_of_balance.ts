import * as Knex from 'knex';
import Table from '../../resources/enums/Table';

export function up(knex: Knex): Promise<any> {

  return knex.schema
    .createTable(Table.HMX_VALUE_OF_BALANCE, function (table) {
      table.increments('id');
      table.string('code').unique();
      table.string('date');
      table.string('value_beginning_of_year');
    })
}

export function down(knex: Knex) {
  return knex.schema.dropTable(Table.FORMULAS);
}