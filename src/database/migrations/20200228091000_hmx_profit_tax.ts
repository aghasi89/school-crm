import * as Knex from 'knex';
import Table from '../../resources/enums/Table';

export function up(knex: Knex): Promise<any> {

  return knex.schema
    .createTable(Table.HMX_PROFIT_TAX, function (table) {
      table.increments('id');
      table.string('code').unique();
      table.string('name');
      table.string('year_price');

    })
}

export function down(knex: Knex) {
  return knex.schema.dropTable(Table.FORMULAS);
}
