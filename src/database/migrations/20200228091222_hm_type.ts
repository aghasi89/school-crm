import * as Knex from 'knex';
import Table from '../../resources/enums/Table';

export function up(knex: Knex): Promise<any> {

  return knex.schema
    .createTable(Table.HM_TYPE, function (table) {
      table.increments('id');
      table.string('code').unique();
      table.string('name');
      table
        .integer('initial_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()
      table
        .integer('stale_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      table.string('usefull_service_duration')
      table
        .integer('hmx_profit_tax_id')
        .unsigned()
        .references('id')
        .inTable(Table.HMX_PROFIT_TAX)
        .onDelete('CASCADE').nullable()

      table
        .integer('parent_id')
        .unsigned()
        .references('id')
        .inTable(Table.HM_TYPE)
        .onDelete('CASCADE').nullable()
    })
}

export function down(knex: Knex) {
  return knex.schema.dropTable(Table.FORMULAS);
}
