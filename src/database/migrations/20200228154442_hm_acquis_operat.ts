import * as Knex from 'knex';
import Table from '../../resources/enums/Table';

export function up(knex: Knex): Promise<any> {
  return knex.schema

    .createTable(Table.HM_ACQUIS_OPERAT, function (table) {
      table.increments('id');
      table.string('name');
      table.integer('for_property_id').unique();
      table.string('full_name');
      table.date('entry_date');
      table.date('operation_date');
      table
        .integer('hm_type_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.HM_TYPE)
        .onDelete('CASCADE');
      table
        .integer('hmx_profit_tax_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable(Table.HMX_PROFIT_TAX)
        .onDelete('CASCADE');
      table.string('hm_acquisition_method');
      table.string('location');
      table
        .integer('partners_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.PARTNERS)
        .onDelete('CASCADE');
      table.string('business_card')
      table.string('document_number')
      table
        .integer('subdivision_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.SUBDIVISION)
        .onDelete('CASCADE');
      table
        .string('subdivision_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();
      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');

      table.string('output_method');
      table.string('purchase_document_info')
      table.string('aah_type')

      table.string('financial_useful_life')
      table.string('financial_calculated_wear')
      table.string('financial_is_worn')
      table.string('finance_residual_value')
      table.string('finance_is_worn')

      table.string('tax_useful_life')
      table.string('tax_initial_cost')
      table.string('tax_calculated_wear')
      table.string('tax_residual_value')
      table.string('tax_is_worn')

      table.string('deferred_revenue_life')
      table.string('calculated_depreciation')
      table.string('deferred_revenue_account')
      table.string('deferred_revenue_worn')

      //account
      table.string('initial_cost_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      table.string('worn_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      table.string('expense_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      table.string('income_statement_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      //currentPortionOfDeferredRevenueAccount
      table.string('cur_p_of_de_re_ac_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      table.string('deferred_revenue_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      //additional
      table.string('startYear')
      table.string('serialNumber')
      table.string('technicalProfile')
      table.string('stamp')
      table.string('briefReview')
      table.string('manufacturer')

    }).createTable(Table.HM_ACQUIS_OPERAT_OPERATIONS, (table) => {
      table.increments('id');
      table
        .integer('hm_acquis_opera_id')
        .unsigned()
        .references('id')
        .inTable(Table.HM_ACQUIS_OPERAT)
        .onDelete('CASCADE').nullable();
      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.SUBSECTION)
    .dropTable(Table.AAH_ACCOUNT_TYPE)
}
