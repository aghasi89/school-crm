import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.TEAR_CALCULATION, function (table) {
      table.increments('id');
      table.date('date');
      table.integer('document_number').unsigned().unique();
      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');
      table.text('comment')
    })
    .createTable(Table.TEAR_CALCULATION_PROPERTY_NUMBER, (table) => {
      table.increments('id');
      table
        .integer('tear_calculation_id')
        .unsigned()
        .references('id')
        .inTable(Table.TEAR_CALCULATION)
        .onDelete('CASCADE').nullable();
      table.text('property_number')
      table.text('name')
      table
        .integer('expense_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();

      table
        .integer('revenue_account_id')
        .unsigned()
        .references('id')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable();

    })
    .createTable(Table.TEAR_CALCULATION_OPERATIONS, (table) => {
      table.increments('id');
      table
        .integer('tear_calculation_id')
        .unsigned()
        .references('id')
        .inTable(Table.TEAR_CALCULATION)
        .onDelete('CASCADE').nullable();
      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.TEAR_CALCULATION)
    .dropTable(Table.TEAR_CALCULATION_PROPERTY_NUMBER)
    .dropTable(Table.TEAR_CALCULATION_OPERATIONS)
}

