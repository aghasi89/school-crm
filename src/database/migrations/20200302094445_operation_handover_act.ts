import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.OPERATION_HANDOVER_ACT, function (table) {
      table.increments('id');
      table.date('date');
      table.integer('document_number').unsigned().unique();
      table.date('calculation_date');
      table.string('payment_type').nullable();
      table.integer('transmitting_unit_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.SUBDIVISION)
        .onDelete('CASCADE').nullable();

      table.integer('transmitting_material_person_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.EMPLOYEE)
        .onDelete('CASCADE').nullable();


      table.integer('receiving_unit_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.SUBDIVISION)
        .onDelete('CASCADE').nullable();

      table.integer('receiving_material_person_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.EMPLOYEE)
        .onDelete('CASCADE').nullable();


      table
        .integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table
        .integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');


      table.text('comment')
    })
    .createTable(Table.OPERATION_HANDOVER_ACT_PROPERTY, (table) => {
      table.increments('id');
      table.integer('op_handover_act_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATION_HANDOVER_ACT)
        .onDelete('CASCADE').nullable();
      table.integer('property_number').unique()
      table.string('name');
      table.string('depreciation_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      table.string('expense_expense_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      table.string('deferred_income_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      table.string('deferred_income_2_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      //currentPortionOfDeferredRevenueAccount
      table.string('income_account_id')
        .references('account')
        .inTable(Table.ACCOUNT_OF_EMPLOYEE_CALCULATIONS)
        .onDelete('CASCADE').nullable()

      table.boolean('is_wear_finance')
      table.boolean('is_wear_fax')
      table.boolean('is_wear_revenue')

    })
    .createTable(Table.OPERATION_HANDOVER_ACT_OPERATIONS, (table) => {
      table.increments('id');
      table
        .integer('op_handover_act_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATION_HANDOVER_ACT)
        .onDelete('CASCADE').nullable();
      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.OPERATION_HANDOVER_ACT)
    .dropTable(Table.OPERATION_HANDOVER_ACT_PROPERTY)
    .dropTable(Table.OPERATION_HANDOVER_ACT_OPERATIONS)
}
