import * as Knex from 'knex';
import Table from '../../resources/enums/Table';


export function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable(Table.HM_REVALUATION, function (table) {
      table.increments('id');
      table.date('date');
      table.integer('document_number').unsigned().unique();
      table.integer('property_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.OPERATION_HANDOVER_ACT_PROPERTY)
        .onDelete('CASCADE').nullable();
      table.string('location');

      table.integer('material_aswer_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.EMPLOYEE)
        .onDelete('CASCADE').nullable();

      table.integer('analitic_group_2_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_2)
        .onDelete('CASCADE');
      table.integer('analitic_group_1_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable(Table.ANALITIC_GROUP_1)
        .onDelete('CASCADE');

      table.text('comment')

      table.decimal('tax_residual_useful_life_old');
      table.decimal('tax_residual_useful_life_new');
      table.decimal('tax_initial_cost_old');
      table.decimal('tax_initial_cost_new');
      table.decimal('tax_calculated_wear_old');
      table.decimal('tax_calculated_wear_new');
      table.decimal('tax_residual_value_old');
      table.decimal('tax_residual_value_new');
      table.decimal('revenue_residual_useful_life_old');
      table.decimal('revenue_residual_useful_life_new');
      table.decimal('revenue_initial_cost_old');
      table.decimal('revenue_initial_cost_new')
      table.decimal('revenue_calculated_wear_old');
      table.decimal('revenue_calculated_wear_new');
      table.decimal('revenue_residual_value_old');
      table.decimal('revenue_residual_value_new');
      table.decimal('finance_residual_useful_life_old');
      table.decimal('finance_residual_useful_life_new');
      table.decimal('finance_initial_cost_old');
      table.decimal('finance_initial_cost_new');
      table.decimal('finance_calculated_wear_old');
      table.decimal('finance_calculated_wear_new');
      table.decimal('finance_residual_value_old');
      table.decimal('finance_residual_value_new')
      
    })
    .createTable(Table.HM_REVALUATION_OPERATIONS, (table) => {
      table.increments('id');
      table
        .integer('hm_revaluation_id')
        .unsigned()
        .references('id')
        .inTable(Table.HM_REVALUATION)
        .onDelete('CASCADE').nullable();
      table.integer('operations_id')
        .unsigned()
        .references('id')
        .inTable(Table.OPERATIONS)
        .onDelete('CASCADE').nullable();
    })
}

export function down(knex: Knex) {
  return knex.schema
    .dropTable(Table.HM_REVALUATION)
    .dropTable(Table.HM_REVALUATION_OPERATIONS)
}
