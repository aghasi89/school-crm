import Partners from '../../models/Partners';
import Subsection from '../../models/Subsection';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import { AahAccountType } from '../../models/AccountProduct';
import Warehouse from '../../models/Warehouse';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';
import {AccountProduct} from '../../models/AccountProduct';
import Operations from '../../models/Operations';

interface AahAccountTypeDetail {
    id?:number;
    name: string
}
interface AccountProductDetail{
    id?:number;
    date:Date;
    documentNumber:string;
    partnersId: number;
    partners?:Partners
    linesCode: string;
    contract: string;
    contractDate: Date;
    subsectionId:number;
    subsections:Subsection
    analiticGroup_2Id:number;
    analiticGroup_1Id:number;
    analiticGroup2:AnaliticGroup2;
    analiticGroup1:AnaliticGroup1;
    seria:string;
    number:string;
    dateWriteOff:string;
    comment:string;
    aahAccountTypeId:number;
    aahAccountType: AahAccountType
    warehouseId:number;
    warehouse: Warehouse;
    address:string;
    aah:string
}
interface AccountProductsOfProductsDetail{
    type:string;
    warehouseId:number;
    code:number;
    name:number;
    point:number;
    count:number;
    price:number;
    money:number
    expenseAccountId:string;
    expenseAccount:AccountOfEmployeeCalculations;
    incomeAccountId:string;
    incomeAccount:AccountOfEmployeeCalculations
    batch:number;
    isAah:number
 
}
interface AccountProductsFunctionsDetail{
  accountProductsId:number;
  accountProducts: AccountProduct;
  operationsId: number;
  operations: Operations
}

export {
    AahAccountTypeDetail,
    AccountProductDetail,
    AccountProductsOfProductsDetail,
    AccountProductsFunctionsDetail
};