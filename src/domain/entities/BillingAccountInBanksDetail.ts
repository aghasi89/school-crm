import Currency from '../../models/Currency';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';

export interface BillingAccountInBanksDetail {
    id?: number;
    billingAccount:string;
    name: string;
    currencyId: number;
    currencies?: Currency;
    accountId:number;
    accounts?: AccountOfEmployeeCalculations;
    isMain: boolean;
    numberOfDocument:string;
    eServices: string
}