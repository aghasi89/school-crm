import CashRegister from '../../models/CashRegister';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import Currency from '../../models/Currency';
import Partners from '../../models/Partners';
interface CashRegisterEntryDetail {
    id?:number;
    cashRegisterId:number;
    cashRegister?: CashRegister;
    date: Date;
    hdmN: string;
    documentNumber:string;
    correspondentAccountId: number;
    accounts?: AccountOfEmployeeCalculations
    entryAccountId: number;
    analiticGroup_1Id: number;
    analiticGroup1?: AnaliticGroup1
    analiticGroup_2Id:number;
    analiticGroup2?:AnaliticGroup2;
    currencyId: number;
    currencies?:Currency;
    amountCurrency1:string;
    amountCurrency2:string;
    partnersId: number;
    partners?: Partners;
    received:string;
    basis:string;
    attached: string;
    optiona:string;
    typicalOperation: string
 }
 
 
 interface CashRegisterEntryFunctionsDetail {
     cashRegisterEntryId?: number;
     operationsId: number
 
 }
 
 export { 
     CashRegisterEntryDetail,
     CashRegisterEntryFunctionsDetail
 };