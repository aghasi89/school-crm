import HmxProfitTax from '../../models/HmxProfitTax';
import HmType from '../../models/HmType';
import Partners from '../../models/Partners';
import Subdivision from '../../models/Subdivision';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';


interface HmAcquisOperatDetail {
    id:number;
    name: string;
    forPropertyId: number;
    fullName: string;
    entryDate: Date;
    operationDate: Date;
    hmTypeId: number;
    hmType?:HmType;
    hmxProfitTaxId: number;
    hmxProfitTax?: HmxProfitTax;
    hmAcquisitionMethod: string;
    location: string;
    partnersId: number;
    partners?: Partners;
    businessCard: string;
    documentNumber: string;
    subdivisionId: number;
    subdivision?:Subdivision;
    subdivisionAccountId: number;
    subdivisionAccount?: Subdivision;
    analiticGroup_1Id: number;
    analiticGroup1?: AnaliticGroup1;
    analiticGroup_2Id: number;
    analiticGroup2?: AnaliticGroup2;
    outputMethod: string;
    purchaseDocumentInfo: string;
    aahType: string;
    financialUsefulLife: string;
    financialCalculatedWear: string;
    financialIsWorn: string;
    financeResidualValue: string;
    financeIsWorn: string;
    taxUsefulLife: string;
    taxInitialCost: string;
    taxCalculatedWear: string;
    taxResidualValue: string;
    taxIsWorn: string;
    deferredRevenueLife: string;
    calculatedDepreciation: string;
    deferredRevenueAccount: string;
    deferredRevenueWorn: string;
    initialCostAccountId: number;
    initialCostAccount?:AccountOfEmployeeCalculations;
    wornAccountId: number;
    wornAccount?: AccountOfEmployeeCalculations;
    expenseAccountId: number;
    expenseAccount?:AccountOfEmployeeCalculations;
    incomeStatementId: number;
    incomeStatement?: AccountOfEmployeeCalculations;
    curPortionOfDeferredRevAccountId: number;
    curPortionOfDeferredRevAccount?: AccountOfEmployeeCalculations;
    deferredRevenueAccountId: number;
    deferredRevenueAccounts?: AccountOfEmployeeCalculations;
    startYear: string;
    serialNumber: string;
    technicalProfile: string;
    stamp: string;
    briefReview: string;
    manufacturer: string
}


interface HmAcquisOperatOperationsDetail {
    HmAcquisOperatId: number;
    operationsId: number;

}

export {
    HmAcquisOperatDetail,
    HmAcquisOperatOperationsDetail
};