import { OperationsHandoverActProperty } from '../../models/OperationsHandoverAct';
import Employee from '../../models/Employee';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import { HmReconstruction } from '../../models/HmReconstruction';

interface HmReconstructionDetail {
    id:number;
    date: Date;
    documentNumber: number;
    propertyId:number;
    property?: OperationsHandoverActProperty;
    location: string;
    materialAnswerId: number;
    materialAnswer?:Employee;
    analiticGroup_1Id: number;
    analiticGroup1?: AnaliticGroup1;
    analiticGroup_2Id:number;
    analiticGroup2?: AnaliticGroup2;
    comment: string;
    taxResidualUsefulLifeOld:string;
    taxResidualUsefulLifeNew:string;
    taxInitialCostOld:string;
    taxInitialCostNew:string;
    revenueResidualUsefulLifeOld:string;
    revenueResidualUsefulLifeNew:string;
    revenueInitialCostOld:string;
    revenueInitialCostNew:string;
    financeResidualUsefulLifeOld:string;
    financeResidualUsefulLifeNew:string;
    financeInitialCostOld:string;
    financeInitialCostNew:string;
 }
 
 
 interface HmReconstructionOperationsDetail {
    hmReconstructionId?: number;
    hmReconstruction: HmReconstruction;
    operationsId: number
 }
 
 export { 
    HmReconstructionDetail,
    HmReconstructionOperationsDetail
 };