import { OperationsHandoverActProperty } from '../../models/OperationsHandoverAct';
import Employee from '../../models/Employee';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import AnaliticGroup1 from '../../models/AnaliticGroup1';

interface HmRevaluationDetail {
    id:number;
    date: Date;
    documentNumber: number;
    propertyId:number;
    property?: OperationsHandoverActProperty;
    location: string;
    materialAnswerId: number;
    materialAnswer?:Employee;
    analiticGroup_1Id: number;
    analiticGroup1?: AnaliticGroup1;
    analiticGroup_2Id:number;
    analiticGroup2?: AnaliticGroup2;
    comment: string;
    taxResidualUsefulLifeOld:number;
    taxResidualUsefulLifeNew:number;
    taxInitialCostOld:number;
    taxInitialCostNew:number;
    taxCalculatedWearOld:number;
    taxCalculatedWearNew:number;
    taxResidualValueOld:number;
    taxResidualValueNew:number;
    revenueResidualUsefulLifeOld:number;
    revenueResidualUsefulLifeNew:number;
    revenueInitialCostOld:number;
    revenueInitialCostNew:number;
    revenueCalculatedWearOld:number;
    revenueCalculatedWearNew:number;
    revenueResidualValueOld:number;
    revenueResidualValueNew:number;
    financeResidualUsefulLifeOld:number;
    financeResidualUsefulLifeNew:number;
    financeInitialCostOld:number;
    financeInitialCostNew:number;
    financeCalculatedWearOld:number;
    financeCalculatedWearNew:number;
    financeResidualValueOld:number;
    financeResidualValueNew:number
 }
 
 
 interface HmRevaluationOperationsDetail {
    hmRevaluationId?: number;
     operationsId: number;
 
 }
 
 export { 
    HmRevaluationDetail,
    HmRevaluationOperationsDetail
 };