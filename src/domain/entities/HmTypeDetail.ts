
/**
 * HmTypeDetail interface.
 */
interface HmTypeDetail {
    id ?:  number,
    code  :string;
    name: string;
    initialAccountId : number;
    staleAccountId : number;
    usefullServiceDuration : string;
    hmxProfitTaxId : number;
    parentId  : number;
}

export default HmTypeDetail;
