/**
 * HmxValueOfBalanceDetail interface.
 */
interface HmxValueOfBalanceDetail {
    id ?:  number,
    code  :string;
    date: Date;
    valueBeginningOfYear  : string;
}

export default HmxValueOfBalanceDetail;