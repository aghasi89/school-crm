/**
 * MaterialValueDetail Interface.
 */
interface MaterialValueDetail {
    id?: number;
    name : string;
    measurementUnitId: number;
    classificationId : number;
    accountId :number;
    wholesalePrice : number;
    retailerPrice : number;
    // currencyId : number;
    // wholesalePriceCurrency : number;
    characteristic : string;
    barCode : number;
    externalCode : number;
    hcbCoefficient : number;
    billingMethodId : number;
    isAah : boolean;
    salesRevenueAccountId : number;
    retailRevenueAccountId : number;
    salesExpenseAccountId : number;
    materialValueGroupId : number;
    measurementUnit?:any;
    classification?:any;
    account?:any;
    billingMethod?:any;
    salesRevenue?:any;
    retailRevenue?:any;
    salesExpense?:any;
    materialValueGroup?:any;
    // expense : string;
    // weighted_average  :string;
}

export default MaterialValueDetail;
