import { WarehouseExitOrder } from '../../models/WarehouseExitOrder';
import { WarehouseEntryOrder } from '../../models/WarehouseEntryOrder';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';

interface MaterialValueMovementsDetail {
    id:number;
    date: Date;
    warehouseExitOrderId: number;
    warehouseExitOrder?: WarehouseExitOrder;
    warehouseEntryOrderId: number;
    warehouseEntryOrder?:WarehouseEntryOrder;
    printAtSalePrices: string;
    analiticGroup_1Id: number;
    analiticGroup1?:AnaliticGroup1;
    analiticGroup_2Id: number;
    analiticGroup2?: AnaliticGroup2;
    comment: string;
    accountant: string;
    mediator: string;
    allow: string;
    shipperRegistrationBookInfo: string;
    outputMethod: string;
    seria: string;
    dateOfDischarge: string;
    hasRequested: string;
    additionalComment: string;
}

interface MaterialValueMovementsProductDetail {
    materialValueMovementsId: number;
    materialValueId: number;
    point: number;
    count: number;
    money: number;
    batch: number

}
interface MaterialValueMovementsFunctionsDetail {
    materialValueMovementsId: number;
    operationsId: number;

}

export {
    MaterialValueMovementsDetail,
    MaterialValueMovementsProductDetail,
    MaterialValueMovementsFunctionsDetail
};