import Subdivision from '../../models/Subdivision';
import Employee from '../../models/Employee';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';

interface OperationsHandoverActDetail {
    id: number;
    date: Date;
    documentNumber: number;
    calculationDate : Date;
    paymentType: string;
    transmittingUnitId: number;
    transmittingUnit?: Subdivision;
    transmittingMaterialPersonId: number;
    transmittingMaterialPerson?:Employee;
    receivingUnitId:number;
    receivingUnit?: Subdivision;
    receivingMaterialPersonId:number;
    receivingMaterialPerson?:Employee;
    analiticGroup_1Id: number;
    analiticGroup1?: AnaliticGroup1;
    analiticGroup_2Id: number;
    analiticGroup2: AnaliticGroup2;
    comment: string
}


interface OperationsHandoverActPropertyDetail {
    opHandoverActId?:number;
    propertyNumber: number;
    name: string;
    depreciationAccountId: number;
    depreciationAccount?: AccountOfEmployeeCalculations;
    expenseAccountId: number;
    expenseAccount?: AccountOfEmployeeCalculations;
    deferredIncomeId: number;
    deferredIncome?:AccountOfEmployeeCalculations;
    deferredIncome_2Id: number;
    deferredIncome2?:AccountOfEmployeeCalculations;
    incomeAccountId:number;
    incomeAccount?: AccountOfEmployeeCalculations;
    isWearFinance:boolean;
    isWearFax: boolean;
    isWearRevenue:boolean
}


interface OperationsHandoverActOperationsDetail {
    opHandoverActId?: number;
    operationsId: number

}

export { 
    OperationsHandoverActDetail,
    OperationsHandoverActPropertyDetail,
    OperationsHandoverActOperationsDetail
};