import Subdivision from '../../models/Subdivision';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import BillingAccountInBanks from '../../models/BillingAccountInBanks';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';
import Employee from '../../models/Employee';


interface PaymentBillingAccountDetail {
    id: number;
    date: Date;
    documentNumber: number;
    calculationDate: Date;
    paymentType: string;
    subdivisionId: number;
    subdivision?: Subdivision;
    analiticGroup_1Id: number;
    analiticGroup1?: AnaliticGroup1;
    analiticGroup_2Id: number;
    analiticGroup2?:AnaliticGroup2;
    settlementAccountId: number;
    settlementAccount?:BillingAccountInBanks;
    comment: string;
    dateOfFormulation: Date;
    correspondentAccountId: number;
    correspondentAccount?: AccountOfEmployeeCalculations;
    exitAccountId: number;
    exitAccount?:AccountOfEmployeeCalculations
}

interface PaymentBillingAccountListDetail {
    paymentBillingAccountId: number;
    subdivisionId: number;
    subdivision?: Subdivision;
    employeeId: number;
    employee?: Employee;
    payMoney: string;
    isCash: string;
}
interface PaymentBillingAccountOperationsDetail {
    paymentBillingAccountId: number;
    operationsId: number;

}

export {
    PaymentBillingAccountDetail,
    PaymentBillingAccountListDetail,
    PaymentBillingAccountOperationsDetail
};