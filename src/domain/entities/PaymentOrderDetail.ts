import Partners from '../../models/Partners';
import BillingAccountInBanks from '../../models/BillingAccountInBanks';
import Currency from '../../models/Currency';
import BillingAccount from '../../models/BillingAccount';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';
import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';

interface PaymentOrderDetail {
    id?:number;
    date: Date;
    dateOfFormulation: Date;
    payerName:string;
    payerHvhh: string;
    provideName: string;
    provideHvhh: string;
    providePartnersId: number;
    providerPartners?: Partners;
    dAccountId:number;
    dAccount?: BillingAccountInBanks;
    dCurrencyId:number;
    dCurreny?: Currency;
    cAccountId:number;
    cAccount?: BillingAccount;
    cCorrespondentAccountId:number;
    cCorrespondentAccount?: AccountOfEmployeeCalculations;
    cCurrencyAccountId:number;
    cCurrencyAccount?:AccountOfEmployeeCalculations;
    amountCurrency_1:string;
    amountCurrency_2:string;
    analiticGroup_1Id: number;
    analiticGroup1: AnaliticGroup1;
    analiticGroup_2Id:number;
    analiticGroup2:AnaliticGroup2;
    typicalOperation: string
 }
 
 
 interface PaymentOrderFunctionsDetail {
     id?:number;
     paymentOrderId?: number;
     operationsId: number;
 
 }
 
 export { 
     PaymentOrderDetail,
     PaymentOrderFunctionsDetail
 };