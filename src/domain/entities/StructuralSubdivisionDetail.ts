import Partners from '../../models/Partners';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';


export interface StructuralSubdivisionDetail {
    id?: number;
    code:string;
    name:string;
    partnerId: number ;
    partners?: Partners;
    expenseAccountId: number;
    expenseAccount?: AccountOfEmployeeCalculations
}