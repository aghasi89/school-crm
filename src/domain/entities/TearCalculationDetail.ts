import AnaliticGroup1 from '../../models/AnaliticGroup1';
import AnaliticGroup2 from '../../models/AnaliticGroup2';
import AccountOfEmployeeCalculations from '../../models/AccountOfEmployeeCalculations';

interface TearCalculationDetail {
    id:number;
    date: Date;
    documentNumber: number;
    analiticGroup_1Id: number;
    analiticGroup1?: AnaliticGroup1;
    analiticGroup_2Id: number;
    analiticGroup2?: AnaliticGroup2;
    comment: string;
}

interface TearCalculationPropertyNumberDetail {
    tearCalculationId: number;
    propertyNumber: string;
    name: string;
    expenseAccountId: number;
    expenseAccount?: AccountOfEmployeeCalculations;
    revenueAccountId: number;
    revenueAccount?:AccountOfEmployeeCalculations
}
interface TearCalculationFunctionsDetail {
    tearCalculationId: number;
    operationsId: number;

}

export {
    TearCalculationDetail,
    TearCalculationPropertyNumberDetail,
    TearCalculationFunctionsDetail
};