interface CashRegisterExitPayload {
    cashRegisterId:number;
    date: Date;
    documentNumber:string;
    correspondentAccountId: number;
    exitAccountId: number;
    analiticGroup_1Id: number;
    analiticGroup_2Id:number;
    currencyId: number;
    amountCurrency1:string;
    amountCurrency2:string;
    partnersId: number;
    received:string;
    npNshPassword:string;
    basis:string;
    appendix: string;
    otherInformation:string;
    optiona:string;
    typicalOperation: string
 }
 
 
 interface CashRegisterExitFunctionsPayload {
     cashRegisterExitId?: number;
     operationsId: number;
 
 }
 
 export { 
     CashRegisterExitPayload,
     CashRegisterExitFunctionsPayload
 };