/**
 * CashRegisterPayload Interface.
 */
interface CashRegisterPayload {
    code:number;
    name : string;
    accountId :number;
    isMain : boolean;
    dmoN : string;
    deoN : string;
    hdmRegisN : string;
    ip : string;
    port : string;
    password : string;
    hdmNonTaxable: string;
    hdmTaxable : string;
    hdmPrintType : string;

}

export default CashRegisterPayload;
