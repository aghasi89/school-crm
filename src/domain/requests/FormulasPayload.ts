

interface FormulasPayload {
   code:string;
   caption:string;
   expression:string
}

export default FormulasPayload;