interface HmAcquisOperatPayload {
    name: string;
    forPropertyId: number;
    fullName: string;
    entryDate: Date;
    operationDate: Date;
    hmTypeId: number;
    hmxProfitTaxId: number;
    hmAcquisitionMethod: string;
    location: string;
    partnersId: number;
    businessCard: string;
    documentNumber: string;
    subdivisionId: number;
    subdivisionAccountId: number;
    analiticGroup_1Id: number;
    analiticGroup_2Id: number;
    outputMethod: string;
    purchaseDocumentInfo: string;
    aahType: string;
    financialUsefulLife: string;
    financialCalculatedWear: string;
    financialIsWorn: string;
    financeResidualValue: string;
    financeIsWorn: string;
    taxUsefulLife: string;
    taxInitialCost: string;
    taxCalculatedWear: string;
    taxResidualValue: string;
    taxIsWorn: string;
    deferredRevenueLife: string;
    calculatedDepreciation: string;
    deferredRevenueAccount: string;
    deferredRevenueWorn: string;
    initialCostAccountId: number;
    wornAccountId: number;
    expenseAccountId: number;
    incomeStatementId: number;
    curPortionOfDeferredRevAccountId: number;
    deferredRevenueAccountId: number;
    startYear: string;
    serialNumber: string;
    technicalProfile: string;
    stamp: string;
    briefReview: string;
    manufacturer: string
}


interface HmAcquisOperatOperationsPayload {
    HmAcquisOperatId: number;
    operationsId: number;

}

export {
    HmAcquisOperatPayload,
    HmAcquisOperatOperationsPayload
};