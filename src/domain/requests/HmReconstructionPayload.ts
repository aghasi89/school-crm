interface HmReconstructionPayload {
    date: Date;
    documentNumber: number;
    propertyId:number;
    location: string;
    materialAnswerId: number;
    analiticGroup_1Id: number;
    analiticGroup_2Id:number;
    comment: string;
    taxResidualUsefulLifeOld:string;
    taxResidualUsefulLifeNew:string;
    taxInitialCostOld:string;
    taxInitialCostNew:string;
    revenueResidualUsefulLifeOld:string;
    revenueResidualUsefulLifeNew:string;
    revenueInitialCostOld:string;
    revenueInitialCostNew:string;
    financeResidualUsefulLifeOld:string;
    financeResidualUsefulLifeNew:string;
    financeInitialCostOld:string;
    financeInitialCostNew:string;
 }
 
 
 interface HmReconstructionOperationsPayload {
     hmReconstructionId?: number;
     operationsId: number;
 
 }
 
 export { 
    HmReconstructionPayload,
    HmReconstructionOperationsPayload
 };