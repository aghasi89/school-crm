interface HmRevaluationPayload {
    date: Date;
    documentNumber: number;
    propertyId:number;
    location: string;
    materialAnswerId: number;
    analiticGroup_1Id: number;
    analiticGroup_2Id:number;
    comment: string;
    taxResidualUsefulLifeOld:number;
    taxResidualUsefulLifeNew:number;
    taxInitialCostOld:number;
    taxInitialCostNew:number;
    taxCalculatedWearOld:number;
    taxCalculatedWearNew:number;
    taxResidualValueOld:number;
    taxResidualValueNew:number;
    revenueResidualUsefulLifeOld:number;
    revenueResidualUsefulLifeNew:number;
    revenueInitialCostOld:number;
    revenueInitialCostNew:number;
    revenueCalculatedWearOld:number;
    revenueCalculatedWearNew:number;
    revenueResidualValueOld:number;
    revenueResidualValueNew:number;
    financeResidualUsefulLifeOld:number;
    financeResidualUsefulLifeNew:number;
    financeInitialCostOld:number;
    financeInitialCostNew:number;
    financeCalculatedWearOld:number;
    financeCalculatedWearNew:number;
    financeResidualValueOld:number;
    financeResidualValueNew:number
 }
 
 
 interface HmRevaluationOperationsPayload {
    hmRevaluationId?: number;
     operationsId: number;
 
 }
 
 export { 
    HmRevaluationPayload,
    HmRevaluationOperationsPayload
 };