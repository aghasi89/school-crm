/**
 * HmTypePayload interface.
 */
interface HmTypePayload {
    id ?:  number,
    code  :string;
    name: string;
    initialAccountId : number;
    staleAccountId : number;
    usefullServiceDuration : string;
    hmxProfitTaxId : number;

    parentId  : number;
}

export default HmTypePayload;
