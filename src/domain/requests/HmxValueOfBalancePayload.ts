/**
 * HmxValueOfBalancePayload interface.
 */
interface HmxValueOfBalancePayload {
    code  :string;
    date: Date;
    valueBeginningOfYear  : string;
}

export default HmxValueOfBalancePayload;