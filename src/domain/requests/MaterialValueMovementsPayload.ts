interface MaterialValueMovementsPayload {
    date: Date;
    warehouseExitOrderId: number;
    warehouseEntryOrderId: number;
    printAtSalePrices: string;
    analiticGroup_1Id: number;
    analiticGroup_2Id: number;
    comment: string;
    accountant: string;
    mediator: string;
    allow: string;
    shipperRegistrationBookInfo: string;
    outputMethod: string;
    seria: string;
    dateOfDischarge: string;
    hasRequested: string;
    additionalComment: string;
}

interface MaterialValueMovementsProductPayload {
    materialValueMovementsId: number;
    materialValueId: number;
    point: number;
    count: number;
    money: number;
    batch: number

}
interface MaterialValueMovementsFunctionsPayload {
    materialValueMovementsId: number;
    operationsId: number;

}

export {
    MaterialValueMovementsPayload,
    MaterialValueMovementsProductPayload,
    MaterialValueMovementsFunctionsPayload
};