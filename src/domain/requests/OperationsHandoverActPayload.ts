interface OperationsHandoverActPayload {
    date: Date;
    documentNumber: number;
    calculationDate : Date;
    paymentType: string;
    transmittingUnitId: number;
    transmittingMaterialPersonId: number;
    receivingUnitId:number;
    receivingMaterialPersonId:number;
    analiticGroup_1Id: number;
    analiticGroup_2Id: number;
    comment: string
}


interface OperationsHandoverActPropertyPayload {
    opHandoverActId?:number;
    propertyNumber: number;
    name: string;
    depreciationAccountId: number;
    expenseAccountId: number;
    deferredIncomeId: number;
    deferredIncome_2Id: number;
    incomeAccountId:number;
    isWearFinance:boolean;
    isWearFax: boolean;
    isWearRevenue:boolean
}


interface OperationsHandoverActOperationsPayload {
    opHandoverActId?: number;
    operationsId: number

}

export { 
    OperationsHandoverActPayload,
    OperationsHandoverActPropertyPayload,
    OperationsHandoverActOperationsPayload
};