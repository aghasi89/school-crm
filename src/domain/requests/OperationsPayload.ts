interface OperationsPayload {
    debitId: number;
    dPartnersId: number;
    dAnaliticGroup_2Id: number;
    dAnaliticGroup_1Id: number;
    creditId: number;
    cPartnersId: number;
    cAnaliticGroup_2Id: number;
    cAnaliticGroup_1Id: number;
    money: number;
    comment : string;
}

export default OperationsPayload