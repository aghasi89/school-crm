interface PaymentBillingAccountPayload {
    date: Date;
    documentNumber: number;
    calculationDate: Date;
    paymentType: string;
    subdivisionId: number;
    analiticGroup_1Id: number;
    analiticGroup_2Id: number;
    settlementAccountId: number;
    comment: string;
    dateOfFormulation: Date;
    correspondentAccountId: number;
    exitAccountId: number;
}

interface PaymentBillingAccountListPayload {
    paymentBillingAccountId: number;
    subdivisionId: number;
    employeeId: number;
    payMoney: string;
    isCash: string;
}
interface PaymentBillingAccountOperationsPayload {
    paymentBillingAccountId: number;
    operationsId: number;

}

export {
    PaymentBillingAccountPayload,
    PaymentBillingAccountListPayload,
    PaymentBillingAccountOperationsPayload
};