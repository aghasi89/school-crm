

import OperationsPayload from './OperationsPayload'

interface PaymentOrderPayload {
    main : {
        date: Date;
        dateOfFormulation: Date;
        payerName:string;
        payerHvhh: string;
        provideName: string;
        provideHvhh: string;
        providePartnersId: number;
        dAccountId:number;
        dCurrencyId:number;
        cAccountId:number;
        cCorrespondentAccountId:number;
        cCurrencyAccountId:number;
        amountCurrency_1:string;
        amountCurrency_2:string;
        analiticGroup_1Id: number;
        analiticGroup_2Id:number;
        typicalOperation: string
    },
    paymentOrderOperation : Array<OperationsPayload>


 }
 
 
 interface PaymentOrderFunctionsPayload {
     paymentOrderId?: number;
     operationsId: number;
 
 }
 
 export { 
     PaymentOrderPayload,
     PaymentOrderFunctionsPayload
 };