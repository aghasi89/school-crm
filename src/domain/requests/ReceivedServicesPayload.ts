interface ReceivedServicesPayload {
    date: Date;
    documentNumber : number;
    currencyId: number;
    currencyExchangeRate1: string;
    currencyExchangeRate2 : string;
    previousDayExchangeRate: boolean;
    partnersId: number;
    providerAccountId: string;
    advancePaymentAccountId:string;
    analiticGroup_1Id: number;
    analiticGroup_2Id: number;
    outputMethod: string;
    purchaseDocumentNumber:number;
    purchaseDocumentDate: Date
    comment: string;
    purchaseTypeOfService: string;
    calculationTypeAah: string;
    includeAahInExpense: boolean;
    formOfReflectionAah: string;
    typicalOperation: string;
    receivedServicesDirectory:Array<ReceivedServicesDirectoryPayload>;
    receivedServicesOperations:Array<ReceivedServicesOperationsPayload>
}

/**
 * WarehouseExitOrderPayload Interface.
 */
interface ReceivedServicesDirectoryPayload {
    receivedServiceId?:number;
    serviceId: number;
    point: number;
    count: number;
    price: number;
    money: number;
    aah: boolean;
    account:string
}

/**
 * WarehouseExitOrderFunctionsPayload Interface.
 */
interface ReceivedServicesOperationsPayload {
    receivedServiceId?: number;
    operationsId: number

}

export { 
    ReceivedServicesPayload,
    ReceivedServicesDirectoryPayload,
    ReceivedServicesOperationsPayload
};
