/**
 * ServicesPayload Interface.
 */
interface ServicesPayload {
    code:number;
    fullName:string;
    name : string;
    measurementUnitId: number;
    classificationId : number;
    accountId :number;
    wholesalePrice : number;
    retailerPrice : number;
    barCode : number;
    isAah : boolean;
    measurementUnit?:any;
    classification?:any;
    account?:any;
}

export default ServicesPayload;
