
interface StructuralSubdivisionPayload {
    code:string;
    name:string;
    partnerId: number ;
    expenseAccountId: number
}

export default StructuralSubdivisionPayload;