interface TearCalculationPayload {
    date: Date;
    documentNumber: number;
    analiticGroup_1Id: number;
    analiticGroup_2Id: number;
    comment: string;
}

interface TearCalculationPropertyNumberPayload {
    tearCalculationId: number;
    propertyNumber: string;
    name: string;
    expenseAccountId: number;
    revenueAccountId: number
}
interface TearCalculationFunctionsPayload {
    tearCalculationId: number;
    operationsId: number;

}

export {
    TearCalculationPayload,
    TearCalculationPropertyNumberPayload,
    TearCalculationFunctionsPayload
};