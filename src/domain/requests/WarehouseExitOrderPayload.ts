/**
 * WarehouseExitOrderPayload Interface.
 */
interface WarehouseExitOrderPayload {
    date: Date;
    warehouse_id: number;
    expenseAccountId: number;
    documentNumber : number
    analiticGroup_2Id: number;
    analiticGroup_1Id: number;
    comment: string;
    powerOfAttorney: string;
    mediator: string;
    container: string;
    accountant: string;
    allow: string;
    hasRequested: string;
    warehouseExitOrderProduct ? : any;
    warehouseExitOrderFunctions ? :any;
}

/**
 * WarehouseExitOrderPayload Interface.
 */
interface WarehouseExitOrderProductPayload {
    warehouseExitOrderId?: number;
    materialValueId: number;
    point: number;
    count: number;
    price: number;
    money: number;
    batch: number;
}

/**
 * WarehouseExitOrderFunctionsPayload Interface.
 */
interface WarehouseExitOrderFunctionsPayload {
    warehouseExitOrderId?: number;
    debitId: number;
    dPartnersId: number;
    dAnaliticGroup_2Id: number;
    dAnaliticGroup_1Id: number;
    creditId: number;
    cPartnersIdisAah: number;
    cAnaliticGroup_2Id: number;
    cAnaliticGroup_1Id: number;
    money: number;
    comment : string;

}

export { 
    WarehouseExitOrderPayload,
    WarehouseExitOrderProductPayload,
    WarehouseExitOrderFunctionsPayload
};
