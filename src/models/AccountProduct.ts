import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Subsection from './Subsection';
import Warehouse from './Warehouse'
import Operations from './Operations'


class AccountProductsFunctions extends bookshelf.Model<AccountProductsFunctions> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.ACCOUNT_PRODUCTS_OF_PRODUCTS;
    }
    accountProducts(): AccountProduct {
        return this.belongsTo(AccountProduct, 'account_products_id')
    }
    operations(): Operations {
        return this.belongsTo(Operations, 'operations_id')
    }
}

class AccountProductsOfProducts extends bookshelf.Model<AccountProductsOfProducts> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.ACCOUNT_PRODUCTS_OF_PRODUCTS;
    }
    expenseAccount(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'expense_account')
    }
    incomeAccount(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'income_account')
    }
}

class AccountProduct extends bookshelf.Model<AccountProduct> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.ACCOUNT_PRODUCTS;
    }
    partners(): Partners {
        return this.belongsTo(Partners, 'partners_id')
    }
    subsections(): Subsection {
        return this.belongsTo(Subsection, 'subsection_id')
    }

    analiticGroup1(): AnaliticGroup1 {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id')
    }
    analiticGroup2(): AnaliticGroup2 {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id')
    }
    aahAccountTypes(): AahAccountType{
        return this.belongsTo(AahAccountType, 'aah_account_type_id')
    }
    warehouse():Warehouse{
        return this.belongsTo(Warehouse, 'warehouse_id')
    }
}



class AahAccountType extends bookshelf.Model<AahAccountType> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.AAH_ACCOUNT_TYPE;
    }
}

export {
    AahAccountType,
    AccountProduct,
    AccountProductsOfProducts,
    AccountProductsFunctions
};