import bookshelf from '../config/bookshelf';
import Currency from './Currency';
import Table from '../resources/enums/Table';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';


class BillingAccountInBanksMethod extends bookshelf.Model<BillingAccountInBanksMethod> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.BILLING_ACCOUNT_IN_BANKS;
    }

    currencies(): Currency {
        return this.belongsTo(Currency, 'currency_id');
    }
    accounts(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'account_id','account');
    }
}

export default BillingAccountInBanksMethod;