import bookshelf from '../config/bookshelf';

import Table from '../resources/enums/Table';

import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'


class CashRegister extends bookshelf.Model<CashRegister> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.CASH_REGISTER;
  }

  account():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'account_id')
  }
}

export default CashRegister;
