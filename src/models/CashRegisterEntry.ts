import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Currency from './Currency';
import CashRegister from './CashRegister';
import Operations from './Operations'

class CashRegisterEntryFunctions extends bookshelf.Model<CashRegisterEntryFunctions> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.CASH_REGISTER_ENTRY_FUNCTIONS;
    }
    operations(): Operations {
        return this.belongsTo(Operations, 'operations_id')
    }
}



class CashRegisterEntry extends bookshelf.Model<CashRegisterEntry> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.CASH_REGISTER_ENTRY;
    }

    cashRegister(): CashRegister {
        return this.belongsTo(CashRegister, 'cash_register_id')
    }
    correspondentAccount(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'correspondent_account_id')
    }
    entryAccount(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'entry_account_id')
    }
    analiticGroup2(): AnaliticGroup2 {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id')
    }

    analiticGroup1(): AnaliticGroup1 {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id')
    }
    currencies(): Currency {
        return this.belongsTo(Currency, 'currency_id')
    }
    partners(): Partners {
        return this.belongsTo(Partners, 'partners_id')
    }



}

export {
    CashRegisterEntry,
    CashRegisterEntryFunctions
};