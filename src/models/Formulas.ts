import bookshelf from '../config/bookshelf';

import Table from '../resources/enums/Table';

class FormulasMethod extends bookshelf.Model<FormulasMethod> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.FORMULAS;
  }
}

export default FormulasMethod;