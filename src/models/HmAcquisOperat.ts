import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Operations from './Operations'
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';
import Subdivision from './Subdivision';
import Partners from './Partners';
import HmxProfitTax from './HmxProfitTax';
import HmType from './HmType';

class HmAcquisOperat extends bookshelf.Model<HmAcquisOperat> {
    get requireFetch() {
        return false;
    }
    get tableName(): string {
        return Table.HM_ACQUIS_OPERAT;
    }
    hmType():HmType{
        return this.belongsTo(HmType, 'hm_type_id')
    }
    hmxProfitTax(): HmxProfitTax{
        return this.belongsTo(HmxProfitTax, 'hmx_profit_tax_id')
    }
    partners(): Partners{
        return this.belongsTo(Partners, 'partners_id')
    }
    subdivision():Subdivision{
        return this.belongsTo(Subdivision, 'subdivision_id')
    }
    subdivisionAccount(): AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations, 'subdivision_account_id')
    }
    analiticGroup2(): AnaliticGroup2 {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id')
    }
    analiticGroup1(): AnaliticGroup1 {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id')
    }
    initialCostAccount(): AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations, 'initial_cost_account_id')
    }
    wornAccount():AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations, 'worn_account_id')
    }
    expenseAccount():AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations, 'expense_account_id')
    }
    incomeStatatement():AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations, 'income_statement_id')
    }
    currentPortionOfDeferredRevenueAccount():AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations, 'cur_p_of_de_re_ac_id')
    }
    deferredRevenueAccount():AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations, 'deferred_revenue_account_id')
    }

}


class HmAcquisOperatOperations extends bookshelf.Model<HmAcquisOperatOperations> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.HM_ACQUIS_OPERAT_OPERATIONS;
    }

    hmAcquisOperat():HmAcquisOperat{
    return this.belongsTo(HmAcquisOperat, 'hm_acquis_opera_id')
   }

    operations(): Operations {
        return this.belongsTo(Operations, 'operations_id')
    }
}

export {
    HmAcquisOperat,
    HmAcquisOperatOperations
};