import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Operations from './Operations';
import {OperationsHandoverActProperty} from './OperationsHandoverAct';
import Employee from './Employee';

class HmReconstruction extends bookshelf.Model<HmReconstruction> {
    get requireFetch() {
        return false;
    }
    get tableName(): string {
        return Table.HM_RECONSTRUCTION;
    }
    property(): OperationsHandoverActProperty{
        return this.belongsTo(OperationsHandoverActProperty,'property_id')
    }
    materialAnswer():Employee{
        return this.belongsTo(Employee,'material_aswer_id')
    }
    analiticGroup2(): AnaliticGroup2 {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id')
    }
    analiticGroup1(): AnaliticGroup1 {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id')
    }
} 

class HmReconstructionOperations extends bookshelf.Model<HmReconstructionOperations> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.HM_RECONSTRUCTION_OPERATIONS;
    }

    hmReconstruction():HmReconstruction{
        return this.belongsTo(HmReconstruction,'hm_reconstruction_id')
    }

    operations():Operations{
        return this.belongsTo(Operations,'operations_id')
      }
}

export {
    HmReconstruction,
    HmReconstructionOperations
};