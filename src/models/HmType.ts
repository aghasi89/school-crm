import bookshelf from '../config/bookshelf';

import Table from '../resources/enums/Table';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
import HmxProfitTax from './HmxProfitTax'

class HmType extends bookshelf.Model<HmType> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.HM_TYPE;
  }

  initialAccount() :AccountOfEmployeeCalculations{
    return this.belongsTo(AccountOfEmployeeCalculations,'initial_account_id')
  }

  staleAccount() :AccountOfEmployeeCalculations{
    return this.belongsTo(AccountOfEmployeeCalculations,'stale_account_id')
  }

  hmxProfitTax() :HmxProfitTax{
    return this.belongsTo(HmxProfitTax,'hmx_profit_tax_id')
  }


  parent() :HmType{
    return this.belongsTo(HmType,'parent_id')
  }



}

export default HmType;
