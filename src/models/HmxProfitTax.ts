import bookshelf from '../config/bookshelf';

import Table from '../resources/enums/Table';

class HmxProfitTax extends bookshelf.Model<HmxProfitTax> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.HMX_PROFIT_TAX;
  }
}

export default HmxProfitTax;
