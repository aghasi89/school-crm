import bookshelf from '../config/bookshelf';

import Table from '../resources/enums/Table';

class HmxValueOfBalance extends bookshelf.Model<HmxValueOfBalance> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.HMX_VALUE_OF_BALANCE;
  }
}

export default HmxValueOfBalance;
