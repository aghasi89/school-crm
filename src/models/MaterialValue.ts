import bookshelf from '../config/bookshelf';

import Table from '../resources/enums/Table';
import MeasurementUnit from './MeasurementUnit';
import Classification from './Classification';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
import BillingMethod from './BillingMethod';
import MaterialValueGroup from './MaterialValueGroup'


class MaterialValueMethod extends bookshelf.Model<MaterialValueMethod> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.MATERIAL_VALUE;
  }

  // get hasTimestamps(): boolean {
  //   return true;
  // }
  measurementUnit() :any{
    return this.belongsTo(MeasurementUnit,'measurement_unit_id')
  }

  classification():any{
    return this.belongsTo(Classification,'classification_id')
  }

  account():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'account_id')
  }


  billingMethod():any{
    return this.belongsTo(BillingMethod,'billing_method_id')
  }

  salesRevenue():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'sales_revenue_account_id')
  }

  retailRevenue():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'retail_revenue_account_id')
  }

  salesExpense():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'sales_expense_account_id')
  }

  materialValueGroup():any{
    return this.belongsTo(MaterialValueGroup,'material_value_group_id')
  }
}

export default MaterialValueMethod;
