import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Operations from './Operations'
import { WarehouseExitOrder } from './WarehouseExitOrder';
import { WarehouseEntryOrder } from './WarehouseEntryOrder';
import MaterialValue from './MaterialValue'

class MaterialValueMovements extends bookshelf.Model<MaterialValueMovements> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.MATERIAL_VALUE_MOVEMENTS;
    }

    warehouseExitOrder(): WarehouseExitOrder {
        return this.belongsTo(WarehouseExitOrder, 'warehouse_exit_order_id')
    }
    warehouseEntryOrder(): WarehouseEntryOrder {
        return this.belongsTo(WarehouseEntryOrder, 'warehouse_entry_order_id')
    }
    analiticGroup2(): AnaliticGroup2 {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id')
    }
    analiticGroup1(): AnaliticGroup1 {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id')
    }
}

class MaterialValueMovementsProduct extends bookshelf.Model<MaterialValueMovementsProduct> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.MATERIAL_VALUE_MOVEMENTS_PRODUCT;
    }

    materialValueMovements(): MaterialValueMovements {
        return this.belongsTo(MaterialValueMovements, 'material_movements_id')
    }

    materialValue(): MaterialValue {
        return this.belongsTo(MaterialValue, 'material_value_id')
    }
}

class MaterialValueMovementsFunctions extends bookshelf.Model<MaterialValueMovementsFunctions> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.MATERIAL_VALUE_MOVEMENTS_FUNCTIONS;
    }

    materialValueMovements(): MaterialValueMovements {
        return this.belongsTo(MaterialValueMovements, 'material_movements_id')
    }

    operations(): Operations {
        return this.belongsTo(Operations, 'operations_id')
    }
}





export {
    MaterialValueMovements,
    MaterialValueMovementsProduct,
    MaterialValueMovementsFunctions
};