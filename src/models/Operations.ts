import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';




class OperationsMethod extends bookshelf.Model<OperationsMethod> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.OPERATIONS;
  }

  debit():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'debit_id')
  }

  dPartners():any{
    return this.belongsTo(Partners,'d_partners_id')
  }
  dAnaliticGroup2():any{
    return this.belongsTo(AnaliticGroup2,'d_analitic_group_2_id')
  }

  dAnaliticGroup1():any{
    return this.belongsTo(AnaliticGroup1,'d_analitic_group_1_id')
  }

  credit():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'credit_id')
  }

  cPartners():any{
    return this.belongsTo(Partners,'c_partners_id')
  }
  cAnaliticGroup2():any{
    return this.belongsTo(AnaliticGroup2,'c_analitic_group_2_id')
  }
  cAnaliticGroup1():any{
    return this.belongsTo(AnaliticGroup1,'c_analitic_group_1_id')
  }
}



export default OperationsMethod
