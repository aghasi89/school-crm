import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
import Operations from './Operations';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Subdivision from './Subdivision';
import Employee from './Employee'

class OperationsHandoverAct extends bookshelf.Model<OperationsHandoverAct> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.OPERATION_HANDOVER_ACT;
    }

    transmittingUnit(): Subdivision {
        return this.belongsTo(Subdivision, 'transmitting_unit_id')
    }

    transmittingMaterialPerson(): Employee {
        return this.belongsTo(Employee, 'transmitting_material_person_id')
    }
    receivingUnit(): Subdivision {
        return this.belongsTo(Subdivision, 'receiving_unit_id')
    }

    receivingMaterialPerson(): Employee {
        return this.belongsTo(Employee, 'receiving_material_person_id')
    }
    analiticGroup2(): AnaliticGroup2 {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id')
    }

    analiticGroup1(): AnaliticGroup1 {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id')
    }



}

class OperationsHandoverActProperty extends bookshelf.Model<OperationsHandoverActProperty> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.OPERATION_HANDOVER_ACT_PROPERTY;
    }
    opHandoverAct(): OperationsHandoverAct {
        return this.belongsTo(OperationsHandoverAct, 'op_handover_act_id')
    }
    depreciationAccount(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'depreciation_account_id')
    }
    expenseAccount(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'expense_expense_id')
    }
    deferredIncome(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'deferred_income_id')
    }
    deferredIncome2(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'deferred_income_2_id')
    }
    incomeAccount(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'income_account_id')
    }
}

class OperationsHandoverActOperations extends bookshelf.Model<OperationsHandoverActOperations> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.OPERATION_HANDOVER_ACT_OPERATIONS;
    }

    opHandoverAct(): OperationsHandoverAct {
        return this.belongsTo(OperationsHandoverAct, 'op_handover_act_id')
    }
    operations(): Operations {
        return this.belongsTo(Operations, 'operations_id')
    }
}

export {
    OperationsHandoverAct,
    OperationsHandoverActProperty,
    OperationsHandoverActOperations
};