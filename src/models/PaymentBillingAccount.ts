import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Operations from './Operations'
import Employee from './Employee';
import Subdivision from './Subdivision';
import BillingAccountInBanks from './BillingAccountInBanks';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';

class PaymentBillingAccount extends bookshelf.Model<PaymentBillingAccount> {
    get requireFetch() {
        return false;
    }
    get tableName(): string {
        return Table.PAYMENTS_BILLING_ACCOUNT;
    }
    subdivision(): Subdivision {
        return this.belongsTo(Subdivision, 'subdivision_id')
    }
    analiticGroup2(): AnaliticGroup2 {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id')
    }
    analiticGroup1(): AnaliticGroup1 {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id')
    }
    settlementAccount():BillingAccountInBanks{
        return this.belongsTo(BillingAccountInBanks, 'settlement_account_id')
    }
    correspondentAccount(): AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations, 'correspondent_account_id')
    }
    exitAccount(): AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations, 'exit_account_id')
    }
}

class PaymentBillingAccountList extends bookshelf.Model<PaymentBillingAccountList> {
    get requireFetch() {
        return false;
    }
    get tableName(): string {
        return Table.PAYMENTS_BILLING_ACCOUNT_LIST;
    }
    paymentsBillingAccount():PaymentBillingAccount{
        return this.belongsTo(PaymentBillingAccount, 'payments_billing_account_id')
    }
    subdivision(): Subdivision {
        return this.belongsTo(Subdivision, 'subdivision_id')
    }
    employees(): Employee {
        return this.belongsTo(Employee, 'employee_id')
    }
}

class PaymentBillingAccountOperations extends bookshelf.Model<PaymentBillingAccountOperations> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.PAYMENTS_BILLING_ACCOUNT_OPERATIONS;
    }

   paymentBillingAccount():PaymentBillingAccount{
    return this.belongsTo(PaymentBillingAccount, 'payments_billing_account_id')
   }

    operations(): Operations {
        return this.belongsTo(Operations, 'operations_id')
    }
}

export {
    PaymentBillingAccount,
    PaymentBillingAccountList,
    PaymentBillingAccountOperations
};