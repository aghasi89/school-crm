import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Currency from './Currency';
import BillingAccount from './BillingAccount';
import Operations from './Operations'
import BillingAccountInBanks from './BillingAccountInBanks'


class PaymentOrderFunctions extends bookshelf.Model<PaymentOrderFunctions> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.PAYMENT_ORDER_FUNCTIONS;
    }

    paymentOrder():PaymentOrder{
        return this.belongsTo(PaymentOrder,'payment_order_id')
    }

    operations():Operations{
        return this.belongsTo(Operations,'operations_id')
      }
}



class PaymentOrder extends bookshelf.Model<PaymentOrder> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.PAYMENT_ORDER;
    }

    providePartners(): Partners{
        return this.belongsTo(Partners,'provide_partners_id')
    }
    dAccount():BillingAccountInBanks{
        return this.belongsTo(BillingAccountInBanks,'d_account_id')
    }
    dCurrency():Currency{
        return this.belongsTo(Currency,'d_currency_id')
    }
    cAccount():BillingAccount{
        return this.belongsTo(BillingAccount,'c_account_id')
    }
    cCorrespondentAccount(): AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations,'c_correspondent_account_id')
    }
    cCurrencyAccount():AccountOfEmployeeCalculations{
        return this.belongsTo(AccountOfEmployeeCalculations,'c_currency_account_id')
    }
    analiticGroup2(): AnaliticGroup2 {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id')
    }
    analiticGroup1(): AnaliticGroup1 {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id')
    }

    receivedServicesOperations():any{
        return this.belongsToMany(Operations,Table.PAYMENT_ORDER_FUNCTIONS,'payment_order_id')
    }

}

export {
    PaymentOrder,
    PaymentOrderFunctions
};