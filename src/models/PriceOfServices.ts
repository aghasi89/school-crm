import bookshelf from '../config/bookshelf';
import Services from './Services';
import Table from '../resources/enums/Table';

class PriceOfServicesMethod extends bookshelf.Model<PriceOfServicesMethod> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.PRICE_OF_SERVICES;
  }

  services(): Services {
    return this.belongsTo(Services, 'services_id');
}
}

export default PriceOfServicesMethod;