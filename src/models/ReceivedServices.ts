import bookshelf from '../config/bookshelf';

import Table from '../resources/enums/Table';

// import MeasurementUnit from './MeasurementUnit';
// import Classification from './Classification';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
// import BillingMethod from './BillingMethod';
// import MaterialValueGroup from './MaterialValueGroup'
// import MaterialValue from './MaterialValue'


// import Warehouse from './Warehouse';
import Operations from './Operations';
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Services from './Services';




class ReceivedServicesOperations extends bookshelf.Model<ReceivedServicesOperations> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.RECEIVED_SERVICES_OPERATIONS;
  }

  operations():Operations{
    return this.belongsTo(Operations,'operations_id')
  }
  
}

class ReceivedServicesDirectory extends bookshelf.Model<ReceivedServicesDirectory> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.RECEIVED_SERVICES_DIRECTORY;
  }

  services():Services{
    return this.belongsTo(Services,'service_id')
  }

  

}

class ReceivedServices extends bookshelf.Model<ReceivedServices> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.RECEIVED_SERVICES;
  }

  providers(): Partners{
    return this.belongsTo(Partners,'partners_id')
  }

  providerAccount():AccountOfEmployeeCalculations{
    return this.belongsTo(AccountOfEmployeeCalculations,'provider_account_id')
  }
  advancePaymentAccount():AccountOfEmployeeCalculations{
    return this.belongsTo(AccountOfEmployeeCalculations,'advance_payment_account_id')
  }
  receivedServicesOperations():any{
    return this.belongsToMany(Operations,Table.RECEIVED_SERVICES_OPERATIONS,'received_service_id')
  }
  receivedServicesDirectory():any{
    return this.hasMany(ReceivedServicesDirectory,'received_service_id')
  }
  analiticGroup2():AnaliticGroup2{
    return this.belongsTo(AnaliticGroup2,'analitic_group_2_id')
  }

  analiticGroup1():AnaliticGroup1{
    return this.belongsTo(AnaliticGroup1,'analitic_group_1_id')
  }



}

export { 
  ReceivedServices,
  ReceivedServicesOperations,
  ReceivedServicesDirectory
};