import bookshelf from '../config/bookshelf';

import Table from '../resources/enums/Table';
import MeasurementUnit from './MeasurementUnit';
import Classification from './Classification';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'


class Services extends bookshelf.Model<Services> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.SERVICES;
  }

  // get hasTimestamps(): boolean {
  //   return true;
  // }
  measurementUnit() :any{
    return this.belongsTo(MeasurementUnit,'measurement_unit_id')
  }

  classification():any{
    return this.belongsTo(Classification,'classification_id')
  }

  account():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'account_id')
  }
}

export default Services;
