import bookshelf from '../config/bookshelf';
import Partners from './Partners';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';
import Table from '../resources/enums/Table';

class StructuralSubdivisionMethod extends bookshelf.Model<StructuralSubdivisionMethod> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.STRUCTURAL_SUBDIVISION;
  }

  partners(): Partners {
    return this.belongsTo(Partners, 'partner_id');
}

expenseAccount(): AccountOfEmployeeCalculations {
    return this.belongsTo(AccountOfEmployeeCalculations, 'expense_account_id','account');
}
}

export default StructuralSubdivisionMethod;