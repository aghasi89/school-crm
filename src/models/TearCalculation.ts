import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';
import Operations from './Operations'
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations';

class TearCalculation extends bookshelf.Model<TearCalculation> {
    get requireFetch() {
        return false;
    }
    get tableName(): string {
        return Table.TEAR_CALCULATION;
    }
    analiticGroup2(): AnaliticGroup2 {
        return this.belongsTo(AnaliticGroup2, 'analitic_group_2_id')
    }
    analiticGroup1(): AnaliticGroup1 {
        return this.belongsTo(AnaliticGroup1, 'analitic_group_1_id')
    }
}

class TearCalculationPropertyNumber extends bookshelf.Model<TearCalculationPropertyNumber> {
    get requireFetch() {
        return false;
    }
    get tableName(): string {
        return Table.TEAR_CALCULATION_PROPERTY_NUMBER;
    }
    paymentsBillingAccount():TearCalculation{
        return this.belongsTo(TearCalculation, 'payments_billing_account_id')
    }
    expenseAccount(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'expense_account_id')
    }
    revenueAccount(): AccountOfEmployeeCalculations {
        return this.belongsTo(AccountOfEmployeeCalculations, 'revenue_account_id')
    }
}

class TearCalculationOperations extends bookshelf.Model<TearCalculationOperations> {
    get requireFetch() {
        return false;
    }

    get tableName(): string {
        return Table.TEAR_CALCULATION_OPERATIONS;
    }

   paymentBillingAccount():TearCalculation{
    return this.belongsTo(TearCalculation, 'tear_calculation_id')
   }

    operations(): Operations {
        return this.belongsTo(Operations, 'operations_id')
    }
}

export {
    TearCalculation,
    TearCalculationPropertyNumber,
    TearCalculationOperations
};