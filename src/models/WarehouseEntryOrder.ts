import bookshelf from '../config/bookshelf';
import Table from '../resources/enums/Table';
import Classification from './Classification';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
import MaterialValue from './MaterialValue'
import Warehouse from './Warehouse';
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';


class WarehouseEntryOrder extends bookshelf.Model<WarehouseEntryOrder> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.WAREHOUSE_ENTRY_ORDER;
  }

  warehouse() :any{
    return this.belongsTo(Warehouse,'warehouse_id')
  }

  partners():any{
    return this.belongsTo(Partners,'partners_id')
  }

  partnersAccount():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'partners_account_id')
  }
  prepaidAccount():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'prepaid_account_id')
  }

  analiticGroup2():any{
    return this.belongsTo(AnaliticGroup2,'analitic_group_2_id')
  }

  analiticGroup1():any{
    return this.belongsTo(AnaliticGroup1,'analitic_group_1_id')
  }

  warehouseEntryOrderFunctions():any{
    return this.hasMany(WarehouseEntryOrderFunctions,'warehouse_entry_order_id')
  }

  warehouseEntryOrderProduct():any{
    return this.hasMany(WarehouseEntryOrderProduct,'warehouse_entry_order_id')
  }

}


class WarehouseEntryOrderFunctions extends bookshelf.Model<WarehouseEntryOrderFunctions> {
  get requireFetch() {
    return false;
  }
  get tableName(): string {
    return Table.WAREHOUSE_ENTRY_ORDER_PRODUCT;
  }

  materialValue():any{
    return this.belongsTo(MaterialValue,'material_value_id')
  }

  accounts():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'accounts_id')
  }

  classification():any{
    return this.belongsTo(Classification,'classification_id')
  }

}

class WarehouseEntryOrderProduct extends bookshelf.Model<WarehouseEntryOrderProduct> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.WAREHOUSE_ENTRY_ORDER_PRODUCT;
  }

  materialValue():any{
    return this.belongsTo(MaterialValue,'material_value_id')
  }

  accounts():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'accounts_id')
  }

  classification():any{
    return this.belongsTo(Classification,'classification_id')
  }

}


export { 
  WarehouseEntryOrder,
  WarehouseEntryOrderFunctions,
  WarehouseEntryOrderProduct
};
