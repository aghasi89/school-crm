import bookshelf from '../config/bookshelf';

import Table from '../resources/enums/Table';

// import MeasurementUnit from './MeasurementUnit';
import Classification from './Classification';
import AccountOfEmployeeCalculations from './AccountOfEmployeeCalculations'
// import BillingMethod from './BillingMethod';
// import MaterialValueGroup from './MaterialValueGroup'
import MaterialValue from './MaterialValue'


import Warehouse from './Warehouse';
import Partners from './Partners';
import AnaliticGroup1 from './AnaliticGroup1';
import AnaliticGroup2 from './AnaliticGroup2';




class WarehouseExitOrderFunctions extends bookshelf.Model<WarehouseExitOrderFunctions> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.WAREHOUSE_EXIT_ORDER_FUNCTIONS;
  }

  debit():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'debit_id')
  }

  dPartners():any{
    return this.belongsTo(Partners,'d_partners_id')
  }
  dAnaliticGroup2():any{
    return this.belongsTo(AnaliticGroup2,'d_analitic_group_2_id')
  }

  dAnaliticGroup1():any{
    return this.belongsTo(AnaliticGroup1,'d_analitic_group_1_id')
  }

  credit():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'credit_id')
  }

  cPartners():any{
    return this.belongsTo(Partners,'c_partners_id')
  }
  cAnaliticGroup2():any{
    return this.belongsTo(AnaliticGroup2,'c_analitic_group_2_id')
  }
  cAnaliticGroup1():any{
    return this.belongsTo(AnaliticGroup1,'c_analitic_group_1_id')
  }
}

class WarehouseExitOrderProduct extends bookshelf.Model<WarehouseExitOrderProduct> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.WAREHOUSE_EXIT_ORDER_PRODUCT;
  }

  materialValue():any{
    return this.belongsTo(MaterialValue,'material_value_id')
  }

  accounts():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'accounts_id')
  }

  classification():any{
    return this.belongsTo(Classification,'classification_id')
  }

}

class WarehouseExitOrder extends bookshelf.Model<WarehouseExitOrder> {
  get requireFetch() {
    return false;
  }

  get tableName(): string {
    return Table.WAREHOUSE_EXIT_ORDER;
  }

  warehouse() :any{
    return this.belongsTo(Warehouse,'warehouse_id')
  }

  partners():any{
    return this.belongsTo(Partners,'partners_id')
  }

  partnersAccount():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'partners_account_id')
  }


  prepaidAccount():any{
    return this.belongsTo(AccountOfEmployeeCalculations,'prepaid_account_id')
  }

  analiticGroup2():any{
    return this.belongsTo(AnaliticGroup2,'analitic_group_2_id')
  }

  analiticGroup1():any{
    return this.belongsTo(AnaliticGroup1,'analitic_group_1_id')
  }

  warehouseExitOrderFunctions():any{
    return this.hasMany(WarehouseExitOrderFunctions,'warehouse_exit_order_id')
  }

  warehouseExitOrderProduct():any{
    return this.hasMany(WarehouseExitOrderProduct,'warehouse_exit_order_id')
  }

}

export { 
  WarehouseExitOrder,
  WarehouseExitOrderFunctions,
  WarehouseExitOrderProduct
};
