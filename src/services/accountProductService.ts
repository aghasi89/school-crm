import { AccountProductDetail } from '../domain/entities/AccountProductDetail';
import { AccountProduct } from '../models/AccountProduct';
import transform from '../utils/transform';
import { AccountProductPayload } from '../domain/requests/AccountProductPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<AccountProductDetail[]> {
    const _accountProduct = await AccountProduct.fetchAll()
    const accountProduct = await (_accountProduct).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'partners',
            'subsections',
            'analiticGroup1',
            'analiticGroup2',
            'aahAccountTypes',
            'warehouse'
        ]
    })

    const res = transform(accountProduct.serialize(), (accountProduct: AccountProductDetail) => ({
        id: accountProduct.id,
        date: accountProduct.date,
        documentNumber: accountProduct.documentNumber,
        partnersId: accountProduct.partnersId,
        partners: accountProduct.partners,
        linesCode: accountProduct.linesCode,
        contract: accountProduct.contract,
        contractDate: accountProduct.contractDate,
        subsectionId: accountProduct.subsectionId,
        subsections: accountProduct.subsections,
        analiticGroup_2Id: accountProduct.analiticGroup_2Id,
        analiticGroup_1Id: accountProduct.analiticGroup_2Id,
        analiticGroup2: accountProduct.analiticGroup2,
        analiticGroup1: accountProduct.analiticGroup1,
        seria: accountProduct.seria,
        number: accountProduct.number,
        dateWriteOff: accountProduct.dateWriteOff,
        comment: accountProduct.comment,
        aahAccountTypeId: accountProduct.aahAccountTypeId,
        aahAccountType: accountProduct.aahAccountType,
        warehouseId: accountProduct.warehouseId,
        warehouse: accountProduct.warehouse,
        address: accountProduct.address,
        aah: accountProduct.aah
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await AccountProduct.fetchAll()).count();
    return { count }
}


export async function insert(params: AccountProductPayload): Promise<AccountProductDetail> {

    const accountProduct = (await new AccountProduct({ ...params }).save()).serialize();

    return accountProduct;


}

export async function getById(id: number): Promise<AccountProductDetail> {

    const accountProduct = (await new AccountProduct({ id: id }).fetch({
        withRelated: ['partners',
            'subsections',
            'analiticGroup1',
            'analiticGroup2',
            'aahAccountTypes',
            'warehouse']
    }));

    if (accountProduct) {
        return accountProduct.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<AccountProductDetail> {

    const res = (await new AccountProduct({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: AccountProductPayload): Promise<AccountProductDetail> {

    const subdivision = (
        await new AccountProduct().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision);
}