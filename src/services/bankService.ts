import Bank from '../models/Bank';
import * as object from '../utils/object';
import transform from '../utils/transform';
import BankDetail from '../domain/entities/BankDetail';
import BankPayload from '../domain/requests/BankPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';

const { errors } = config;
/**
 * Fetch all users from users table.
 *
 * @returns {Promise<BankDetail[]>}
 */

export async function fetchAll(limit:number,offset:number): Promise<BankDetail[]> {
  const _banks = await Bank.fetchAll()
  const banks = await (_banks).query((qb)=>{
      qb.limit(limit),
      qb.offset(offset)
  }).fetch()

  const res = transform(banks.serialize(), (bank: BankDetail) => ({
    id: bank.id,
    code: bank.code,
    name: bank.name,
    swift: bank.swift
  }))

  return res;
}


export async function count(): Promise<object> {
  const count = await (await Bank.fetchAll()).count();
  return {count}
}


export async function insert(params: BankPayload): Promise<BankDetail> {

  const banks = (await new Bank({ ...params }).save()).serialize();

  return banks;


}

export async function getById(id: number): Promise<BankDetail> {

  const banks = (await new Bank({ id: id }).fetch());

  if(banks){
      return banks.serialize();
  }
  else{
      throw new NotFoundError(errors.notFound);
  }
}

export async function destroy(id: number): Promise<BankDetail> {

  const res = (await new Bank({ id: id }).destroy()).serialize();

  return res;
}

export async function update(id: number, params: BankPayload): Promise<BankDetail> {

  const subdivision = (
      await new Bank().where({ id: id }).save({ ...params }, { patch: true })
  ).serialize();

  return object.camelize(subdivision);
}
