import {BillingAccountInBanksDetail} from '../domain/entities/BillingAccountInBanksDetail';
import BillingAccountInBanks from '../models/BillingAccountInBanks';
import transform from '../utils/transform';
import  BillingAccountInBanksPayload  from '../domain/requests/BillingAccountInBanksPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit:number,offset:number): Promise<BillingAccountInBanksDetail[]> {
    const _billingAccountInBanks = await BillingAccountInBanks.fetchAll()
    const billingAccountInBanks = await (_billingAccountInBanks).query((qb)=>{
        qb.limit(limit),
        qb.offset(offset)
    }).fetch({ withRelated: ['currencies','accounts']})

    const res = transform(billingAccountInBanks.serialize(), (billingAccountInBanks: BillingAccountInBanksDetail) => ({
        id: billingAccountInBanks.id,        
        billingAccount:billingAccountInBanks.billingAccount,
        name: billingAccountInBanks.name,
        currencyId: billingAccountInBanks.currencyId,
        currencies: billingAccountInBanks.currencies,
        accountId:billingAccountInBanks.accountId,
        accounts: billingAccountInBanks.accounts,
        isMain: billingAccountInBanks.isMain,
        numberOfDocument:billingAccountInBanks.numberOfDocument,
        eServices: billingAccountInBanks.eServices
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await BillingAccountInBanks.fetchAll()).count();
    return {count}
}


export async function insert(params: BillingAccountInBanksPayload): Promise<BillingAccountInBanksDetail> {

    const billingAccountInBanks = (await new BillingAccountInBanks({ ...params }).save()).serialize();

    return billingAccountInBanks;


}

export async function getById(id: number): Promise<BillingAccountInBanksDetail> {

    const billingAccountInBanks = (await new BillingAccountInBanks({ id: id }).fetch({ withRelated:  ['currencies','accounts'] }));

    if(billingAccountInBanks){
        return billingAccountInBanks.serialize();
    }
    else{
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<BillingAccountInBanksDetail> {

    const res = (await new BillingAccountInBanks({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: BillingAccountInBanksPayload): Promise<BillingAccountInBanksDetail> {

    const subdivision = (
        await new BillingAccountInBanks().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision);
}