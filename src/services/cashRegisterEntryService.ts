import { CashRegisterEntryDetail } from '../domain/entities/CashRegisterEntryDetail';
import { CashRegisterEntry } from '../models/CashRegisterEntry';
import transform from '../utils/transform';
import { CashRegisterEntryPayload } from '../domain/requests/CashRegisterEntryPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<CashRegisterEntryDetail[]> {
    const _cashRegisterEntry = await CashRegisterEntry.fetchAll()
    const cashRegisterEntry = await (_cashRegisterEntry).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'cashRegister',
            'correspondentAccount',
            'entryAccount',
            'analiticGroup2',
            'analiticGroup1',
            'currencies',
            'partners'
        ]
    })

    const res = transform(cashRegisterEntry.serialize(), (cashRegisterEntry: CashRegisterEntryDetail) => ({
        id: cashRegisterEntry.id,
        cashRegisterId:cashRegisterEntry.cashRegisterId,
        date: cashRegisterEntry.date,
        hdmN: cashRegisterEntry.hdmN,
        documentNumber: cashRegisterEntry.documentNumber,
        correspondentAccountId: cashRegisterEntry.correspondentAccountId,
        entryAccountId: cashRegisterEntry.entryAccountId,
        analiticGroup_1Id: cashRegisterEntry.analiticGroup_1Id,
        analiticGroup_2Id: cashRegisterEntry.analiticGroup_2Id,
        currencyId: cashRegisterEntry.currencyId,
        amountCurrency1: cashRegisterEntry.amountCurrency1,
        amountCurrency2: cashRegisterEntry.amountCurrency2,
        partnersId: cashRegisterEntry.partnersId,
        received: cashRegisterEntry.received,
        basis: cashRegisterEntry.basis,
        attached: cashRegisterEntry.attached,
        optiona: cashRegisterEntry.optiona,
        typicalOperation: cashRegisterEntry.typicalOperation,
        cashRegister: cashRegisterEntry.cashRegister,
        accounts: cashRegisterEntry.accounts,
        analiticGroup1:cashRegisterEntry.analiticGroup1,
        analiticGroup2:cashRegisterEntry.analiticGroup2,
        currencies: cashRegisterEntry.currencies,
        partners: cashRegisterEntry.partners
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await CashRegisterEntry.fetchAll()).count();
    return { count }
}


export async function insert(params: CashRegisterEntryPayload): Promise<any> {

    let data = { ...params };
    console.log(data)

    const rs:any | undefined = await PaymentOrder.forge(data.main)
    if(rs==undefined) return
     rs.save()

 
    params.paymentOrderOperation.map(async (d) => {
        await rs.related('receivedServicesOperations').create(d);
    })
    await rs;
    return rs;


}

export async function getById(id: number): Promise<CashRegisterEntryDetail> {

    const cashRegisterEntry = (await new CashRegisterEntry({ id: id }).fetch({
        withRelated: [
            'cashRegister',
            'correspondentAccount',
            'entryAccount',
            'analiticGroup2',
            'analiticGroup1',
            'currencies',
            'partners']
    }));

    if (cashRegisterEntry) {
        return cashRegisterEntry.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<CashRegisterEntryDetail> {

    const res = (await new CashRegisterEntry({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: CashRegisterEntryPayload): Promise<CashRegisterEntryDetail> {

    const subdivision = (
        await new CashRegisterEntry().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision)

}