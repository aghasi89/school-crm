import { CashRegisterExitDetail } from '../domain/entities/CashRegisterExitDetail';
import { CashRegisterExit } from '../models/CashRegisterExit';
import transform from '../utils/transform';
import { CashRegisterExitPayload } from '../domain/requests/CashRegisterExitPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<CashRegisterExitDetail[]> {
    const _cashRegisterExit = await CashRegisterExit.fetchAll()
    const cashRegisterExit = await (_cashRegisterExit).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'cashRegister',
            'correspondentAccount',
            'exitAccount',
            'analiticGroup2',
            'analiticGroup1',
            'currencies',
            'partners'
        ]
    })

    const res = transform(cashRegisterExit.serialize(), (cashRegisterExit: CashRegisterExitDetail) => ({
        id: cashRegisterExit.id,
        cashRegisterId: cashRegisterExit.cashRegisterId,
        date: cashRegisterExit.date,
        documentNumber: cashRegisterExit.documentNumber,
        correspondentAccountId: cashRegisterExit.correspondentAccountId,
        exitAccountId: cashRegisterExit.exitAccountId,
        analiticGroup_1Id: cashRegisterExit.analiticGroup_1Id,
        analiticGroup_2Id: cashRegisterExit.analiticGroup_2Id,
        currencyId: cashRegisterExit.currencyId,
        amountCurrency1: cashRegisterExit.amountCurrency1,
        amountCurrency2: cashRegisterExit.amountCurrency2,
        partnersId: cashRegisterExit.partnersId,
        received: cashRegisterExit.received,
        npNshPassword: cashRegisterExit.npNshPassword,
        basis: cashRegisterExit.basis,
        appendix: cashRegisterExit.appendix,
        otherInformation: cashRegisterExit.otherInformation,
        optiona: cashRegisterExit.optiona,
        typicalOperation: cashRegisterExit.typicalOperation,
        cashRegister: cashRegisterExit.cashRegister,
        analiticGroup1: cashRegisterExit.analiticGroup1,
        analiticGroup2: cashRegisterExit.analiticGroup2,
        currencies: cashRegisterExit.currencies,
        partners: cashRegisterExit.partners
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await CashRegisterExit.fetchAll()).count();
    return { count }
}


export async function insert(params: CashRegisterExitPayload): Promise<CashRegisterExitDetail> {

    const cashRegisterExit = (await new CashRegisterExit({ ...params }).save()).serialize();

    return cashRegisterExit;


}

export async function getById(id: number): Promise<CashRegisterExitDetail> {

    const cashRegisterExit = (await new CashRegisterExit({ id: id }).fetch({
        withRelated: [
            'cashRegister',
            'correspondentAccount',
            'exitAccount',
            'analiticGroup2',
            'analiticGroup1',
            'currencies',
            'partners']
    }));

    if (cashRegisterExit) {
        return cashRegisterExit.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<CashRegisterExitDetail> {

    const res = (await new CashRegisterExit({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: CashRegisterExitPayload): Promise<CashRegisterExitDetail> {

    const subdivision = (
        await new CashRegisterExit().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision)

}