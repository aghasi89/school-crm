import CashRegister from '../models/CashRegister';
import logger from '../utils/logger';
import * as object from '../utils/object';

import transform from '../utils/transform';
import CashRegisterDetail from '../domain/entities/CashRegisterDetail';
import CashRegisterPayload from '../domain/requests/CashRegisterPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';

const { errors } = config;


/**
 * Fetch all users from users table.
 *
 * @returns {Promise<CashRegisterDetail[]>}
 */
export async function fetchAll(limit: number, offset: number): Promise<CashRegisterDetail[]> {
    logger.log('info', 'Fetching users from database');



    const _cashRegisters = await CashRegister.fetchAll()
    if (!limit || limit == 0) {
        limit = Number(_cashRegisters.count())
        offset = 0;
    }
    const cashRegisters = await (_cashRegisters).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({ withRelated: ['account'] })
    const res = transform(cashRegisters.serialize(), (cashRegister: CashRegisterDetail) => ({
        account : cashRegister.account,
        accountId : cashRegister.accountId,
        code : cashRegister.code,
        deoN : cashRegister.deoN,
        dmoN : cashRegister.dmoN,
        hdmNonTaxable : cashRegister.hdmNonTaxable,
        hdmPrintType: cashRegister.hdmPrintType,
        hdmRegisN : cashRegister.hdmRegisN,
        hdmTaxable : cashRegister.hdmTaxable,
        ip : cashRegister.ip,
        isMain : cashRegister.isMain,
        name : cashRegister.name,
        password : cashRegister.password,
        port : cashRegister.port,
        id : cashRegister.id
    }));

    logger.log('debug', 'Fetched all users successfully:', res);

    return res;
}

export async function count(): Promise<object> {
    const count = await (await CashRegister.fetchAll()).count();
    return { count }
}

/**
 * Insert user from given user payload
 *
 * @param {CashRegisterPayload} params
 * @returns {Promise<ClassificationDetail>}
 */
export async function insert(params: CashRegisterPayload): Promise<CashRegisterDetail> {



    console.log(params,'*********')
    const cashRegister = (await new CashRegister({ ...params }).save()).serialize();

    logger.log('debug', 'Inserted user successfully:', cashRegister);

    return object.camelize(cashRegister);

}


export async function getById(id: number): Promise<CashRegisterDetail> {

    const position = (await new CashRegister({ id: id }).fetch({ withRelated: ['measurementUnit', 'classification', 'account'] }));

    if (position) {
        return position.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<CashRegisterDetail> {

    const res = (await new CashRegister({ id: id }).destroy()).serialize();

    return res;
}


export async function update(id: number, params: CashRegisterPayload): Promise<CashRegisterDetail> {

    // const billingAccounts = params['billingAccounts'];
    // const additionalAddressePartners = params['additionalAddressePartners']

    // delete params['additionalAddressePartners']
    // delete params['billingAccounts'];

    // const partnersId = id;

    // for (const billingAccount of billingAccounts) {
    //     billingAccount['partnersId'] = partnersId;
    //     try {
    //         console.log(billingAccount);
    //         if (billingAccount['id'] && !billingAccount['isDeleted']) {
    //             (await new BillingAccount().where({ id: billingAccount['id'] }).save({ ...billingAccount }, { patch: true })).serialize();
    //         } else if (billingAccount['id'] && billingAccount['isDeleted']) {
    //             (await new BillingAccount({ id: billingAccount['id'] }).destroy()).serialize();
    //         } else {
    //             (await new BillingAccount({ ...billingAccount }).save()).serialize();
    //         }
    //     } catch (error) {
    //         console.log(error)
    //     }
    // }

    // for (const additionalAddressePartner of additionalAddressePartners) {
    //     additionalAddressePartner['partnersId'] = partnersId;
    //     try {
    //         if (additionalAddressePartner['id'] && !additionalAddressePartner['isDeleted']) {
    //             (await new AdditionalAddressePartners().where({ id: additionalAddressePartner['id'] }).save({ ...additionalAddressePartner }, { patch: true })).serialize();
    //         } else if (additionalAddressePartner['id'] && additionalAddressePartner['isDeleted']) {
    //             (await new AdditionalAddressePartners({ id: additionalAddressePartner['id'] }).destroy()).serialize();
    //         } else {
    //             (await new AdditionalAddressePartners({ ...additionalAddressePartner }).save()).serialize();
    //         }
    //     } catch (error) {
    //         console.log(error)
    //     }
    // }

    const partners = (
        await new CashRegister().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(partners);
}