import { FormulasDetail } from '../domain/entities/FormulasDetail';
import Formulas from '../models/Formulas';
import transform from '../utils/transform';
import  FormulasPayload  from '../domain/requests/FormulasPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit:number,offset:number): Promise<FormulasDetail[]> {
    const _formulas = await Formulas.fetchAll()
    const formulas = await (_formulas).query((qb)=>{
        qb.limit(limit),
        qb.offset(offset)
    }).fetch()

    const res = transform(formulas.serialize(), (formulas: FormulasDetail) => ({
        id: formulas.id,
        code:formulas.code,
        caption:formulas.caption,
        expression:formulas.expression
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await Formulas.fetchAll()).count();
    return {count}
}


export async function insert(params: FormulasPayload): Promise<FormulasDetail> {

    const formulas = (await new Formulas({ ...params }).save()).serialize();

    return formulas;


}

export async function getById(id: number): Promise<FormulasDetail> {

    const formulas = (await new Formulas({ id: id }).fetch());

    if(formulas){
        return formulas.serialize();
    }
    else{
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<FormulasDetail> {

    const res = (await new Formulas({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: FormulasPayload): Promise<FormulasDetail> {

    const subdivision = (
        await new Formulas().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision);
}