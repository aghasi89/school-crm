import { HmAcquisOperatDetail } from '../domain/entities/HmAcquisOperatDetail';
import { HmAcquisOperat } from '../models/HmAcquisOperat';
import transform from '../utils/transform';
import { HmAcquisOperatPayload } from '../domain/requests/HmAcquisOperatPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';


const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<HmAcquisOperatDetail[]> {
    const _hmAcquisOperat = await HmAcquisOperat.fetchAll()
    const hmAcquisOperat = await (_hmAcquisOperat).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'hmType',
            'hmxProfitTax',
            'partners',
            'subdivision',
            'subdivisionAccount',
            'analiticGroup2',
            'analiticGroup1',
            'initialCostAccount',
            'wornAccount',
            'expenseAccount',
            'incomeStatatement',
            'currentPortionOfDeferredRevenueAccount',
            'deferredRevenueAccount'
        ]
    })

    const res = transform(hmAcquisOperat.serialize(), (hmAcquisOperat: HmAcquisOperatDetail) => ({
        id: hmAcquisOperat.id,
        name: hmAcquisOperat.name,
        forPropertyId: hmAcquisOperat.forPropertyId,
        fullName: hmAcquisOperat.fullName,
        entryDate: hmAcquisOperat.entryDate,
        operationDate: hmAcquisOperat.operationDate,
        hmTypeId: hmAcquisOperat.hmTypeId,
        hmType: hmAcquisOperat.hmType,
        hmxProfitTaxId: hmAcquisOperat.hmxProfitTaxId,
        hmxProfitTax: hmAcquisOperat.hmxProfitTax,
        hmAcquisitionMethod: hmAcquisOperat.hmAcquisitionMethod,
        location: hmAcquisOperat.location,
        partnersId: hmAcquisOperat.partnersId,
        partners: hmAcquisOperat.partners,
        businessCard: hmAcquisOperat.businessCard,
        documentNumber: hmAcquisOperat.documentNumber,
        subdivisionId: hmAcquisOperat.subdivisionId,
        subdivision: hmAcquisOperat.subdivision,
        subdivisionAccountId: hmAcquisOperat.subdivisionAccountId,
        subdivisionAccount: hmAcquisOperat.subdivisionAccount,
        analiticGroup_1Id: hmAcquisOperat.analiticGroup_1Id,
        analiticGroup1: hmAcquisOperat.analiticGroup1,
        analiticGroup_2Id: hmAcquisOperat.analiticGroup_2Id,
        analiticGroup2: hmAcquisOperat.analiticGroup2,
        outputMethod: hmAcquisOperat.outputMethod,
        purchaseDocumentInfo: hmAcquisOperat.purchaseDocumentInfo,
        aahType: hmAcquisOperat.aahType,
        financialUsefulLife: hmAcquisOperat.financialUsefulLife,
        financialCalculatedWear: hmAcquisOperat.financialCalculatedWear,
        financialIsWorn: hmAcquisOperat.financialIsWorn,
        financeResidualValue: hmAcquisOperat.financeResidualValue,
        financeIsWorn: hmAcquisOperat.financeIsWorn,
        taxUsefulLife: hmAcquisOperat.taxUsefulLife,
        taxInitialCost: hmAcquisOperat.taxInitialCost,
        taxCalculatedWear: hmAcquisOperat.taxCalculatedWear,
        taxResidualValue: hmAcquisOperat.taxResidualValue,
        taxIsWorn: hmAcquisOperat.taxIsWorn,
        deferredRevenueLife: hmAcquisOperat.deferredRevenueLife,
        calculatedDepreciation: hmAcquisOperat.calculatedDepreciation,
        deferredRevenueAccount: hmAcquisOperat.deferredRevenueAccount,
        deferredRevenueWorn: hmAcquisOperat.deferredRevenueWorn,
        initialCostAccountId: hmAcquisOperat.initialCostAccountId,
        initialCostAccount: hmAcquisOperat.initialCostAccount,
        wornAccountId: hmAcquisOperat.wornAccountId,
        wornAccount: hmAcquisOperat.wornAccount,
        expenseAccountId: hmAcquisOperat.expenseAccountId,
        expenseAccount: hmAcquisOperat.expenseAccount,
        incomeStatementId: hmAcquisOperat.incomeStatementId,
        incomeStatement: hmAcquisOperat.incomeStatement,
        curPortionOfDeferredRevAccountId: hmAcquisOperat.curPortionOfDeferredRevAccountId,
        curPortionOfDeferredRevAccount: hmAcquisOperat.curPortionOfDeferredRevAccount,
        deferredRevenueAccountId: hmAcquisOperat.deferredRevenueAccountId,
        deferredRevenueAccounts: hmAcquisOperat.deferredRevenueAccounts,
        startYear: hmAcquisOperat.startYear,
        serialNumber: hmAcquisOperat.serialNumber,
        technicalProfile: hmAcquisOperat.technicalProfile,
        stamp: hmAcquisOperat.stamp,
        briefReview: hmAcquisOperat.briefReview,
        manufacturer: hmAcquisOperat.manufacturer
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await HmAcquisOperat.fetchAll()).count();
    return { count }
}


export async function insert(params: HmAcquisOperatPayload): Promise<HmAcquisOperatDetail> {

    const hmAcquisOperat = (await new HmAcquisOperat({ ...params }).save()).serialize();

    return hmAcquisOperat;


}

export async function getById(id: number): Promise<HmAcquisOperatDetail> {

    const hmAcquisOperat = (await new HmAcquisOperat({ id: id }).fetch({
        withRelated: ['hmType',
            'hmxProfitTax',
            'partners',
            'subdivision',
            'subdivisionAccount',
            'analiticGroup2',
            'analiticGroup1',
            'initialCostAccount',
            'wornAccount',
            'expenseAccount',
            'incomeStatatement',
            'currentPortionOfDeferredRevenueAccount',
            'deferredRevenueAccount']
    }));

    if (hmAcquisOperat) {
        return hmAcquisOperat.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<HmAcquisOperatDetail> {

    const res = (await new HmAcquisOperat({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: HmAcquisOperatPayload): Promise<HmAcquisOperatDetail> {

    const subdivision = (
        await new HmAcquisOperat().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision);
}