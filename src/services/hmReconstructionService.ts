import { HmReconstructionDetail } from '../domain/entities/HmReconstructionDetail';
import { HmReconstruction } from '../models/HmReconstruction';
import transform from '../utils/transform';
import { HmReconstructionPayload } from '../domain/requests/HmReconstructionPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<HmReconstructionDetail[]> {
    const _hmReconstruction = await HmReconstruction.fetchAll()
    const hmReconstruction = await (_hmReconstruction).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'property',
            'materialAnswer',
            'analiticGroup2',
            'analiticGroup1'
        ]
    })

    const res = transform(hmReconstruction.serialize(), (hmReconstruction: HmReconstructionDetail) => ({
        id: hmReconstruction.id,
        date: hmReconstruction.date,
        documentNumber: hmReconstruction.documentNumber,
        propertyId: hmReconstruction.propertyId,
        property: hmReconstruction.property,
        location: hmReconstruction.location,
        materialAnswerId: hmReconstruction.materialAnswerId,
        materialAnswer: hmReconstruction.materialAnswer,
        analiticGroup_1Id: hmReconstruction.analiticGroup_1Id,
        analiticGroup1: hmReconstruction.analiticGroup1,
        analiticGroup_2Id: hmReconstruction.analiticGroup_2Id,
        analiticGroup2: hmReconstruction.analiticGroup2,
        comment: hmReconstruction.comment,
        taxResidualUsefulLifeOld: hmReconstruction.taxResidualUsefulLifeOld,
        taxResidualUsefulLifeNew: hmReconstruction.taxResidualUsefulLifeNew,
        taxInitialCostOld: hmReconstruction.taxInitialCostOld,
        taxInitialCostNew: hmReconstruction.taxInitialCostNew,
        revenueResidualUsefulLifeOld: hmReconstruction.revenueResidualUsefulLifeOld,
        revenueResidualUsefulLifeNew: hmReconstruction.revenueResidualUsefulLifeNew,
        revenueInitialCostOld: hmReconstruction.revenueInitialCostOld,
        revenueInitialCostNew: hmReconstruction.revenueInitialCostNew,
        financeResidualUsefulLifeOld: hmReconstruction.financeResidualUsefulLifeOld,
        financeResidualUsefulLifeNew: hmReconstruction.financeResidualUsefulLifeNew,
        financeInitialCostOld: hmReconstruction.financeInitialCostOld,
        financeInitialCostNew: hmReconstruction.financeInitialCostNew
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await HmReconstruction.fetchAll()).count();
    return { count }
}


export async function insert(params: HmReconstructionPayload): Promise<HmReconstructionDetail> {

    const hmReconstruction = (await new HmReconstruction({ ...params }).save()).serialize();

    return hmReconstruction;


}

export async function getById(id: number): Promise<HmReconstructionDetail> {

    const hmReconstruction = (await new HmReconstruction({ id: id }).fetch({
        withRelated: [
            'property',
            'materialAnswer',
            'analiticGroup2',
            'analiticGroup1']
    }));

    if (hmReconstruction) {
        return hmReconstruction.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<HmReconstructionDetail> {

    const res = (await new HmReconstruction({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: HmReconstructionPayload): Promise<HmReconstructionDetail> {

    const subdivision = (
        await new HmReconstruction().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision)

}