import { HmRevaluationDetail } from '../domain/entities/HmRevaluationDetail';
import { HmRevaluation } from '../models/HmRevaluation';
import transform from '../utils/transform';
import { HmRevaluationPayload } from '../domain/requests/HmRevaluationPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<HmRevaluationDetail[]> {
    const _hmRevaluation = await HmRevaluation.fetchAll()
    const hmRevaluation = await (_hmRevaluation).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'property',
            'materialAnswer',
            'analiticGroup2',
            'analiticGroup1'
        ]
    })

    const res = transform(hmRevaluation.serialize(), (hmRevaluation: HmRevaluationDetail) => ({
        id: hmRevaluation.id,
        date: hmRevaluation.date,
        documentNumber: hmRevaluation.documentNumber,
        propertyId: hmRevaluation.propertyId,
        property: hmRevaluation.property,
        location: hmRevaluation.location,
        materialAnswerId: hmRevaluation.materialAnswerId,
        materialAnswer: hmRevaluation.materialAnswer,
        analiticGroup_1Id: hmRevaluation.analiticGroup_1Id,
        analiticGroup1: hmRevaluation.analiticGroup1,
        analiticGroup_2Id: hmRevaluation.analiticGroup_2Id,
        analiticGroup2: hmRevaluation.analiticGroup2,
        comment: hmRevaluation.comment,
        taxResidualUsefulLifeOld: hmRevaluation.taxResidualUsefulLifeOld,
        taxResidualUsefulLifeNew: hmRevaluation.taxResidualUsefulLifeNew,
        taxInitialCostOld: hmRevaluation.taxInitialCostOld,
        taxInitialCostNew: hmRevaluation.taxInitialCostNew,
        taxCalculatedWearOld: hmRevaluation.taxCalculatedWearOld,
        taxCalculatedWearNew: hmRevaluation.taxCalculatedWearOld,
        taxResidualValueOld: hmRevaluation.taxResidualValueOld,
        taxResidualValueNew: hmRevaluation.taxResidualValueNew,
        revenueResidualUsefulLifeOld: hmRevaluation.revenueResidualUsefulLifeOld,
        revenueResidualUsefulLifeNew: hmRevaluation.revenueResidualUsefulLifeNew,
        revenueInitialCostOld: hmRevaluation.revenueInitialCostOld,
        revenueInitialCostNew: hmRevaluation.revenueInitialCostNew,
        revenueCalculatedWearOld: hmRevaluation.revenueCalculatedWearOld,
        revenueCalculatedWearNew: hmRevaluation.revenueCalculatedWearOld,
        revenueResidualValueOld: hmRevaluation.revenueResidualValueOld,
        revenueResidualValueNew: hmRevaluation.revenueResidualValueNew,
        financeResidualUsefulLifeOld: hmRevaluation.financeResidualUsefulLifeOld,
        financeResidualUsefulLifeNew: hmRevaluation.financeResidualUsefulLifeNew,
        financeInitialCostOld: hmRevaluation.financeInitialCostOld,
        financeInitialCostNew: hmRevaluation.financeInitialCostNew,
        financeCalculatedWearOld: hmRevaluation.financeCalculatedWearOld,
        financeCalculatedWearNew: hmRevaluation.financeCalculatedWearOld,
        financeResidualValueOld: hmRevaluation.financeResidualValueOld,
        financeResidualValueNew: hmRevaluation.financeResidualValueNew,
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await HmRevaluation.fetchAll()).count();
    return { count }
}


export async function insert(params: HmRevaluationPayload): Promise<HmRevaluationDetail> {

    const hmRevaluation = (await new HmRevaluation({ ...params }).save()).serialize();

    return hmRevaluation;


}

export async function getById(id: number): Promise<HmRevaluationDetail> {

    const hmRevaluation = (await new HmRevaluation({ id: id }).fetch({
        withRelated: [
            'property',
            'materialAnswer',
            'analiticGroup2',
            'analiticGroup1']
    }));

    if (hmRevaluation) {
        return hmRevaluation.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<HmRevaluationDetail> {

    const res = (await new HmRevaluation({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: HmRevaluationPayload): Promise<HmRevaluationDetail> {

    const subdivision = (
        await new HmRevaluation().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision)

}