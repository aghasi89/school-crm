import HmTypeDetail from '../domain/entities/HmTypeDetail';
import HmType from '../models/HmType';
import transform from '../utils/transform';
import HmTypePayload from '../domain/requests/HmTypePayload';
import * as object from '../utils/object';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<HmTypeDetail[]> {

    const _hmTypes = await HmType.fetchAll()
    if (!limit || limit == 0) {
        limit = Number(_hmTypes.count())
        offset = 0;
    }
    const hmTypes = await (_hmTypes).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch()

    const res = transform(hmTypes.serialize(), (hmType: HmTypeDetail) => ({
        id: hmType.id,
        name: hmType.name,
        code: hmType.code,
        initialAccountId: hmType.initialAccountId,
        parentId: hmType.parentId,
        staleAccountId: hmType.staleAccountId,
        usefullServiceDuration: hmType.usefullServiceDuration,
        hmxProfitTaxId : hmType.hmxProfitTaxId

    }))

    return res;
}


export async function count(): Promise<object> {
    const count = await (await HmType.fetchAll()).count();
    return { count }
}


export async function insert(param: HmTypePayload): Promise<HmTypePayload> {

    const hmType = (await new HmType({ ...param }).save()).serialize();

    return object.camelize(hmType);

}

export async function getById(id: number): Promise<HmTypeDetail> {

    const position = (await new HmType({ id: id }).fetch({withRelated:['initialAccount','staleAccount','hmxProfitTax','parent']}));

    if (position) {
        return position.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<HmTypeDetail> {

    const res = (await new HmType({ id: id }).destroy()).serialize();

    return res;
}


export async function update(id: number, params: HmTypePayload): Promise<HmTypeDetail> {


    const hmType = (
        await new HmType().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(hmType);
}
