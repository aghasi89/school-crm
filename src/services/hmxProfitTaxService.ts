import HmxProfitTaxDetail from '../domain/entities/HmxProfitTaxDetail';
import HmxProfitTax from '../models/HmxProfitTax';
import transform from '../utils/transform';
import HmxProfitTaxPayload from '../domain/requests/HmxProfitTaxPayload';
import * as object from '../utils/object';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<HmxProfitTaxDetail[]> {

    const _hmxProfitTaxs = await HmxProfitTax.fetchAll()
    if (!limit || limit == 0) {
        limit = Number(_hmxProfitTaxs.count())
        offset = 0;
    }
    const hmxProfitTaxs = await (_hmxProfitTaxs).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch( )

    const res = transform(hmxProfitTaxs.serialize(), (hmxProfitTax: HmxProfitTaxDetail) => ({
        id: hmxProfitTax.id,
        name: hmxProfitTax.name,
        code : hmxProfitTax.code,
        yearPrice : hmxProfitTax.yearPrice

    }))

    return res;
}


export async function count(): Promise<object> {
    const count = await (await HmxProfitTax.fetchAll()).count();
    return { count }
}


export async function insert(param: HmxProfitTaxPayload): Promise<HmxProfitTaxPayload> {

    const hmxProfitTax = (await new HmxProfitTax({ ...param }).save()).serialize();

    return object.camelize(hmxProfitTax);

}

export async function getById(id: number): Promise<HmxProfitTaxDetail> {

    const position = (await new HmxProfitTax({ id: id }).fetch());

    if (position) {
        return position.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<HmxProfitTaxDetail> {

    const res = (await new HmxProfitTax({ id: id }).destroy()).serialize();

    return res;
}


export async function update(id: number, params: HmxProfitTaxPayload): Promise<HmxProfitTaxDetail> {


    const hmxProfitTax = (
        await new HmxProfitTax().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(hmxProfitTax);
}
