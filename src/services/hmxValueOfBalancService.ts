import HmxValueOfBalanceDetail from '../domain/entities/HmxValueOfBalanceDetail';
import HmxValueOfBalance from '../models/HmxValueOfBalance';
import transform from '../utils/transform';
import HmxValueOfBalancePayload from '../domain/requests/HmxValueOfBalancePayload';
import * as object from '../utils/object';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<HmxValueOfBalanceDetail[]> {

    const _hmxValueOfBalances = await HmxValueOfBalance.fetchAll()
    if (!limit || limit == 0) {
        limit = Number(_hmxValueOfBalances.count())
        offset = 0;
    }
    const hmxValueOfBalances = await (_hmxValueOfBalances).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch()

    const res = transform(hmxValueOfBalances.serialize(), (hmxValueOfBalance: HmxValueOfBalanceDetail) => ({
        id: hmxValueOfBalance.id,
        date: hmxValueOfBalance.date,
        code : hmxValueOfBalance.code,
        valueBeginningOfYear : hmxValueOfBalance.valueBeginningOfYear

    }))

    return res;
}


export async function count(): Promise<object> {
    const count = await (await HmxValueOfBalance.fetchAll()).count();
    return { count }
}


export async function insert(param: HmxValueOfBalancePayload): Promise<HmxValueOfBalancePayload> {

    const hmxValueOfBalance = (await new HmxValueOfBalance({ ...param }).save()).serialize();

    return object.camelize(hmxValueOfBalance);

}

export async function getById(id: number): Promise<HmxValueOfBalanceDetail> {

    const position = (await new HmxValueOfBalance({ id: id }).fetch());

    if (position) {
        return position.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<HmxValueOfBalanceDetail> {

    const res = (await new HmxValueOfBalance({ id: id }).destroy()).serialize();

    return res;
}


export async function update(id: number, params: HmxValueOfBalancePayload): Promise<HmxValueOfBalanceDetail> {


    const hmxValueOfBalance = (
        await new HmxValueOfBalance().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(hmxValueOfBalance);
}
