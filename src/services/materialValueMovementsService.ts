import { MaterialValueMovementsDetail } from '../domain/entities/MaterialValueMovementsDetail';
import { MaterialValueMovements } from '../models/MaterialValueMovements';
import transform from '../utils/transform';
import { MaterialValueMovementsPayload } from '../domain/requests/MaterialValueMovementsPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<MaterialValueMovementsDetail[]> {
    const _materialValueMovements = await MaterialValueMovements.fetchAll()
    const materialValueMovements = await (_materialValueMovements).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'warehouseExitOrder',
            'warehouseEntryOrder',
            'analiticGroup2',
            'analiticGroup1'
        ]
    })

    const res = transform(materialValueMovements.serialize(), (materialValueMovements: MaterialValueMovementsDetail) => ({
        id: materialValueMovements.id,
        date: materialValueMovements.date,
        warehouseExitOrderId: materialValueMovements.warehouseExitOrderId,
        warehouseExitOrder: materialValueMovements.warehouseExitOrder,
        warehouseEntryOrderId: materialValueMovements.warehouseEntryOrderId,
        warehouseEntryOrder: materialValueMovements.warehouseEntryOrder,
        printAtSalePrices: materialValueMovements.printAtSalePrices,
        analiticGroup_1Id: materialValueMovements.analiticGroup_1Id,
        analiticGroup1: materialValueMovements.analiticGroup1,
        analiticGroup_2Id: materialValueMovements.analiticGroup_2Id,
        analiticGroup2: materialValueMovements.analiticGroup2,
        comment: materialValueMovements.comment,
        accountant: materialValueMovements.accountant,
        mediator: materialValueMovements.mediator,
        allow: materialValueMovements.allow,
        shipperRegistrationBookInfo: materialValueMovements.shipperRegistrationBookInfo,
        outputMethod: materialValueMovements.outputMethod,
        seria: materialValueMovements.seria,
        dateOfDischarge: materialValueMovements.dateOfDischarge,
        hasRequested: materialValueMovements.hasRequested,
        additionalComment: materialValueMovements.additionalComment,


    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await MaterialValueMovements.fetchAll()).count();
    return { count }
}


export async function insert(params: MaterialValueMovementsPayload): Promise<MaterialValueMovementsDetail> {

    const materialValueMovements = (await new MaterialValueMovements({ ...params }).save()).serialize();

    return materialValueMovements;


}

export async function getById(id: number): Promise<MaterialValueMovementsDetail> {

    const materialValueMovements = (await new MaterialValueMovements({ id: id }).fetch({
        withRelated: [
            'warehouseExitOrder',
            'warehouseEntryOrder',
            'analiticGroup2',
            'analiticGroup1']
    }));

    if (materialValueMovements) {
        return materialValueMovements.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<MaterialValueMovementsDetail> {

    const res = (await new MaterialValueMovements({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: MaterialValueMovementsPayload): Promise<MaterialValueMovementsDetail> {

    const subdivision = (
        await new MaterialValueMovements().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision)

}