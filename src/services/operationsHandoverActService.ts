import { OperationsHandoverActDetail } from '../domain/entities/OperationsHandoverActDetail';
import { OperationsHandoverAct } from '../models/OperationsHandoverAct';
import transform from '../utils/transform';
import { OperationsHandoverActPayload } from '../domain/requests/OperationsHandoverActPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<OperationsHandoverActDetail[]> {
    const _operationsHandoverAct = await OperationsHandoverAct.fetchAll()
    const operationsHandoverAct = await (_operationsHandoverAct).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'transmittingUnit',
            'transmittingMaterialPerson',
            'receivingUnit',
            'receivingMaterialPerson',
            'analiticGroup2',
            'analiticGroup1'
        ]
    })

    const res = transform(operationsHandoverAct.serialize(), (operationsHandoverAct: OperationsHandoverActDetail) => ({
        id: operationsHandoverAct.id,
        date: operationsHandoverAct.date,
        documentNumber: operationsHandoverAct.documentNumber,
        calculationDate: operationsHandoverAct.calculationDate,
        paymentType: operationsHandoverAct.paymentType,
        transmittingUnitId: operationsHandoverAct.transmittingUnitId,
        transmittingUnit: operationsHandoverAct.transmittingUnit,
        transmittingMaterialPersonId: operationsHandoverAct.transmittingMaterialPersonId,
        transmittingMaterialPerson: operationsHandoverAct.transmittingMaterialPerson,
        receivingUnitId: operationsHandoverAct.receivingUnitId,
        receivingUnit: operationsHandoverAct.receivingUnit,
        receivingMaterialPersonId: operationsHandoverAct.receivingMaterialPersonId,
        receivingMaterialPerson: operationsHandoverAct.receivingMaterialPerson,
        analiticGroup_1Id: operationsHandoverAct.analiticGroup_1Id,
        analiticGroup1: operationsHandoverAct.analiticGroup1,
        analiticGroup_2Id: operationsHandoverAct.analiticGroup_2Id,
        analiticGroup2: operationsHandoverAct.analiticGroup2,
        comment: operationsHandoverAct.comment
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await OperationsHandoverAct.fetchAll()).count();
    return { count }
}


export async function insert(params: OperationsHandoverActPayload): Promise<OperationsHandoverActDetail> {

    const operationsHandoverAct = (await new OperationsHandoverAct({ ...params }).save()).serialize();

    return operationsHandoverAct;


}

export async function getById(id: number): Promise<OperationsHandoverActDetail> {

    const operationsHandoverAct = (await new OperationsHandoverAct({ id: id }).fetch({
        withRelated: [
            'transmittingUnit',
            'transmittingMaterialPerson',
            'receivingUnit',
            'receivingMaterialPerson',
            'analiticGroup2',
            'analiticGroup1'
        ]
    }));

    if (operationsHandoverAct) {
        return operationsHandoverAct.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<OperationsHandoverActDetail> {

    const res = (await new OperationsHandoverAct({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: OperationsHandoverActPayload): Promise<OperationsHandoverActDetail> {

    const subdivision = (
        await new OperationsHandoverAct().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision);
}