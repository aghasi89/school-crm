import OperationsDetail from '../domain/entities/OperationsDetail';
import Operations from '../models/Operations';
import transform from '../utils/transform';
import OperationsPayload from '../domain/requests/OperationsPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<OperationsDetail[]> {
    const _operations = await Operations.fetchAll()
    const operations = await (_operations).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'debit',
            'dPartners',
            'dAnaliticGroup2',
            'dAnaliticGroup1',
            'credit',
            'cPartners',
            'cAnaliticGroup2',
            'cAnaliticGroup1'
        ]
    })

    const res = transform(operations.serialize(), (operations: OperationsDetail) => ({
        id: operations.id,
        debitId: operations.debitId,
        dPartnersId: operations.dPartnersId,
        dAnaliticGroup_2Id: operations.dAnaliticGroup_2Id,
        dAnaliticGroup_1Id: operations.dAnaliticGroup_1Id,
        creditId: operations.creditId,
        cPartnersId: operations.cPartnersId,
        cAnaliticGroup_2Id: operations.cAnaliticGroup_2Id,
        cAnaliticGroup_1Id: operations.cAnaliticGroup_1Id,
        money: operations.money,
        comment: operations.comment,
        partners: operations.partners,
        analiticGroups1: operations.analiticGroups1,
        analiticGroups2: operations.analiticGroups2
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await Operations.fetchAll()).count();
    return { count }
}


export async function insert(params: OperationsPayload): Promise<OperationsDetail> {

    const operations = (await new Operations({ ...params }).save()).serialize();

    return operations;


}

export async function getById(id: number): Promise<OperationsDetail> {

    const operations = (await new Operations({ id: id }).fetch({
        withRelated: [
            'debit',
            'dPartners',
            'dAnaliticGroup2',
            'dAnaliticGroup1',
            'credit',
            'cPartners',
            'cAnaliticGroup2',
            'cAnaliticGroup1'
        ]
    }));

    if (operations) {
        return operations.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<OperationsDetail> {

    const res = (await new Operations({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: OperationsPayload): Promise<OperationsDetail> {

    const subdivision = (
        await new Operations().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision);
}