import { PaymentBillingAccountDetail } from '../domain/entities/PaymentBillingAccountDetail';
import { PaymentBillingAccount } from '../models/PaymentBillingAccount';
import transform from '../utils/transform';
import { PaymentBillingAccountPayload } from '../domain/requests/PaymentBillingAccountPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<PaymentBillingAccountDetail[]> {
    const _paymentBillingAccount = await PaymentBillingAccount.fetchAll()
    const paymentBillingAccount = await (_paymentBillingAccount).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'subdivision',
            'analiticGroup2',
            'analiticGroup1',
            'settlementAccount',
            'correspondentAccount',
            'exitAccount'
        ]
    })

    const res = transform(paymentBillingAccount.serialize(), (paymentBillingAccount: PaymentBillingAccountDetail) => ({
        id: paymentBillingAccount.id,
        date: paymentBillingAccount.date,
        documentNumber: paymentBillingAccount.documentNumber,
        calculationDate: paymentBillingAccount.calculationDate,
        paymentType: paymentBillingAccount.paymentType,
        subdivisionId: paymentBillingAccount.subdivisionId,
        subdivision: paymentBillingAccount.subdivision,
        analiticGroup_1Id: paymentBillingAccount.analiticGroup_1Id,
        analiticGroup1: paymentBillingAccount.analiticGroup1,
        analiticGroup_2Id: paymentBillingAccount.analiticGroup_2Id,
        analiticGroup2: paymentBillingAccount.analiticGroup2,
        settlementAccountId: paymentBillingAccount.settlementAccountId,
        settlementAccount: paymentBillingAccount.settlementAccount,
        comment: paymentBillingAccount.comment,
        dateOfFormulation: paymentBillingAccount.dateOfFormulation,
        correspondentAccountId: paymentBillingAccount.correspondentAccountId,
        correspondentAccount: paymentBillingAccount.correspondentAccount,
        exitAccountId: paymentBillingAccount.exitAccountId,
        exitAccount: paymentBillingAccount.exitAccount
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await PaymentBillingAccount.fetchAll()).count();
    return { count }
}


export async function insert(params: PaymentBillingAccountPayload): Promise<PaymentBillingAccountDetail> {

    const paymentBillingAccount = (await new PaymentBillingAccount({ ...params }).save()).serialize();

    return paymentBillingAccount;


}

export async function getById(id: number): Promise<PaymentBillingAccountDetail> {

    const paymentBillingAccount = (await new PaymentBillingAccount({ id: id }).fetch({
        withRelated: [
            'subdivision',
            'analiticGroup2',
            'analiticGroup1',
            'settlementAccount',
            'correspondentAccount',
            'exitAccount']
    }));

    if (paymentBillingAccount) {
        return paymentBillingAccount.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<PaymentBillingAccountDetail> {

    const res = (await new PaymentBillingAccount({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: PaymentBillingAccountPayload): Promise<PaymentBillingAccountDetail> {

    const subdivision = (
        await new PaymentBillingAccount().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision)

}