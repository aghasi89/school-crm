import { PaymentOrderDetail } from '../domain/entities/PaymentOrderDetail';
import { PaymentOrder } from '../models/PaymentOrder';
import transform from '../utils/transform';
import { PaymentOrderPayload } from '../domain/requests/PaymentOrderPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';
import OperationsPayload from '../domain/requests/OperationsPayload'

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<PaymentOrderDetail[]> {
    const _paymentOrder = await PaymentOrder.fetchAll()
    const paymentOrder = await (_paymentOrder).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'providePartners',
            'dAccount',
            'dCurrency',
            'cAccount',
            'cCorrespondentAccount',
            'cCurrencyAccount',
            'analiticGroup2',
            'analiticGroup1',
        ]
    })

    const res = transform(paymentOrder.serialize(), (paymentOrder: PaymentOrderDetail) => ({
        id: paymentOrder.id,
        date: paymentOrder.date,
        dateOfFormulation: paymentOrder.dateOfFormulation,
        payerName: paymentOrder.payerName,
        payerHvhh: paymentOrder.payerHvhh,
        provideName: paymentOrder.provideName,
        provideHvhh: paymentOrder.provideHvhh,
        providePartnersId: paymentOrder.providePartnersId,
        providerPartners: paymentOrder.providerPartners,
        dAccountId: paymentOrder.dAccountId,
        dAccount: paymentOrder.dAccount,
        dCurrencyId: paymentOrder.dCurrencyId,
        dCurreny: paymentOrder.dCurreny,
        cAccountId: paymentOrder.cAccountId,
        cAccount: paymentOrder.cAccount,
        cCorrespondentAccountId: paymentOrder.cCorrespondentAccountId,
        cCorrespondentAccount: paymentOrder.cCorrespondentAccount,
        cCurrencyAccountId: paymentOrder.cCurrencyAccountId,
        cCurrencyAccount: paymentOrder.cCurrencyAccount,
        amountCurrency_1: paymentOrder.amountCurrency_1,
        amountCurrency_2: paymentOrder.amountCurrency_2,
        analiticGroup_1Id: paymentOrder.analiticGroup_1Id,
        analiticGroup1: paymentOrder.analiticGroup1,
        analiticGroup_2Id: paymentOrder.analiticGroup_2Id,
        analiticGroup2: paymentOrder.analiticGroup2,
        typicalOperation: paymentOrder.typicalOperation,


    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await PaymentOrder.fetchAll()).count();
    return { count }
}


export async function insert(params: PaymentOrderPayload): Promise<any> {


    let data = { ...params };
    console.log(data)

    const rs:any | undefined = await PaymentOrder.forge(data.main)
    if(rs==undefined) return
     rs.save()

 
    params.paymentOrderOperation.map(async (d) => {
        await rs.related('receivedServicesOperations').create(d);
    })
    await rs;
    return rs;


    const paymentOrder = (await new PaymentOrder({ ...params }).save()).serialize();

    return paymentOrder;


}

export async function getById(id: number): Promise<PaymentOrderDetail> {

    const paymentOrder = (await new PaymentOrder({ id: id }).fetch({
        withRelated: [
            'providePartners',
            'dAccount',
            'dCurrency',
            'cAccount',
            'cCorrespondentAccount',
            'cCurrencyAccount',
            'analiticGroup2',
            'analiticGroup1',]
    }));

    if (paymentOrder) {
        return paymentOrder.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<PaymentOrderDetail> {

    const res = (await new PaymentOrder({ id: id }).destroy()).serialize();

    return res;
}

export  function getPaymentOrderOperation(params: any): Array<OperationsPayload> {

    console.log(params,"getWarehouseExitOrderProductPayload");
    const res : Array<OperationsPayload> =  [{
        debitId: 1,
        dPartnersId: 1,
        dAnaliticGroup_2Id:1,
        dAnaliticGroup_1Id: 1,
        creditId: 1,
        cAnaliticGroup_2Id: 1,
        cAnaliticGroup_1Id: 1,
        money: 10,
        comment: "test",
        cPartnersId : 2
    }]

    return res;
}


export async function update(id: number, params: PaymentOrderPayload): Promise<PaymentOrderDetail> {

    const subdivision = (
        await new PaymentOrder().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision)

}