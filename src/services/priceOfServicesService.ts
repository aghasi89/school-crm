import { PriceOfServicesDetail } from '../domain/entities/PriceOfServicesDetail';
import PriceOfServices from '../models/PriceOfServices';
import transform from '../utils/transform';
import  PriceOfServicesPayload  from '../domain/requests/PriceOfServicesPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit:number,offset:number): Promise<PriceOfServicesDetail[]> {
    const _priceOfServices = await PriceOfServices.fetchAll()
    const priceOfServices = await (_priceOfServices).query((qb)=>{
        qb.limit(limit),
        qb.offset(offset)
    }).fetch({ withRelated: ['services']})

    const res = transform(priceOfServices.serialize(), (priceOfServices: PriceOfServicesDetail) => ({
        id: priceOfServices.id,
        startDate:priceOfServices.startDate,
        endDate:priceOfServices.endDate,
        servicesId:priceOfServices.servicesId,
        services: priceOfServices.services,
        type:priceOfServices.type
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await PriceOfServices.fetchAll()).count();
    return {count}
}


export async function insert(params: PriceOfServicesPayload): Promise<PriceOfServicesDetail> {

    const priceOfServices = (await new PriceOfServices({ ...params }).save()).serialize();

    return priceOfServices;


}

export async function getById(id: number): Promise<PriceOfServicesDetail> {

    const priceOfServices = (await new PriceOfServices({ id: id }).fetch({ withRelated: ['services'] }));

    if(priceOfServices){
        return priceOfServices.serialize();
    }
    else{
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<PriceOfServicesDetail> {

    const res = (await new PriceOfServices({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: PriceOfServicesPayload): Promise<PriceOfServicesDetail> {

    const subdivision = (
        await new PriceOfServices().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision);
}