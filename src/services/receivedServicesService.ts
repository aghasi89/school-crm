import { ReceivedServicesDetail } from '../domain/entities/ReceivedServicesDetail';
import { ReceivedServices } from '../models/ReceivedServices';
import transform from '../utils/transform';
import { ReceivedServicesPayload ,ReceivedServicesDirectoryPayload} from '../domain/requests/ReceivedServicesPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';
import OperationsPayload from '../domain/requests/OperationsPayload'

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<ReceivedServicesDetail[]> {

    const _receivedServices = await ReceivedServices.fetchAll();
    if (!limit || limit == 0) {
        limit = Number(_receivedServices.count())
        offset = 0;
    }
    const receivedServices = await (_receivedServices).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'providers',
            'providerAccount',
            'advancePaymentAccount',
            'analiticGroup2',
            'analiticGroup1']
    })
    const res = transform(receivedServices.serialize(), (receivedServices: ReceivedServicesDetail) => ({
        id: receivedServices.id,
        currencyId: receivedServices.currencyId,
        currencyExchangeRate1: receivedServices.currencyExchangeRate1,
        currencyExchangeRate2: receivedServices.currencyExchangeRate2,
        previousDayExchangeRate: receivedServices.previousDayExchangeRate,
        partnersId: receivedServices.partnersId,
        providerAccountId: receivedServices.providerAccountId,
        advancePaymentAccountId: receivedServices.advancePaymentAccountId,
        analiticGroup_1Id: receivedServices.analiticGroup_1Id,
        analiticGroup_2Id: receivedServices.analiticGroup_2Id,
        outputMethod: receivedServices.outputMethod,
        purchaseDocumentNumber: receivedServices.purchaseDocumentNumber,
        purchaseDocumentDate: receivedServices.purchaseDocumentDate,
        comment: receivedServices.comment,
        purchaseTypeOfService: receivedServices.purchaseTypeOfService,
        calculationTypeAah: receivedServices.calculationTypeAah,
        includeAahInExpense: receivedServices.includeAahInExpense,
        formOfReflectionAah: receivedServices.formOfReflectionAah,
        typicalOperation: receivedServices.typicalOperation,
        currencies: receivedServices.currencies,
        partners: receivedServices.partners,
        analiticGroup1: receivedServices.analiticGroup1,
        analiticGroup2: receivedServices.analiticGroup2,
        date: receivedServices.date,
        documentNumber: receivedServices.documentNumber
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await ReceivedServices.fetchAll()).count();
    return { count }
}

export async function insert(params: ReceivedServicesPayload): Promise<any> {
    let data = { ...params };
    delete data.receivedServicesDirectory;
    delete data.receivedServicesOperations
    const rs:any | undefined = await ReceivedServices.forge(data)
    if(rs==undefined) return
     rs.save()

    params.receivedServicesDirectory.map(async (d) => {
        await rs.related('receivedServicesDirectory').create(d);
    })
    
    params.receivedServicesOperations.map(async (d) => {
        await rs.related('receivedServicesOperations').create(d);
    })
    await rs;
    return rs;
}

export async function getById(id: number): Promise<ReceivedServicesDetail> {

    const receivedServices = (await new ReceivedServices({ id: id }).fetch({
        withRelated: [
            'providers',
            'providerAccount',
            'advancePaymentAccount',
            'analiticGroup2',
            'analiticGroup1'
        ]
    }));

    if (receivedServices) {
        return receivedServices.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export  function getReceivedServicesDirectoryPayload(params: Array<ReceivedServicesDirectoryPayload>): Array<OperationsPayload> {

    console.log(params,"getWarehouseExitOrderProductPayload");
    const res : Array<OperationsPayload> =  [{
        debitId: 1,
        dPartnersId: 1,
        dAnaliticGroup_2Id:1,
        dAnaliticGroup_1Id: 1,
        creditId: 1,
        cAnaliticGroup_2Id: 1,
        cAnaliticGroup_1Id: 1,
        money: 10,
        comment: "test",
        cPartnersId : 2
    }]

    return res;
}






export async function destroy(id: number): Promise<ReceivedServicesDetail> {

    const res = (await new ReceivedServices({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: ReceivedServicesPayload): Promise<ReceivedServicesDetail> {

    const receivedServices = (
        await new ReceivedServices().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(receivedServices);
}