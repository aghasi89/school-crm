import Services from '../models/Services';
import logger from '../utils/logger';
import * as object from '../utils/object';

import transform from '../utils/transform';
import ServicesDetail from '../domain/entities/ServicesDetail';
import ServicesPayload from '../domain/requests/ServicesPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';

const { errors } = config;


/**
 * Fetch all users from users table.
 *
 * @returns {Promise<ServicesDetail[]>}
 */
export async function fetchAll(limit: number, offset: number): Promise<ServicesDetail[]> {
    logger.log('info', 'Fetching users from database');



    const _servicess = await Services.fetchAll()
    if (!limit || limit == 0) {
        limit = Number(_servicess.count())
        offset = 0;
    }
    const servicess = await (_servicess).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({ withRelated: ['measurementUnit', 'classification', 'account'] })
    const res = transform(servicess.serialize(), (services: ServicesDetail) => ({
        code: services.code,
        fullName: services.fullName,
        accountId: services.accountId,
        barCode: services.barCode,
        classificationId: services.classificationId,
        // currencyId : services.currencyId,
        isAah: services.isAah,
        measurementUnitId: services.measurementUnitId,
        name: services.name,
        retailerPrice: services.retailerPrice,
        wholesalePrice: services.wholesalePrice,
        account: services.account,
        classification: services.classification,
        measurementUnit: services.measurementUnit,
        // wholesalePriceCurrency : services.wholesalePriceCurrency,
        id: services.id
        // updatedAt: new Date(user.updatedAt).toLocaleString(),
        // createdAt: new Date(user.updatedAt).toLocaleString()
    }));

    logger.log('debug', 'Fetched all users successfully:', res);

    return res;
}

export async function count(): Promise<object> {
    const count = await (await Services.fetchAll()).count();
    return { count }
}

/**
 * Insert user from given user payload
 *
 * @param {ServicesPayload} params
 * @returns {Promise<ClassificationDetail>}
 */
export async function insert(params: ServicesPayload): Promise<ServicesDetail> {



    console.log(params,'*********')
    const services = (await new Services({ ...params }).save()).serialize();

    logger.log('debug', 'Inserted user successfully:', services);

    return object.camelize(services);

}


export async function getById(id: number): Promise<ServicesDetail> {

    const position = (await new Services({ id: id }).fetch({ withRelated: ['measurementUnit', 'classification', 'account'] }));

    if (position) {
        return position.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<ServicesDetail> {

    const res = (await new Services({ id: id }).destroy()).serialize();

    return res;
}


export async function update(id: number, params: ServicesPayload): Promise<ServicesDetail> {

    // const billingAccounts = params['billingAccounts'];
    // const additionalAddressePartners = params['additionalAddressePartners']

    // delete params['additionalAddressePartners']
    // delete params['billingAccounts'];

    // const partnersId = id;

    // for (const billingAccount of billingAccounts) {
    //     billingAccount['partnersId'] = partnersId;
    //     try {
    //         console.log(billingAccount);
    //         if (billingAccount['id'] && !billingAccount['isDeleted']) {
    //             (await new BillingAccount().where({ id: billingAccount['id'] }).save({ ...billingAccount }, { patch: true })).serialize();
    //         } else if (billingAccount['id'] && billingAccount['isDeleted']) {
    //             (await new BillingAccount({ id: billingAccount['id'] }).destroy()).serialize();
    //         } else {
    //             (await new BillingAccount({ ...billingAccount }).save()).serialize();
    //         }
    //     } catch (error) {
    //         console.log(error)
    //     }
    // }

    // for (const additionalAddressePartner of additionalAddressePartners) {
    //     additionalAddressePartner['partnersId'] = partnersId;
    //     try {
    //         if (additionalAddressePartner['id'] && !additionalAddressePartner['isDeleted']) {
    //             (await new AdditionalAddressePartners().where({ id: additionalAddressePartner['id'] }).save({ ...additionalAddressePartner }, { patch: true })).serialize();
    //         } else if (additionalAddressePartner['id'] && additionalAddressePartner['isDeleted']) {
    //             (await new AdditionalAddressePartners({ id: additionalAddressePartner['id'] }).destroy()).serialize();
    //         } else {
    //             (await new AdditionalAddressePartners({ ...additionalAddressePartner }).save()).serialize();
    //         }
    //     } catch (error) {
    //         console.log(error)
    //     }
    // }

    const partners = (
        await new Services().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(partners);
}