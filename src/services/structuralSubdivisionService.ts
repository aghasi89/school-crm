import { StructuralSubdivisionDetail } from '../domain/entities/StructuralSubdivisionDetail';
import StructuralSubdivision from '../models/StructuralSubdivision';
import transform from '../utils/transform';
import  StructuralSubdivisionPayload  from '../domain/requests/StructuralSubdivisionPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit:number,offset:number): Promise<StructuralSubdivisionDetail[]> {
    const _structuralSubdivision = await StructuralSubdivision.fetchAll()
    const structuralSubdivision = await (_structuralSubdivision).query((qb)=>{
        qb.limit(limit),
        qb.offset(offset)
    }).fetch({ withRelated: ['partners','expenseAccount']})

    const res = transform(structuralSubdivision.serialize(), (structuralSubdivision: StructuralSubdivisionDetail) => ({
        id: structuralSubdivision.id,
        code: structuralSubdivision.code,
        name: structuralSubdivision.name,
        partnerId: structuralSubdivision.partnerId,
        expenseAccountId: structuralSubdivision.expenseAccountId,
        expenseAccount:structuralSubdivision.expenseAccount,
        partners: structuralSubdivision.partners
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await StructuralSubdivision.fetchAll()).count();
    return {count}
}


export async function insert(params: StructuralSubdivisionPayload): Promise<StructuralSubdivisionDetail> {

    const structuralSubdivision = (await new StructuralSubdivision({ ...params }).save()).serialize();

    return structuralSubdivision;


}

export async function getById(id: number): Promise<StructuralSubdivisionDetail> {

    const structuralSubdivision = (await new StructuralSubdivision({ id: id }).fetch({ withRelated: ['partners','expenseAccount'] }));

    if(structuralSubdivision){
        return structuralSubdivision.serialize();
    }
    else{
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<StructuralSubdivisionDetail> {

    const res = (await new StructuralSubdivision({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: StructuralSubdivisionPayload): Promise<StructuralSubdivisionDetail> {

    const subdivision = (
        await new StructuralSubdivision().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision);
}