import { TearCalculationDetail } from '../domain/entities/TearCalculationDetail';
import { TearCalculation } from '../models/TearCalculation';
import transform from '../utils/transform';
import { TearCalculationPayload } from '../domain/requests/TearCalculationPayload';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';
import * as object from '../utils/object';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<TearCalculationDetail[]> {
    const _tearCalculation = await TearCalculation.fetchAll()
    const tearCalculation = await (_tearCalculation).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'analiticGroup2',
            'analiticGroup1'
        ]
    })

    const res = transform(tearCalculation.serialize(), (tearCalculation: TearCalculationDetail) => ({
        id: tearCalculation.id,
        date: tearCalculation.date,
        documentNumber: tearCalculation.documentNumber,
        analiticGroup_1Id: tearCalculation.analiticGroup_1Id,
        analiticGroup1: tearCalculation.analiticGroup1,
        analiticGroup_2Id: tearCalculation.analiticGroup_2Id,
        analiticGroup2: tearCalculation.analiticGroup2,
        comment: tearCalculation.comment
    }))

    return res;
}

export async function count(): Promise<object> {
    const count = await (await TearCalculation.fetchAll()).count();
    return { count }
}


export async function insert(params: TearCalculationPayload): Promise<TearCalculationDetail> {

    const tearCalculation = (await new TearCalculation({ ...params }).save()).serialize();

    return tearCalculation;


}

export async function getById(id: number): Promise<TearCalculationDetail> {

    const tearCalculation = (await new TearCalculation({ id: id }).fetch({
        withRelated: [
            'analiticGroup2',
            'analiticGroup1'
        ]
    }));

    if (tearCalculation) {
        return tearCalculation.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<TearCalculationDetail> {

    const res = (await new TearCalculation({ id: id }).destroy()).serialize();

    return res;
}

export async function update(id: number, params: TearCalculationPayload): Promise<TearCalculationDetail> {

    const subdivision = (
        await new TearCalculation().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(subdivision)

}