import { WarehouseEntryOrderDetail } from '../domain/entities/WarehouseEntryOrderDetail';
import { WarehouseEntryOrder } from '../models/WarehouseEntryOrder';
import { WarehouseEntryOrderProduct } from '../models/WarehouseEntryOrder';

import transform from '../utils/transform';
import { WarehouseEntryOrderPayload, WarehouseEntryOrderFunctionsPayload, WarehouseEntryOrderProductPayload } from '../domain/requests/WarehouseEntryOrderPayload';
import * as object from '../utils/object';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<WarehouseEntryOrderDetail[]> {

    const _warehouseEntryOrders = await WarehouseEntryOrder.fetchAll();
    if (!limit || limit == 0) {
        limit = Number(_warehouseEntryOrders.count())
        offset = 0;
    }
    const warehouseEntryOrders = await (_warehouseEntryOrders).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'warehouse',
            'partners',
            'partnersAccount',
            'prepaidAccount',
            'analiticGroup2',
            'analiticGroup1',
            'warehouseEntryOrderFunctions',
            'warehouseEntryOrderProduct']
    })
    const res = transform(warehouseEntryOrders.serialize(), (warehouseEntryOrder: WarehouseEntryOrderDetail) => ({
        id: warehouseEntryOrder.id,
        accept: warehouseEntryOrder.accept,
        accountant: warehouseEntryOrder.accountant,
        allow: warehouseEntryOrder.allow,
        analiticGroup_1Id: warehouseEntryOrder.analiticGroup_1Id,
        analiticGroup_2Id: warehouseEntryOrder.analiticGroup_2Id,
        calculationStyleOfAah: warehouseEntryOrder.calculationStyleOfAah,
        comment: warehouseEntryOrder.comment,
        container: warehouseEntryOrder.container,
        date: warehouseEntryOrder.date,
        documentDate: warehouseEntryOrder.documentDate,
        documentN: warehouseEntryOrder.documentN,
        documentOfTransport: warehouseEntryOrder.documentOfTransport,
        documentOfTransportDate: warehouseEntryOrder.documentOfTransportDate,
        includeAahInCost: warehouseEntryOrder.includeAahInCost,
        mediator: warehouseEntryOrder.mediator,
        partnersAccountId: warehouseEntryOrder.partnersAccountId,
        partnersId: warehouseEntryOrder.partnersId,
        powerOfAttorney: warehouseEntryOrder.powerOfAttorney,
        prepaidAccountId: warehouseEntryOrder.prepaidAccountId,
        typeOfAcquisitionOfNa: warehouseEntryOrder.typeOfAcquisitionOfNa,
        warehouseId: warehouseEntryOrder.warehouseId,
        warehouse:warehouseEntryOrder.warehouse,
        partners:warehouseEntryOrder.partners,
        analiticGroup1:warehouseEntryOrder.analiticGroup1,
        analiticGroup2:warehouseEntryOrder.analiticGroup2

    }))

    return res;
}


// export async function fetchAllByType(id:number): Promise<WarehouseEntryOrderDetail[]> {

//     const _warehouseEntryOrders = await WarehouseEntryOrder.fetchAll();
//     const warehouseEntryOrders = await (_warehouseEntryOrders).query((qb)=>{
//         qb.where({typesOfActionId:id})
//     }).fetch({withRelated:['customerAccount','aahAccount','prepaidAccountReceived','typesOfAction']})
//     const res = transform(warehouseEntryOrders.serialize(), (warehouseEntryOrder: WarehouseEntryOrderDetail) => ({
//         id: warehouseEntryOrder.id,
//         accept : warehouseEntryOrder.accept,
//         accountant : warehouseEntryOrder.accountant,
//         allow : warehouseEntryOrder.allow,
//         analiticGroup_1Id : warehouseEntryOrder.analiticGroup_1Id,
//         analiticGroup_2Id : warehouseEntryOrder.analiticGroup_2Id,
//         calculationStyleOfAah : warehouseEntryOrder.calculationStyleOfAah,
//         comment : warehouseEntryOrder.comment,
//         container : warehouseEntryOrder.container,
//         date : warehouseEntryOrder.date,
//         documentDate : warehouseEntryOrder.documentDate,
//         documentN : warehouseEntryOrder.documentN,
//         documentOfTransport : warehouseEntryOrder.documentOfTransport,
//         documentOfTransportDate : warehouseEntryOrder.documentOfTransportDate,
//         includeAahInCost : warehouseEntryOrder.includeAahInCost,
//         mediator:warehouseEntryOrder.mediator,
//         partnersAccountId : warehouseEntryOrder.partnersAccountId,
//         partnersId : warehouseEntryOrder.partnersId,
//         powerOfAttorney : warehouseEntryOrder.powerOfAttorney,
//         prepaidAccountId : warehouseEntryOrder.prepaidAccountId,
//         typeOfAcquisitionOfNa : warehouseEntryOrder.typeOfAcquisitionOfNa,
//         warehouseId : warehouseEntryOrder.warehouseId

//     }))

//     return res;
// }


export async function count(): Promise<object> {
    const count = await (await WarehouseEntryOrder.fetchAll()).count();
    return { count }
}


export async function insert(param: WarehouseEntryOrderPayload): Promise<WarehouseEntryOrderPayload> {

    const warehouseEntryOrderProduct = param.warehouseEntryOrderProduct;
    delete param.warehouseEntryOrderProduct;
    delete param.warehouseEntryOrderFunctions;

    const warehouseEntryOrder = (await new WarehouseEntryOrder({ ...param }).save()).serialize();

    for (const iterator of warehouseEntryOrderProduct) {
        iterator.warehouseEntryOrderId = warehouseEntryOrder.id;
        (await new WarehouseEntryOrderProduct({ ...iterator }).save()).serialize();
    }

    return object.camelize(warehouseEntryOrder);

}

export async function getById(id: number): Promise<WarehouseEntryOrderDetail> {

    const position = (await new WarehouseEntryOrder({ id: id }).fetch({
        withRelated: [
            'warehouse',
            'partners',
            'partnersAccount',
            'prepaidAccount',
            'analiticGroup2',
            'analiticGroup1',
            'warehouseEntryOrderFunctions',
            'warehouseEntryOrderProduct.materialValue',
            'warehouseEntryOrderProduct.accounts',
            'warehouseEntryOrderProduct.classification'
        ]
    }));

    if (position) {
        return position.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<WarehouseEntryOrderDetail> {

    const res = (await new WarehouseEntryOrder({ id: id }).destroy()).serialize();

    return res;
}


export  function getWarehouseEntryOrderProductPayload(params: Array<WarehouseEntryOrderProductPayload>): Array<WarehouseEntryOrderFunctionsPayload> {

    console.log(params,"getWarehouseEntryOrderProductPayload");
    const res : Array<WarehouseEntryOrderFunctionsPayload> =  [{
        debitId: 1,
        dPartnersId: 1,
        dAnaliticGroup_2Id:1,
        dAnaliticGroup_1Id: 1,
        creditId: 1,
        cPartnersIdisAah: 1,
        cAnaliticGroup_2Id: 1,
        cAnaliticGroup_1Id: 1,
        money: 10,
        comment: "test",
    }]

    return res;
}




export async function update(id: number, params: WarehouseEntryOrderPayload): Promise<WarehouseEntryOrderDetail> {
    const warehouseEntryOrderProducts = params.warehouseEntryOrderProduct;
    delete params.warehouseEntryOrderProduct;
    delete params.warehouseEntryOrderFunctions;
    for (const warehouseEntryOrderProduct of warehouseEntryOrderProducts) {
        warehouseEntryOrderProduct['warehouseEntryOrderId'] = id;
        try {
            if (warehouseEntryOrderProduct['id'] && !warehouseEntryOrderProduct['isDeleted']) {
                (await new WarehouseEntryOrderProduct().where({ id: warehouseEntryOrderProduct['id'] }).save({ ...warehouseEntryOrderProduct }, { patch: true })).serialize();
            } else if (warehouseEntryOrderProduct['id'] && warehouseEntryOrderProduct['isDeleted']) {
                (await new WarehouseEntryOrderProduct({ id: warehouseEntryOrderProduct['id'] }).destroy()).serialize();
            } else {
                (await new WarehouseEntryOrderProduct({ ...warehouseEntryOrderProduct }).save()).serialize();
            }
        } catch (error) {
            console.log(error)
        }
    }


    const warehouseEntryOrder = (
        await new WarehouseEntryOrder().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(warehouseEntryOrder);
}
