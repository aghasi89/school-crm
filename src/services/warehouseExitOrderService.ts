import { WarehouseExitOrderDetail } from '../domain/entities/WarehouseExitOrderDetail';
import { WarehouseExitOrder } from '../models/WarehouseExitOrder';
import { WarehouseExitOrderProduct } from '../models/WarehouseExitOrder';

import transform from '../utils/transform';
import { WarehouseExitOrderPayload, WarehouseExitOrderProductPayload, WarehouseExitOrderFunctionsPayload } from '../domain/requests/WarehouseExitOrderPayload';
import * as object from '../utils/object';
import NotFoundError from '../exceptions/NotFoundError';
import config from '../config/config';

const { errors } = config;

export async function fetchAll(limit: number, offset: number): Promise<WarehouseExitOrderDetail[]> {

    const _warehouseExitOrders = await WarehouseExitOrder.fetchAll();
    if (!limit || limit == 0) {
        limit = Number(_warehouseExitOrders.count())
        offset = 0;
    }
    const warehouseExitOrders = await (_warehouseExitOrders).query((qb) => {
        qb.limit(limit),
            qb.offset(offset)
    }).fetch({
        withRelated: [
            'warehouse',
            'partners',
            'partnersAccount',
            'prepaidAccount',
            'analiticGroup2',
            'analiticGroup1',
            'warehouseExitOrderFunctions',
            'warehouseExitOrderProduct']
    })
    const res = transform(warehouseExitOrders.serialize(), (warehouseExitOrder: WarehouseExitOrderDetail) => ({
        id: warehouseExitOrder.id,
        documentNumber : warehouseExitOrder.documentNumber,
        expenseAccountId : warehouseExitOrder.expenseAccountId,
        hasRequested :warehouseExitOrder.hasRequested,
        accountant: warehouseExitOrder.accountant,
        allow: warehouseExitOrder.allow,
        analiticGroup_1Id: warehouseExitOrder.analiticGroup_1Id,
        analiticGroup_2Id: warehouseExitOrder.analiticGroup_2Id,
        comment: warehouseExitOrder.comment,
        container: warehouseExitOrder.container,
        date: warehouseExitOrder.date,
        mediator: warehouseExitOrder.mediator,
        powerOfAttorney: warehouseExitOrder.powerOfAttorney,
        warehouseId: warehouseExitOrder.warehouseId

    }))

    return res;
}


// export async function fetchAllByType(id:number): Promise<WarehouseExitOrderDetail[]> {

//     const _warehouseExitOrders = await WarehouseExitOrder.fetchAll();
//     const warehouseExitOrders = await (_warehouseExitOrders).query((qb)=>{
//         qb.where({typesOfActionId:id})
//     }).fetch({withRelated:['customerAccount','aahAccount','prepaidAccountReceived','typesOfAction']})
//     const res = transform(warehouseExitOrders.serialize(), (warehouseExitOrder: WarehouseExitOrderDetail) => ({
//         id: warehouseExitOrder.id,
//         accept : warehouseExitOrder.accept,
//         accountant : warehouseExitOrder.accountant,
//         allow : warehouseExitOrder.allow,
//         analiticGroup_1Id : warehouseExitOrder.analiticGroup_1Id,
//         analiticGroup_2Id : warehouseExitOrder.analiticGroup_2Id,
//         calculationStyleOfAah : warehouseExitOrder.calculationStyleOfAah,
//         comment : warehouseExitOrder.comment,
//         container : warehouseExitOrder.container,
//         date : warehouseExitOrder.date,
//         documentDate : warehouseExitOrder.documentDate,
//         documentN : warehouseExitOrder.documentN,
//         documentOfTransport : warehouseExitOrder.documentOfTransport,
//         documentOfTransportDate : warehouseExitOrder.documentOfTransportDate,
//         includeAahInCost : warehouseExitOrder.includeAahInCost,
//         mediator:warehouseExitOrder.mediator,
//         partnersAccountId : warehouseExitOrder.partnersAccountId,
//         partnersId : warehouseExitOrder.partnersId,
//         powerOfAttorney : warehouseExitOrder.powerOfAttorney,
//         prepaidAccountId : warehouseExitOrder.prepaidAccountId,
//         typeOfAcquisitionOfNa : warehouseExitOrder.typeOfAcquisitionOfNa,
//         warehouseId : warehouseExitOrder.warehouseId

//     }))

//     return res;
// }


export async function count(): Promise<object> {
    const count = await (await WarehouseExitOrder.fetchAll()).count();
    return { count }
}


export async function insert(param: WarehouseExitOrderPayload): Promise<WarehouseExitOrderPayload> {

    const warehouseExitOrderProduct = param.warehouseExitOrderProduct;
    delete param.warehouseExitOrderProduct;
    delete param.warehouseExitOrderFunctions;

    console.log(param,'***********')
    const warehouseExitOrder = (await new WarehouseExitOrder({ ...param }).save()).serialize();

    for (const iterator of warehouseExitOrderProduct) {
        iterator.warehouseExitOrderId = warehouseExitOrder.id;
        (await new WarehouseExitOrderProduct({ ...iterator }).save()).serialize();
    }

    return object.camelize(warehouseExitOrder);

}

export async function getById(id: number): Promise<WarehouseExitOrderDetail> {

    const position = (await new WarehouseExitOrder({ id: id }).fetch({
        withRelated: [
            'warehouse',
            'partners',
            'partnersAccount',
            'prepaidAccount',
            'analiticGroup2',
            'analiticGroup1',
            'warehouseExitOrderFunctions',
            'warehouseExitOrderProduct.materialValue',
            'warehouseExitOrderProduct.accounts',
            'warehouseExitOrderProduct.classification'
        ]
    }));

    if (position) {
        return position.serialize();
    }
    else {
        throw new NotFoundError(errors.notFound);
    }
}

export async function destroy(id: number): Promise<WarehouseExitOrderDetail> {

    const res = (await new WarehouseExitOrder({ id: id }).destroy()).serialize();

    return res;
}


export  function getWarehouseExitOrderProductPayload(params: Array<WarehouseExitOrderProductPayload>): Array<WarehouseExitOrderFunctionsPayload> {

    console.log(params,"getWarehouseExitOrderProductPayload");
    const res : Array<WarehouseExitOrderFunctionsPayload> =  [{
        debitId: 1,
        dPartnersId: 1,
        dAnaliticGroup_2Id:1,
        dAnaliticGroup_1Id: 1,
        creditId: 1,
        cPartnersIdisAah: 1,
        cAnaliticGroup_2Id: 1,
        cAnaliticGroup_1Id: 1,
        money: 10,
        comment: "test",
    }]

    return res;
}




export async function update(id: number, params: WarehouseExitOrderPayload): Promise<WarehouseExitOrderDetail> {
    const warehouseExitOrderProducts = params.warehouseExitOrderProduct;
    delete params.warehouseExitOrderProduct;
    delete params.warehouseExitOrderFunctions;

    for (const warehouseExitOrderProduct of warehouseExitOrderProducts) {
        warehouseExitOrderProduct['warehouseExitOrderId'] = id;
        try {
            if (warehouseExitOrderProduct['id'] && !warehouseExitOrderProduct['isDeleted']) {
                (await new WarehouseExitOrderProduct({id: warehouseExitOrderProduct['id']}).where({ id: warehouseExitOrderProduct['id'] }).save({ ...warehouseExitOrderProduct }, { patch: true })).serialize();
            } else if (warehouseExitOrderProduct['id'] && warehouseExitOrderProduct['isDeleted']) {
                (await new WarehouseExitOrderProduct().where({ id: warehouseExitOrderProduct['id'] }).destroy()).serialize();
            } else {
                (await new WarehouseExitOrderProduct({ ...warehouseExitOrderProduct }).save()).serialize();
            }
        } catch (error) {
            console.log(error,'8888888888')
        }
    }


    const warehouseExitOrder = (
        await new WarehouseExitOrder().where({ id: id }).save({ ...params }, { patch: true })
    ).serialize();

    return object.camelize(warehouseExitOrder);
}
