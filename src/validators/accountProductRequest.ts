import Joi from 'joi';

export const accountProductPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        date: Joi.date()
            .label('date')
            .required(),
        documentNumber: Joi.string()
            .label('documentNumber')
            .required(),
        partnersId: Joi.number()
            .label('partnersId')
            .required(),
        linesCode: Joi.string()
            .label('linesCode')
            .required(),
        contract: Joi.string()
            .label('contract')
            .required(),
        contractDate: Joi.date()
            .label('contractDate')
            .required(),
        subsectionId: Joi.number()
            .label('subsectionId')
            .required(),
        analiticGroup_1Id: Joi.number()
            .label('analiticGroup_1Id')
            .required(),
        analiticGroup_2Id: Joi.number()
            .label('analiticGroup_2Id')
            .required(),
        seria: Joi.string()
            .label('seria')
            .required(),
        number: Joi.string()
            .label('number')
            .required(),
        dateWriteOff: Joi.date()
            .label('dateWriteOff')
            .required(),
        comment: Joi.string()
            .label('comment')
            .required(),
        aahAccountTypeId: Joi.number()
            .label('aahAccountTypeId')
            .required(),
        warehouseId: Joi.number()
            .label('warehouseId')
            .required(),
        address: Joi.string()
            .label('address')
            .required(),
        aah: Joi.string()
            .label('aah')
            .required(),

        accountProductsOfProducts: Joi.array().items(Joi.object({
            type: Joi.string()
                .label('type')
                .required(),
            warehouseId: Joi.number()
                .label('warehouseId')
                .required(),
            code: Joi.number()
                .label('code')
                .required(),
            name: Joi.string()
                .label('name')
                .required(),
            point: Joi.number()
                .label('point')
                .required(),
            count: Joi.number()
                .label('count')
                .required(),
            price: Joi.number()
                .label('price')
                .required(),
            money: Joi.number()
                .label('money')
                .required(),
            expenseAccountId: Joi.string()
                .label('expenseAccountId')
                .required(),
            incomeAccountId: Joi.string()
                .label('incomeAccountId')
                .required(),
            batch: Joi.number()
                .label('batch')
                .required(),
            isAah: Joi.number()
                .label('isAah')
                .required(),
        })),
    });