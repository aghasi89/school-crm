import Joi from 'joi';

export const bankPOSTSchema = Joi.object()
  .options({ abortEarly: false })
  .keys({
    code: Joi.string()
      .label('code')
      .required(),
    name: Joi.string()
      .min(4)
      .max(100)
      .label('Name')
      .required(),
    swift: Joi.string()
      .label('swift')
      .required(),
  });
