import Joi from 'joi';

export const billingAccountInBanksPOSTSchema = Joi
    .object()
    .keys({
        billingAccount: Joi.string()
            .label('billingAccount')
            .required(),
        name: Joi.string()
            .label('name')
            .required(),
        currencyId: Joi.number()
            .min(1)
            .label('currencyId')
            .required(),
        accountId: Joi.number()
            .min(1)
            .label('accountId')
            .required(),
        isMain: Joi.boolean()
            .label('isMain')
            .required(),
        numberOfDocument: Joi.string()
            .label('numberOfDocument')
            .required(),
        eServices: Joi.string()
            .label('eServices')
            .required(),
    });