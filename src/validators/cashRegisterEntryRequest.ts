import Joi from 'joi';

export const cashRegisterEntryPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        main: Joi.object({
            cashRegisterId: Joi.number()
                .label('cashRegisterId')
                .required(),
            date: Joi.date()
                .label('date')
                .required(),
            hdmN: Joi.string()
                .label('hdmN')
                .required(),
            documentNumber: Joi.string()
                .label('documentNumber')
                .required(),
            correspondentAccountId: Joi.number()
                .label('correspondentAccountId')
                .required(),
            entryAccountId: Joi.number()
                .label('entryAccountId')
                .required(),
            analiticGroup_1Id: Joi.number()
                .label('analiticGroup_1Id')
                .required(),
            analiticGroup_2Id: Joi.number()
                .label('analiticGroup_2Id')
                .required(),
            currencyId: Joi.number()
                .label('currencyId')
                .required(),
            amountCurrency1: Joi.string()
                .label('amountCurrency1')
                .required(),
            amountCurrency2: Joi.string()
                .label('amountCurrency2')
                .required(),
            partnersId: Joi.number()
                .label('partnersId')
                .required(),
            received: Joi.string()
                .label('received')
                .required(),
            basis: Joi.string()
                .label('basis')
                .required(),
            attached: Joi.string()
                .label('attached')
                .required(),
            optiona: Joi.string()
                .label('optiona')
                .required(),
            typicalOperation: Joi.string()
                .label('typicalOperation')
                .required()
        }),
        cashRegisterEntryFunctions: Joi.array().items(Joi.object({
            debitId: Joi.number().min(1),
            dPartnersId: Joi.number().min(1),
            dAnaliticGroup_2Id: Joi.number().min(1),
            dAnaliticGroup_1Id: Joi.number().min(1),
            creditId: Joi.number().min(1),
            cPartnersId: Joi.number().min(1),
            cPartnersIdisAah: Joi.boolean(),
            cAnaliticGroup_2Id: Joi.number().min(1),
            cAnaliticGroup_1Id: Joi.number().min(1),
            money: Joi.number(),
            comment: Joi.string(),
        })),
    });