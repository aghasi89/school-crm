import Joi from 'joi';

export const cashRegisterExitPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        cashRegisterId: Joi.number()
            .label('cashRegisterId')
            .required(),
        date: Joi.date()
            .label('date')
            .required(),
        documentNumber: Joi.string()
            .label('documentNumber')
            .required(),
        correspondentAccountId: Joi.number()
            .label('correspondentAccountId')
            .required(),
        exitAccountId: Joi.number()
            .label('exitAccountId')
            .required(),
        analiticGroup_1Id: Joi.number()
            .label('analiticGroup_1Id')
            .required(),
        analiticGroup_2Id: Joi.number()
            .label('analiticGroup_2Id')
            .required(),
        currencyId: Joi.number()
            .label('currencyId')
            .required(),
        amountCurrency_1: Joi.string()
            .label('amountCurrency_1')
            .required(),
        amountCurrency_2: Joi.string()
            .label('amountCurrency_2')
            .required(),
        partnersId: Joi.number()
            .label('partnersId')
            .required(),
        received: Joi.string()
            .label('received')
            .required(),
        npNshPassword: Joi.string()
            .label('npNshPassword')
            .required(),
        basis: Joi.string()
            .label('basis')
            .required(),
        appendix: Joi.string()
            .label('appendix')
            .required(),
        otherInformation: Joi.string()
            .label('otherInformation')
            .required(),
        optiona: Joi.string()
            .label('optiona')
            .required(),
        typicalOperation: Joi.string()
            .label('typicalOperation')
            .required(),
        // cashRegisterExitFunctions: Joi.array().items(Joi.object({
        //     debitId: Joi.number().min(1),
        //     dPartnersId: Joi.number().min(1),
        //     dAnaliticGroup_2Id: Joi.number().min(1),
        //     dAnaliticGroup_1Id: Joi.number().min(1),
        //     creditId: Joi.number().min(1),
        //     cPartnersIdisAah: Joi.number().min(1),
        //     cAnaliticGroup_2Id: Joi.number().min(1),
        //     cAnaliticGroup_1Id: Joi.number().min(1),
        //     money: Joi.number(),
        //     comment: Joi.string(),
        // })),
    });