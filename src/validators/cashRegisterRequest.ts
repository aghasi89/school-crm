import Joi from 'joi';

export const cashRegisterPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        code: Joi.string()
            .max(100)
            .label('code')
            .required(),
        name: Joi.string()
            .max(100)
            .label('name')
            .required(),
        isMain: Joi.boolean()
            .label('isMain')
            .required(),
        accountId: Joi.number()
            .label('accountId')
            .required(),
            dmoN: Joi.string()
            .label('dmoN')
            .required(),
            deoN: Joi.string()
            .label('deoN')
            .required(),
            hdmRegisN: Joi.string()
            .label('hdmRegisN')
            .required(),
            ip: Joi.string()
            .label('ip')
            .required(),
            port: Joi.string()
            .label('port')
            .required(),
            password: Joi.string()
            .label('password')
            .required(),
            hdmNonTaxable: Joi.string()
            .label('hdmNonTaxable')
            .required(),
            hdmTaxable: Joi.string()
            .label('hdmTaxable')
            .required(),
            hdmPrintType: Joi.string()
            .label('hdmPrintType')
            .required()
    });
