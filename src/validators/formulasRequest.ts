import Joi from 'joi';

export const formulasPOSTSchema = Joi
    .object()
    .keys({

        code: Joi.string()
            .label('Code')
            .required(),
        caption: Joi.string()
            .label('Caption')
            .required(),
        expression: Joi.string()
            .label('Expression')
            .required(),
    });

