
import Joi from 'joi';

export const hmAcquisOperatPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        name: Joi.string()
            .label('comment')
            .required(),
        forPropertyId: Joi.number()
            .label('forPropertyId')
            .required(),
        fullName: Joi.string()
            .label('fullName')
            .required(),
        entryDate: Joi.date()
            .label('entryDate')
            .required(),
        operationDate: Joi.date()
            .label('operationDate')
            .required(),
        hmTypeId: Joi.number()
            .label('hmTypeId')
            .required(),
        hmxProfitTaxId: Joi.number()
            .label('hmxProfitTaxId')
            .required(),
        hmAcquisitionMethod: Joi.string()
            .label('hmAcquisitionMethod')
            .required(),
        location: Joi.string()
            .label('location')
            .required(),
        partnersId: Joi.number()
            .label('partnersId')
            .required(),
        businessCard: Joi.string()
            .label('businessCard')
            .required(),
        documentNumber: Joi.string()
            .label('documentNumber')
            .required(),
        subdivisionId: Joi.number()
            .label('subdivisionId')
            .required(),
        subdivisionAccountId: Joi.number()
            .label('subdivisionAccountId')
            .required(),
        analiticGroup_1Id: Joi.number()
            .label('analiticGroup_1Id')
            .required(),
        analiticGroup_2Id: Joi.number()
            .label('analiticGroup_2Id')
            .required(),
        outputMethod: Joi.string()
            .label('outputMethod')
            .required(),
            purchaseDocumentInfo: Joi.string()
            .label('purchaseDocumentInfo')
            .required(),
            aahType: Joi.string()
            .label('aahType')
            .required(),
            financialUsefulLife: Joi.string()
            .label('financialUsefulLife')
            .required(),
            financialCalculatedWear: Joi.string()
            .label('financialCalculatedWear')
            .required(),
            financialIsWorn: Joi.string()
            .label('financialIsWorn')
            .required(),
            financeResidualValue: Joi.string()
            .label('financeResidualValue')
            .required(),
            financeIsWorn: Joi.string()
            .label('financeIsWorn')
            .required(),
            taxUsefulLife: Joi.string()
            .label('taxUsefulLife')
            .required(),
            taxInitialCost: Joi.string()
            .label('taxInitialCost')
            .required(),
            taxCalculatedWear: Joi.string()
            .label('taxCalculatedWear')
            .required(),
            taxResidualValue: Joi.string()
            .label('taxResidualValue')
            .required(),
            taxIsWorn: Joi.string()
            .label('taxIsWorn')
            .required(),
            deferredRevenueLife: Joi.string()
            .label('deferredRevenueLife')
            .required(),
            calculatedDepreciation: Joi.string()
            .label('calculatedDepreciation')
            .required(),
            deferredRevenueAccount: Joi.string()
            .label('deferredRevenueAccount')
            .required(),
            deferredRevenueWorn: Joi.string()
            .label('deferredRevenueWorn')
            .required(),
            initialCostAccountId: Joi.number()
            .label('initialCostAccountId')
            .required(),
            wornAccountId: Joi.number()
            .label('wornAccountId')
            .required(),
            expenseAccountId: Joi.number()
            .label('expenseAccountId')
            .required(),
            incomeStatementId: Joi.number()
            .label('incomeStatementId')
            .required(),
            curPortionOfDeferredRevAccountId: Joi.number()
            .label('curPortionOfDeferredRevAccountId')
            .required(),
            deferredRevenueAccountId: Joi.number()
            .label('deferredRevenueAccountId')
            .required(),
            startYear: Joi.string()
            .label('startYear')
            .required(),
            serialNumber: Joi.string()
            .label('serialNumber')
            .required(),
            technicalProfile: Joi.string()
            .label('technicalProfile')
            .required(),
            stamp: Joi.string()
            .label('stamp')
            .required(),
            briefReview: Joi.string()
            .label('briefReview')
            .required(),
            manufacturer: Joi.string()
            .label('manufacturer')
            .required()
        // hmAcquisOperatOperations: Joi.array().items(Joi.object({
        //     debitId: Joi.number().min(1),
        //     dPartnersId: Joi.number().min(1),
        //     dAnaliticGroup_2Id: Joi.number().min(1),
        //     dAnaliticGroup_1Id: Joi.number().min(1),
        //     creditId: Joi.number().min(1),
        //     cPartnersIdisAah: Joi.number().min(1),
        //     cAnaliticGroup_2Id: Joi.number().min(1),
        //     cAnaliticGroup_1Id: Joi.number().min(1),
        //     money: Joi.number(),
        //     comment: Joi.string(),
        // })),
    });