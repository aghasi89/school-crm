import Joi from 'joi';

export const hmReconstructionPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        date: Joi.date()
            .label('date')
            .required(),
        documentNumber: Joi.number()
            .label('documentNumber')
            .required(),
        propertyId: Joi.number()
            .label('propertyId')
            .required(),
        location: Joi.string()
            .label('location')
            .required(),
        materialAnswerId: Joi.number()
            .label('materialAnswerId')
            .required(),
        analiticGroup_1Id: Joi.number()
            .label('analiticGroup_1Id')
            .required(),
        analiticGroup_2Id: Joi.number()
            .label('analiticGroup_2Id')
            .required(),
        comment: Joi.string()
            .label('comment')
            .required(),
        taxResidualUsefulLifeOld: Joi.string()
            .label('taxResidualUsefulLifeOld')
            .required(),
        taxResidualUsefulLifeNew: Joi.string()
            .label('taxResidualUsefulLifeNew')
            .required(),
        taxInitialCostOld: Joi.string()
            .label('taxInitialCostOld')
            .required(),
        taxInitialCostNew: Joi.string()
            .label('taxInitialCostNew')
            .required(),
        revenueResidualUsefulLifeOld: Joi.string()
            .label('revenueResidualUsefulLifeOld')
            .required(),
        revenueResidualUsefulLifeNew: Joi.string()
            .label('revenueResidualUsefulLifeNew')
            .required(),
        revenueInitialCostOld: Joi.string()
            .label('revenueInitialCostOld')
            .required(),
        revenueInitialCostNew: Joi.string()
            .label('revenueInitialCostNew')
            .required(),
        financeResidualUsefulLifeOld: Joi.string()
            .label('financeResidualUsefulLifeOld')
            .required(),
        financeResidualUsefulLifeNew: Joi.string()
            .label('financeResidualUsefulLifeNew')
            .required(),
        financeInitialCostOld: Joi.string()
            .label('financeInitialCostOld')
            .required(),
        financeInitialCostNew: Joi.string()
            .label('financeInitialCostNew')
            .required(),
        // hmReconstructionOperations: Joi.array().items(Joi.object({
        //     debitId: Joi.number().min(1),
        //     dPartnersId: Joi.number().min(1),
        //     dAnaliticGroup_2Id: Joi.number().min(1),
        //     dAnaliticGroup_1Id: Joi.number().min(1),
        //     creditId: Joi.number().min(1),
        //     cPartnersIdisAah: Joi.number().min(1),
        //     cAnaliticGroup_2Id: Joi.number().min(1),
        //     cAnaliticGroup_1Id: Joi.number().min(1),
        //     money: Joi.number(),
        //     comment: Joi.string(),
        // })),
    });