import Joi from 'joi';

export const hmRevaluationPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        date: Joi.date()
            .label('date')
            .required(),
        documentNumber: Joi.number()
            .label('documentNumber')
            .required(),
        propertyId: Joi.number()
            .label('propertyId')
            .required(),
        location: Joi.string()
            .label('location')
            .required(),
        materialAnswerId: Joi.number()
            .label('materialAnswerId')
            .required(),
        analiticGroup_1Id: Joi.number()
            .label('analiticGroup_1Id')
            .required(),
        analiticGroup_2Id: Joi.number()
            .label('analiticGroup_2Id')
            .required(),
        comment: Joi.string()
            .label('comment')
            .required(),
        taxResidualUsefulLifeOld: Joi.number()
            .label('taxResidualUsefulLifeOld')
            .required(),
        taxResidualUsefulLifeNew: Joi.number()
            .label('taxResidualUsefulLifeNew')
            .required(),
        taxInitialCostOld: Joi.number()
            .label('taxInitialCostOld')
            .required(),
        taxInitialCostNew: Joi.number()
            .label('taxInitialCostNew')
            .required(),
        taxCalculatedWearOld: Joi.number()
            .label('taxCalculatedWearOld')
            .required(),
        taxCalculatedWearNew: Joi.number()
            .label('taxCalculatedWearNew')
            .required(),
        taxResidualValueOld: Joi.number()
            .label('taxResidualValueOld')
            .required(),
        taxResidualValueNew: Joi.number()
            .label('taxResidualValueNew')
            .required(),
        revenueResidualUsefulLifeOld: Joi.number()
            .label('revenueResidualUsefulLifeOld')
            .required(),
        revenueResidualUsefulLifeNew: Joi.number()
            .label('revenueResidualUsefulLifeNew')
            .required(),
        revenueInitialCostOld: Joi.number()
            .label('revenueInitialCostOld')
            .required(),
        revenueInitialCostNew: Joi.number()
            .label('revenueInitialCostNew')
            .required(),
        revenueCalculatedWearOld: Joi.number()
            .label('revenueCalculatedWearOld')
            .required(),
        revenueCalculatedWearNew: Joi.number()
            .label('revenueCalculatedWearNew')
            .required(),
        revenueResidualValueOld: Joi.number()
            .label('revenueResidualValueOld')
            .required(),
        revenueResidualValueNew: Joi.number()
            .label('revenueResidualValueNew')
            .required(),
        financeResidualUsefulLifeOld: Joi.number()
            .label('financeResidualUsefulLifeOld')
            .required(),
        financeResidualUsefulLifeNew: Joi.number()
            .label('financeResidualUsefulLifeNew')
            .required(),
        financeInitialCostOld: Joi.number()
            .label('financeInitialCostOld')
            .required(),
        financeInitialCostNew: Joi.number()
            .label('financeInitialCostNew')
            .required(),
        financeCalculatedWearOld: Joi.number()
            .label('financeCalculatedWearOld')
            .required(),
        financeCalculatedWearNew: Joi.number()
            .label('financeCalculatedWearNew')
            .required(),
        financeResidualValueOld: Joi.number()
            .label('financeResidualValueOld')
            .required(),
        financeResidualValueNew: Joi.number()
            .label('financeResidualValueNew')
            .required(),
        // hmReconstructionOperations: Joi.array().items(Joi.object({
        //     debitId: Joi.number().min(1),
        //     dPartnersId: Joi.number().min(1),
        //     dAnaliticGroup_2Id: Joi.number().min(1),
        //     dAnaliticGroup_1Id: Joi.number().min(1),
        //     creditId: Joi.number().min(1),
        //     cPartnersIdisAah: Joi.number().min(1),
        //     cAnaliticGroup_2Id: Joi.number().min(1),
        //     cAnaliticGroup_1Id: Joi.number().min(1),
        //     money: Joi.number(),
        //     comment: Joi.string(),
        // })),
    });