import Joi from 'joi';

export const hmTypePOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        code: Joi.string()
            .label('code')
            .required(),
        name: Joi.string()
            .min(2)
            .max(100)
            .label('name')
            .required(),
        initialAccountId: Joi.number()
            .min(1)
            .label('initialAccountId')
            .required(),
        staleAccountId: Joi.number()
            .min(1)
            .label('staleAccountId')
            .required(),
        parentId: Joi.number()
            .min(1)
            .label('parentId').allow('null'),
        usefullServiceDuration: Joi.string()
            .label('usefullServiceDuration')
            .required(),
        hmxProfitTaxId: Joi.number()
            .label('hmxProfitTaxId')
            .required()
    });

