import Joi from 'joi';

export const hmxProfitTaxPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        code: Joi.string()
            .label('code')
            .required(),
        name: Joi.string()
            .min(2)
            .max(100)
            .label('name')
            .required(),
        yearPrice: Joi.string()
            .label('yearPrice')
            .required(),

    });
