import Joi from 'joi';

export const hmxValueOfBalancePOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        code: Joi.string()
            .label('code')
            .required(),
        date: Joi.string()
            .label('date')
            .required(),
        valueBeginningOfYear: Joi.string()
            .label('yearPrice')
            .required(),

    });