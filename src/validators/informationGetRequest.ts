import Joi from 'joi';

export const informationGetRequest = Joi.object()
    .options({ abortEarly: false })
    .keys({
        key: Joi.string()
            .label('key')
            .required()
    });
