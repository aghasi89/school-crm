import Joi from 'joi';

export const materialValueMovementsPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        date: Joi.date()
            .label('date')
            .required(),
        warehouseExitOrderId: Joi.number()
            .label('warehouseExitOrderId')
            .required(),
        warehouseEntryOrderId: Joi.number()
            .label('warehouseEntryOrderId')
            .required(),
        printAtSalePrices: Joi.string()
            .label('printAtSalePrices')
            .required(),
        analiticGroup_1Id: Joi.number()
            .label('analiticGroup_1Id')
            .required(),
        analiticGroup_2Id: Joi.number()
            .label('analiticGroup_2Id')
            .required(),
        comment: Joi.string()
            .label('comment')
            .required(),
        accountant: Joi.string()
            .label('accountant')
            .required(),
        mediator: Joi.string()
            .label('mediator')
            .required(),
        allow: Joi.string()
            .label('allow')
            .required(),
        shipperRegistrationBookInfo: Joi.string()
            .label('shipperRegistrationBookInfo')
            .required(),
        outputMethod: Joi.string()
            .label('outputMethod')
            .required(),
        seria: Joi.string()
            .label('seria')
            .required(),
        dateOfDischarge: Joi.string()
            .label('dateOfDischarge')
            .required(),
        hasRequested: Joi.string()
            .label('hasRequested')
            .required(),
        additionalComment: Joi.string()
            .label('additionalComment')
            .required(),
        // materialValueMovementsFunctions: Joi.array().items(Joi.object({
        //     debitId: Joi.number().min(1),
        //     dPartnersId: Joi.number().min(1),
        //     dAnaliticGroup_2Id: Joi.number().min(1),
        //     dAnaliticGroup_1Id: Joi.number().min(1),
        //     creditId: Joi.number().min(1),
        //     cPartnersIdisAah: Joi.number().min(1),
        //     cAnaliticGroup_2Id: Joi.number().min(1),
        //     cAnaliticGroup_1Id: Joi.number().min(1),
        //     money: Joi.number(),
        //     comment: Joi.string(),
        // })),
    });