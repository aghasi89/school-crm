import Joi from 'joi';

export const operationsPOSTSchema = Joi
.object()
.keys({
    debitId: Joi.number().min(1),
    dPartnersId: Joi.number().min(1),
    dAnaliticGroup_2Id: Joi.number().min(1),
    dAnaliticGroup_1Id: Joi.number().min(1),
    creditId: Joi.number().min(1),
    cPartnersId: Joi.number().min(1),
    cAnaliticGroup_2Id: Joi.number().min(1),
    cAnaliticGroup_1Id: Joi.number().min(1),
    money : Joi.number(),
    comment : Joi.string(),
    isDeleted: Joi.boolean(),
    id: Joi.number().min(1)
    });