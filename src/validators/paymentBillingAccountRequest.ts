import Joi from 'joi';

export const paymentBillingAccountPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        date: Joi.date()
            .label('date')
            .required(),
        documentNumber: Joi.number()
            .label('documentNumber')
            .required(),
        calculationDate: Joi.date()
            .label('calculationDate')
            .required(),
        paymentType: Joi.string()
            .label('paymentType')
            .required(),
        subdivisionId: Joi.number()
            .label('subdivisionId')
            .required(),
        analiticGroup_1Id: Joi.number()
            .label('analiticGroup_1Id')
            .required(),
        analiticGroup_2Id: Joi.number()
            .label('analiticGroup_2Id')
            .required(),
        settlementAccountId: Joi.number()
            .label('settlementAccountId')
            .required(),
        comment: Joi.string()
            .label('comment')
            .required(),
        dateOfFormulation: Joi.date()
            .label('dateOfFormulation')
            .required(),
        correspondentAccountId: Joi.number()
            .label('correspondentAccountId')
            .required(),
        exitAccountId: Joi.number()
            .label('exitAccountId')
            .required(),

        // paymentBillingAccountOperations: Joi.array().items(Joi.object({
        //     debitId: Joi.number().min(1),
        //     dPartnersId: Joi.number().min(1),
        //     dAnaliticGroup_2Id: Joi.number().min(1),
        //     dAnaliticGroup_1Id: Joi.number().min(1),
        //     creditId: Joi.number().min(1),
        //     cPartnersIdisAah: Joi.number().min(1),
        //     cAnaliticGroup_2Id: Joi.number().min(1),
        //     cAnaliticGroup_1Id: Joi.number().min(1),
        //     money: Joi.number(),
        //     comment: Joi.string(),
        // })),
    });