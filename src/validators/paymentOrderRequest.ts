import Joi from 'joi';

export const paymentOrderPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        main: Joi.object({

            paymentPurpose : Joi.string()
                .label('Payment purpose')
                .required(),
            documentNumber: Joi.number()
                .label('documentNumber')
                .required(),
            date: Joi.date()
                .label('date')
                .required(),
            dateOfFormulation: Joi.date()
                .label('dateOfFormulation')
                .required(),
            payerName: Joi.string()
                .label('payerName')
                .required(),
            payerHvhh: Joi.string()
                .label('payerHvhh')
                .required(),
            provideName: Joi.string()
                .label('provideName')
                .required(),
            provideHvhh: Joi.string()
                .label('provideHvhh')
                .required(),
            providePartnersId: Joi.number()
                .label('providePartnersId')
                .required(),
            dAccountId: Joi.number()
                .label('dAccountId')
                .required(),
            dCurrencyId: Joi.number()
                .label('currencyId')
                .required(),
            cAccountId: Joi.number()
                .label('cAccountId')
                .required(),
            cCorrespondentAccountId: Joi.number()
                .label('cCorrespondentAccountId')
                .required(),
            cCurrencyAccountId: Joi.number()
                .label('cCurrencyAccountId')
                .required(),
            amountCurrency_1: Joi.string()
                .label('amountCurrency_1')
                .required(),
            amountCurrency_2: Joi.string()
                .label('amountCurrency_2')
                .required(),
            analiticGroup_1Id: Joi.number()
                .label('analiticGroup_1Id')
                .required(),
            analiticGroup_2Id: Joi.number()
                .label('analiticGroup_2Id')
                .required(),
            typicalOperation: Joi.string()
                .label('typicalOperation')
                .required(),
        }),
        paymentOrderOperation: Joi.array().items(Joi.object({
            debitId: Joi.number().min(1),
            dPartnersId: Joi.number().min(1),
            dAnaliticGroup_2Id: Joi.number().min(1),
            dAnaliticGroup_1Id: Joi.number().min(1),
            creditId: Joi.number().min(1),
            cPartnersId: Joi.number().min(1),
            cPartnersIdisAah: Joi.boolean(),
            cAnaliticGroup_2Id: Joi.number().min(1),
            cAnaliticGroup_1Id: Joi.number().min(1),
            money: Joi.number(),
            comment: Joi.string(),
        })),
    });