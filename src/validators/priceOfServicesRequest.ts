import Joi from 'joi';

export const priceOfServicesPOSTSchema = Joi
.object()
.keys({
        startDate: Joi.date()
        .label('startDate')
        .required(),
        endDate: Joi.date()
        .label('endDate')
        .required(),
        servicesId: Joi.number()
        .min(1)
        .label('serviceId')
        .required(),
        type: Joi.string()
            .label('Type')
            .required(),
    });