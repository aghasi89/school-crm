import Joi from 'joi';

export const receivedServicesPOSTSchema = Joi.object()
    .options({ abortEarly: false })
    .keys({
        currencyId: Joi.number()
            .label('currencyId')
            .required(),
        date: Joi.date()
            .label('date')
            .required(),
        documentNumber: Joi.number()
            .label('documentNumber')
            .required(),
        currencyExchangeRate1: Joi.string()
            .label('currencyExchangeRate1')
            .required(),
        currencyExchangeRate2: Joi.string()
            .label('currencyExchangeRate2')
            .required(),
        previousDayExchangeRate: Joi.boolean()
            .label('previousDayExchangeRate')
            .required(),
        partnersId: Joi.number()
            .label('partnersId')
            .required(),
        providerAccountId: Joi.string()
            .label('providerAccountId')
            .required(),
        advancePaymentAccountId: Joi.string()
            .label('advancePaymentAccountId')
            .required(),
        analiticGroup_1Id: Joi.number()
            .label('analiticGroup_1Id')
            .required(),
        analiticGroup_2Id: Joi.number()
            .label('analiticGroup_2Id')
            .required(),
        outputMethod: Joi.string()
            .label('outputMethod')
            .required(),
        purchaseDocumentNumber: Joi.number()
            .label('purchaseDocumentNumber')
            .required(),
        purchaseDocumentDate: Joi.date()
            .label('purchaseDocumentDate')
            .required(),
        comment: Joi.string()
            .label('comment')
            .required(),
        purchaseTypeOfService: Joi.string()
            .label('purchaseTypeOfService')
            .required(),
        calculationTypeAah: Joi.string()
            .label('calculationTypeAah')
            .required(),
        includeAahInExpense: Joi.boolean()
            .label('includeAahInExpense')
            .required(),
        formOfReflectionAah: Joi.string()
            .label('formOfReflectionAah')
            .required(),
        typicalOperation: Joi.string()
            .label('typicalOperation')
            .required(),
        receivedServicesDirectory: Joi.array().items(Joi.object({
            serviceId: Joi.number().min(1),
            point: Joi.number(),
            count: Joi.number(),
            price: Joi.number(),
            money: Joi.number(),
            aah: Joi.boolean(),
            account: Joi.string()
        })).required(),
        receivedServicesOperations: Joi.array().items(Joi.object({
            debitId: Joi.number().min(1),
            dPartnersId: Joi.number().min(1),
            dAnaliticGroup_2Id: Joi.number().min(1),
            dAnaliticGroup_1Id: Joi.number().min(1),
            creditId: Joi.number().min(1),
            cPartnersId: Joi.number().min(1),
            cPartnersIdisAah: Joi.boolean(),
            cAnaliticGroup_2Id: Joi.number().min(1),
            cAnaliticGroup_1Id: Joi.number().min(1),
            money: Joi.number(),
            comment: Joi.string(),
        })),
    });


export   const      receivedServicesDirectory = Joi.array().items(Joi.object({
    serviceId: Joi.number().min(1),
    point: Joi.number(),
    count: Joi.number(),
    price: Joi.number(),
    money: Joi.number(),
    aah: Joi.boolean(),
    account: Joi.string()
  }))

