import Joi from 'joi';

export const servicesPOSTSchema = Joi.object()
  .options({ abortEarly: false })
  .keys({
    code: Joi.string()
    .max(100)
    .label('code')
    .required(),    
    fullName: Joi.string()
    .max(100)
    .label('fullName')
    .required(),
    name: Joi.string()
      .max(100)
      .label('name')
      .required(),
    measurementUnitId: Joi.number()
      .label('measurementUnitId')
      .required(),
    classificationId: Joi.number()
      .label('classificationId')
      .required(),
    accountId: Joi.number()
      .label('accountId')
      .required(),
    wholesalePrice: Joi.number()
      .label('wholesalePrice')
      .required(),
    retailerPrice: Joi.number()
      .label('retailerPrice')
      .required(),
    barCode: Joi.number()
      .label('barCode')
      .required(),
    isAah: Joi.boolean()
      .label('isAah')
      .required()
  });
