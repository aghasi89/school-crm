import Joi from 'joi';

export const structuralSubdivisionPOSTSchema = Joi.object()
  .options({ abortEarly: false })
  .keys({
    code: Joi.string()
    .label('code')
    .required(),    
    name: Joi.string()
      .label('name')
      .required(),
    partnerId: Joi.number()
      .label('partnerId')
      .required(),
    expenseAccountId: Joi.number()
      .label('expenseAccountId')
      .required(),
    
  });
