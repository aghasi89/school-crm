import Joi from 'joi';

export const warehouseEntryOrderPOSTSchema = Joi.object()
  .options({ abortEarly: false })
  .keys({
    date: Joi.date()
      .label('date')
      .required(),
    warehouseId: Joi.number()
      .label('warehouseId')
      .required(),
    partnersId: Joi.number()
      .label('partnersId')
      .required(),
    partnersAccountId: Joi.number()
      .label('partnersAccountId')
      .required(),
    prepaidAccountId: Joi.number()
      .label('prepaidAccountId')
      .required(),

    documentNumber:Joi.number()
      .label('documentNumber')
      .required(),
      name:Joi.string()
      .label('name')
      .required(),
    analiticGroup_2Id: Joi.number()
      .label('analiticGroup_2Id')
      .required(),
    analiticGroup_1Id: Joi.number()
      .label('analiticGroup_1Id')
      .required(),
    documentN: Joi.date()
      .label('documentN')
      .required(),
    documentDate: Joi.date()
      .label('documentDate')
      .required(),
    comment: Joi.string()
      .label('comment')
      .required(),
    powerOfAttorney: Joi.string()
      .label('powerOfAttorney')
      .required(),
    mediator: Joi.string()
      .label('mediator')
      .required(),
    container: Joi.string()
      .label('container')
      .required(),
    accountant: Joi.string()
      .label('accountant')
      .required(),
    allow: Joi.string()
      .label('allow')
      .required(),
    accept: Joi.string()
      .label('accept')
      .required(),
    documentOfTransport: Joi.string()
      .label('documentOfTransport')
      .required(),
    documentOfTransportDate: Joi.date()
      .label('documentOfTransportDate')
      .required(),
    typeOfAcquisitionOfNa: Joi.string()
      .label('typeOfAcquisitionOfNa')
      .required(),

    calculationStyleOfAah: Joi.string()
      .label('typeOfAcquisitionOfNa')
      .required(),
    includeAahInCost: Joi.boolean()
      .label('includeAahInCost')
      .required(),
    warehouseEntryOrderProduct: Joi.array().items(Joi.object({
      materialValueId: Joi.number().min(1),
      isDeleted: Joi.boolean(),
      id: Joi.number().min(1),
      point: Joi.number(),
      count: Joi.number(),
      price: Joi.number(),
      money: Joi.number(),
      isAah: Joi.boolean(),
      accountsId: Joi.number().min(1),
      classificationId: Joi.number().min(1),
    })).required()
    // warehouseEntryOrderFunctions: Joi.array().items(Joi.object({
    //   debitId: Joi.number().min(1),
    //   dPartnersId: Joi.number().min(1),
    //   dAnaliticGroup_2Id: Joi.number().min(1),
    //   dAnaliticGroup_1Id: Joi.number().min(1),
    //   creditId: Joi.number().min(1),
    //   cPartnersIdisAah: Joi.number().min(1),
    //   cAnaliticGroup_2Id: Joi.number().min(1),
    //   cAnaliticGroup_1Id: Joi.number().min(1),
    //   money : Joi.number(),
    //   comment : Joi.string(),
    //   isDeleted: Joi.boolean(),
    //   id: Joi.number().min(1)
    // })),
  });


  export const warehouseEntryOrderProduct = Joi.array().items(Joi.object({
    materialValueId: Joi.number().min(1),
    isDeleted: Joi.boolean(),
    id: Joi.number().min(1),
    point: Joi.number(),
    count: Joi.number(),
    price: Joi.number(),
    money: Joi.number(),
    isAah: Joi.boolean(),
    accountsId: Joi.number().min(1),
    classificationId: Joi.number().min(1),
  }))