import Joi from 'joi';

export const warehouseExitOrderPOSTSchema = Joi.object()
  .options({ abortEarly: false })
  .keys({
    date: Joi.date()
      .label('date')
      .required(),
    warehouseId: Joi.number()
      .label('warehouseId')
      .required(),
      expenseAccountId: Joi.string()
      .label('expenseAccountId')
      .required(),
    documentNumber:Joi.number()
        .label('documentNumber')
        .required(),
    analiticGroup_2Id: Joi.number()
      .label('analiticGroup_2Id')
      .required(),
    analiticGroup_1Id: Joi.number()
      .label('analiticGroup_1Id')
      .required(),
    comment: Joi.string()
      .label('comment')
      .required(),
    powerOfAttorney: Joi.string()
      .label('powerOfAttorney')
      .required(),
    mediator: Joi.string()
      .label('mediator')
      .required(),
    container: Joi.string()
      .label('container')
      .required(),
    accountant: Joi.string()
      .label('accountant')
      .required(),
    allow: Joi.string()
      .label('allow')
      .required(),
      hasRequested: Joi.string()
      .label('hasRequested')
      .required(),
    warehouseExitOrderProduct: Joi.array().items(Joi.object({
      materialValueId: Joi.number().min(1),
      isDeleted: Joi.boolean(),
      id: Joi.number().min(1),
      point: Joi.number(),
      count: Joi.number(),
      price: Joi.number(),
      money: Joi.number(),
      batch: Joi.number()
    })).required(),
    warehouseExitOrderFunctions: Joi.array().items(Joi.object({
      debitId: Joi.number().min(1),
      dPartnersId: Joi.number().min(1),
      dAnaliticGroup_2Id: Joi.number().min(1),
      dAnaliticGroup_1Id: Joi.number().min(1),
      creditId: Joi.number().min(1),
      cPartnersIdisAah: Joi.number().min(1),
      cAnaliticGroup_2Id: Joi.number().min(1),
      cAnaliticGroup_1Id: Joi.number().min(1),
      money : Joi.number(),
      comment : Joi.string(),
      isDeleted: Joi.boolean(),
      id: Joi.number().min(1)
    })),
  });


  export const warehouseExitOrderProduct = Joi.array().items(Joi.object({
    materialValueId: Joi.number().min(1),
    isDeleted: Joi.boolean(),
    id: Joi.number().min(1),
    point: Joi.number(),
    count: Joi.number(),
    price: Joi.number(),
    money: Joi.number(),
    isAah: Joi.boolean(),
    accountsId: Joi.number().min(1),
    classificationId: Joi.number().min(1),
  }))